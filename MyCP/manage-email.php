<?php
$PageTitle = " Email";
include("header.php");

$sql = "SELECT * FROM mail_body Where status = '1' ";

$result = $db->query($sql);
$no_record_error = "Record not found.";
?>
<!--header end-->
<?php include("sidebar.php"); ?>
<!--sidebar end-->
<!--dynamic table-->
<link href="<?php echo base_path_admin ?>assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
<link href="<?php echo base_path_admin ?>assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
<link rel="<?php echo base_path_admin ?>stylesheet" href="assets/data-tables/DT_bootstrap.css" />

<!-- Custom styles for this template -->
<link href="<?php echo base_path_admin ?>css/style.css" rel="stylesheet">
<link href="<?php echo base_path_admin ?>css/style-responsive.css" rel="stylesheet" />

<!--main content start-->
<section id="main-content">
	<section class="wrapper">
        <!-- page start-->
		<div class="row">
			<div class="col-md-12">
				<!--breadcrumbs start -->
				<ul class="breadcrumb">
					<li><a href="<?php echo base_path_admin ?>welcome.php"><i class="fa fa-home"></i> Dashboard</a></li>
					<li class="active"><?php echo trim($PageTitle); ?></li>
				</ul>
				<!--breadcrumbs end -->
			</div>
		</div>
		<?php //session msg bar
		if (isset($_SESSION["msg"]) && trim($_SESSION["msg"]) != '') {
			?>
			<div class="alert alert-success fade in">
				<button type="button" class="close close-sm" data-dismiss="alert">
					<i class="fa fa-times"></i>
				</button>
			<?php echo trim($_SESSION["msg"]);
			unset($_SESSION["msg"]); ?>
			</div>
		<?php
		} else if (isset($_SESSION["err_msg"]) && trim($_SESSION["err_msg"]) != '') {
			?>
			<div class="alert alert-block alert-danger fade in">
				<button type="button" class="close close-sm" data-dismiss="alert">
					<i class="fa fa-times"></i>
				</button>
			<?php echo $_SESSION["err_msg"];
			unset($_SESSION["err_msg"]); ?>
			</div>
		<?php
		} else {

		}
		//session msg bar end
		?>
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
<?php echo ucwords(trim(strtolower($PageTitle))); ?>
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>

						</span>
                    </header>
                    <div class="panel-body">
						<div class="adv-table">
							<table  class="display table table-bordered table-striped" id="dynamic-table">
								<thead>
									<tr>
										<th>S.No</th>
										<th>E-Mail Type </th>
										<th>E-Mail Subject</th>
										<th>Edit</th>
									</tr>
								</thead>
								<tbody>
									<?php
									if ($result->size() > 0) {
										$i = 0;
										while ($row = $result->fetch()) {
											$i++;
											?>
											<tr>
												<td><?php echo $i; ?></td>
												<td><?php echo $row['type']; ?></td>
												<td><?php echo $row['subject']; ?></td>
												<td ><a href="<?php echo base_path_admin ?>compose-mail.php?mail=<?php echo base64_encode($row['id']); ?>"><i class="fa fa-edit"></i><span class="text-muted"></span></a></td>
											</tr>
											<?php
										}
									}
									?>
								</tbody>
								<tfoot>
									<tr>
										<th>S.No</th>
										<th>E-Mail Type </th>
										<th>E-Mail Subject</th>
										<th >Edit</th>
									</tr>
								</tfoot>
							</table>
						</div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
	</section>
</section>
<!--main content end-->
<!--right sidebar start-->

<!--right sidebar end-->
<script type="text/javascript" language="javascript" src="<?php echo base_path_admin; ?>assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_path_admin; ?>assets/data-tables/DT_bootstrap.js"></script>
<script>
	$(document).ready(function () {
		$('#dynamic-table').dataTable();
	});
</script>
<?php include("footer.php") ?>