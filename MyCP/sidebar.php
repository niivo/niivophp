<aside>
    <div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
            <li><a class="active" href="<?php echo base_path_admin ?>welcome.php"><i class="fa fa-dashboard"></i><span>Dashboard</span></a></li>

            <li><a class="" href="<?php echo base_path_admin ?>user_list.php"><i class="fa fa-user"></i><span>User List</span></a></li>

            <!--<li><a class="" href="<?php echo base_path_admin ?>add-nav-master.php"><i class="fa fa-upload"></i><span>Upload Nav Master</span></a></li>-->

            <li><a class="" href="<?php echo base_path_admin ?>fund-category.php"><i class="fa fa-list"></i><span>Fund Category</span></a></li>
            
            <li><a class="" href="<?php echo base_path_admin ?>product-list.php"><i class="fa fa-list"></i><span>Product List</span></a></li>
            
            <li><a class="" href="<?php echo base_path_admin ?>order-list.php"><i class="fa fa-list"></i><span>Order List</span></a></li>

            <li><a class="" href="<?php echo base_path_admin ?>kyc-status.php"><i class="fa fa-list"></i><span>KYC Status List</span></a></li>
            
            <li><a class="" href="<?php echo base_path_admin ?>kyc-requested-list.php"><i class="fa fa-list"></i><span>KYC Requested List</span></a></li>
            
            <li><a class="" href="<?php echo base_path_admin ?>sip-installment-due.php"><i class="fa fa-list"></i><span>Sip Installment Due List</span></a></li>

            <li><a class="" href="<?php echo base_path_admin ?>transaction-list.php"><i class="fa fa-list"></i><span>Transaction List</span></a></li>
            
            <li><a class="" href="<?php echo base_path_admin ?>api-setting.php"><i class="fa fa-circle"></i><span>Api Setting</span></a></li>
         
            <li class="sub-menu"><a href="javascript:void(0);"><i class=" fa fa-cog"></i><span>Manage advisor code</span></a>
                <ul class="sub">
                    <li><a href="<?php echo base_path_admin ?>add-advisor-code.php"> Add advisor code</a></li>
                    <li><a href="<?php echo base_path_admin ?>manage-advisor-code.php"> Advisor code list</a></li>
                </ul>
            </li>
            <li class="sub-menu"><a href="javascript:void(0);"><i class=" fa fa-cog"></i><span>Settings</span></a>
                <ul class="sub">
                    <li><a href="<?php echo base_path_admin ?>password-change.php"> Change Password</a></li>
                    <li><a href="<?php echo $_SERVER['PHP_SELF'] ?>?mode=logout">Log Out</a></li>
                </ul>
            </li>
        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>