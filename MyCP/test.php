<?php

require_once('../includes/app_top.php');
require_once('../includes/mysql.class.php');
require_once('../includes/global.inc.php');
require_once('../includes/functions_general.php');



$sql = "SELECT * FROM `users_ori` where 1";

$que = $db->query($sql);
while ($row = $que->fetch()) {
   
    
    $gender = $row['gender'];

    $account_type = $row['bank_ac_type'];
    $account_number = $row['account_no'];
    $ifsc_code = $row['ifsc'];
    $account_holder_name = $row['account_name'];
    $bank_id            = $row['bank_id'];

    $email = $row['email'];
    $nominee_name = $row['nominee_name'];
    $nominee_dob = $row['nominee_dob'];
    $nominee_relationship = $row['nominee_relationship'];
    $nominee_address = $row['nominee_address'];

    $wealth_source = $row['wealth_source'];

    $occupation = ($row['occupation_code']!="")?occuption($row['occupation_code']):"";

    $annualIncodeCode   =   ($row['anualIncomeCode']!="")?annualIncome($row['anualIncomeCode']):"03";

    $address1 = $row['address1'];
    $address2 = $row['address2'];
    $address3 = $row['address3'];
    $main_address = $address1 . " " . $address2 . " " . $address3;
    $pin = $row['pin'];
    $city = $row['city'];
    $state = $row['state_code'];
    $country = $row['country'];
    
    
    $photo          = $row['photo'];
    if($row['aadhar_verify'] == "1"){
        $kycStatus = "COMPLETE";
        $kycStep    =   "COMPLETE";
    }else{
        $kycStatus  = "NOT STARTED";
        $kycStep    =   "PENDING";
    }
    //kyc staus and step update into users
   

    $userid = $row['userid'];
    $sql1 = "select * from user_details where userid = '" . $userid . "'";
    
    $res1 = $db->query($sql1);
    
    if ($res1->size() > 0) {
       
        $sql_updt = "update user_details set "
                . "gender='" . $gender . "',"
                . "country='" . $country . "',"
                . "account_type='" . $account_type . "',"
                . "account_number='" . $account_number . "',"
                . "ifsc_code='" . $ifsc_code . "',"
                . "account_holder_name='" . $account_holder_name . "',"
                . "bank_id='" . $bank_id . "',"
                . "email='" . $email . "',"
                . "nominee_name='" . $nominee_name . "',"
                . "nominee_dob='" . $nominee_dob . "',"
                . "nominee_relationship='" . $nominee_relationship . "',"
                . "nominee_address='" . $nominee_address . "',"
                . "wealth_source='" . $wealth_source . "',"
                . "occupation='" . $occupation . "',"
                . "ca_pincode='" . $pin . "',"
                . "ca_address='" . $main_address . "',"
                . "ca_city='" . $city . "',"
                . "ca_state='" . $state . "',"
                . "ca_country='" . $country . "',"
                . "pa_pincode='" . $pin . "',"
                . "pa_address='" . $main_address . "',"
                . "pa_city='" . $city . "',"
                . "pa_state='" . $state . "',"
                . "pa_country='" . $country . "',"
                . "birth_country='101',"
                . "birth_place='101',"
                . "politically_exposed_person='NA',"
                . "tax_residency_country='101',"
                . "gross_annual_income='".$annualIncodeCode."',"
                . "photo='" . $photo. "'"
                . " where userid=" . $userid;
        
        $result = $db->query($sql_updt);
    } else {
        $sql_updt = "insert into user_details set "
                . "gender='" . $gender . "',"
                . "country='" . $country . "',"
                . "account_type='" . $account_type . "',"
                . "account_number='" . $account_number . "',"
                . "ifsc_code='" . $ifsc_code . "',"
                . "account_holder_name='" . $account_holder_name . "',"
                . "email='" . $email . "',"
                . "nominee_name='" . $nominee_name . "',"
                . "nominee_dob='" . $nominee_dob . "',"
                . "nominee_relationship='" . $nominee_relationship . "',"
                . "nominee_address='" . $nominee_address . "',"
                . "wealth_source='" . $wealth_source . "',"
                . "occupation='" . $occupation . "',"
                . "ca_pincode='" . $pin . "',"
                . "ca_address='" . $main_address . "',"
                . "ca_city='" . $city . "',"
                . "ca_state='" . $state . "',"
                . "ca_country='" . $country . "',"
                . "pa_pincode='" . $pin . "',"
                . "pa_address='" . $main_address . "',"
                . "pa_city='" . $city . "',"
                . "pa_state='" . $state . "',"
                . "pa_country='" . $country . "', "
                . "userid='" . $userid . "'";

        $result = $db->query($sql_updt);
    }
}

   function occuption($occupation_code) {
        global $db;
        $code="";

        $data = (object)[];
        $sql = "SELECT NIIVO_CODE FROM mapping_occupation_code WHERE UCC_CODE = '" . $occupation_code . "' ";
        
     
        $que = $db->query($sql);
        if ($que->size() > 0) {
            $row = $que->fetch();
            $code = $row['NIIVO_CODE'];
        }
        
        return $code;
    }
   function annualIncome($code) {
        global $db;

        $data = (object)[];
        $sql = "SELECT NIIVO_CODE FROM mapping_income_code WHERE FATCA_CODE = '" . $code . "' ";
       
        
     
        $que = $db->query($sql);
        if ($que->size() > 0) {
            $row = $que->fetch();
            $data = $row['NIIVO_CODE'];
        }
        return $data;
    }
