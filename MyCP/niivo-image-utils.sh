INPUT_JSON=$1
OUTPUT_TIFF=$2

if [ $# -ne 2 ] || [ -z $1 ] || [ -z $2 ]; then
echo "Improper arguments provided"
echo "Script usage is as follows:"
echo "$NIIVO_HOME/bin/$(basename $0) <input JSON file path> <output TIFF file path>"
exit 1
fi

java -cp "/niivo/lib/*:/usr/share/java/*:$NIIVO_HOME/bin/niivo-image-utils-1.0.jar" com.claroinvestments.imageutils.BseImageMain $INPUT_JSON $OUTPUT_TIFF
EXIT_CODE=$?

if [ $EXIT_CODE -ne 0 ]; then
echo "Error!"
echo "$NIIVO_HOME/bin/$(basename $0) <input JSON file path> <output TIFF file path>"
exit 1
fi

exit 0 
