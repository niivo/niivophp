<?php

require_once('../includes/app_top.php');

require_once('../includes/mysql.class.php');

// Include database connection

require_once('../includes/global.inc.php');

// Include general functions

require_once('../includes/functions_general.php');

$PageTitle	=	site_name." Administrator Login ";

if(isset($_SESSION[md5('userid')]) && trim($_SESSION[md5('userid')])!='')

{

	cheader("MyCP/index.php");

}



if(isset($_SESSION['admin']) && trim($_SESSION['admin'])!='')

{

	cheader("MyCP/welcome.php");

}







?>

<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8">



    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="">

    <meta name="author" content="ThemeBucket">

    <link rel="shortcut icon" href="<?php echo base_path; ?>images/favicon.ico">



    <title><?php echo $PageTitle; ?></title>



    <!--Core CSS -->

    <link href="<?php echo base_path_admin; ?>bs3/css/bootstrap.min.css" rel="stylesheet">

    <!--<link href="<?php echo base_path_admin; ?>css/bootstrap-reset.css" rel="stylesheet">-->

    <link href="<?php echo base_path_admin; ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />



    <!-- Custom styles for this template -->

    <link href="<?php echo base_path_admin; ?>css/style.css" rel="stylesheet">

    <!--<link href="<?php echo base_path_admin; ?>css/style-responsive.css" rel="stylesheet" />-->



    <!-- Just for debugging purposes. Don't actually copy this line! -->

    <!--[if lt IE 9]><script src="js/ie8/ie8-responsive-file-warning.js"></script><![endif]-->



    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

    <![endif]-->

	<style>

    .error_msg

	{

		color:#f00 !important;

	}

    </style>

</head>



  <body class="login-body">



    <div class="container">

        <div class="logo_container login_logo">

                <a href="<?php echo base_path_admin ?>">

                    <img src="<?php echo base_path_admin ?>images/logo.png" alt=""  class="logo">

                </a>



   		 </div>

      <form class="form-signin" action="login.php" method="post" id="LoginForm">



        <?php if (isset($_GET["err"])) { ?>

        	<div class="alert alert-block alert-danger fade in" style="margin-bottom:0px">

                                <button type="button" class="close close-sm" data-dismiss="alert">

                                    <i class="fa fa-times"></i>

                                </button>

                                <strong>Invalid username, password!. Please try again.</strong></div>

        <?php }

			else if (isset($_SESSION['forget_msg']) && !empty($_SESSION['forget_msg']))

			{ ?>

               <div class="alert alert-success fade in" style="margin-bottom:0px">

                <button type="button" class="close close-sm" data-dismiss="alert">

                    <i class="fa fa-times"></i>

                </button>

                <strong><?php echo $_SESSION['forget_msg']; unset($_SESSION['forget_msg']); ?></strong>

            </div>

        <?php }

			  else{} ?>

        <div class="login-wrap text_center">



            <div class="user-login-info">



                <p><input type="text" class="form-control" name="userid" placeholder="User Name"  autofocus></p>

                <p><input type="password" class="form-control" name="pass"  placeholder="Password"></p>

            </div>

            <button class="btn btn-lg btn-login btn-block" name="submit" type="submit">Login</button>

			<label class="checkbox">

            	<!--<a data-toggle="modal" href="#myModal" id="ForgotPassword"> Forgot Password?</a>-->

            </label>



 <?php

//session msg bar

        if (isset($_SESSION["msg"]) && trim($_SESSION["msg"]) != '') {

            ?>

            <div class="alert alert-success fade in">

                <button type="button" class="close close-sm" data-dismiss="alert">

                    <i class="fa fa-times"></i>

                </button>

                <?php

                echo trim($_SESSION["msg"]);

                unset($_SESSION["msg"]);

                ?>

            </div>

            <?php

        } else if (isset($_SESSION["err_msg"]) && trim($_SESSION["err_msg"]) != '') {

            ?>

            <div class="alert alert-block alert-danger fade in">

                <button type="button" class="close close-sm" data-dismiss="alert">

                    <i class="fa fa-times"></i>

                </button>

                <?php

                echo $_SESSION["err_msg"];

                unset($_SESSION["err_msg"]);

                ?>

            </div>

            <?php

        }

        //session msg bar end

        ?>

        </div>



          <!-- Modal -->

          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">

              <div class="modal-dialog">

                  <div class="modal-content">

                      <div class="modal-header">

                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                          <h4 class="modal-title">Forgot Password ?</h4>

                      </div>

                      <div class="modal-body">

                          <p>Enter your User Name below to reset your password.</p>

                          <input type="text"  name="forgot_userid" id="forgot_userid" placeholder="User Name" autocomplete="off" class="form-control placeholder-no-fix">

					<?php if (isset($_GET["forgot_err"])) { ?>

                          <p class="error_msg">Invalid user name !. Please try again.</p>

                    <?php } ?>

                      </div>

                      <div class="modal-footer">

                          <button data-dismiss="modal"  class="btn btn-default" type="button">Cancel</button>

                          <button class="btn btn-info" type="submit"  name="submit_forgot" >Submit</button>

                      </div>

                  </div>

              </div>

          </div>

          <!-- modal -->



      </form>



    </div>







    <!-- Placed js at the end of the document so the pages load faster -->



    <!--Core js-->

    <script src="<?php echo base_path_admin; ?>js/lib/jquery.js"></script>

    <script src="<?php echo base_path_admin; ?>bs3/js/bootstrap.min.js"></script>



	<script>

		jQuery.fn.center = function () {

			this.css("position","absolute");

			this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) +

														$(window).scrollTop()) + "px");

			this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) +

														$(window).scrollLeft()) + "px");

			return this;

		}



            <?php if(isset($_GET['forgot_err']))

			echo '$("#ForgotPassword").click();'; ?>





    </script>

  </body>

</html>

