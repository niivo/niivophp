<?php
	$PageTitle = "Content Manager";
	include('header.php');
	$row_over_alternate_color="#F2F1EE";
	$row_over_color="#FFF8DB";
	$bgcolor="#FBFCFC";
	 
	 extract($_GET);
	 $where=" where 1";
	 
	
	
	//if delete
	if (isset($_REQUEST["mode"]))
	{	
		if (($_REQUEST["mode"]=="del") || isset($_REQUEST["item_id"]))
		{
			$sql="delete from items where item_id=".$item_id;
			$res=$db->query($sql);
			$_SESSION['msg']="Items deleted successfully";
		}
	}
?>


<script type="text/javascript">


	function Changepagesize(url, pagesize)
	{
		var url = url+"&pagesize="+pagesize;
		window.location=url;
	}
	
</script>

<?php
	$sql="select * from html_pages WHERE page_id !='10' AND pagecode !='disputes_about'";
	$resultA=$db->query($sql);
?>
<?php include("sidebar.php"); ?>
<!--***********************List all html_pages***************************-->
<div id="title-bar">
  <ul id="breadcrumbs">
    <li><a href="welcome.php" title="Home"><span id="bc-home"></span></a></li>
  	<li class="no-hover">Content Manager >> Edit Page Contents</li>
  </ul>
</div>

<div class="shadow-bottom shadow-titlebar"></div>
<div id=main-content>
  <div class="wrapper">
    <div class="row">
    
    <div class="col-sm-12">
    <section class="panel">
    <header class="panel-heading">Edit Page Contents
    <span class="tools pull-right">
         <a class="fa fa-chevron-down" href="javascript:;"></a>

      </span>
     </header>
     <div class="panel-body">
      <div class="block-border">
        <div class=block-content>
          
         <?php 	//session msg bar
					if(isset($_SESSION["msg"]) && trim($_SESSION["msg"])!='') 
					{
						?>
            <div class="alert alert-success fade in">
                <button type="button" class="close close-sm" data-dismiss="alert">
                    <i class="fa fa-times"></i>
                </button>
                <?php echo trim($_SESSION["msg"]); unset($_SESSION["msg"]); ?>
            </div>
           	 <?php 
					}
				  else if(isset($_SESSION["err_msg"]) && trim($_SESSION["err_msg"])!='')
				  { ?>
				  <div class="alert alert-block alert-danger fade in">
                        <button type="button" class="close close-sm" data-dismiss="alert">
                            <i class="fa fa-times"></i>
                        </button>
                        <?php echo $_SESSION["err_msg"]; unset($_SESSION["err_msg"]); ?>
                   </div>
			<?php 
					}
			
				//session msg bar end
			?>
         
          <table class="table table-bordered">
            <tr>
              <?php
		$rsCount = $resultA->size();
		if ($rsCount > 0)
		{
	
					if(!isset($_GET['pagesize']))
					{	
						$pagesize =20;
					}
					else
					{
						$pagesize = $_GET['pagesize'];
					}			
					if(!isset($_GET['pageindex']))
					{
						$pageindex = 1 ;
					}
					else
					{
						$pageindex = $_GET['pageindex'] ;		
					}
					
					$totalpages = ceil($rsCount/$pagesize);
					$limitstr = "limit ".($pageindex-1)*$pagesize.", ".$pagesize;
					$rcount = $pageindex*$pagesize;
										
					if (isset($_GET["so"]))
					{
						$so=$_GET["so"];
						if ($so=="ASC")
							$so="DESC";
						else
							$so="ASC";
					}
					else
						$so="ASC";
					
					if (isset($_GET["oby"]) && $_GET["oby"]!="")
					{
						$queryB = $sql." order by ".$_GET["oby"]." $so $limitstr" ;
					}	
					else
					{
						$queryB = $sql." order by page_id $limitstr" ;
					}	
					
					$resultB = $db->query($queryB);
					
					$qStr=$_SERVER['PHP_SELF']."?pageindex=".$pageindex."&pagesize=".($pagesize)."&so=$so";
					$qry_string=$_SERVER['QUERY_STRING'];
				   $qStrPageSize=$_SERVER['PHP_SELF']."?".$qry_string;
					?>
            <thead>
            <th width="10%"><div align="left">S.No</div></th>
              <th width="30%"><div align="left"> Page Name</div></th>
              <th width="30%"><div align="left"> Page Title</div></th>
              <?php /*?><th width="30%"><div align="left"> File Name</div></th><?php */?>
              <th width="20%"><div align="left"> Date Updated</div></th>
              <th width="10%" colspan="3">Content Control</th>
                </thead>
              <?php
			$result			=	$db->query($sql);
			$i				=	$pagesize*($pageindex-1);
			while ($rows		=	$resultB->fetch()):
				$i++;
				if ($i%2)
					$bgcolor=$row_over_alternate_color;
				else
					$bgcolor="#FBFCFC";
		
		if($rows['pagecode']!='support')
		{
		
		?>
            <tbody>
            <form name="page-contents<?php echo $row['page_id']?>" method="post" action="#">
              <input type="hidden" id="page_id" name="page_id" value="<?php echo $rows['page_id']?>" />
              <tr id="Row<?php echo $i ?>" bgcolor="<?php echo $bgcolor?>" onMouseOver="this.bgColor='<?php echo $row_over_color?>'" onMouseOut="this.bgColor='<?php echo $bgcolor?>'" height="25px">
                <td><?php echo $i ?></td>
                <td><?php echo $rows['page_title'] ?></td>
                <td><?php echo $rows['page_heading'] ?></td>
                <?php /*?><td><?php echo $rows['file_name'] ?></td><?php */?>
                <td><?php echo FormatDateTime($rows['dtdate']); ?></td>
                <td><span  class="inner-link" style="cursor:pointer;"> <a href="html-pages.php?id=<?php echo base64_encode($rows['page_id']); ?>&mode=edit"><img src="images/pencil.png" border="0" alt="Edit"  /></a></span> </td>
              </tr>
                </tbody>
              
            </form>
            <?php }  endwhile;	}
		else
			{?>
            <tr>
              <td colspan="7"  widht="150" align="center">No records found</td>
            </tr>
            <?php	}?>
          </table>
        </div>
        <div class="block-footer tb_fot">
          <div style="float:left" class="text-red t_r"> <span>View</span>
            <select name="pagesize" onchange="Changepagesize('<?=$qStrPageSize?>',this.value)">
              <?php
							$i = 20;
		                    while($i <= $rsCount+20)
							{
								if(($i%20) == 0)
								{
		                      		if( $i == $pagesize)
									{
									?>
              <option value="<?php echo($i) ; ?>" selected><?php echo($i) ; ?></option>
              <?php
									}						
									else
									{
									?>
              <option value="<?php echo($i) ; ?>" ><?php echo($i) ; ?></option>
              <?php
									}
								}
								$i=$i+20;
							} // end of for loop
							?>
            </select>
            <abbr>Row(s) per page</abbr></div>
          <div class="pagn_2">
            <?php								
				
				$qry_string=$_SERVER['QUERY_STRING'];
				$url=$_SERVER['PHP_SELF']."?".$query_string;
				 getPagingHtml($rsCount,$pagesize,$pageindex,$url)				
			?>
          </div>
          <div class="clr"></div>
        </div>
      </div>
      </div>
      </section>
    </div>
    </div>  
  </div>
  <div style="clear:both; padding-bottom:20px;"></div>
</div>

<?php include("footer.php"); ?>