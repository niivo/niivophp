<?php
$PageTitle = 'Order List';
$userid = base64_decode($_GET['userid']);
include("header.php");
?>
<!--header end-->
<?php include("sidebar.php"); ?>

<!--sidebar end-->
<!--dynamic table-->
<link href="<?php echo base_path_admin ?>assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
<link href="<?php echo base_path_admin ?>assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
<link rel="<?php echo base_path_admin ?>stylesheet" href="assets/data-tables/DT_bootstrap.css" />

<!-- Custom styles for this template -->
<link href="<?php echo base_path_admin ?>css/style.css" rel="stylesheet">
<link href="<?php echo base_path_admin ?>css/style-responsive.css" rel="stylesheet" />

<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-md-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_path_admin ?>welcome.php"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li><?php echo $PageTitle; ?></li>
                </ul>
                <!--breadcrumbs end -->
            </div>
        </div>
        <?php
//session msg bar
        if (isset($_SESSION["msg"]) && trim($_SESSION["msg"]) != '') {
            ?>
            <div class="alert alert-success fade in">
                <button type="button" class="close close-sm" data-dismiss="alert">
                    <i class="fa fa-times"></i>
                </button>
                <?php
                echo trim($_SESSION["msg"]);
                unset($_SESSION["msg"]);
                ?>
            </div>
            <?php
        } else if (isset($_SESSION["err_msg"]) && trim($_SESSION["err_msg"]) != '') {
            ?>
            <div class="alert alert-block alert-danger fade in">
                <button type="button" class="close close-sm" data-dismiss="alert">
                    <i class="fa fa-times"></i>
                </button>
                <?php
                echo $_SESSION["err_msg"];
                unset($_SESSION["err_msg"]);
                ?>
            </div>
            <?php
        }
        //session msg bar end
        ?>
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading"><?php echo $PageTitle; ?>

                        <span class="tools pull-right">
                            <a href="javascript:void(0);" class="fa fa-chevron-down"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="form-group col-lg-6">
                            <label class="control-label col-lg-4"> User :</label> 
                            <div class="col-lg-2"> 

                                <select id="class" name="class" onchange="changeMenu(this.value)" >     
                                    <option value="">Select User</option>  
                                    <?php
                                    $getStore1 = "SELECT * FROM users";
                                    $exeQuery1 = $db->query($getStore1);
                                    if ($exeQuery1->size() > 0) {

                                        while ($comp1 = $exeQuery1->fetch()) {
                                            ?>
                                            <option value="<?php echo base64_encode($comp1['userid']) ?>" <?php if (base64_decode($_GET['userid']) == $comp1['userid']) { ?> selected="selected" <?php } ?>><?php echo $comp1['name']; ?></option>
                                            <?php
                                        }
                                    }
                                    ?> 


                                </select>  
                            </div>
                        </div>
                        <div class="adv-table ">

                            <table class="display table table-bordered table-striped" id="dynamic-table_ajax">
                                <thead>
                                    <tr>
                                        <th>S.No </th>
                                        <th>Order Id</th>
                                        <th>Order Type</th>
                                        <th>SCHEME CODE</th>
                                        <th>User</th>
                                        <th>Amount</th>
                                        <th>Quantity</th>
                                        <th>Folio No</th>
                                        <th>Order Status</th>
                                        <th>Allotment</th>
                                        <th>Date</th>
                                        <th>XML File</th>
                                        <th>Detail</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>S.No </th>
                                        <th>Order Id</th>
                                        <th>Order Type</th>
                                        <th>SCHEME CODE</th>
                                        <th>User</th>
                                        <th>Amount</th>
                                        <th>Quantity</th>
                                        <th>Folio No</th>
                                        <th>Order Status</th>
                                         <th>Allotment</th>
                                        <th>Date</th>
                                        <th>XML File</th>
                                        <th>Detail</th>
                                    </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
<!--right sidebar start-->
<div class="right-sidebar">
    <ul class="right-side-accordion">
        <li class="widget-collapsible">
            <ul class="widget-container">
                <li>
                    <div class="prog-row side-mini-stat clearfix">
                        <div class="side-mini-graph">
                            <div class="target-sell">
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </li>
    </ul>
</div>
<!--right sidebar end-->



<!-- Placed js at the end of the document so the pages load faster -->
<!--<link href="datatable/css/jquery.dataTables.css" rel="stylesheet">-->
<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>

<!--script for this page only-->
<!--<script src="js/editable-table/table-editable.js"></script>-->

<!-- END JAVASCRIPTS -->
<script type="text/javascript">
                                    jQuery(document).ready(function () {
                                        $('#dynamic-table_ajax').dataTable({
                                            "aoColumnDefs": [
                                                {'bSortable': true, 'aTargets': [0]},
                                                {'bSortable': true, 'aTargets': [1]},
                                                {'bSortable': true, 'aTargets': [2]},
                                                {'bSortable': true, 'aTargets': [3]},
                                                {'bSortable': true, 'aTargets': [4]},
                                                {'bSortable': true, 'aTargets': [5]},
                                                {'bSortable': true, 'aTargets': [6]},
                                                {'bSortable': false, 'aTargets': [7]},
                                                {'bSortable': true, 'aTargets': [8]},
                                                {'bSortable': false, 'aTargets': [9]},
                                            ],
                                            "order": [[1, "DESC"]],
                                            "processing": true,
                                            "serverSide": true,
                                            "ajax": "ajax/order-list.php?userid=<?php echo base64_encode($userid); ?>"
                                        });
                                    });

                                    function confirmDelete() {
                                        var agree = confirm("Are you sure you want to delete this?");
                                        if (agree)
                                            return true;
                                        else
                                            return false;
                                    }


                                    function confirmUpdate() {
                                        var agree = confirm("Are you sure you want to Update this?");
                                        if (agree)
                                            return true;
                                        else
                                            return false;
                                    }

                                    function changeMenu(id)
                                    {
                                        var url = '<?php echo base_path_admin ?>order-list.php?userid='+ id;
                                        window.location.href = url;

                                    }
</script>
<?php include("footer.php"); ?>