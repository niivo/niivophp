<?php
$PageTitle = 'Change Password';
include("header.php");

//if (isset($_POST["btnsubmit"]) && $_SESSION['admin'] != '')
if (isset($_POST["btnsubmit"])) {
    $str = true;

    $oldpass = security(trim($_POST["oldpassword"]));
    $pass = security(trim($_POST["password"]));
    $confpass = security(trim($_POST["confirm_password"]));

    if ($oldpass == '') {
        $_SESSION["errormsg"] .= "Please provide a old password" . "<br>";
        $str = false;
    }

    if ($pass == '') {
        $_SESSION["errormsg"] .= "Please provide a password" . "<br>";
        $str = false;
    }

    if ($confpass == '') {
        $_SESSION["errormsg"] .= "Please provide a confirm password" . "<br>";
        $str = false;
    }

    if ($pass != $confpass) {
        $_SESSION["errormsg"] .= "Please enter the same password as above" . "<br>";
        $str = false;
    }

//		if (strlen($pass) < 6) {
//			$_SESSION['err_msg'] .= "Very Weak! (Must contain at least 6 characters.)"."<br>";
//			$str = false;
//		}

    if ($str == true) {
        $sql = "select * from adminlogin where username='" . $_SESSION['admin'] . "' " . " and password='" . md5($oldpass) . "'";

        $result1 = $db->query($sql);
        if ($result1->size() > 0) {
            $sql1 = "update `adminlogin` set `password`='" . md5($pass) . "'  where `username`='" . $_SESSION['admin'] . "'";
            $result = $db->query($sql1);
            cheader("MyCP/password-change.php?mode=logout");
        } else
            $_SESSION["errormsg"] = "Old password does not match !";
         $str = false;
    }
}
?>
<!--header end-->
<?php include("sidebar.php"); ?>
<!--sidebar end-->
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
         <!-- page start-->
        <div class="row">
            <div class="col-md-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_path_admin ?>welcome.php"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li class="active"><?php echo trim($PageTitle); ?></li>
                </ul>
                <!--breadcrumbs end -->
            </div>
        </div>
        <?php
                                        $ERROR_MSG = isset($_SESSION["errormsg"]) ? $_SESSION["errormsg"] : '';
                                        $MSG = isset($_SESSION["msg"]) ? $_SESSION["msg"] : '';
                                        if ($ERROR_MSG != "") { ?>
                                            <div class="alert alert-danger" style="margin-top:20px;">
                                                <button data-dismiss="alert" class="close" type="button"><i class="fa fa-times-circle"></i></button>
                                            <?php echo $ERROR_MSG; ?>
                                            </div>
											<?php } elseif ($MSG != "") { ?>
                                            <div class="alert alert-success" style="margin-top:20px;">
                                                <button data-dismiss="alert" class="close" type="button"><i class="fa fa-times-circle"></i></button>
                                            <?php echo $MSG; ?>
                                            </div>
                                        <?php
										}
                                        unset($_SESSION["errormsg"]);
                                        unset($_SESSION["msg"]);
                                        ?>
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
<?php echo $PageTitle ?>
                        <span class="tools pull-right">
                            <a class="fa fa-chevron-down" href="javascript:void(0);"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="form">
                            <form class="cmxform form-horizontal " id="signupForm" method="post" action=""  autocomplete="off" >
                                <div class="form-group ">
                                    <label for="username" class="control-label col-lg-3">Old Password</label>
                                    <div class="col-lg-6">
                                        <input class="form-control" id="oldpassword" name="oldpassword" type="password" placeholder="Old Password" required />
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="password" class="control-label col-lg-3">Password</label>
                                    <div class="col-lg-6">
                                        <input class="form-control" id="password" name="password" type="password" placeholder="Password" required />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="confirm_password" class="control-label col-lg-3">Confirm Password</label>
                                    <div class="col-lg-6">
                                        <input class="form-control" id="confirm_password" name="confirm_password" type="password" placeholder="Confirm Password" required />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-info" name="btnsubmit" type="submit">Save</button>
                                        <button class="btn btn-default" type="reset">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
<!--right sidebar start-->

<!--right sidebar end-->

<?php include("footer.php"); ?>