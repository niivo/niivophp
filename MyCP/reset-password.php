<?php
require_once('../includes/app_top.php');
require_once('../includes/mysql.class.php');
// Include database connection
require_once('../includes/global.inc.php');
// Include general functions
require_once('../includes/functions_general.php');
$PageTitle	=	" Reset Password ";
$chkpage		=	true;
//echo "MyCP/reset-password.php?".$_SERVER['QUERY_STRING'];

$userid		=	base64_decode(security((trim($_GET['username']))));
$actverifycode	=	trim($_GET['verifycode']);
if($userid!='' && $actverifycode !='')
	{
		$sql	=	"SELECT  * FROM  admin_reset_password ".
					" WHERE code  = '".$actverifycode."' and status = 1 " ;
		$res	=	$db->query($sql);
		
		if($res->size()>0){
			$row = $res->fetch();
			
			$sqlLast	=	"UPDATE  admin_reset_password SET ".
								"status = 0 ".
								" WHERE  userno = ".$row['userno'];	
			$db->query($sqlLast);
	
			 $_chk_user	=	" SELECT userno from adminlogin WHERE  userno =".$row['userno'].
							"  AND active = 1 ";
			
			$user_check	=	$db->query($_chk_user);
			if($user_check->size()>0){
					
				$rowss 		= $user_check->fetch();
				$req_date 	= $row["req_date"];
				
				$expdate 	= date("Ymd", strtotime($req_date . " +1 day"));					
				$cdate		= date('Ymd');
				
				$diff = (strtotime($expdate) - strtotime($cdate));		
				
				if($diff>=0)
				{
					$_SESSION["msg"]		=	'Your account is verified please change your password';
					$chkpage		=	false;
				}else{
					$_SESSION["msg"]		=	'Reset password link has been expired';
					$chkpage	=	true;
				}
			
			}else
			{
				$err_msg	=	'Something is wrong with activation code. Please try again !';
				$chkpage	=	true;	
			}
		}
		else
		{
			$_SESSION["msg"]	=	'Something is wrong with activation code. Please try again !';
			$chkpage	=	true;	
		}
	}

if(isset($_POST['submit']) && $chkpage ==true)
{
	
	 
	 $userid			=	security(trim($_POST['userid']));
	 $u_pass			=	security(trim($_POST['password']));
	 $u_repass			=	security(trim($_POST['cpasspassword']));
	 $keyval			=	$_POST['keyval'];
	
	$str			=	true;
	if($u_pass=='')
	{
		$_SESSION["ferror"]	=	$_SESSION["ferror"]."*This  is Required field"."<br>";
					$str		=	false;
	}
	
	else
	{
		if( $u_pass!=$u_repass)
		{
			 $_SESSION["ferror"]	=	$_SESSION["ferror"]."*Re-Entered Password Doesn't Match"."<br>";
			$str		=	false;
		}
	}
	
	if($userid==''){
		 $_SESSION["ferror"]	.=	"Something is wrong with activation code. Please try again"."<br>";
		 $str		=	false;
		}
	
	if($str==true)
	{
			
			  $__sqlRegister2	=	"UPDATE  adminlogin  SET pass = '".md5($u_pass)."' WHERE userno = '".$userid."' ";
				 
				
			 $whrcls			=	" WHERE userno =".$userid." ";
			 
			$__sqlRegister	=	" delete from admin_reset_password ".$whrcls;	
			
			$db->query($__sqlRegister);
			
			
			
		if(!$result	=	$db->query($__sqlRegister2))
		{
			$_SESSION["ferror"]	.=	"Please try again !"."<br>";
		}
		else
		{
            //session_destroy();
			
			unset($_SESSION['admin']);		
			unset($_SESSION['adminname']);		
			unset($_SESSION['LAST_LOGIN']);	
			unset($_SESSION['LOGIN_TYPE']);	
			unset($_SESSION['LOGIN_TYPE_SUB']);
			$_SESSION["forget_msg"] = "Password Changed Successfully";
			
			//die;
			//cheader("MyCP/reset-password.php?".$_SERVER['QUERY_STRING']);
			cheader("MyCP/index.php");
			
		}
	}
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="<?php echo base_path_admin; ?>images/favicon.html">

    <title><?php echo $PageTitle; ?></title>

    <!--Core CSS -->
    <link href="<?php echo base_path_admin; ?>bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_path_admin; ?>css/bootstrap-reset.css" rel="stylesheet">
    <link href="<?php echo base_path_admin; ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="<?php echo base_path_admin; ?>css/style.css" rel="stylesheet">
    <link href="<?php echo base_path_admin; ?>css/style-responsive.css" rel="stylesheet" />

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="js/ie8/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	<style>
    .error_msg
	{
		color:#f00 !important;
	}
    </style>
</head>

  <body class="login-body">

    <div class="container">

      <form class="form-signin" action="" method="post"  autocomplete="off" >
       <input type="hidden" name="userid" value="<?php echo $userid	; ?>" />
        <input type="hidden" name="keyval" value="<?php echo $actverifycode	; ?>" />
        <h2 class="form-signin-heading">Reset Password</h2>
               <?php if(isset($err_msg) && trim($err_msg)!='' ){ ?>
               <div class="alert alert-block alert-danger fade in" style="margin-bottom:0px">
                                <button type="button" class="close close-sm" data-dismiss="alert">
                                    <i class="fa fa-times"></i>
                                </button>
                                <strong><?php echo $err_msg; ?></strong>
                            </div>
			   
			   <?php  }
			   		  else if(isset($_SESSION["msg"]) && trim($_SESSION["msg"])!='')
					  { ?>
              	<div class="alert alert-success fade in" style="margin-bottom:0px">
                <button type="button" class="close close-sm" data-dismiss="alert">
                    <i class="fa fa-times"></i>
                </button>
                <strong><?php echo trim($_SESSION["msg"]); unset($_SESSION["msg"]); ?></strong>
            </div>
               <?php  } ?>
               
            
               <div class="login-wrap">
       
            <div class="user-login-info">
            
                <input type="password" class="form-control" name="password" placeholder="Password" autofocus >
                <input type="password" class="form-control" name="cpasspassword" placeholder="Confirm Password">
            </div>
            
            <button class="btn btn-lg btn-login btn-block" name="submit" type="submit">Submit</button>

            

        </div>

      </form>

    </div>



    <!-- Placed js at the end of the document so the pages load faster -->

    <!--Core js-->
    <script src="<?php echo base_path_admin; ?>js/lib/jquery.js"></script>
    <script src="<?php echo base_path_admin; ?>bs3/js/bootstrap.min.js"></script>
	<script>
    	$(document).ready(function(e) {
            <?php if(isset($_GET['forgot_err']))
					echo '$("#ForgotPassword").click();'; ?>
        });
    </script>
  </body>
</html>
