var Script = function () {

    /*$.validator.setDefaults({
        submitHandler: function() { //alert("submitted!"); }
    });*/

    $().ready(function() {
        // validate the comment form when it is submitted
        $("#commentForm").validate();

        // validate signup form on keyup and submit
        $("#signupForm").validate({
            rules: {
                oldpassword: {
                    required: true,
                    minlength: 5
                },
                password: {
                    required: true,
                    minlength: 5
                },
                confirm_password: {
                    required: true,
                    minlength: 5,
                    equalTo: "#password"
                }
            },
            messages: {
                oldpassword: {
                    required: "Please provide a old password",
                    minlength: "Your old password must be at least 5 characters long"
                },
                password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                },
                confirm_password: {
                    required: "Please provide a confirm password",
                    minlength: "Your confirm password must be at least 5 characters long",
                    equalTo: "Please enter the same password as above"
                }
            }
        });

        
    });


}();