<?php
$PageTitle = 'Add advisor code';
include("header.php");
$id = base64_decode($_REQUEST['id']);
if ($id != '') {
    $sql = "select * from advisor_code where id='" . $id . "'";

    $RES = $db->query($sql);
    $row = $RES->fetch();
}



if (isset($_POST["btnsubmit"])) {
    $str = true;

    $name = addslashes(utf8_encode($_POST["name"]));
    $code = security(trim($_POST["code"]));
    $code = strtoupper($code);
    $pk = security(trim($_POST["pk"]));

    if ($name == '') {
        $_SESSION["errormsg"] .= "Please provide a name" . "<br>";
        $str = false;
    }


    if ($code == '') {
        $_SESSION["errormsg"] .= "Please provide code" . "<br>";
        $str = false;
    }

    if ($str == true) {
        if ($pk == '') {

            $sql = "select * from advisor_code where name='" . $name . "'";
            $res = $db->query($sql);
            if ($res->size() > 0) {
                $_SESSION["errormsg"] = "Name already exist try again";


                cheader("MyCP/add-advisor-code.php");
            } else {
                $sql = "select * from advisor_code where code='" . $code . "'";
                $res = $db->query($sql);
                if ($res->size() > 0) {

                    $_SESSION["errormsg"] = "Code already exist try again";


                    cheader("MyCP/add-advisor-code.php");
                } else {
                    $sql = "insert into advisor_code  SET "
                            . " name = '" . $name . "',"
                            . " code = '" . $code . "' ,"
                            . " dtdate = '" . nowDate() . "'"
                            . " ";
                    $result = $db->query($sql);
                    $_SESSION["msg"] = "Success";


                    cheader("MyCP/manage-advisor-code.php");
                }
            }
        } else {


            $sql = "select * from advisor_code where name='" . $name . "' and id!='" . $pk . "'";
            $res = $db->query($sql);
            if ($res->size() > 0) {
                $_SESSION["errormsg"] = "Name already exist try again";


                cheader("MyCP/add-advisor-code.php");
            } else {
                $sql = "select * from advisor_code where code='" . $code . "' and id!='" . $pk . "'";
                $res = $db->query($sql);
                if ($res->size() > 0) {

                    $_SESSION["errormsg"] = "Code already exist try again";


                    cheader("MyCP/add-advisor-code.php");
                } else {

                    $sql = "update advisor_code set "
                            . " name = '" . $name . "',"
                            . " code = '" . $code . "' ,"
                            . " dtdate = '" . nowDate() . "'"
                            . "where id='" . $pk . "'"
                            . " ";
                    $result = $db->query($sql);
                    $_SESSION["msg"] = "Success";


                    cheader("MyCP/manage-advisor-code.php");
                }
            }
        }
    }
}
?>
<!--header end-->
<?php include("sidebar.php"); ?>
<!--sidebar end-->
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-md-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_path_admin ?>welcome.php"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li class="active"><?php echo trim($PageTitle); ?></li>
                </ul>
                <!--breadcrumbs end -->
            </div>
        </div>
        <?php
        $ERROR_MSG = isset($_SESSION["errormsg"]) ? $_SESSION["errormsg"] : '';
        $MSG = isset($_SESSION["msg"]) ? $_SESSION["msg"] : '';
        if ($ERROR_MSG != "") {
            ?>
            <div class="alert alert-danger" style="margin-top:20px;">
                <button data-dismiss="alert" class="close" type="button"><i class="fa fa-times-circle"></i></button>
                <?php echo $ERROR_MSG; ?>
            </div>
        <?php } elseif ($MSG != "") { ?>
            <div class="alert alert-success" style="margin-top:20px;">
                <button data-dismiss="alert" class="close" type="button"><i class="fa fa-times-circle"></i></button>
                <?php echo $MSG; ?>
            </div>
            <?php
        }
        unset($_SESSION["errormsg"]);
        unset($_SESSION["msg"]);
        ?>
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        <?php echo $PageTitle ?>
                        <span class="tools pull-right">
                            <a class="fa fa-chevron-down" href="javascript:void(0);"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="form">
                            <form class="cmxform form-horizontal " id="signupForm" method="post" action=""  autocomplete="off" >
                                <div class="form-group ">
                                    <label for=name" class="control-label col-lg-3">NAME</label>
                                    <div class="col-lg-6">
                                        <input class="form-control" id="name" name="name" type="text" placeholder="Enter name"  value="<?php echo $row['name'] ?>" required />
                                        <input type="hidden" name="pk" id="pk" value="<?php echo $row['id']; ?>" > 
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="code" class="control-label col-lg-3">ADVISOR CODE</label>
                                    <div class="col-lg-6">
                                        <input class="form-control" id="code"  name="code" type="text" placeholder="Advisor code" value="<?php echo $row['code'] ?>" required />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-info" name="btnsubmit" type="submit">Save</button>
                                        <button class="btn btn-default" type="reset">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
<!--right sidebar start-->

<!--right sidebar end-->

<?php include("footer.php"); ?>