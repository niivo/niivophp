<?php 
$PageTitle	=	" Compose E-mail ";
include("header.php"); 

if(intval(base64_decode($_REQUEST['mail']))<=0 || !isset($_REQUEST['mail']))
{
	$_SESSION['err_msg']	=	$record_not_exites;
	cheader("MyCP/manage-email.php");
} 
if(isset($_POST['save']))
{
	
	if(intval(base64_decode($_REQUEST['mail']))<=0 || !isset($_REQUEST['mail']))
	{
		
	$_SESSION['err_msg']	=	$record_not_exites;
	cheader("MyCP/manage-email.php");
	}
//	echo "<pre>";print_r($_POST); die;
	
	$mail_id	=	intval(base64_decode($_POST['mail']));
	$subject	=	addslashes($_POST['subject']);
	$mail_body	=	addslashes($_POST['mail_body']);
	//echo $mail_body."DSD"; die;
	$_upSql	=	" UPDATE mail_body SET ".
				" mail_body	= '".$mail_body."'".
				" ,subject	= '".$subject."' ".
				" WHERE id=".$mail_id;
				
	if($db->query($_upSql))
	$_SESSION["msg"]	=	"Mail Body successfully Updated!!";
	cheader("MyCP/manage-email.php");
}

function rteSafe($strText)
	{
		//returns safe code for preloading in the RTE
		$tmpString = $strText;
		//convert all types of single quotes
		$tmpString = str_replace(chr(145), chr(39), $tmpString);
		$tmpString = str_replace(chr(146), chr(39), $tmpString);
		$tmpString = str_replace("'", "&#39;", $tmpString);
		
		//convert all types of double quotes
		$tmpString = str_replace(chr(147), chr(34), $tmpString);
		$tmpString = str_replace(chr(148), chr(34), $tmpString);
	//	$tmpString = str_replace("\"", "\"", $tmpString);
		
		//replace carriage returns & line feeds
		$tmpString = str_replace(chr(10), " ", $tmpString);
		$tmpString = str_replace(chr(13), " ", $tmpString);
		
		return $tmpString;
	}

$mail_id	=	base64_decode($_REQUEST['mail']);
$mail_sql	=	"SELECT * FROM mail_body where id='".$mail_id."'";
$exe_query	=	$db->query($mail_sql);
if($exe_query->size()>0)
{
	$row	=	$exe_query->fetch();
	
	$mail_subject	=	($row['subject']);
	$mail_body		=	($row['mail_body']);
}


?>

<!--header end-->
<?php include("sidebar.php"); ?>
<!--sidebar end--> 
<!--main content start-->
<section id="main-content">
  <section class="wrapper">
  <!-- page start-->
  <div class="row">
    <div class="col-md-12"> 
      <!--breadcrumbs start -->
      <ul class="breadcrumb">
        <li><a href="<?php echo base_path_admin ?>welcome.php"><i class="fa fa-home"></i> Dashboard</a></li>
        <li class="active"><a href="<?php echo base_path_admin ?>manage-email.php"><?php echo trim('Email Manager '); ?></a></li>
        <li class="active"><?php echo trim($PageTitle); ?></li>
      </ul>
      <!--breadcrumbs end --> 
    </div>
  </div>
  <?php 	//session msg bar
					if(isset($_SESSION["msg"]) && trim($_SESSION["msg"])!='') {?>
  <div class="alert alert-success fade in">
    <button type="button" class="close close-sm" data-dismiss="alert"> <i class="fa fa-times"></i> </button>
    <?php echo trim($_SESSION["msg"]); unset($_SESSION["msg"]); ?> </div>
  <?php }
				  else if(isset($_SESSION["err_msg"]) && trim($_SESSION["err_msg"])!='')
				  { ?>
  <div class="alert alert-block alert-danger fade in">
    <button type="button" class="close close-sm" data-dismiss="alert"> <i class="fa fa-times"></i> </button>
    <?php echo $_SESSION["err_msg"]; unset($_SESSION["err_msg"]); ?> </div>
  <?php }
				  else{} 
				//session msg bar end
			?>
   <div id="custom_error_pop" class="alert alert-block alert-danger fade in" style="display:none;">
    <button type="button" class="close close-sm" data-dismiss="alert"> <i class="fa fa-times"></i> </button> <span id="error_msg_error_msg"></span></div>
  <div class="row">
    <div class="col-sm-12">
      <section class="panel">
      <header class="panel-heading"> <?php echo  ucwords(strtolower(trim($PageTitle))); ?> <span class="tools pull-right"> <a href="javascript:;" class="fa fa-chevron-down"></a><a href="javascript:;" class="fa fa-times"></a> </span> </header>
      <div class="panel-body">
        <div class="position-left col-sm-9">
          <form role="form" name="form_main" id="form_main" class="form-horizontal" action="" enctype="multipart/form-data" method="post" onSubmit="return Validate(this);" >
          <input type="hidden" name="mail"  value="<?php echo $_REQUEST['mail'] ?>" />
          <div class="form-group">
            <label class="col-lg-2 col-sm-2 control-label" for="inputEmail1">Subject *</label>
            <div class="col-lg-10">
              <input type="text" name="subject" placeholder="subject" id="subject" class="form-control" value="<?php echo $mail_subject;  ?>">
              <p class="help-block">Example block-level help text here.</p>
            </div>
          </div>
         
        <div class="form-group">
            <label class="control-label col-md-2">Mail Body *</label>
            <div class="col-md-10" >
<link href="<?php echo base_path_admin; ?>ckeditor/ckeditor.css" rel="stylesheet" />
<script language="JavaScript" type="text/javascript" src="<?php echo base_path_admin?>js/jquery-1.7.js"></script>
<script language="JavaScript" type="text/javascript" src="<?php echo base_path_admin?>js/jqueryeditor.js"></script>
<script language="JavaScript" type="text/javascript" src="<?php echo base_path_admin?>html2xhtml.js"></script> 
<script language="JavaScript" type="text/javascript" src="<?php echo base_path_admin?>richtext.js"></script>            
                
           <div style="height:445px;">
        
        
        <script language="JavaScript" type="text/javascript">
		  initRTE("images/", "", "", true);
        <!--
        //Usage: writeRichText(fieldname, html, width, height, buttons, readOnly)
        
        var rte1 = new richTextEditor('mail_body');
        rte1.html = '<?php echo rteSafe(stripslashes($mail_body))?>';
        rte1.width=900;
        rte1.height=300;
        rte1.build();
        //-->
        </script>
        </div>
        
            </div>
        </div>
        
        
        <div class="form-group">
          <div class="col-lg-offset-2 col-lg-10">
           <?php /*?> <button class="btn btn-success" name="save" type="submit" value="save" >&nbsp; Save &nbsp;</button><?php */?>
           
            <input  type="submit"  name="save" value="save" class="btn btn-success" />
            <button class="btn btn-danger" type="reset">Cancel</button>
          </div>
        </div>
        </form>
      </div>
    </div>
    </section>
  </div>
  </div>
  <!-- page end--> 
</section>
</section>
<!--Core js-->



<!--main content end--> 
<!--right sidebar start--> 

<!--right sidebar end-->
<script>
function Validate(frm)
	{
		
		updateRTE('mail_body');
		strValid="";
		var illegalChars = /\W/; // allow letters, numbers, and underscores
		
		if ($("#subject").val().trim()=="")
			strValid+="Enter Subject.<br>";
		
		if ($("textarea[name=mail_body]").val().trim()=="")
			strValid+="Enter Message.<br>";
		
		if (strValid!="")
		{
			
			document.getElementById("error_msg_error_msg").innerHTML="<p><b>Please correct the following error's : </b><br>"+strValid+"</p>";
			$("#custom_error_pop").show();
			window.scrollTo(0,0);
			return false;
		}
			return true;
	//return false
	}
</script>
<?php include("footer.php") ?>
