<?php
$PageTitle = "KYC Requested List";

include('header.php');

$row_over_alternate_color = "#F2F1EE";
$row_over_color = "#FFF8DB";
$bgcolor = "#FBFCFC";



?>
<style>
    #cp{
        float: left;
        left: 55% !important;
        margin: 0 5px 10px 0;
        position: absolute !important;
        top: 58% !important;
        z-index: 9999;
    }
    #editor_area >table {
        height:91%;
    }
</style>
<?php include("sidebar.php"); ?>
<div class="shadow-bottom shadow-titlebar"></div>




<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-md-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_path_admin ?>welcome.php"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li class="active"><?php echo trim($PageTitle); ?></li>
                </ul>
                <!--breadcrumbs end -->
            </div>
        </div>
        <?php
//session msg bar
        if (isset($_SESSION["msg"]) && trim($_SESSION["msg"]) != '') {
            ?>
            <div class="alert alert-success fade in">
                <button type="button" class="close close-sm" data-dismiss="alert">
                    <i class="fa fa-times"></i>
                </button>
                <?php
                echo trim($_SESSION["msg"]);
                unset($_SESSION["msg"]);
                ?>
            </div>
            <?php
        } else if (isset($_SESSION["err_msg"]) && trim($_SESSION["err_msg"]) != '') {
            ?>
            <div class="alert alert-block alert-danger fade in">
                <button type="button" class="close close-sm" data-dismiss="alert">
                    <i class="fa fa-times"></i>
                </button>
                <?php
                echo $_SESSION["err_msg"];
                unset($_SESSION["err_msg"]);
                ?>
            </div>
            <?php
        }
        //session msg bar end
        ?>
        <script>
            function confirmSendAll() {
                if ($('.checkbox:checked').length <= 0) {
                    alert("Please checked at least one row");
                    return false;
                }



                var yes = confirm("Are you sure want to export");

                if (yes) {

                    var confirmyes = confirm("Do you want to confirm export");

                    if (confirmyes) {
                        return true;
                    }

                }
                return false;

            }
            function Changepagesize(url, pagesize)
            {
                var url = url + "&pagesize=" + pagesize + "&<?= $queryStr ?>";
                window.location = url;
            }
        </script>
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading"><?php echo $PageTitle; ?>
                        <span class="tools pull-right">
                            <a href="javascript:void(0);" class="fa fa-chevron-down"></a>
                        </span>
                    </header>
                    <div class="panel-body">

                        <div class="adv-table ">
                            <?php
                            $where = " WHERE 1 AND kyc_status = 'REQUESTED' ";
                            $PAGE_SIZE = 10;
                            $so = "DESC";

                            // search based on menu name
                            if (isset($_REQUEST['full_name']) && trim($_REQUEST['full_name']) != '') {
                                $name = security(trim($_REQUEST['full_name']));
                                $where .= " and U.name like '%$name%'";
                            }


                            $sql = "SELECT U.* FROM users AS U "
                                    . " $where"
                                    . " GROUP BY userid ";


//                            $sql = "SELECT * FROM users $where";


                            $res = $db->query($sql);
                            $resultCount = $res->size();
                            $so = "DESC";
                            if ($resultCount > 0) {
                                if (!isset($_GET['pagesize']))
                                    $pagesize = $PAGE_SIZE;
                                else {
                                    if (intval($_GET['pagesize']) <= 0)
                                        $pagesize = $PAGE_SIZE;
                                    else
                                        $pagesize = intval($_GET['pagesize']);
                                }

                                if (!isset($_GET['pageindex']))
                                    $pageindex = 1;
                                else {
                                    if (intval($_GET['pageindex']) <= 0)
                                        $pageindex = 1;
                                    else
                                        $pageindex = intval($_GET['pageindex']);
                                }
                                $totalpages = ceil($resultCount / $pagesize);
                                $limitstr = "limit " . ($pageindex - 1) * $pagesize . ", " . $pagesize;
                                $rcount = $pageindex * $pagesize;

                                if (isset($_GET["so"])) {
                                    $so = $_GET["so"];
                                    if ($so == "ASC")
                                        $so = "DESC";
                                    else
                                        $so = "ASC";
                                } else
                                    $so = "ASC";

                                if (isset($_GET["oby"]) && $_GET["oby"] != "") {
                                    switch ($_GET["oby"]) {
                                        case "name":
                                            $orderBy = "name";
                                            break;

                                        default:
                                            $orderBy = "userid";
                                    }
                                    $queryB = $sql . " order by " . $orderBy . " $so $limitstr";
                                } else {
                                    $queryB = $sql . " ORDER BY userid DESC $limitstr";
                                }


                                $resultB = $db->query($queryB);
                            }
                            $qStr = $_SERVER['PHP_SELF'] . "?pageindex=" . $pageindex . "&pagesize=" . ($pagesize) . "&so=" . $so . "&full_name=" . $_GET["full_name"] . "&email=" . $_GET["email"];
                            $qStrPageSize = $_SERVER['PHP_SELF'] . "?pageindex=" . $pageindex . "&so=" . $so . "&oby=" . $_GET["oby"] . "&full_name=" . $_GET["full_name"] . "&email=" . $_GET["email"];
                            ?>
                            <form name="myform" action="" method="post">



                                <!--Search Div-->
                                <div style="">
                                    <div class='col-sm-12'>
                                        <section class="panel">
                                            <form action="category.php?<?= $queryStr ?>" method="get">
                                                <table cellpadding="2" cellspacing="2" width="100%" style="float: left">

                                                    <tr>

                                                        <td  align="right"> Name</td>
                                                        <td  align="left">
                                                            <input type="text" name="full_name" value="<?php echo $_REQUEST['full_name'] ?>" />
                                                        </td>


                                                        <td>
                                                            <input  class="button" type="submit" name="btngo" value="Search" style="padding:0px; width:100px; margin-top:5px"/>
                                                        </td>
                                                    </tr>


                                                </table>
                                            </form>
                                        </section>
                                    </div>
                                </div>
                                <form name="myform" action="export-request-kyc.php" method="post">
                                    <!--Search Div-->

                                    <table class="display table table-bordered table-striped" id="dynamic-table_ajax">




                                        <thead>
                                            <tr>
                                                <th width="100" height="">S. No </th>

                                                <th width="100" height=""><a href="<?php echo $qStr . '&oby=' . 'full_name' ?>">Name<?php
                                                        if ($_GET['oby'] == 'full_name')
                                                            echo '<img src="' . base_path . 'restro/images/' . strtolower($so) . '.gif" />';
                                                        ?></a> </th>		
                                                <th width="100" height=""><a href="<?php echo $qStr . '&oby=' . 'mobile' ?>">Mobile<?php
                                                        if ($_GET['oby'] == 'mobile')
                                                            echo '<img src="' . base_path . 'restro/images/' . strtolower($so) . '.gif" />';
                                                        ?></a> </th>		
                                                <th>Pan card</th>
                                                <th>DOB</th>
                                                <th>Father Name</th>
                                                <th>KYC Status</th>


                                                <th width="70"><input type="checkbox" name="select_all" id="select_all" value=""/></th>


                                            </tr>
                                        </thead>

                                        <tbody>

                                            <?php
                                            if ($resultCount > 0) {
                                                $result = $db->query($sql);
                                                $i = $pagesize * ($pageindex - 1);
                                                while ($row = $resultB->fetch()) {

                                                    $i++;
                                                    ?>
                                                    <tr>
                                                        <td height="30px;" valign="top"><?php echo $i; ?></td>
                                                        <td valign="top"><?php echo utf8_decode(urldecode($row['name'])); ?></td>

                                                        <td valign="top"><?php echo utf8_decode(urldecode($row['mobile'])); ?></td>
                                                        <td valign="top"><?php echo utf8_decode(urldecode($row['pencard'])); ?></td>
                                                        <td valign="top"><?php echo utf8_decode(urldecode($row['dob'])); ?></td>
                                                        <td valign="top"><?php echo utf8_decode(urldecode($row['father_name'])); ?></td>
                                                        <td valign="top"><?php echo utf8_decode(urldecode($row['kyc_status'])); ?></td>

                                                        <td align="top"><input type="checkbox" name="checked_id[]" class="checkbox" value="<?php echo $row['userid']; ?>"/></td>   
                                                        <td valign="top">
                                                            <input type="hidden" name="userid" value="<?php echo $row['userid']; ?>"/>

                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="15"><div class="pagination" style="width:100%;">
                                                            <div  class="align-left pull-left"><abbr>View</abbr>
                                                                <select name="pagesize" onchange="Changepagesize('<?php echo $qStrPageSize ?>', this.value)">
                                                                    <?php
                                                                    $i = $PAGE_SIZE;
                                                                    while ($i <= $resultCount + $PAGE_SIZE) {
                                                                        if (($i % $PAGE_SIZE) == 0) {
                                                                            if ($i == $pagesize) {
                                                                                ?>
                                                                                <option value="<?php echo($i); ?>" selected><?php echo($i); ?></option>
                                                                                <?php
                                                                            } else {
                                                                                ?>
                                                                                <option value="<?php echo($i); ?>" ><?php echo($i); ?></option>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        $i = $i + $PAGE_SIZE;
                                                                    } // end of for loop
                                                                    ?>
                                                                </select>
                                                                <abbr>Row(s) per page</abbr>
                                                            </div>
                                                            <div class="align-right pull-right">
                                                                <?php
                                                                $url = $_SERVER['PHP_SELF'] . "?so=" . $_GET["so"] . "&pagesize=" . ($pagesize) . "&oby=" . $_GET["oby"] . "&name=" . $_GET["name"] . "&" . $queryStr;
                                                                echo getPagingHtml($resultCount, $pagesize, $pageindex, $url)
                                                                ?>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div></td>
                                                </tr>
                                            </tfoot>
                                            <?php
                                        } else {
                                            ?>
                                            <tr><td colspan="15"><center>Record not found.</center></td></tr>
                                            <?php
                                        }
                                        ?>
                                    </table>

                                    <input type="submit" class="btn btn-danger" style="padding:0px; width:100px; margin-top:5px" onclick="return confirmSendAll()" name="bulk_send_submit" value="Export"/>
                                </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>

<script type="text/javascript">


    $(document).ready(function () {
        $('#select_all').on('click', function () {
            if (this.checked) {
                $('.checkbox').each(function () {
                    this.checked = true;
                });
            } else {
                $('.checkbox').each(function () {
                    this.checked = false;
                });
            }
        });

        $('.checkbox').on('click', function () {

            if ($('.checkbox:checked').length == $('.checkbox').length) {
                $('#select_all').prop('checked', true);
            } else {
                $('#select_all').prop('checked', false);
            }
        });
    });



</script>
<?php include('footer.php'); ?>
