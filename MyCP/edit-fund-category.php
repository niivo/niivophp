<?php
$PageTitle = "Edit fund category";

include('header.php');

$row_over_alternate_color = "#F2F1EE";
$row_over_color = "#FFF8DB";
$bgcolor = "#FBFCFC";

$id = base64_decode($_GET['id']);



if (isset($_POST['edit']) && $_POST['edit'] == "EDIT") {
    global $db;
    $str = true;

    $growth = addslashes(trim($_POST['growth']));


    if ($growth == "") {
        $_SESSION["errormsg"] .= "Please enter growth.";
        $str = false;
    }

    if ($str == true) {
        $sql = " UPDATE fund_category SET"
                . " growth  = '" . $growth . "'"
                . " where id =" . $id;

        $result = $db->query($sql);


        $_SESSION["msg"] = "Updated successfully.";
    }


    if ($_SESSION["errormsg"] != '') {
        fheader('edit-fund-category.php?id=' . base64_encode($id) . '');
    } else {
        fheader('fund-category.php');
    }
}

$sql = "select * from fund_category where id='" . $id . "'";
$res = $db->query($sql);
$row = $res->fetch();
//echo "<pre>"; print_r($row);
?>
<script>
    $(document).ready(function () {

        $("#storeform").validate({
            rules: {
                name: {
                    required: true,
                    maxlength: 30
                },
            },
            messages: {
                name: {
                    required: "Please enter fund_category name.",
                    maxlength: "Brand name must be less than 30 character.",
                },
            },
        });



    });
</script>
<?php include("sidebar.php"); ?>
<div class="shadow-bottom shadow-titlebar"></div>
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-md-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_path_admin ?>welcome.php"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li><a href="<?php echo base_path_admin ?>fund-category.php">Manage Fund Category</a></li>
                    <li class="active"><?php echo trim($PageTitle); ?></li>
                </ul>
                <!--breadcrumbs end -->
            </div>
        </div>

        <div class="row">
            <div class='col-sm-12'>
                <section class="panel">
                    <header class="panel-heading"> <?php echo $PageTitle ?>
                        <span class="tools pull-right">
                            <a class="fa fa-chevron-down" href="javascript:;"></a>

                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="form">
                            <?php
                            $ERROR_MSG = isset($_SESSION["errormsg"]) ? $_SESSION["errormsg"] : '';
                            $MSG = isset($_SESSION["msg"]) ? $_SESSION["msg"] : '';
                            if ($ERROR_MSG != "") {
                                ?>
                                <div class="msg" id="msgError" >
                                    <div class="msg-error">
                                        <p><?php echo $ERROR_MSG; ?></p>
                                        <a class="close" href="javascript:showDetails('msgError');">close</a></div></div>
                            <?php } elseif ($err_msg != '') { ?>
                                <div class="msg" id="msgError" >
                                    <div class="msg-error">
                                        <p><?php echo $err_msg; ?></p>
                                        <a class="close" href="javascript:showDetails('msgError');">close</a></div></div>
                            <?php } elseif ($MSG != "") { ?>
                                <div class="msg" id="msgOk" >
                                    <div class="msg-ok">
                                        <p><?php echo $MSG; ?></p><a class="close" href="javascript:showDetails('msgOk');">close</a></div></div>
                                <?php
                            }
                            unset($_SESSION["errormsg"]);
                            unset($_SESSION["msg"]);
                            ?>
                            <form method="post" id="storeform" name="storeform" onsubmit="return validate(this);" class="cmxform form-horizontal" enctype="multipart/form-data">
                                <div class="panel-body">

                                    <div class="form-group">
                                        <label class="control-label col-lg-3"> Name :</label>
                                        <div class="col-lg-6">
                                            <?php echo $row['name']; ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-lg-3"> Growth :</label>
                                        <div class="col-lg-6">
                                            <input type="text" placeholder="Enter Growth"  name="growth"   id="growth" class="form-control" value="<?php echo $row['growth']; ?>"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-lg-offset-3 col-lg-9">
                                            <button class="btn btn-info" name="edit" value="EDIT" type="submit">Save</button> 
                                            <a class="btn btn-default" href="<?php echo base_path_admin ?>manage-fund_category.php">Cancel</a>
                                        </div>
                                    </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>

<?php include('footer.php'); ?>