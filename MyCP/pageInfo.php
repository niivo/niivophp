<?php
$PageTitle = "Page Info";

include('header.php');

$row_over_alternate_color = "#F2F1EE";
$row_over_color = "#FFF8DB";
$bgcolor = "#FBFCFC";


$id = base64_decode($_REQUEST['id']);

$sql = " SELECT * FROM business_page where id = " . $id . " ";

$res = $db->query($sql);
$row = $res->fetch();
?>
<style>
    #cp{
        float: left;
        left: 55% !important;
        margin: 0 5px 10px 0;
        position: absolute !important;
        top: 58% !important;
        z-index: 9999;
    }
    #editor_area >table {
        height:91%;
    }
</style>
<?php include("sidebar.php"); ?>
<div class="shadow-bottom shadow-titlebar"></div>
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-md-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_path_admin ?>welcome.php"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li class="active"><a href="<?php echo base_path_admin ?>business_page_list.php"> Manage Business Page</a></li>
                    <li class="active"><?php echo trim($PageTitle); ?></li>
                </ul>
                <!--breadcrumbs end -->
            </div>
        </div>

        <div class="row">
            <div class='col-sm-12'>
                <section class="panel">
                    <header class="panel-heading"> <?php echo $PageTitle ?>
                        <span class="tools pull-right">
                            <a class="fa fa-chevron-down" href="javascript:;"></a>
                            <a class="fa fa-times" href="javascript:;"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <div  class="Registerinner">

                                <section class="outer_row">
                                    <section class="outer_box">
                                        <h2>Page Info</h2>	
                                        <section class="inner_box">
                                            <ul class="row">
                                                <li class="col-sm-3">Page Name</li>
                                                <li class="bold col-sm-9"><?php echo $row['page_name']; ?></li>
                                            </ul>
                                            <ul class="row">
                                                <li class="col-sm-3">Username</li>
                                                <li class="bold col-sm-9"><?php echo $row['username']; ?></li>
                                            </ul>
                                            <ul class="row">
                                                <li class="col-sm-3">Password</li>
                                                <li class="bold col-sm-9"><?php echo $row['password']; ?></li>
                                            </ul>
                                            <ul class="row">
                                                <li class="col-sm-3">Location</li>
                                                <li class="bold col-sm-9"><?php echo $row['location']; ?></li>
                                            </ul>
                                            <ul class="row">
                                                <li class="col-sm-3">Address</li>
                                                <li class="bold col-sm-9"><?php echo $row['address']; ?></li>
                                            </ul>
                                            <ul class="row">
                                                <li class="col-sm-3">City</li>
                                                <li class="bold col-sm-9"><?php echo $row['city']; ?></li>
                                            </ul>
                                            <ul class="row">
                                                <li class="col-sm-3">State</li>
                                                <li class="bold col-sm-9"><?php echo $row['state']; ?></li>
                                            </ul>
                                            <ul class="row">
                                                <li class="col-sm-3">Country</li>
                                                <li class="bold col-sm-9"><?php echo $row['country']; ?></li>
                                            </ul>
                                            <ul class="row">
                                                <li class="col-sm-3">Description</li>
                                                <li class="bold col-sm-9"><?php echo $row['description']; ?></li>
                                            </ul>
                                            <ul class="row">
                                                <li class="col-sm-3">Image Ad Price</li>
                                                <li class="bold col-sm-9"><?php echo $row['image_price']; ?></li>
                                            </ul>
                                            <ul class="row">
                                                <li class="col-sm-3">Image</li>
                                                <li class="bold col-sm-9"><?php
                                                    if ($row['image'] != '') {
                                                        ?>
                                                    <img src="<?php echo base_path . $row['image']; ?>" height="200" width="200"/>
                                                        <?php
                                                    }
                                                    ?></li>
                                            </ul>
                                            <ul class="row">
                                                <li class="col-sm-3">Video Ad Price</li>
                                                <li class="bold col-sm-9"><?php echo $row['video_price']; ?></li>
                                            </ul>

                                        </section>
                                    </section>
                                </section>

                            </div>
                        </div>
                    </div>


                </section>
            </div>
        </div>
    </section>
</section>

<?php include('footer.php'); ?>