<?php
$PageTitle = "Dashboard";

include("header.php");
?>
<?php ?>
<!--header end-->
<!--sidebar start-->
<?php include("sidebar.php"); ?>
<!--sidebar end-->

<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <?php
        $ERROR_MSG = isset($_SESSION["errormsg"]) ? $_SESSION["errormsg"] : '';
        $MSG = isset($_SESSION["msg"]) ? $_SESSION["msg"] : '';
        if ($ERROR_MSG != "") {
            ?>
            <div class="alert alert-danger" style="margin-top:20px;">
                <button data-dismiss="alert" class="close" type="button"><i class="fa fa-times-circle"></i></button>
                <?php echo $ERROR_MSG; ?>
            </div>
        <?php } elseif ($MSG != "") { ?>
            <div class="alert alert-success" style="margin-top:20px;">
                <button data-dismiss="alert" class="close" type="button"><i class="fa fa-times-circle"></i></button>
                <?php echo $MSG; ?>
            </div>
            <?php
        }
        unset($_SESSION["errormsg"]);
        unset($_SESSION["msg"]);
        ?>




        <div class="row">


            <div class="col-md-6">
                <section class="panel">
                    <a href="user_list.php">
                        <div class="panel-body">
                            <div class="col-md-4 text-center totalUser">
                                <h2><?php echo User::totalUser(); ?></h2> Total Users
                            </div>
                            <div class="col-md-4 text-center"> 
                                <span class="totalUser_icon "><i class="fa fa-user"></i></span>
                            </div>
                        </div>
                    </a>
                </section>
            </div>
<!--            <div class="col-md-6">
                <section class="panel">
                    <a href="business_owner_list.php">
                        <div class="panel-body">
                            <div class="col-md-4 text-center totalUser">
                                <h2><?php // echo BusinessOwner::totalBusinessOwner(); ?></h2> Total Business Owner
                            </div>
                            <div class="col-md-4 text-center"> 
                                <span class="totalUser_icon "><i class="fa fa-user"></i></span>
                            </div>
                        </div>
                    </a>
                </section>
            </div>-->


        </div>
<!--        <div class="row">
            <div class="col-sm-6">
                <section class="panel">
                    <header class="panel-heading">
                        <strong>Overview</strong>
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                        </span>
                    </header>
                    <a href="business_page_list.php">
                        <div class="panel-body">
                            <div class="col-md-6 text-center totalUser"> <span><?php // echo BusinessPage::totalPages(); ?></span>&nbsp; Total Pages</div>
                        </div>
                    </a>
                    <a href="manage-ads.php">
                        <div class="panel-body">
                            <div class="col-md-6 text-center totalUser"> <span><?php // echo Ads::totalAds(); ?></span>&nbsp; Total Ads</div>
                        </div>
                    </a>



                </section>
            </div>
            <div class="col-sm-6">
                <section class="panel">
                    <header class="panel-heading">
                        <strong>What you should do</strong>
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="col-md-6 text-center totalUser"> <span><?php // echo BusinessPage::totalPages(TRUE); ?></span>&nbsp; Page added today</div>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-6 text-center totalUser"> <span><?php // echo Ads::totalAds(TRUE); ?></span>&nbsp; Ad added today</div>
                    </div>




                </section>
            </div>





        </div>-->

        <!-- page end-->
    </section>
</section>
<!--main content end-->

<?php include("footer.php"); ?>