<?php
$PageTitle = "amfiindia Nav File";

include('header.php');

$row_over_alternate_color = "#F2F1EE";
$row_over_color = "#FFF8DB";
$bgcolor = "#FBFCFC";

//  "create table products (SELECT nav_master.scheme_code,nav_master.scheme_name,nav_master.RTA_SCHEME_CODE,nav_master.DIV_REINVESTFLAG,nav_master.RTA_CODE,nav_master.ISIN,nav_master.NAV_DATE,amfi_products.Net_Asset_Value FROM nav_master LEFT JOIN amfi_products ON nav_master.ISIN = amfi_products.ISIN_Payout)"

if (isset($_POST['add']) && $_POST['add'] == "Add") {

    $db->query("DELETE FROM amfi_products WHERE 1 ");

    $lines = file('http://portal.amfiindia.com/spages/NAV0.txt');

    foreach ($lines as $line_num => $line) {
//    echo "Line #<b>{$line_num}</b> : " . htmlspecialchars($line) . "<br />\n";
        $line = explode(";", htmlspecialchars($line));


        if ($line[1] != '') {
            $Scheme_Code = $line[0];
            $ISIN_Payout = $line[1];
            $ISIN_Reinvestment = $line[2];
            $Scheme_Name = $line[3];
            $Net_Asset_Value = $line[4];
            $Repurchase_Price = $line[5];
            $Sale_Price = $line[6];
            $date = $line[7];

            $sqlset = "INSERT INTO amfi_products  SET "
                    . " Scheme_Code  = '" . $Scheme_Code . "', "
                    . " ISIN_Payout  = '" . $ISIN_Payout . "',"
                    . " ISIN_Reinvestment  = '" . $ISIN_Reinvestment . "',"
                    . " Scheme_Name  = '" . $Scheme_Name . "',"
                    . " Net_Asset_Value  = '" . $Net_Asset_Value . "',"
                    . " Repurchase_Price  = '" . $Repurchase_Price . "',"
                    . " Sale_Price  = '" . $Sale_Price . "', "
                    . " date  = '" . $date . "', "
                    . " dtdate  = '" . nowDateTime() . "' "
                    . " ";

            $resultset = $db->query($sqlset);
        }


        $_SESSION["msg"] = "Added successfully.";
    }

    fheader('add-amfi-products.php');
}
?>

<?php include("sidebar.php"); ?>

<script>
    $(document).ready(function () {

        $("#pageform").validate({
            rules: {
                name: "required",

            },
            messages: {
                name: "Please enter category name.",

            },
        });
    });


</script>


<div class="shadow-bottom shadow-titlebar"></div>
<section id="main-content">

    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-md-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_path_admin ?>welcome.php"><i class="fa fa-home"></i> Dashboard</a></li>

                    <li class="active"><?php echo trim($PageTitle); ?></li>
                </ul>
                <!--breadcrumbs end -->
            </div>
        </div>

        <div class="row">
            <div class='col-sm-12'>
                <section class="panel">
                    <header class="panel-heading"> <?php echo $PageTitle ?>
                        <span class="tools pull-right">
                            <a class="fa fa-chevron-down" href="javascript:;"></a>

                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="form">
                            <?php
                            $ERROR_MSG = isset($_SESSION["errormsg"]) ? $_SESSION["errormsg"] : '';
                            $MSG = isset($_SESSION["msg"]) ? $_SESSION["msg"] : '';
                            if ($ERROR_MSG != "") {
                                ?>
                                <div class="msg" id="msgError" >
                                    <div class="msg-error">
                                        <p><?php echo $ERROR_MSG; ?></p>
                                        <a class="close" href="javascript:showDetails('msgError');">close</a></div></div>
                            <?php } elseif ($err_msg != '') { ?>
                                <div class="msg" id="msgError" >
                                    <div class="msg-error">
                                        <p><?php echo $err_msg; ?></p>
                                        <a class="close" href="javascript:showDetails('msgError');">close</a></div></div>
                            <?php } elseif ($MSG != "") { ?>
                                <div class="msg" id="msgOk" >
                                    <div class="msg-ok">
                                        <p><?php echo $MSG; ?></p><a class="close" href="javascript:showDetails('msgOk');">close</a></div></div>
                                <?php
                            }
                            unset($_SESSION["errormsg"]);
                            unset($_SESSION["msg"]);
                            ?>

                            <!--<div id="myMap"></div>-->

                            <div class="row">
                                <div class="col-lg-6 col-sm-6 col-xs-12">
                                    <form method="post" id="pageform" name="pageform" onsubmit="return validate(this);" class="cmxform form-horizontal" enctype="multipart/form-data">

                                        <div class="panel-body">



                                            <div class="form-group">
                                                <div class="col-lg-offset-4 col-lg-10">
                                                    <button class="btn btn-info" name="add" value="Add" id="Add" type="submit">Upload</button>
                                                    <button class="btn btn-default" type="reset">Cancel</button>
                                                </div>
                                            </div>

                                        </div>
                                    </form>

                                </div>
                                <div class="col-lg-6 col-sm-6 col-xs-12">
                                    <div id="myMap"></div>
                                </div>
                            </div>

                        </div>

                </section>
            </div>
        </div>
    </section>
</section>

<?php include('footer.php'); ?>