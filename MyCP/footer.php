
<!--<script>
                                $(document).ready(function () {
                                    $("#address").geocomplete().bind("geocode:result", function (event, result) {
                                    }).bind("geocode:error", function (event, status) {
                                    });
                                });
                            </script>-->
<!--right sidebar start-->
<div class="right-sidebar">
	<ul class="right-side-accordion">
		<li class="widget-collapsible">
			<ul class="widget-container">
				<li>
					<div class="prog-row side-mini-stat clearfix">
						<div class="side-graph-info"></div>
						<div class="side-mini-graph">
							<div class="target-sell"></div>
						</div>
					</div>
				</li>
			</ul>
		</li>
	</ul>
</div>
<!--right sidebar end-->

</section>
<!-- Placed js at the end of the document so the pages load faster -->

<!--Core js-->

<script src="<?php echo base_path_admin; ?>bs3/js/bootstrap.min.js"></script>
<script src="<?php echo base_path_admin; ?>js/lib/jquery-ui-1.9.2.custom.min.js"></script>
<script src="<?php echo base_path_admin;?>js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script>
<script src="<?php echo base_path_admin; ?>js/scrollTo/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_path_admin; ?>js/nicescroll/jquery.nicescroll.js" type="text/javascript"></script>
<!--Easy Pie Chart-->
<script src="<?php echo base_path_admin; ?>assets/easypiechart/jquery.easypiechart.js"></script>
<!--Sparkline Chart-->
<script src="<?php echo base_path_admin; ?>assets/sparkline/jquery.sparkline.js"></script>
<!--jQuery Flot Chart-->
<script src="<?php echo base_path_admin; ?>assets/flot-chart/jquery.flot.js"></script>
<script src="<?php echo base_path_admin; ?>assets/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="<?php echo base_path_admin; ?>assets/flot-chart/jquery.flot.resize.js"></script>
<script src="<?php echo base_path_admin; ?>assets/flot-chart/jquery.flot.pie.resize.js"></script>


<!--common script init for all pages-->
<script src="<?php echo base_path_admin; ?>js/scripts.js"></script>

<script type="text/javascript" src="<?php echo base_path_admin; ?>js/jquery-validate/jquery.validate.min.js"></script>

<script src="<?php echo base_path_admin; ?>js/jquery-validate/validation-init.js"></script>

</body>
</html>