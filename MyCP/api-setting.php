<?php
$PageTitle = 'Api Keys Setting';
include("header.php");


$sql = "SELECT * FROM api_keys WHERE 1 ";
$que = $db->query($sql);
$row = $que->fetch();


if (isset($_POST["btnsubmit"])) {
    $str = true;

    $API_USER = security(trim($_POST["API_USER"]));
    $API_MEMBER = security(trim($_POST["API_MEMBER"]));
    $API_PASS = security(trim($_POST["API_PASS"]));
    $API_EUIN = security(trim($_POST["API_EUIN"]));
    $CVL_POS_CODE = security(trim($_POST["CVL_POS_CODE"]));
    $CVL_IPV_NAME = security(trim($_POST["CVL_IPV_NAME"]));
    $CVL_IPV_DESG = security(trim($_POST["CVL_IPV_DESG"]));
    $CVL_IPV_ORGAN = security(trim($_POST["CVL_IPV_ORGAN"]));
    $CAMS_POS_CODE = security(trim($_POST["CAMS_POS_CODE"]));
    $CAMS_OTHER_KRA_CODE = security(trim($_POST["CAMS_OTHER_KRA_CODE"]));
    $CAMS_USERNAME = security(trim($_POST["CAMS_USERNAME"]));
    $CAMS_PASSWORD = security(trim($_POST["CAMS_PASSWORD"]));


    if ($API_USER == '') {
        $_SESSION["errormsg"] .= "Please provide a API USER" . "<br>";
        $str = false;
    }

    if ($API_MEMBER == '') {
        $_SESSION["errormsg"] .= "Please provide a API MEMBER" . "<br>";
        $str = false;
    }

    if ($API_PASS == '') {
        $_SESSION["errormsg"] .= "Please provide a API PASS" . "<br>";
        $str = false;
    }

    if ($API_EUIN == '') {
        $_SESSION["errormsg"] .= "Please provide a API EUIN" . "<br>";
        $str = false;
    }

    if ($CVL_POS_CODE == '') {
        $_SESSION["errormsg"] .= "Please provide a CVL_POS_CODE" . "<br>";
        $str = false;
    }

    if ($CVL_IPV_NAME == '') {
        $_SESSION["errormsg"] .= "Please provide a CVL IPV NAME" . "<br>";
        $str = false;
    }

    if ($CVL_IPV_DESG == '') {
        $_SESSION["errormsg"] .= "Please provide a CVL IPV DESG" . "<br>";
        $str = false;
    }

    if ($CVL_IPV_ORGAN == '') {
        $_SESSION["errormsg"] .= "Please provide a CVL IPV ORGAN" . "<br>";
        $str = false;
    }

    if ($CAMS_POS_CODE == '') {
        $_SESSION["errormsg"] .= "Please provide a CAMS POS CODE" . "<br>";
        $str = false;
    }

    if ($CAMS_OTHER_KRA_CODE == '') {
        $_SESSION["errormsg"] .= "Please provide a CAMS OTHER KRA CODE" . "<br>";
        $str = false;
    }

    if ($CAMS_USERNAME == '') {
        $_SESSION["errormsg"] .= "Please provide a CAMS USERNAME" . "<br>";
        $str = false;
    }
    if ($CAMS_PASSWORD == '') {
        $_SESSION["errormsg"] .= "Please provide a CAMS PASSWORD" . "<br>";
        $str = false;
    }


    if ($str == true) {

        $sql = "UPDATE api_keys SET "
                . " API_USER = '" . $API_USER . "',"
                . " API_MEMBER = '" . $API_MEMBER . "' ,"
                . " API_PASS = '" . $API_PASS . "',"
                . " API_EUIN = '" . $API_EUIN . "',"
                . " CVL_POS_CODE = '" . $CVL_POS_CODE . "',"
                . " CVL_IPV_NAME = '" . $CVL_IPV_NAME . "',"
                . " CVL_IPV_DESG = '" . $CVL_IPV_DESG . "',"
                . " CVL_IPV_ORGAN = '" . $CVL_IPV_ORGAN . "',"
                . " CAMS_POS_CODE = '" . $CAMS_POS_CODE . "',"
                . " CAMS_OTHER_KRA_CODE = '" . $CAMS_OTHER_KRA_CODE . "',"
                . " CAMS_USERNAME = '" . $CAMS_USERNAME . "',"
                . " CAMS_PASSWORD = '" . $CAMS_PASSWORD . "' "
                . " "
                . " ";
        $result = $db->query($sql);
        $_SESSION["msg"] = "Success";


        cheader("MyCP/api-setting.php");
    }
}
?>
<!--header end-->
<?php include("sidebar.php"); ?>
<!--sidebar end-->
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-md-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_path_admin ?>welcome.php"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li class="active"><?php echo trim($PageTitle); ?></li>
                </ul>
                <!--breadcrumbs end -->
            </div>
        </div>
        <?php
        $ERROR_MSG = isset($_SESSION["errormsg"]) ? $_SESSION["errormsg"] : '';
        $MSG = isset($_SESSION["msg"]) ? $_SESSION["msg"] : '';
        if ($ERROR_MSG != "") {
            ?>
            <div class="alert alert-danger" style="margin-top:20px;">
                <button data-dismiss="alert" class="close" type="button"><i class="fa fa-times-circle"></i></button>
                <?php echo $ERROR_MSG; ?>
            </div>
        <?php } elseif ($MSG != "") { ?>
            <div class="alert alert-success" style="margin-top:20px;">
                <button data-dismiss="alert" class="close" type="button"><i class="fa fa-times-circle"></i></button>
                <?php echo $MSG; ?>
            </div>
            <?php
        }
        unset($_SESSION["errormsg"]);
        unset($_SESSION["msg"]);
        ?>
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        <?php echo $PageTitle ?>
                        <span class="tools pull-right">
                            <a class="fa fa-chevron-down" href="javascript:void(0);"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="form">
                            <form class="cmxform form-horizontal " id="signupForm" method="post" action=""  autocomplete="off" >
                                <div class="form-group ">
                                    <label for="API_USER" class="control-label col-lg-3">API USER</label>
                                    <div class="col-lg-6">
                                        <input class="form-control" id="API_USER" name="API_USER" type="text" placeholder="API USER"  value="<?php echo $row['API_USER'] ?>" required />
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="API_MEMBER" class="control-label col-lg-3">API MEMBER</label>
                                    <div class="col-lg-6">
                                        <input class="form-control" id="API_MEMBER" name="API_MEMBER" type="text" placeholder="API MEMBER" value="<?php echo $row['API_MEMBER'] ?>" required />
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="API_PASS" class="control-label col-lg-3">API PASS</label>
                                    <div class="col-lg-6">
                                        <input class="form-control" id="API_PASS" name="API_PASS" type="text" placeholder="API PASS" value="<?php echo $row['API_PASS'] ?>" required />
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="API_EUIN" class="control-label col-lg-3">API EUIN</label>
                                    <div class="col-lg-6">
                                        <input class="form-control" id="API_EUIN" name="API_EUIN" type="text" placeholder="API EUIN" value="<?php echo $row['API_EUIN'] ?>" required />
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label for="CVL POS CODE" class="control-label col-lg-3">CVL POS CODE</label>
                                    <div class="col-lg-6">
                                        <input class="form-control" id="CVL_POS_CODE" name="CVL_POS_CODE" type="text" placeholder="CVL POS CODE" value="<?php echo $row['CVL_POS_CODE'] ?>" required />
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label for="CVL IPV NAME" class="control-label col-lg-3">CVL IPV NAME</label>
                                    <div class="col-lg-6">
                                        <input class="form-control" id="CVL_IPV_NAME" name="CVL_IPV_NAME" type="text" placeholder="CVL IPV NAME" value="<?php echo $row['CVL_IPV_NAME'] ?>" required />
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label for="CVL IPV DESG" class="control-label col-lg-3">CVL IPV DESG</label>
                                    <div class="col-lg-6">
                                        <input class="form-control" id="CVL_IPV_DESG" name="CVL_IPV_DESG" type="text" placeholder="CVL IPV DESG" value="<?php echo $row['CVL_IPV_DESG'] ?>" required />
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label for="CVL IPV ORGAN" class="control-label col-lg-3">CVL IPV ORGAN</label>
                                    <div class="col-lg-6">
                                        <input class="form-control" id="CVL_IPV_ORGAN" name="CVL_IPV_ORGAN" type="text" placeholder="CVL IPV ORGAN" value="<?php echo $row['CVL_IPV_ORGAN'] ?>" required />
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label for="CAMS POS CODE" class="control-label col-lg-3">CAMS POS CODE</label>
                                    <div class="col-lg-6">
                                        <input class="form-control" id="CAMS_POS_CODE" name="CAMS_POS_CODE" type="text" placeholder="CAMS POS CODE" value="<?php echo $row['CAMS_POS_CODE'] ?>" required />
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label for="CAMS OTHER KRA CODE" class="control-label col-lg-3">CAMS OTHER KRA CODE</label>
                                    <div class="col-lg-6">
                                        <input class="form-control" id="CAMS_OTHER_KRA_CODE" name="CAMS_OTHER_KRA_CODE" type="text" placeholder="CAMS OTHER KRA CODE" value="<?php echo $row['CAMS_OTHER_KRA_CODE'] ?>" required />
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label for="CAMS USERNAME" class="control-label col-lg-3">CAMS USERNAME</label>
                                    <div class="col-lg-6">
                                        <input class="form-control" id="CAMS_USERNAME" name="CAMS_USERNAME" type="text" placeholder="CAMS USERNAME" value="<?php echo $row['CAMS_USERNAME'] ?>" required />
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label for="CAMS PASSWORD" class="control-label col-lg-3">CAMS PASSWORD</label>
                                    <div class="col-lg-6">
                                        <input class="form-control" id="CAMS_PASSWORD" name="CAMS_PASSWORD" type="text" placeholder="CAMS PASSWORD" value="<?php echo $row['CAMS_PASSWORD'] ?>" required />
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label for="API_EUIN" class="control-label col-lg-3">Last Update</label>
                                    <div class="col-lg-6">
                                        <div><?php echo $row['UPDATED_AT'] ?></div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <button class="btn btn-info" name="btnsubmit" type="submit">Save</button>
                                        <button class="btn btn-default" type="reset">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
<!--right sidebar start-->

<!--right sidebar end-->

<?php include("footer.php"); ?>