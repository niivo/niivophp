<?php
$PageTitle = "Upload Nav Master File";

include('header.php');

$row_over_alternate_color = "#F2F1EE";
$row_over_color = "#FFF8DB";
$bgcolor = "#FBFCFC";



if (isset($_POST['add']) && $_POST['add'] == "Add") {

    $str = true;

    if ($str == true) {





        $mp = "../";
        $path = "uploads/nav/";
        if (!file_exists($mp . $path)) {
            @mkdir("../uploads/nav", 0777, true);
            @mkdir($mp . $path, 0777, true);
        }


        if ($_FILES['file']['name'] != "") {
            $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);

            $ext = strtolower($ext);


            $actual_image_name = $_FILES['file']['name'];
            $tmp = $_FILES['file']['tmp_name'];

            if (move_uploaded_file($tmp, $mp . $path . $actual_image_name)) {

                $lines = file($mp . $path . $actual_image_name);

// Loop through our array, show HTML source as HTML source; and line numbers too.
// Loop through our array, show HTML source as HTML source; and line numbers too.
                foreach ($lines as $line_num => $line) {
//    echo "Line #<b>{$line_num}</b> : " . htmlspecialchars($line) . "<br />\n";
                    $line = explode("|", htmlspecialchars($line));


                    $NAV_DATE = $line[0];
                    $Scheme_Code = $line[1];
                    $scheme_name = $line[2];
                    $RTA_SCHEME_CODE = $line[3];
                    $DIV_REINVESTFLAG = $line[4];
                    $ISIN = $line[5];
                    $NAV_VALUE = $line[6];
                    $RTA_CODE = $line[7];


                    $sqlset = "INSERT INTO nav_master  SET "
                            . " ISIN  = '" . $ISIN . "',"
                            . " scheme_code  = '" . $Scheme_Code . "', "
                            . " scheme_name  = '" . $scheme_name . "', "
                            . " RTA_SCHEME_CODE	  = '" . $RTA_SCHEME_CODE . "', "
                            . " DIV_REINVESTFLAG	  = '" . $DIV_REINVESTFLAG . "', "
                            . " NAV_VALUE  = '" . $NAV_VALUE . "', "
                            . " RTA_CODE  = '" . $RTA_CODE . "', "
                            . " NAV_DATE  = '" . $NAV_DATE . "', "
                            . " dtdate  = '" . nowDateTime() . "' "
                            . " ";

                    $resultset = $db->query($sqlset);
                }



                $result = $db->query($sql);
            }
        }

        $_SESSION["msg"] = "Added successfully.";
    }

    fheader('add-nav-master.php');
}
?>

<?php include("sidebar.php"); ?>

<script>
    $(document).ready(function () {

        $("#pageform").validate({
            rules: {
                name: "required",

            },
            messages: {
                name: "Please enter category name.",

            },
        });
    });
    function confirmUpdate() {
        var agree = confirm("Are you sure you want to Update this?");
        if (agree)
            return true;
        else
            return false;
    }

</script>


<div class="shadow-bottom shadow-titlebar"></div>
<section id="main-content">

    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-md-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_path_admin ?>welcome.php"><i class="fa fa-home"></i> Dashboard</a></li>

                    <li class="active"><?php echo trim($PageTitle); ?></li>
                    <li style="float: right"><a button class="btn btn-default" onClick="return confirmUpdate();" href="product-list.php?mode=update">Update Product List</a></li>
                </ul>
                <!--breadcrumbs end -->
            </div>
        </div>

        <div class="row">
            <div class='col-sm-12'>
                <section class="panel">
                    <header class="panel-heading"> <?php echo $PageTitle ?>
                        <span class="tools pull-right">
                            <a class="fa fa-chevron-down" href="javascript:;"></a>

                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="form">
                            <?php
                            $ERROR_MSG = isset($_SESSION["errormsg"]) ? $_SESSION["errormsg"] : '';
                            $MSG = isset($_SESSION["msg"]) ? $_SESSION["msg"] : '';
                            if ($ERROR_MSG != "") {
                                ?>
                                <div class="msg" id="msgError" >
                                    <div class="msg-error">
                                        <p><?php echo $ERROR_MSG; ?></p>
                                        <a class="close" href="javascript:showDetails('msgError');">close</a></div></div>
                            <?php } elseif ($err_msg != '') { ?>
                                <div class="msg" id="msgError" >
                                    <div class="msg-error">
                                        <p><?php echo $err_msg; ?></p>
                                        <a class="close" href="javascript:showDetails('msgError');">close</a></div></div>
                            <?php } elseif ($MSG != "") { ?>
                                <div class="msg" id="msgOk" >
                                    <div class="msg-ok">
                                        <p><?php echo $MSG; ?></p><a class="close" href="javascript:showDetails('msgOk');">close</a></div></div>
                                <?php
                            }
                            unset($_SESSION["errormsg"]);
                            unset($_SESSION["msg"]);
                            ?>

                            <!--<div id="myMap"></div>-->

                            <div class="row">
                                <div class="col-lg-6 col-sm-6 col-xs-12">
                                    <form method="post" id="pageform" name="pageform" onsubmit="return validate(this);" class="cmxform form-horizontal" enctype="multipart/form-data">

                                        <div class="panel-body">



                                            <div class="form-group">
                                                <label class="control-label col-lg-4"> File :</label>
                                                <div class="col-lg-7">
                                                    <input type="file"  class="form-control upload" style="height:auto;"  id="file" name="file" />
                                                    <span style="color:red"> NOTE:- Upload txt file from "http://bsestarmfdemo.bseindia.com/RptNavMaster.aspx" </span>
                                                </div>
                                            </div>


                                            <div class="clr"></div>
                                            <div class="form-group">
                                                <div class="col-lg-offset-4 col-lg-10">
                                                    <button class="btn btn-info" name="add" value="Add" id="Add" type="submit">Save</button>
                                                    <button class="btn btn-default" type="reset">Cancel</button>
                                                </div>
                                            </div>

                                        </div>
                                    </form>

                                </div>
                                <div class="col-lg-6 col-sm-6 col-xs-12">
                                    <div id="myMap"></div>
                                </div>
                            </div>

                        </div>

                </section>
            </div>
        </div>
    </section>
</section>

<?php include('footer.php'); ?>