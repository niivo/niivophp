<?php
$PageTitle = "View User Detail";

include('header.php');

$row_over_alternate_color = "#F2F1EE";
$row_over_color = "#FFF8DB";
$bgcolor = "#FBFCFC";
extract($_GET);
$userid = base64_decode($_GET["userid"]);




/* ===========   get user Basic =============  */
//$sql = " SELECT * FROM users where userid = " . $userid . " ";
/*
$sql=   "SELECT US.name as uName,US.mobile as uMobile,US.email as uEmail,US.dob as uDob,US.occupation as uOccupation,US.father_name as uFname,US.mother_name as uMname,UA.*,UI.*,UN.*,UB.*,UP.* from users as US
    LEFT JOIN user_address_details as UA ON US.userid = UA.userid
    LEFT JOIN user_identity_details as UI ON US.userid = UI.userid
    LEFT JOIN user_nominee_details as UN ON US.userid = UN.userid
    LEFT JOIN user_bank_details as UB ON US.userid = UB.userid
    LEFT JOIN user_pancard_details as UP ON US.userid = UP.userid where US.userid = " . $userid . "";*/

$sql=   "SELECT US.name as uName,US.mobile as uMobile,UD.email as uEmail,US.dob as uDob,UD.occupation as uOccupation,US.father_name as uFname,UD.*  from users as US
    LEFT JOIN user_details as UD ON US.userid = UD.userid where US.userid = " . $userid . "";


$res = $db->query($sql);
$row = $res->fetch();

/* ===========   get User Address Detail =============
$addressSql = " SELECT * FROM user_address_details where userid = " . $userid . " ";
$addressRes = $db->query($addressSql);
$addressRow = $addressRes->fetch();
 */


if ($row['thumb_image'] != '') {
    $image = $row['thumb_image'];
} else {
    $image = "MyCP/images/login-logo.png";
}
?>

<style>
    #cp{
        float: left;
        left: 55% !important;
        margin: 0 5px 10px 0;
        position: absolute !important;
        top: 58% !important;
        z-index: 9999;
    }
    #editor_area >table {
        height:91%;
    }


    .panel-default>.panel-heading {
        color: #333;
        background-color: #fff;
        border-color: #e4e5e7;
        padding: 0;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    .panel-default>.panel-heading a {
        display: block;
        padding: 10px 15px;
        background-color: #eee;
    }

    .panel-default>.panel-heading a:after {
        content: "";
        position: relative;
        top: 1px;
        display: inline-block;
        font-family: 'Glyphicons Halflings';
        font-style: normal;
        font-weight: 400;
        line-height: 1;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        float: right;
        transition: transform .25s linear;
        -webkit-transition: -webkit-transform .25s linear;
    }

    .panel-default>.panel-heading a[aria-expanded="true"] {

    }

    .panel-default>.panel-heading a[aria-expanded="true"]:after {
        content: "\2212";
        -webkit-transform: rotate(180deg);
        transform: rotate(180deg);
    }


    .panel-default>.panel-heading a.collapsed:after {
        content: "\002b";
        -webkit-transform: rotate(90deg);
        transform: rotate(90deg);
    }



    .accordion-option {
        width: 100%;
        float: left;
        clear: both;
        margin: 15px 0;
    }

    .accordion-option .title {
        font-size: 20px;
        font-weight: bold;
        float: left;
        padding: 0;
        margin: 0;
    }

    .accordion-option .toggle-accordion {
        float: right;
        font-size: 16px;
        color: #6a6c6f;
    }

    .accordion-option .toggle-accordion:before {
        content: "Expand All";
    }

    .accordion-option .toggle-accordion.active:before {
        content: "Collapse All";
    }


</style>

<?php include("sidebar.php"); ?>


<div class="shadow-bottom shadow-titlebar"></div>
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-md-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_path_admin ?>welcome.php"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li class="active"><a href="<?php echo base_path_admin ?>user_list.php">User</a></li>
                    <li class="active"><?php echo trim($PageTitle); ?></li>
                </ul>
                <!--breadcrumbs end -->
            </div>
        </div>

        <div class="row">
            <div class='col-sm-12'>



                <section class="panel">


                <div class="clearfix"></div>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="basicDetail">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapseOne">
                                        Basic Detail
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="basicDetail">
                                <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover">
                                                <tbody>
                                                <tr>
                                                    <th>Image :</th>
                                                    <td><a href="<?php echo base_path . $image; ?>" target="_blank"><img src="<?php echo base_path . $image; ?>" height="100" width="100"/></a></td>
                                                </tr>
                                                <tr>
                                                    <th>Name :</th>
                                                    <td><?php echo $row['uName']; ?></td>
                                                </tr>

                                                <tr>
                                                    <th>Mobile :</th>
                                                    <td><?php echo $row['uMobile']; ?></td>
                                                </tr>
                                                <tr>
                                                    <th>Email :</th>
                                                    <td><?php echo $row['uEmail']; ?></td>
                                                </tr>
                                                <tr>
                                                    <th>Gender :</th>
                                                    <td><?php echo $row['uGender']; ?></td>
                                                </tr>

                                                <tr>
                                                    <th colspan="2" class="text-center"><h4>Pan Card Detail</h4></th>
                                                </tr>
                                                <tr>
                                                    <th>Pan Card Number :</th>
                                                    <td><?php echo $row['pan_card_number']; ?></td>
                                                </tr>
                                                <tr>
                                                    <th>Dob :</th>
                                                    <td><?php echo $row['dob']; ?></td>
                                                </tr>
                                                <tr>
                                                    <th>Father Name :</th>
                                                    <td><?php echo $row['father_name']; ?></td>
                                                </tr>

                                                <tr>
                                                    <th>Pan card Image :</th>
                                                    <td>
                                                        <?php
                                                        if ($row['pan_card_image'] != '') {
                                                            $panCardImage = $row['pan_card_image'];
                                                        } else {
                                                            $panCardImage = "MyCP/images/no_image.jpeg";
                                                        }
                                                        ?>
                                                        <a href="<?php echo base_path . $panCardImage; ?>" target="_blank"><img src="<?php echo base_path . $panCardImage; ?>"  width="250"/></a></td>
                                                </tr>

                                                </tbody>
                                            </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="addressDetail">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapseTwo">
                                        Address Detail
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="addressDetail">
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover">
                                            <tbody>
                                            <tr>
                                                <th colspan="2" class="text-center"><h4>Corresponding Address</h4></th>
                                            </tr>
                                            <tr>
                                                <th>Address :</th>
                                                <td><?php echo $row['ca_address']; ?></td>
                                            </tr>

                                            <tr>
                                                <th>City :</th>
                                                <td><?php echo $row['ca_city']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>State :</th>
                                                <td><?php echo $row['ca_state']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Country :</th>
                                                <td><?php echo $row['ca_country']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Zipcode :</th>
                                                <td><?php echo $row['ca_pincode']; ?></td>
                                            </tr>
                                            <tr>
                                                <th colspan="2"  class="text-center"><h4>Personal Address</h4></th>
                                            </tr>
                                            <tr>
                                                <th>Address :</th>
                                                <td><?php echo $row['pa_address']; ?></td>
                                            </tr>

                                            <tr>
                                                <th>City :</th>
                                                <td><?php echo $row['pa_city']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>State :</th>
                                                <td><?php echo $row['pa_state']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Country :</th>
                                                <td><?php echo $row['pa_country']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Zipcode :</th>
                                                <td><?php echo $row['pa_pincode']; ?></td>
                                            </tr>
                                            <tr>
                                                <th colspan="2"  class="text-center"><h4>Address Proof</h4></th>
                                            </tr>
                                            <tr>
                                                <th>Address Proof Type :</th>
                                                <td><?php echo $row['address_proof_type']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Front Image :</th>
                                                <td>
                                                    <?php
                                                    if ($row['front_image'] != '') {
                                                        $addressFrontImage = $row['front_image'];
                                                    } else {
                                                        $addressFrontImage = "MyCP/images/no_image.jpeg";
                                                    }
                                                    ?>

                                                    <a href="<?php echo base_path . $addressFrontImage; ?>" target="_blank"><img src="<?php echo base_path . $addressFrontImage; ?>" height="100" width="200"/></a></td>
                                            </tr>
                                            <tr>
                                                <th>Back Image :</th>
                                                <td>
                                                    <?php
                                                    if ($row['back_image'] != '') {
                                                        $addressBackImage = $row['back_image'];
                                                    } else {
                                                        $addressBackImage = "MyCP/images/no_image.jpeg";
                                                    }
                                                    ?>
                                                    <a href="<?php echo base_path . $addressBackImage; ?>" target="_blank"><img src="<?php echo base_path . $addressBackImage; ?>" height="100" width="200"/></a></td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="identityDetail">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapseThree">
                                       Identity Detail
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="identityDetail">
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover">
                                            <tbody>

                                            <tr>
                                                <th>Birth Country :</th>
                                                <td><?php echo $row['birth_country']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Birth Place :</th>
                                                <td><?php echo $row['birth_place']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Address Type :</th>
                                                <td><?php echo $row['address_type']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Gross Annual Income :</th>
                                                <td><?php echo $row['gross_annual_income']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Occupation :</th>
                                                <td><?php echo $row['occupation']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Tax Residency Country :</th>
                                                <td><?php echo $row['tax_residency_country']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Wealth Source :</th>
                                                <td><?php echo $row['wealth_source']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Nationality :</th>
                                                <td><?php echo $row['nationality']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Gender :</th>
                                                <td><?php echo $row['gender']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Marital Status :</th>
                                                <td><?php echo $row['marital_status']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>No Of Children :</th>
                                                <td><?php echo $row['no_of_children']; ?></td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="nomineeDetail">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="true" aria-controls="collapseThree">
                                        Nominee Detail
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="nomineeDetail">
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover">
                                            <tbody>

                                            <tr>
                                                <th>Nominee Name :</th>
                                                <td><?php echo $row['nominee_name']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Nominee Dob :</th>
                                                <td><?php echo $row['nominee_dob']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Nominee Relationship :</th>
                                                <td><?php echo $row['nominee_relationship']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Nominee Address :</th>
                                                <td><?php echo $row['nominee_address']; ?></td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="bankDetail">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="true" aria-controls="collapseThree">
                                        Bank Detail
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="bankDetail">
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover">
                                            <tbody>

                                            <tr>
                                                <th>Bank Account Type :</th>
                                                <td><?php echo $row['account_type']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Account Holder Name :</th>
                                                <td><?php echo $row['account_holder_name']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Bank Account Number :</th>
                                                <td><?php echo $row['account_number']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>IFSC :</th>
                                                <td><?php echo $row['ifsc_code']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Cheque Leaf :</th>
                                                <td><?php echo $row['cheque_leaf']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Digital Signature :</th>
                                                <td><?php echo $row['digital_signature']; ?></td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>







                </section>
            </div>
        </div>
    </section>
</section>


<?php include('footer.php'); ?>