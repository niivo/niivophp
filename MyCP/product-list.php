<?php
$PageTitle = 'Product List';

include("header.php");
//if (isset($_REQUEST["mode"])) {
//
//    $db->query("DROP TABLE products");
//    $sql = "create table products (SELECT nav_master.scheme_code,nav_master.scheme_name,nav_master.RTA_SCHEME_CODE,nav_master.DIV_REINVESTFLAG,nav_master.RTA_CODE,nav_master.ISIN,nav_master.NAV_DATE,amfi_products.Net_Asset_Value FROM nav_master LEFT JOIN amfi_products ON nav_master.ISIN = amfi_products.ISIN_Payout)";
//    $result = $db->query($sql);
//    cheader("MyCP/product-list.php");
//}
?>
<!--header end-->
<?php include("sidebar.php"); ?>

<!--sidebar end-->
<!--dynamic table-->
<link href="<?php echo base_path_admin ?>assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
<link href="<?php echo base_path_admin ?>assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
<link rel="<?php echo base_path_admin ?>stylesheet" href="assets/data-tables/DT_bootstrap.css" />

<!-- Custom styles for this template -->
<link href="<?php echo base_path_admin ?>css/style.css" rel="stylesheet">
<link href="<?php echo base_path_admin ?>css/style-responsive.css" rel="stylesheet" />

<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-md-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_path_admin ?>welcome.php"><i class="fa fa-home"></i> Dashboard</a></li>

                    <li><?php echo $PageTitle; ?></li>
                    <!--<li style="float: right"><a button class="btn btn-default" onClick="return confirmUpdate();" href="product-list.php?mode=update">Update</a></li>-->

                </ul>
                <!--breadcrumbs end -->
            </div>
        </div>
        <?php
//session msg bar
        if (isset($_SESSION["msg"]) && trim($_SESSION["msg"]) != '') {
            ?>
            <div class="alert alert-success fade in">
                <button type="button" class="close close-sm" data-dismiss="alert">
                    <i class="fa fa-times"></i>
                </button>
                <?php
                echo trim($_SESSION["msg"]);
                unset($_SESSION["msg"]);
                ?>
            </div>
            <?php
        } else if (isset($_SESSION["err_msg"]) && trim($_SESSION["err_msg"]) != '') {
            ?>
            <div class="alert alert-block alert-danger fade in">
                <button type="button" class="close close-sm" data-dismiss="alert">
                    <i class="fa fa-times"></i>
                </button>
                <?php
                echo $_SESSION["err_msg"];
                unset($_SESSION["err_msg"]);
                ?>
            </div>
            <?php
        }
        //session msg bar end
        ?>
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading"><?php echo $PageTitle; ?>

                        <span class="tools pull-right">
                            <a href="javascript:void(0);" class="fa fa-chevron-down"></a>
                        </span>
                    </header>
                    <div class="panel-body">

                        <div class="adv-table ">

                            <table class="display table table-bordered table-striped" id="dynamic-table_ajax">
                                <thead>
                                    <tr>
                                        <th>S. No </th>
                                        <th>Scheme Code</th>
                                        <th>Scheme Name</th>
                                        <th>Fund Name</th>
                                        <th>ISIN</th>
                                        <th>NAV</th>
                                        <th>STARMF MIN PUR AMT</th>
                                       
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>S. No </th>
                                        <th>Scheme Code</th>
                                        <th>Scheme Name</th>
                                        <th>Fund Name</th>
                                        <th>ISIN</th>
                                        <th>NAV</th>
                                        <th>STARMF MIN PUR AMT</th>
                                       
                                    </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
<!--right sidebar start-->
<div class="right-sidebar">
    <ul class="right-side-accordion">
        <li class="widget-collapsible">
            <ul class="widget-container">
                <li>
                    <div class="prog-row side-mini-stat clearfix">
                        <div class="side-mini-graph">
                            <div class="target-sell">
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </li>
    </ul>
</div>
<!--right sidebar end-->



<!-- Placed js at the end of the document so the pages load faster -->
<!--<link href="datatable/css/jquery.dataTables.css" rel="stylesheet">-->
<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>

<!--script for this page only-->
<!--<script src="js/editable-table/table-editable.js"></script>-->

<!-- END JAVASCRIPTS -->
<script type="text/javascript">
                        jQuery(document).ready(function () {
                            $('#dynamic-table_ajax').dataTable({
                                "aoColumnDefs": [
                                    {'bSortable': false, 'aTargets': [0]},
                                    {'bSortable': false, 'aTargets': [1]}
                                ],
                                "order": [[1, "DESC"]],
                                "processing": true,
                                "serverSide": true,
                                "ajax": "ajax/product-list.php"
                            });
                        });

                        function confirmDelete() {
                            var agree = confirm("Are you sure you want to delete this?");
                            if (agree)
                                return true;
                            else
                                return false;
                        }


                        function confirmUpdate() {
                            var agree = confirm("Are you sure you want to Update this?");
                            if (agree)
                                return true;
                            else
                                return false;
                        }
</script>
<?php include("footer.php"); ?>