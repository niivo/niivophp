<?php
$PageTitle = "View Order Detail";

include('header.php');

$row_over_alternate_color = "#F2F1EE";
$row_over_color = "#FFF8DB";
$bgcolor = "#FBFCFC";
extract($_GET);
$id = base64_decode($_GET["id"]);
$sql = " SELECT * FROM orders where id = " . $id . " ";

$res = $db->query($sql);
$row = $res->fetch();
?>
<style>
    #cp{
        float: left;
        left: 55% !important;
        margin: 0 5px 10px 0;
        position: absolute !important;
        top: 58% !important;
        z-index: 9999;
    }
    #editor_area >table {
        height:91%;
    }
</style>
<?php include("sidebar.php"); ?>
<div class="shadow-bottom shadow-titlebar"></div>
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-md-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_path_admin ?>welcome.php"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li class="active"><a href="<?php echo base_path_admin ?>user_list.php">Order</a></li>
                    <li class="active"><?php echo trim($PageTitle); ?></li>
                </ul>
                <!--breadcrumbs end -->
            </div>
        </div>

        <div class="row">
            <div class='col-sm-12'>
                <section class="panel">
                    <header class="panel-heading"> <?php echo $PageTitle ?>
                        <span class="tools pull-right">
                            <a class="fa fa-chevron-down" href="javascript:;"></a>
                            <a class="fa fa-times" href="javascript:;"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <tbody>
                                    <tr>
                                        <th>Order Id :</th>
                                        <td><?php echo $row['order_id']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Scheme Code :</th>
                                        <td><?php echo $row['scheme_code']; ?></td>
                                    </tr>

                                    <tr>
                                        <th>Bse Order No :</th>
                                        <td><?php echo $row['bse_order_no']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Order Type :</th>
                                        <td><?php echo $row['order_type']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Amount :</th>
                                        <td><?php echo $row['amount']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Quantity :</th>
                                        <td><?php echo $row['quantity']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Folio No :</th>
                                        <td><?php echo $row['folio_no']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Remarks :</th>
                                        <td><?php echo $row['remarks']; ?></td>
                                    </tr>

                                    <tr>
                                        <th>Order Status :</th> 
                                        <td><?php echo $row['order_status']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Fund Name :</th>
                                        <td><?php echo $row['FUND_NAME']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Date :</th>
                                        <td><?php echo FormatDateTime($row['dtdate']); ?></td>
                                    </tr>
                                    <?php
                                    if ($row['is_goal'] == '1') {
                                        $goal_detail = BSE_ORDER_API::goalDetail($row['order_id']);
                                        $row['goal_detail'] = $goal_detail;
                                        ?>
                                        <tr>
                                            <th>Goal Name :</th>
                                            <td><?php echo ($goal_detail['goal_name']); ?></td>
                                        </tr>  
                                        <tr>
                                            <th>Investment Type :</th>
                                            <td><?php echo ($goal_detail['investment_type']); ?></td>
                                        </tr>  
                                        <tr>
                                            <th>Goal Year :</th>
                                            <td><?php echo ($goal_detail['goal_year']); ?></td>
                                        </tr>  
                                        <tr>
                                            <th>Goal Ammount :</th>
                                            <td><?php echo ($goal_detail['goal_ammount']); ?></td>
                                        </tr>  
                                        <?php
                                    }
                                    ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php
                    $sql1 = " SELECT * FROM sip_child_order_detail where ref_no = " . $row['order_id'] . " ";

                    $res1 = $db->query($sql1);
                    $row1 = $res->fetch();
                    if ($res1->size() > 0) {
                        ?>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <div  class="Registerinner">

                                    <section class="outer_row">
                                        <section class="outer_box">
                                            <h2>Child Order Detail</h2>	

                                            <section class="inner_box">
                                                <ul>
                                                    <li>Order No</li>
                                                    <li class="bold"><?php echo $row1['order_no']; ?></li>
                                                </ul>
                                                <ul>
                                                    <li>Scheme Name</li>
                                                    <li class="bold"><?php echo $row1['scheme_name']; ?></li>
                                                </ul>
                                                <ul>
                                                    <li>Amount</li>
                                                    <li class="bold"><?php echo FormatNumber(round($row1['amount'])); ?></li>
                                                </ul>
                                                <ul>
                                                    <li>Folio No</li>
                                                    <li class="bold"><?php echo (($row1['foliono'])); ?></li>
                                                </ul>
                                                <ul>
                                                    <li> Quantity</li>
                                                    <li class="bold"><?php echo $row1['qty']; ?></li>
                                                </ul>

                                            </section>
                                        </section>





                                    </section>

                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </section>
            </div>
        </div>
    </section>
</section>

<?php include('footer.php'); ?>