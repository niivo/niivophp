<?php

require_once('../includes/app_top.php');
require_once('../includes/mysql.class.php');
// Include database connection
//echo "Gwqgw";

require_once('../includes/global.inc.php');

// Include user functions
require_once('../includes/user.class.php');
// Include general functions
require_once('../includes/functions_general.php');


if (isset($_POST['bulk_send_submit'])) {

    global $db;
    $status = "true";

    $allUser = $_POST['checked_id'];

    if ($status == "true") {

        $user = '';
        foreach ($allUser as $i => $user) {
            $user_info = User::getUserinfo($user);
            $user_data = $user_info['data'];

            $occupation = $user_data['occupation'];
            $occupation_string = Mapping::mapping_occupation_code($occupation);
            $user_data['occupation'] = ($occupation != '') ? $occupation_string['CVL_CODE'] : "";

            $gross_annual_income = $user_data['gross_annual_income'];
            $gross_annual_income = Mapping::mapping_income_code($gross_annual_income);
            $user_data['gross_annual_income'] = ($gross_annual_income != '') ? $gross_annual_income['CVL_CODE'] : "";


            


            $arr['data_new'][] = $user_data;
//            $arr['data_new'][] = array("name" => $user_data['name'], "mobile" => $user_data['mobile'],"pencard"=> $user_data['pencard'],"dob" => $user_data['dob'],"father_name"=> $user_data['father_name'],"kyc_status"=> $user_data['kyc_status']);
        }
        $data['user_detail'] = $arr['data_new'];


        $aws_detail['bucket'] = bucket;
        $aws_detail['region'] = region;
        $aws_detail['AWS_accessKey'] = AWS_accessKey;
        $aws_detail['AWS_secretKey'] = AWS_secretKey;
        $data['aws_detail'] = $aws_detail;

        $export_name = "Pending_Kyc_List" . "_" . date('Y-m-d') . "-" . date('H:i:s') . "" . ".txt";

//        file_put_contents($export_name, json_encode($data));
//        $content = file_get_contents($export_name);
//        header('Content-Type: application/octet-stream');
//        echo $content;
//        die;
        $handle = fopen($export_name, "w");
        fwrite($handle, json_encode($data));
        fclose($handle);

        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . basename($export_name));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($export_name));
        readfile($export_name);
        exit;

        $_SESSION["msg"] = "Sent successfully.";
    }
    cheader('MyCP/mass-notification.php');
}