<?php
require_once('../includes/app_top.php');
require_once('../includes/mysql.class.php');
// Include database connection

require_once('../includes/global.inc.php');
// Include user functions
require_once('../includes/user.class.php');


// Include general functions
require_once('../includes/functions_general.php');
//validation msg for system
require_once('../includes/validation_msg.php');
require_once('../includes/mailer.class.php');
require_once('../includes/phpmailer/class.smtp.php');
require_once('../includes/phpmailer/class.phpmailer.php');
include('../includes/image.class.php');
include('../includes/upl_function.php');



if ($_GET["mode"] == "logout") {
    session_unset();
    session_destroy();
    cheader("MyCP/index.php");
}
if (!isset($_SESSION['admin']) || trim($_SESSION['admin']) == '') {
    cheader("MyCP/index.php");
}


$USER = new UserClass;
//$PRODUCT= new ProductClass;
$MAILER = new MailerClass;
$PHPMailer = new PHPMailer();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ThemeBucket">
        <link rel="shortcut icon" href="<?php echo base_path; ?>images/favicon.ico">

        <title><?php echo $PageTitle; ?></title>

        <!--Core CSS -->

        <link href="<?php echo base_path_admin; ?>assets/jquery-ui/jquery-ui-1.10.1.custom.min.css" rel="stylesheet">
        <link href="<?php echo base_path_admin; ?>bs3/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_path_admin; ?>css/bootstrap-reset.css" rel="stylesheet">
        <link href="<?php echo base_path_admin; ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

        <!-- Custom styles for this template -->

        <link href="<?php echo base_path_admin; ?>css/style-responsive.css" rel="stylesheet" />
        <link href="<?php echo base_path_admin; ?>css/style.css" rel="stylesheet">
        <link href="<?php echo base_path_admin; ?>bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet">

        <script src="<?php echo base_path_admin; ?>js/lib/jquery.js"></script>
        <script src="<?php echo base_path_admin; ?>js/moment.js"></script>
        <script src="<?php echo base_path_admin; ?>js/bootstrap-datetimepicker.min.js"></script>
        
        <script type="text/javascript" src="<?php echo base_path_admin ?>js/jquery.validate.js"></script>
        <script src="<?php echo base_path_admin; ?>ckeditor/ckeditor.js" language="JavaScript" type="text/javascript" ></script>
          <!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>-->
        <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyC5nJl9DhtmcohBzbsuwDS2q-85KzavgJ8"></script>
        <script src="<?php base_path_admin ?>js/jquery.geocomplete.js"></script>
        <script>
            function showDetails(id)
            {
                var area = document.getElementById(id);

                if (area.style.display == "none")
                {
                    //area.style.display="block";
                    $(area).show("slow");
                } else
                {
                    //area.style.display="none";
                    $(area).hide("slow");
                }
            }

        </script>
    </head>

    <body>

        <section id="container">
            <!--header start-->
            <header class="header fixed-top clearfix">
                <!--logo start-->
                <div class="brand">

                    <a href="<?php echo base_path_admin; ?>">
                        <img src="<?php echo base_path_admin; ?>images/logo.png" alt="" width="95px" height="70px" class="" >
                    </a>
                    <div class="sidebar-toggle-box">
                        <div class="fa fa-bars"></div>
                    </div>
                </div>
                <!--logo end-->
                <div class="top-nav clearfix">
                    <!--search & user info start-->
                    <ul class="nav pull-right top-menu">

                        <!-- user login dropdown start-->
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <img alt="" src="<?php echo base_path_admin; ?>images/avatar1_small.jpg">
                                <span class="username">Admin</span>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu extended logout">
                                <li><a href="<?php echo base_path_admin ?>password-change.php"><i class="fa fa-key"></i> Change Password</a></li>

                                <li><a href="<?php echo $_SERVER['PHP_SELF'] ?>?mode=logout"><i class="fa fa-power-off"></i> Log Out</a></li>
                            </ul>
                        </li>
                        <!-- user login dropdown end -->

                    </ul>
                    <!--search & user info end-->
                </div>


            </header>
