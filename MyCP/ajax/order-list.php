<?php

require_once('../../includes/app_top.php');
require_once('../../includes/mysql.class.php');
// Include database connection
require_once('../../includes/global.inc.php');
// Include user functions
require_once('../../includes/user.class.php');
// Include general functions
require_once('../../includes/functions_general.php');

//validation msg for system
require_once('../../includes/validation_msg.php');

require_once('../../includes/mailer.class.php');

require_once '../../includes/classes/Models.class.php';
require_once('../../includes/classes/User.class.php');

$COLMN_SIZE = 6;

function get_colmn_name($counter) {
    $ret = '';

    switch ($counter) {

        case 0:
            $colmn_ord = "order_id";
            break;
        case 1:
            $colmn_ord = "order_type";
            break;
        case 2:
            $colmn_ord = "scheme_code";
            break;
        case 5:
            $colmn_ord = "amount";
            break;
        case 6:
            $colmn_ord = "quantity";
            break;
        case 8:
            $colmn_ord = "order_status";
            break;
        default:
            $colmn_ord = " id ";
    }
    return $ret;
}

$arr = array();
$search = security(trim($_REQUEST['search']['value']));
$order_colmn = $_REQUEST['order'][0]['column'];
$order_colmn_asc_desc = $_REQUEST['order'][0]['dir'];

$where = " WHERE 1";



$orderby = 'ORDER BY id DESC';
if ($search != '') {
    $where .= " AND (scheme_code like '%" . $search . "%')  ";
}

if (isset($_REQUEST['userid']) && $_REQUEST['userid'] != '' && !empty($_REQUEST['userid'])) {
    $userid = base64_decode($_REQUEST['userid']);

    $where .= " AND `userid`=" . $userid;
}

/* WHEN WE ADD TEXT BOXES FOR CUSTOM SEARCH TEXT VALUE */
for ($i = 0; $i < $COLMN_SIZE; $i++) {
    if (!empty($_REQUEST['columns'][$i]['search']['value'])) {
        $colmn_name = get_colmn_name($i);
        $where .= " AND $colmn_name LIKE '" . $_REQUEST['columns'][$i]['search']['value'] . "%' ";
    }
}

switch ($order_colmn) {
    case 0:
        $colmn_ord = "order_id";
        break;
    case 1:
        $colmn_ord = "order_type";
        break;
    case 2:
        $colmn_ord = "scheme_code";
        break;
    case 5:
        $colmn_ord = "amount";
        break;
    case 6:
        $colmn_ord = "quantity";
        break;
    case 8:
        $colmn_ord = "order_status";
        break;

    default:
        $colmn_ord = " id ";
}

$orderby = " ORDER BY $colmn_ord $order_colmn_asc_desc ";

$getUserDetail_main = "SELECT * FROM orders " . $where . $orderby;

$getUserDetail = "SELECT * FROM orders" . $where . $orderby . " LIMIT " . $_REQUEST['start'] . "," . $_REQUEST['length'] . "";

$exeQuery = $db->query($getUserDetail);

$exeQuery_main = $db->query($getUserDetail_main);

$arr['draw'] = $_REQUEST['draw'];
$arr['recordsTotal'] = $exeQuery_main->size();
$arr['recordsFiltered'] = $exeQuery_main->size();
$arr['data'] = array();

if ($exeQuery->size() > 0) {

    $i = 0;

    while ($row = $exeQuery->fetch()) {
        $i++;
        $userid = $row['userid'];
        $user_data = User::getUserinfo($userid);
        $user_info = $user_data['data'];

        if ($row['xml_file'] != '') {
            $xml_file = '<a download class="btn btn-default" href="' . base_path . "request/" . ($row['xml_file']) . '">Download</a';
        } else {
            $xml_file = "-";
        }

        $view = '<a button class="btn btn-default" href="' . base_path_admin . 'order-detail.php?id=' . base64_encode($row['id']) . '">View</a';
        $button = '<a button class="btn btn-default" onClick="return confirmDelete();" href="' . base_path_admin . 'user_list.php?userid=' . ($row['userid']) . '&mode=delete">Delete</a';


        if ($row['is_allotment'] == "1") {
            $is_allotment = "Yes";
        } else {
            $is_allotment = "No";
        }

        $arr['data'][] = array($i, $row['order_id'], $row['order_type'], $row['scheme_code'], $user_info['name'], $row['amount'], $row['quantity'], $row['folio_no'], $row['order_status'], $is_allotment, FormatDate($row['dtdate']), $xml_file, $view);
    }
}
echo json_encode($arr);
