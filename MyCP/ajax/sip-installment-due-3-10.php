<?php

require_once('../../includes/app_top.php');
require_once('../../includes/mysql.class.php');
// Include database connection
require_once('../../includes/global.inc.php');
// Include user functions
require_once('../../includes/user.class.php');
// Include general functions
require_once('../../includes/functions_general.php');

//validation msg for system
require_once('../../includes/validation_msg.php');

require_once('../../includes/mailer.class.php');


$COLMN_SIZE = 6;

function get_colmn_name($counter) {
    $ret = '';

    switch ($counter) {

        case 0:
            $colmn_ord = "ref_no";
            break;

        default:
            $colmn_ord = " id ";
    }
    return $ret;
}

$arr = array();
$search = security(trim($_REQUEST['search']['value']));
$order_colmn = $_REQUEST['order'][0]['column'];
$order_colmn_asc_desc = $_REQUEST['order'][0]['dir'];

$where = " WHERE 1";



$orderby = 'ORDER BY id DESC';
if ($search != '') {
    $where .= " AND (ref_no like '%" . $search . "%' OR sip_registration_no  like '%" . $search . "%')  ";
}

/* WHEN WE ADD TEXT BOXES FOR CUSTOM SEARCH TEXT VALUE */
for ($i = 0; $i < $COLMN_SIZE; $i++) {
    if (!empty($_REQUEST['columns'][$i]['search']['value'])) {
        $colmn_name = get_colmn_name($i);
        $where .= " AND $colmn_name LIKE '" . $_REQUEST['columns'][$i]['search']['value'] . "%' ";
    }
}

switch ($order_colmn) {
    case 0:
        $colmn_ord = "ref_no";
        break;

    default:
        $colmn_ord = " id ";
}

$orderby = " ORDER BY $colmn_ord $order_colmn_asc_desc ";

$getUserDetail_main = "SELECT * FROM sip_installment_dues " . $where . $orderby;

$getUserDetail = "SELECT * FROM sip_installment_dues" . $where . $orderby . " LIMIT " . $_REQUEST['start'] . "," . $_REQUEST['length'] . "";


$exeQuery = $db->query($getUserDetail);

$exeQuery_main = $db->query($getUserDetail_main);

$arr['draw'] = $_REQUEST['draw'];
$arr['recordsTotal'] = $exeQuery_main->size();
$arr['recordsFiltered'] = $exeQuery_main->size();
$arr['data'] = array();

if ($exeQuery->size() > 0) {

    $i = 0;

    while ($row = $exeQuery->fetch()) {
        $userid = $row['client_code'];
        $user_info  = User::getUserinfo($userid);
        $user_data = $user_info['data'];
        $name = $user_data['name'];
        $i++;
        $button = '<a button class="btn btn-default" onClick="return confirmDelete();" href="' . base_path_admin . 'user_list.php?userid=' . ($row['userid']) . '&mode=delete">Delete</a';

        $arr['data'][] = array($i, $name, $row['ref_no'], $row['sip_registration_no'], $row['fund_name'], $row['amount'], $row['prev_paid_date'], $row['due_date']);
    }
}
echo json_encode($arr);
