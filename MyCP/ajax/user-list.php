<?php

require_once('../../includes/app_top.php');
require_once('../../includes/mysql.class.php');
// Include database connection
//echo "Gwqgw";

require_once('../../includes/global.inc.php');

// Include user functions
require_once('../../includes/user.class.php');
// Include general functions
require_once('../../includes/functions_general.php');

//validation msg for system
require_once('../../includes/validation_msg.php');

require_once('../../includes/mailer.class.php');

require_once '../../includes/classes/Models.class.php';
require_once('../../includes/classes/User.class.php');

$COLMN_SIZE = 6;

function get_colmn_name($counter) {
    $ret = '';

    switch ($counter) {

        case 0:
            $colmn_ord = "userid";
            break;

        case 1:
            $colmn_ord = "name";
            break;

        case 2:
            $colmn_ord = "mobile";
            break;

        default:
            $colmn_ord = " userid ";
    }
    return $ret;
}

$arr = array();
$search = security(trim($_REQUEST['search']['value']));
$order_colmn = $_REQUEST['order'][0]['column'];
$order_colmn_asc_desc = $_REQUEST['order'][0]['dir'];

$where = " WHERE 1";



$orderby = 'ORDER BY userid DESC';
if ($search != '') {
    $where .= " AND (mobile like '%" . $search . "%')  ";
}

/* WHEN WE ADD TEXT BOXES FOR CUSTOM SEARCH TEXT VALUE */
for ($i = 0; $i < $COLMN_SIZE; $i++) {
    if (!empty($_REQUEST['columns'][$i]['search']['value'])) {
        $colmn_name = get_colmn_name($i);
        $where .= " AND $colmn_name LIKE '" . $_REQUEST['columns'][$i]['search']['value'] . "%' ";
    }
}

switch ($order_colmn) {
    case 0:
        $colmn_ord = "userid";
        break;
    case 1:
        $colmn_ord = "name";
        break;

    case 2:
        $colmn_ord = "mobile";
        break;
    default:
        $colmn_ord = " userid ";
}

$orderby = " ORDER BY $colmn_ord $order_colmn_asc_desc ";

$getUserDetail_main = "SELECT * FROM users " . $where . $orderby;

$getUserDetail = "SELECT * FROM users" . $where . $orderby . " LIMIT " . $_REQUEST['start'] . "," . $_REQUEST['length'] . "";


$exeQuery = $db->query($getUserDetail);

$exeQuery_main = $db->query($getUserDetail_main);

$arr['draw'] = $_REQUEST['draw'];
$arr['recordsTotal'] = $exeQuery_main->size();
$arr['recordsFiltered'] = $exeQuery_main->size();
$arr['data'] = array();

if ($exeQuery->size() > 0) {

    $i = 0;

    while ($row = $exeQuery->fetch()) {
        $i++;
        $kyc_status = $row['kyc_status'];


        $countOrder = User::totalUserOrder($row['userid']);
        $count_order = '<a href="' . base_path_admin . 'order-list.php?userid=' . base64_encode($row['userid']) . '">' . $countOrder . '</a';

        $button = '<a button class="btn btn-default" onClick="return confirmDelete();" href="' . base_path_admin . 'user_list.php?userid=' . ($row['userid']) . '&mode=delete">Delete</a>';
        $view = '<a button class="btn btn-default" href="' . base_path_admin . 'user-detail.php?userid=' . base64_encode($row['userid']) . '">View</a';

        $image_upload_status = '
                 <select id="coinType" onchange=image_upload_status("' . $row['userid'] . '",this)>
                    <option ';
        $image_upload_status.= ($row['image_upload_status'] == "PENDING" ) ? 'selected' : '';
        $image_upload_status .= ' id="PENDING" value="PENDING">Pending</option>
                    id="PENDING" value="PENDING">Pending </option>

                    <option ';
        $image_upload_status.= ($row['image_upload_status'] == "ACCEPTED" ) ? 'selected' : '';
        $image_upload_status .= ' id="ACCEPTED" value="ACCEPTED">Accepted</option>
                    </select>
                     ';
        $arr['data'][] = array($i, $row['name'], $row['mobile'], $count_order, $kyc_status, $view, $button);
    }
}
echo json_encode($arr);
