<?php
$PageTitle = "Edit Page Content";

include('header.php');

$row_over_alternate_color = "#F2F1EE";
$row_over_color = "#FFF8DB";
$bgcolor = "#FBFCFC";
extract($_GET);
$id = base64_decode($id);
$sql = "select * from html_pages where page_id=" . $id;
$res = $db->query($sql);
$row = $res->fetch();
$desc = stripslashes($row["page_text"]);

function rteSafe($strText) {
	//returns safe code for preloading in the RTE
	$tmpString = $strText;
	//convert all types of single quotes
	$tmpString = str_replace(chr(145), chr(39), $tmpString);
	$tmpString = str_replace(chr(146), chr(39), $tmpString);
	$tmpString = str_replace("'", "&#39;", $tmpString);

	//convert all types of double quotes
	$tmpString = str_replace(chr(147), chr(34), $tmpString);
	$tmpString = str_replace(chr(148), chr(34), $tmpString);
	//	$tmpString = str_replace("\"", "\"", $tmpString);
	//replace carriage returns & line feeds
	$tmpString = str_replace(chr(10), " ", $tmpString);
	$tmpString = str_replace(chr(13), " ", $tmpString);

	return $tmpString;
}

if (isset($_POST["btnsubmit"])) {
	/*$page_name = security(trim($_POST["name"]));*/
	$page_title = security(trim($_POST["title"]));
	$page_heading = security(trim($_POST["heading"]));
	$desc = addslashes(trim($_POST["desc"]));
	$meta_title = security(trim($_POST["meta_title"]));
	$meta_description = security(trim($_POST["meta_description"]));
	 
	$str = true;
	/*if ($page_name == '') {
		$err_msg = $err_msg . "Page Name is a required field" . "<br>";
		$str = false;
	}*/
	if ($page_title == '') {
		$err_msg = $err_msg . "Page Title is a required field" . "<br>";
		$str = false;
	}

	if ($page_heading == '') {
		$err_msg = $err_msg . "Page Heading is a required field" . "<br>";
		$str = false;
	}

	if ($desc == '') {
		$err_msg = $err_msg . "Page Content is a required field" . "<br>";
		$str = false;
	}

	if ($str == true) {
		$sql_msg = "UPDATE `html_pages` SET " .
			" `page_title`='$page_title', " .
			" `page_heading`='$page_heading', " .
			" `page_text`='$desc', dtdate='" . date('Y-m-d h:i:s a') . "' where `page_id`='$id'";

		$result_msg = $db->query($sql_msg);

		if ($result_msg) {
			echo "hello";
			$_SESSION["msg"] = "Page content has been successfully Updated.";
			cheader("MyCP/list-of-page-contents.php");
		} else
			$err_msg = $err_msg . "Error occured while updating content.<br>Please try again.";
	}
}

?>
<style>
	#cp{
		float: left;
		left: 55% !important;
		margin: 0 5px 10px 0;
		position: absolute !important;
		top: 58% !important;
		z-index: 9999;
	}
	#editor_area >table {
		height:91%;
	}
</style>
<?php include("sidebar.php"); ?>
<div class="shadow-bottom shadow-titlebar"></div>
<section id="main-content">
	<section class="wrapper">
		<!-- page start-->
		<div class="row">
			<div class="col-md-12">
				<!--breadcrumbs start -->
				<ul class="breadcrumb">
					<li><a href="<?php echo base_path_admin ?>welcome.php"><i class="fa fa-home"></i> Dashboard</a></li>
					<li class="active"><a href="<?php echo base_path_admin ?>list-of-page-contents.php"><?php echo trim('Content Manager '); ?></a></li>
					<li class="active"><?php echo trim($PageTitle); ?></li>
				</ul>
				<!--breadcrumbs end -->
			</div>
		</div>

		<div class="row">
			<div class='col-sm-12'>
				<section class="panel">
					<header class="panel-heading"> <?php echo $PageTitle ?>
						<span class="tools pull-right">
							<a class="fa fa-chevron-down" href="javascript:;"></a>
							<a class="fa fa-times" href="javascript:;"></a>
						</span>
					</header>
					<div class="panel-body">
						<div class="position-left col-sm-9">
						<?php
                        	$ERROR_MSG = isset($_SESSION["errormsg"]) ? $_SESSION["errormsg"] : '';
                        	$MSG = isset($_SESSION["msg"]) ? $_SESSION["msg"] : '';
                        	if ($ERROR_MSG != "") { ?>
								<div class="msg" id="msgError" >
									<div class="msg-error">
										<p><?php echo $ERROR_MSG; ?></p>
										<a class="close" href="javascript:showDetails('msgError');">close</a></div></div>
<?php } elseif ($err_msg != '') { ?>
								<div class="msg" id="msgError" >
									<div class="msg-error">
										<p><?php echo $err_msg; ?></p>
										<a class="close" href="javascript:showDetails('msgError');">close</a></div></div>
							<?php } elseif ($MSG != "") { ?>
								<div class="msg" id="msgOk" >
									<div class="msg-ok">
										<p><?php echo $MSG; ?></p><a class="close" href="javascript:showDetails('msgOk');">close</a></div></div>
<?php
}
unset($_SESSION["errormsg"]);
unset($_SESSION["msg"]);
?>
							<form method="post" name="frmmsg" onsubmit="return validate(this);" class="form-horizontal">
								<div class="form-group">
									<label class="col-lg-2 col-sm-2 control-label" for=file>Page Title:*</label>
									<div class="col-lg-10">
                                        <input type="hidden" name="pageid" value="<?php echo $id; ?>" />
										<input class="form-control" type="text" name="title" size="35" value="<?php echo $row['page_title'] ?>"/>
										<div id="advice-required-entry-title" class="validation-hide"></div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-2 col-sm-2 control-label" for=file>Page Heading:*</label>
									<div class="col-lg-10">
										<input class="form-control" type="text" name="heading" size="35" value="<?php echo $row['page_heading'] ?>"/>
										<div id="advice-required-entry-heading" class="validation-hide"></div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-2 col-sm-2 control-label" for=textarea>Page Content:*</label>
									<div class="col-lg-10" id="editor_area">
										<textarea class="form-control ckeditor" id="desc" name="desc" ><?php echo $desc; ?></textarea>
									</div>
								</div>
								<?php /*
                                <div class="form-group">
									<label class="col-lg-2 col-sm-2 control-label" for=file>Meta Title:*</label>
									<div class="col-lg-10">
										<input class="form-control" type="text" name="meta_title" size="35" value="<?php echo $row['meta_title'] ?>"/>
									</div>
								</div>
                                <div class="form-group">
									<label class="col-lg-2 col-sm-2 control-label" for=file>Meta Description:*</label>
									<div class="col-lg-10">
                                        <textarea class="form-control" name="meta_description"><?php echo $row['meta_description'] ?></textarea>
									</div>
								</div>
								*/ ?>
								<div class="clr"></div>
								<div class="form-group">
									<div class="col-lg-offset-2 col-lg-10">
										<input class="btn btn-info" type="submit" value="Update" name="btnsubmit">
										<button class="btn btn-default" type="reset">Cancel</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
</section>
<?php include('footer.php'); ?>