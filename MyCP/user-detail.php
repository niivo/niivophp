<?php
$PageTitle = "View User Detail";

include('header.php');

$row_over_alternate_color = "#F2F1EE";
$row_over_color = "#FFF8DB";
$bgcolor = "#FBFCFC";
extract($_GET);
$userid = base64_decode($_GET["userid"]);
$sql = " SELECT * FROM users where userid = " . $userid . " ";

$res = $db->query($sql);
$row = $res->fetch();

if ($row['thumb_image'] != '') {
    $image = $row['thumb_image'];
} else {
    $image = "MyCP/images/login-logo.png";
}
?>
<style>
    #cp{
        float: left;
        left: 55% !important;
        margin: 0 5px 10px 0;
        position: absolute !important;
        top: 58% !important;
        z-index: 9999;
    }
    #editor_area >table {
        height:91%;
    }
</style>
<?php include("sidebar.php"); ?>
<div class="shadow-bottom shadow-titlebar"></div>
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-md-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_path_admin ?>welcome.php"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li class="active"><a href="<?php echo base_path_admin ?>user_list.php">User</a></li>
                    <li class="active"><?php echo trim($PageTitle); ?></li>
                </ul>
                <!--breadcrumbs end -->
            </div>
        </div>

        <div class="row">
            <div class='col-sm-12'>
                <section class="panel">
                    <header class="panel-heading"> <?php echo $PageTitle ?>
                        <span class="tools pull-right">
                            <a class="fa fa-chevron-down" href="javascript:;"></a>
                            <a class="fa fa-times" href="javascript:;"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <tbody>
                                    <tr>
                                        <th>Name :</th>
                                        <td><?php echo $row['name']; ?></td>
                                    </tr>

                                    <tr>
                                        <th>Mobile :</th>
                                        <td><?php echo $row['mobile']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Email :</th>
                                        <td><?php echo $row['email']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Dob :</th>
                                        <td><?php echo $row['dob']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Gender :</th>
                                        <td><?php echo $row['gender']; ?></td>
                                    </tr>

                                    <tr>
                                        <th>Occupation :</th>
                                        <td><?php echo $row['occupation']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Address 1 :</th>
                                        <td><?php echo $row['address1']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Address 2 :</th>
                                        <td><?php echo $row['address2']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Address 3 :</th>
                                        <td><?php echo $row['address3']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>City :</th>
                                        <td><?php echo $row['city']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>State :</th>
                                        <td><?php echo $row['state']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Country :</th>
                                        <td><?php echo $row['country']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Zipcode :</th>
                                        <td><?php echo $row['pin']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Image :</th>
                                        <td><img src="<?php echo base_path . $image; ?>" height="100" width="100"/></td>
                                    </tr>
                                    <tr>
                                        <th>Pancard :</th>
                                        <td><?php echo $row['Pencard']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Bank Account Type :</th>
                                        <td><?php echo $row['bank_ac_type']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Bank Account Number :</th>
                                        <td><?php echo $row['account_no']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>IFSC :</th>
                                        <td><?php echo $row['ifsc']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Account Name :</th>
                                        <td><?php echo $row['account_name']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Nominee Name :</th>
                                        <td><?php echo $row['nominee_name']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Nominee Dob :</th>
                                        <td><?php echo $row['nominee_dob']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Nominee Relationship :</th>
                                        <td><?php echo $row['nominee_relationship']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Nominee Address :</th>
                                        <td><?php echo $row['nominee_address']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Father Name :</th>
                                        <td><?php echo $row['father_name']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Mother Name :</th>
                                        <td><?php echo $row['mother_name']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Pancard Verify :</th>
                                        <td><?php echo $row['pancard_verify'] == 1 ? "Yes" : "No"; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Address Verify :</th>
                                        <td><?php echo $row['address_verify'] == 1 ? "Yes" : "No"; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Identity Verify :</th>
                                        <td><?php echo $row['identity_verify'] == 1 ? "Yes" : "No"; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Nominee Verify :</th>
                                        <td><?php echo $row['nominee_verify'] == 1 ? "Yes" : "No"; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Bank Verify :</th>
                                        <td><?php echo $row['bank_verify'] == 1 ? "Yes" : "No"; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Aadhar Verify :</th>
                                        <td><?php echo  $row['aadhar_verify'] == 1 ? "Yes" : "No"; ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>

<?php include('footer.php'); ?>