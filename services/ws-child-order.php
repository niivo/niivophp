<?php

header('Content-Type: application/json; Charset=UTF-8');
require_once('../includes/app_top.php');
require_once('../includes/mysql.class.php');
require_once('../includes/global.inc.php');
require_once('../includes/functions_general.php');
require_once('../includes/image_lib/image.class.php');

require_once('../includes/user.class.php');

require_once ('../includes/signature.class.php');
require_once ('../includes/mailer.class.php');
require_once ('../includes/validation_msg.php');
require_once '../includes/phpmailer/class.phpmailer.php';

require_once '../includes/classes/Child_Order.class.php';

require_once '../includes/config.php';


User::isToken();

$SIGNATURE = new SIGNATURE;
$USER = new User();

$BSE_CC = new Child_Order();


//$resp= $BSI->orderEntry();
//echo json_encode($resp);
//
//


ErrorLog::log("REQUEST URL", $_SERVER['REQUEST_URI']);
ErrorLog::log("REQUEST", $_REQUEST);
$arr = array("msg" => "no api found", "status" => "false");



if (strtoupper($_REQUEST['type']) == "GET-ORDER") {
    $data = $_POST;
    $arr = $BSE_CC->orderDetail($data);
}

if (strtoupper($_REQUEST['type']) == "SIPORDERDETAIL") {
    $data = $_POST;
    $arr = BSE_PAYMENT_API::sipOrderDetail($data);
}


echo json_encode($arr);
ErrorLog::log("RESPONSE", $arr);
