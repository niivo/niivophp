<?php

header('Content-Type: application/json; Charset=UTF-8');
require_once('../includes/app_top.php');
require_once('../includes/mysql.class.php');
require_once('../includes/global.inc.php');
require_once('../includes/functions_general.php');
require_once('../includes/image_lib/image.class.php');

require_once('../includes/user.class.php');

require_once ('../includes/signature.class.php');
require_once ('../includes/mailer.class.php');
require_once ('../includes/validation_msg.php');
require_once '../includes/phpmailer/class.phpmailer.php';
require_once '../includes/classes/User.class.php';
require_once '../includes/classes/ErrorLog.class.php';
require_once '../includes/classes/Product_Intermediate.class.php';

//User::isToken();

//$SIGNATURE = new SIGNATURE;
//$USER = new User();

ErrorLog::log("REQUEST URL", $_SERVER['REQUEST_URI']);
ErrorLog::log("REQUEST", $_REQUEST);
$arr = array("msg" => "no api found", "status" => "false");



if (strtoupper($_REQUEST['type']) == "EQUITYFUND") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $sub_section = $data[0]->sub_section;
    $type = $data[0]->type; // RG for growth plans, RD for Divind plan

    $arr = Product_Intermediate::EquityFund($sub_section,$type);
}

if (strtoupper($_REQUEST['type']) == "DEBTFUND") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $sub_section = $data[0]->sub_section;
	$type = $data[0]->type; // RG for growth plans, RD for Divind plan
    $arr = Product_Intermediate::DebtFund($sub_section,$type);
}


if (strtoupper($_REQUEST['type']) == "HYBRIDFUND") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $sub_section = $data[0]->sub_section;
    $type = $data[0]->type; // RG for growth plans, RD for Divind plan
    $arr = Product_Intermediate::HybridFund($sub_section,$type);
}

if (strtoupper($_REQUEST['type']) == "TAXSAVINGFUND") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $type = $data[0]->type; // RG for growth plans, RD for Divind plan
    $arr = Product_Intermediate::TaxsavingFund($type);
}
if (strtoupper($_REQUEST['type']) == "OTHERFUND") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $sub_section = $data[0]->sub_section;
	$type = $data[0]->type; // RG for growth plans, RD for Divind plan
    $arr = Product_Intermediate::OtherFund($sub_section,$type);
}
if (strtoupper($_REQUEST['type']) == "NEWFUND") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $type = $data[0]->type; // RG for growth plans, RD for Divind plan
    $arr = Product_Intermediate::NewFund($type);
}

if (strtoupper($_REQUEST['type']) == "GOALFUND") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
	$fund_category = $data[0]->fund_category;
    $sip_flag = $data[0]->sip_flag;
	$userid = $data[0]->userid;
	$duration = $data[0]->duration;
	
    $arr = Product_Intermediate::Goals_list($fund_category, $sip_flag, $userid, $duration);
}

echo json_encode($arr);
ErrorLog::log("RESPONSE", $arr);
