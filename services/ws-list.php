<?php

header('Content-Type: application/json; Charset=UTF-8');
require_once('../includes/app_top.php');
require_once('../includes/mysql.class.php');
require_once('../includes/global.inc.php');
require_once('../includes/functions_general.php');
require_once('../includes/image_lib/image.class.php');

require_once('../includes/user.class.php');
require_once '../includes/classes/User.class.php';
require_once ('../includes/signature.class.php');
require_once ('../includes/mailer.class.php');
require_once ('../includes/validation_msg.php');
require_once '../includes/phpmailer/class.phpmailer.php';
require_once '../includes/classes/ErrorLog.class.php';
require_once '../includes/classes/Listing.class.php';
User::isToken();

$SIGNATURE = new SIGNATURE;
$USER = new User();
//User::isActive();

//services/ws-list.php?type=STATELIST
//services/ws-list.php?type=CITYLIST&data=[{"StateID":"1966"}]
//services/ws-list.php?type=LONGTERM
//services/ws-list.php?type=MOSTPERSISTANCE
//services/ws-list.php?type=RISKAVERSE
//services/ws-list.php?type=RISKTAKER
//services/ws-list.php?type=BIGGNERS&data=[{"fund_category":"1","sip_flag":"Y"}]
//services/ws-list.php?type=FUNDCATEGORYLIST




ErrorLog::log("REQUEST URL", $_SERVER['REQUEST_URI']);
ErrorLog::log("REQUEST", $_REQUEST);
$arr = array("msg" => "no api found", "status" => "false");



if (strtoupper($_REQUEST['type']) == "STATELIST") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $arr = Listing::stateList();
}

if (strtoupper($_REQUEST['type']) == "CITYLIST") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $StateID = $data[0]->StateID;
    $arr = Listing::cityList($StateID);
}

if (strtoupper($_REQUEST['type']) == "LONGTERM") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));

    $arr = Listing::longTerm();
}


if (strtoupper($_REQUEST['type']) == "MOSTPERSISTANCE") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));

    $arr = Listing::mostPersistent();
}

if (strtoupper($_REQUEST['type']) == "RISKAVERSE") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));

    $arr = Listing::riskAverse();
}

if (strtoupper($_REQUEST['type']) == "RISKTAKER") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));

    $arr = Listing::riskTaker();
}

if (strtoupper($_REQUEST['type']) == "BIGGNERS") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $fund_category = $data[0]->fund_category;
    $sip_flag = $data[0]->sip_flag;
    $arr = Listing::biggnersList($fund_category, $sip_flag);
}

if (strtoupper($_REQUEST['type']) == "FUNDCATEGORYLIST") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $arr = Listing::fundCategory();
}



echo json_encode($arr);
ErrorLog::log("RESPONSE", $arr);
