<?php

header('Content-Type: application/json; Charset=UTF-8');
require_once('../includes/app_top.php');
require_once('../includes/mysql.class.php');
require_once('../includes/global.inc.php');
require_once('../includes/functions_general.php');
require_once('../includes/image_lib/image.class.php');

require_once('../includes/user.class.php');

require_once ('../includes/signature.class.php');
require_once ('../includes/mailer.class.php');
require_once ('../includes/validation_msg.php');
require_once '../includes/phpmailer/class.phpmailer.php';
require_once '../includes/classes/BSE_ORDER_API.class.php';
require_once '../includes/classes/BSE_SIP_ORDER_API.class.php';

require_once '../includes/config.php';

$SIGNATURE = new SIGNATURE;
$USER = new User();

$BSE_PAY = new BSE_PAYMENT_API();


//$resp= $BSI->orderEntry();
//echo json_encode($resp);
//
//


ErrorLog::log("REQUEST URL", $_SERVER['REQUEST_URI']);
ErrorLog::log("REQUEST", $_REQUEST);
$arr = array("msg" => "no api found", "status" => "false");



if (strtoupper($_REQUEST['type']) == "PAYMENT") {
    $data = $_POST;
    $arr = $BSE_PAY->payOrder($data);
}
if (strtoupper($_REQUEST['type']) == "SIP-PURCHASE") {
    $data = $_POST;
    $arr = $SIP->saveSipOrder($data);
//    $arr =  $SIP->sipOrderEntry($data);
}

if (strtoupper($_REQUEST['type']) == "CANCEL") {
    $data = $_POST;
    $arr = $BSI->cancelOrder($data);
}


if (strtoupper($_REQUEST['type']) == "ORDERLIST") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $userid = $data[0]->userid;
    $order_status = $data[0]->order_status;
    $arr = $BSI->orderList($userid, $order_status);
}

if (strtoupper($_REQUEST['type']) == "SIPORDER") {
    $data = $_POST;
    $arr = $BSI->sipOrderEntry($data);
}


if (strtoupper($_REQUEST['type']) == "RATINGORDER") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $userid = $data[0]->userid;
    $order_id = $data[0]->order_id;
    $rating = $data[0]->rating;
    $arr = BSE_ORDER_API::ratingOrder($userid, $order_id, $rating);
}


echo json_encode($arr);
ErrorLog::log("RESPONSE", $arr);
