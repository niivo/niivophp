<?php

header('Content-Type: application/json; Charset=UTF-8');
require_once('../includes/app_top.php');
require_once('../includes/mysql.class.php');
require_once('../includes/global.inc.php');
require_once('../includes/functions_general.php');
require_once('../includes/image_lib/image.class.php');

require_once ('../includes/signature.class.php');
require_once ('../includes/mailer.class.php');

require_once ('../includes/validation_msg.php');
require_once '../includes/phpmailer/class.phpmailer.php';


$SIGNATURE = new SIGNATURE;
//User::isActive();


//services/ws-verificaton.php?type=ADDPANCARD
//services/ws-verificaton.php?type=ADDADDRESS
//services/ws-verificaton.php?type=ADDIDETITY
//services/ws-verificaton.php?type=ADDNOMINEE
//services/ws-verificaton.php?type=ADDBANK


ErrorLog::log("REQUEST URL", $_SERVER['REQUEST_URI']);
ErrorLog::log("REQUEST", $_REQUEST);
$arr = array("msg" => "no api found", "status" => "false");


if (strtoupper($_REQUEST['type']) == "ADDPANCARD") {
    $data = $_POST;
    $arr = Verifiation::addPanCard($data);
}
if (strtoupper($_REQUEST['type']) == "ADDADDRESS") {
    $data = $_POST;
    $arr = Verifiation::addAddress($data);
}
if (strtoupper($_REQUEST['type']) == "ADDIDETITY") {
    $data = $_POST;
    $arr = Verifiation::addIdentity($data);
}
if (strtoupper($_REQUEST['type']) == "ADDNOMINEE") {
    $data = $_POST;
    $arr = Verifiation::addNominee($data);
}
if (strtoupper($_REQUEST['type']) == "ADDBANK") {
    $data = $_POST;
    $arr = Verifiation::addBank($data);
}

echo json_encode($arr);
ErrorLog::log("RESPONSE", $arr);
