<?php

header('Content-Type: application/json; Charset=UTF-8');
require_once('../includes/app_top.php');
require_once('../includes/mysql.class.php');
require_once('../includes/global.inc.php');
require_once('../includes/functions_general.php');
require_once('../includes/image_lib/image.class.php');

require_once('../includes/user.class.php');

require_once ('../includes/signature.class.php');
require_once ('../includes/mailer.class.php');
require_once ('../includes/validation_msg.php');
require_once '../includes/phpmailer/class.phpmailer.php';
require_once '../includes/classes/BSE_ORDER_API.class.php';
require_once '../includes/classes/BSE_SIP_ORDER_API.class.php';

require_once '../includes/config.php';




$SIGNATURE = new SIGNATURE;
$USER = new User();

$BSI = new BSE_ORDER_API();
$SIP = new BSE_SIP_ORDER_API;

//$resp= $BSI->orderEntry();
//echo json_encode($resp);
//
//
//exit;
//        
//User::isActive();
//services/ws-order.php?type=PURCHASE
//services/ws-order.php?type=CANCEL
//services/ws-order.php?type=ORDERLIST&data=[{"userid":"21","order_status":"CONFIRM","payment_status":"1"}]
//services/ws-order.php?type=RATINGORDER&data=[{"userid":"21","order_id":"22","rating":"3"}]
//services/ws-order.php?type=MYMONEY&data=[{"userid":"21"}]
//services/ws-order.php?type=SIP-DUE-REPORT&data=[{"userid":"21"}]
//services/ws-order.php?type=WITHDRAW



function getLumpSumAmount($requiredAmount, $timeInYrs, $annualGrowthRate) {
    $lumpSumAmount = $requiredAmount * pow((1 + $annualGrowthRate / 100), (-1 * $timeInYrs));
    return $lumpSumAmount;
}

function getSipAmount($requiredAmount, $timeInYrs, $annualGrowthRate) {
    $divisionFactor = 0;
    $timeInMths = $timeInYrs * 12;
    for ($i = 1; $i <= $timeInMths; $i++) {
        $divisionFactor += pow((1 + $annualGrowthRate / (12 * 100)), $i);
    }
    $sipAmt = $requiredAmount / $divisionFactor;
    return $sipAmt;
}

ErrorLog::log("REQUEST URL", $_SERVER['REQUEST_URI']);
ErrorLog::log("REQUEST", $_REQUEST);
$arr = array("msg" => "no api found", "status" => "false");



if (strtoupper($_REQUEST['type']) == "PURCHASE") {
    $data = $_POST;
    $arr = $BSI->saveLocalOrder($data);
}
if (strtoupper($_REQUEST['type']) == "SIP-PURCHASE") {
    $data = $_POST;
    $arr = $SIP->saveSipOrder($data);
//    $arr =  $SIP->sipOrderEntry($data);
}
if (strtoupper($_REQUEST['type']) == "WITHDRAW") {
    $data = $_POST;
    $arr = $BSI->orderWithdrawEntry($data);
}

if (strtoupper($_REQUEST['type']) == "CANCEL") {
    $data = $_POST;
    $arr = $BSI->cancelOrder($data);
}


if (strtoupper($_REQUEST['type']) == "ORDERLIST") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $userid = $data[0]->userid;
    $order_status = $data[0]->order_status;
    $payment_status = $data[0]->payment_status;
    $arr = $BSI->orderList($userid, $order_status, $payment_status);
}

if (strtoupper($_REQUEST['type']) == "SIPORDER") {
    $data = $_POST;
    $arr = $BSI->sipOrderEntry($data);
}


if (strtoupper($_REQUEST['type']) == "RATINGORDER") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $userid = $data[0]->userid;
    $order_id = $data[0]->order_id;
    $rating = $data[0]->rating;
    $arr = BSE_ORDER_API::ratingOrder($userid, $order_id, $rating);
}


if (strtoupper($_REQUEST['type']) == "MYMONEY") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $userid = $data[0]->userid;
    $arr = BSE_ORDER_API::myMoney($userid);
}

if (strtoupper($_REQUEST['type']) == "SIP-DUE-REPORT") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $userid = $data[0]->userid;
    $arr = BSE_ORDER_API::sipDueReport($userid);
}


echo json_encode($arr);
ErrorLog::log("RESPONSE", $arr);
