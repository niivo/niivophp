<?php

header('Content-Type: application/json; Charset=UTF-8');
require_once('../includes/app_top.php');
require_once('../includes/mysql.class.php');
require_once('../includes/global.inc.php');
require_once('../includes/functions_general.php');
require_once('../includes/image_lib/image.class.php');
require_once ('../includes/signature.class.php');
require_once ('../includes/mailer.class.php');
require_once ('../includes/validation_msg.php');
require_once '../includes/phpmailer/class.phpmailer.php';
require_once '../includes/classes/BSE_API.class.php';

require_once '../includes/config.php';

$SIGNATURE = new SIGNATURE;
//User::isActive();
//services/ws-user.php?type=SENDOTP&data=[{"mobile":"8107725258","password":"123456"}]
//services/ws-user.php?type=OTPVERIFY&data=[{"mobile":"8107725258","otp":"6908","device_token":"465890606"}]
//services/ws-user.php?type=RESENDOTP&data=[{"mobile":"8107725258"}]
//services/ws-user.php?type=LOGIN&data=[{"mobile":"8107725258","password":"123456","device_token":"465890606"}]
//services/ws-user.php?type=GETPROFILE&data=[{"userid":"1"}]
//services/ws-user.php?type=LOGOUT&data=[{"userid":"1","device_token":"43567890"}]
//services/ws-user.php?type=CHANGEPASS&data=[{"oldpass":"123456","newpass":"123456789","userid":"1"}]
//services/ws-user.php?type=FORGOTPASS&data=[{"mobile":"8107725258"}]
//services/ws-user.php?type=PASSCHANGE&data=[{"mobile":"8107725258","newpass":"123456789","otp":"1234"}]
//services/ws-user.php?type=UPDATE-PROFILE&data
//services/ws-user.php?type=WTHDRAWOTP&data=[{"userid":"1"}]
//services/ws-user.php?type=VERIFY-IFSC&data=[{"ifsc_code":"UTIB0000433"}]

ErrorLog::log("REQUEST URL", $_SERVER['REQUEST_URI']);
ErrorLog::log("REQUEST", $_REQUEST);
$arr = array("msg" => "no api found", "status" => "false");


if (strtoupper($_REQUEST['type']) == "SENDOTP") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $arr = User::sendOtp($data[0]);
}


if (strtoupper($_REQUEST['type']) == "OTPVERIFY") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $arr = User::otpVerify($data[0]);
}


if (strtoupper($_REQUEST['type']) == "RESENDOTP") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $arr = User::reSendOtp($data[0]);
}

if (strtoupper($_REQUEST['type']) == "LOGIN") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $arr = User::login($data[0]);
}

if (strtoupper($_REQUEST['type']) == "GETPROFILE") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $userid = $data[0]->userid;
    $arr = User::getUserinfo($userid);
}


if (strtoupper($_REQUEST['type']) == "LOGOUT") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $arr = User::logout($data[0]);
}


if (strtoupper($_REQUEST['type']) == "CHANGEPASS") {

    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $oldpass = $data[0]->oldpass;
    $pass = $data[0]->newpass;
    $userid = $data[0]->userid;
    $arr = User::changePassword($userid, $oldpass, $pass);
}



if (strtoupper($_REQUEST['type']) == "FORGOTPASS") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $arr = User::forgotPassword($data[0]);
}


if (strtoupper($_REQUEST['type']) == "PASSCHANGE") {

    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));

    $pass = $data[0]->newpass;
    $mobile = $data[0]->mobile;
    $otp = $data[0]->otp;
    $arr = User::passwordChange($mobile, $pass, $otp);
}

if (strtoupper($_REQUEST['type']) == "UPDATE-PROFILE") {
    $data = $_POST;
    $arr = User::updateProfile($data);
}

if (strtoupper($_REQUEST['type']) == "WTHDRAWOTP") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $userid = $data[0]->userid;
    $arr = User::withdrawOtp($userid);
}

if (strtoupper($_REQUEST['type']) == "JSON-UPLOAD") {
    $data=  $_POST;
    $arr = User::jsonUpload($data);
}
if (strtoupper($_REQUEST['type']) == "VERIFY-IFSC") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $ifscCode = $data[0]->ifsc_code;
    $arr = User::verifyIFSC($ifscCode);
}

echo json_encode($arr);
ErrorLog::log("RESPONSE", $arr);
