<?php

header('Content-Type: application/json; Charset=UTF-8');
require_once('../includes/app_top.php');
require_once('../includes/mysql.class.php');
require_once('../includes/global.inc.php');
require_once('../includes/functions_general.php');
require_once('../includes/image_lib/image.class.php');

require_once('../includes/user.class.php');

require_once ('../includes/signature.class.php');
require_once ('../includes/mailer.class.php');
require_once ('../includes/validation_msg.php');
require_once '../includes/phpmailer/class.phpmailer.php';
$SIGNATURE = new SIGNATURE;
$USER = new User();
//User::isActive();


//services/ws-intermediate.php?type=EQUITYFUNDS&data=[{"sub_section":"Large","type":"RD"}]
//services/ws-intermediate.php?type=DEBTFUNDS&data=[{"type":"RD"}]
//services/ws-intermediate.php?type=BALANCEDFUNDS&data=[{"sub_section":"Equity","type":"RD"}]
//services/ws-intermediate.php?type=TAXSAVINGFUNDS&data=[{"type":"RD"}]



ErrorLog::log("REQUEST URL", $_SERVER['REQUEST_URI']);
ErrorLog::log("REQUEST", $_REQUEST);
$arr = array("msg" => "no api found", "status" => "false");



if (strtoupper($_REQUEST['type']) == "EQUITYFUNDS") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $sub_section = $data[0]->sub_section;
    $type = $data[0]->type; // RG for growth plans, RD for Divind plan
    $arr = Intermediate::equityFunds($sub_section,$type);
}

if (strtoupper($_REQUEST['type']) == "DEBTFUNDS") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $type = $data[0]->type; // RG for growth plans, RD for Divind plan
    $arr = Intermediate::debtFunds($type);
}


if (strtoupper($_REQUEST['type']) == "BALANCEDFUNDS") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $sub_section = $data[0]->sub_section;
    $type = $data[0]->type; // RG for growth plans, RD for Divind plan
    $arr = Intermediate::balancedFunds($sub_section,$type);
}

if (strtoupper($_REQUEST['type']) == "TAXSAVINGFUNDS") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $type = $data[0]->type; // RG for growth plans, RD for Divind plan
    $arr = Intermediate::taxSavingFunds($type);
}

echo json_encode($arr);
ErrorLog::log("RESPONSE", $arr);
