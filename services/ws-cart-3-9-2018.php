<?php

header('Content-Type: application/json; Charset=UTF-8');
require_once('../includes/app_top.php');
require_once('../includes/mysql.class.php');
require_once('../includes/global.inc.php');
require_once('../includes/functions_general.php');
require_once('../includes/image_lib/image.class.php');
require_once ('../includes/validation_msg.php');
require_once('../includes/user.class.php');

$USER = new User();

//services/ws-cart.php?type=ADDCART&data=
//services/ws-cart.php?type=REMOVECART&data=[{"userid":"1","cart_id":"2"}]
//services/ws-cart.php?type=CARTLIST&data=[{"userid":"1"}]
//services/ws-cart.php?type=EDITCARTAMOUNT&data=[{"userid":"1","cart_id":"2","amount":"2000"}]
//services/ws-cart.php?type=CHECKCART&data=[{"userid":"1","scheme_code":"FCPEG-GR"}]


ErrorLog::log("REQUEST URL", $_SERVER['REQUEST_URI']);
ErrorLog::log("REQUEST", $_REQUEST);
$arr = array("msg" => "no api found", "status" => "false");


if (strtoupper($_REQUEST['type']) == "ADDCART") {
    $data = $_POST;
    $arr = Cart::addCart($data);
}


if (strtoupper($_REQUEST['type']) == "REMOVECART") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $userid = $data[0]->userid;
    $cart_id = $data[0]->cart_id;
    $arr = Cart::removeCart($userid, $cart_id);
}

if (strtoupper($_REQUEST['type']) == "CARTLIST") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $userid = $data[0]->userid;
    $arr = Cart::cartList($userid);
}


if (strtoupper($_REQUEST['type']) == "EDITCARTAMOUNT") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $userid = $data[0]->userid;
    $cart_id = $data[0]->cart_id;
    $amount = $data[0]->amount;
    $arr = Cart::editCartAmount($userid, $cart_id, $amount);
}

if (strtoupper($_REQUEST['type']) == "CHECKCART") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $userid = $data[0]->userid;
    $scheme_code = $data[0]->scheme_code;
    $arr = Cart::checkCart($userid, $scheme_code);
}



echo json_encode($arr);
ErrorLog::log("RESPONSE", $arr);
