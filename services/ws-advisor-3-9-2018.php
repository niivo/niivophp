<?php

header('Content-Type: application/json; Charset=UTF-8');
require_once('../includes/app_top.php');
require_once('../includes/mysql.class.php');
require_once('../includes/global.inc.php');
require_once('../includes/functions_general.php');
require_once('../includes/image_lib/image.class.php');

require_once('../includes/user.class.php');

require_once ('../includes/signature.class.php');
require_once ('../includes/mailer.class.php');
require_once ('../includes/validation_msg.php');
require_once '../includes/phpmailer/class.phpmailer.php';



$SIGNATURE = new SIGNATURE;
$USER = new User();

//services/ws-advisor.php?type=SETADVISOR&data=[{"user_id":"10000023","advisor_code":"ABC123"}]
//services/ws-advisor.php?type=GETUSERADVISOR&data=[{"user_id":"10000023"}]
//services/ws-advisor.php?type=REMOVEADVISOR&data=[{"user_id":"10000023"}]


ErrorLog::log("REQUEST URL", $_SERVER['REQUEST_URI']);
ErrorLog::log("REQUEST", $_REQUEST);
$arr = array("msg" => "no api found", "status" => "false");




if (strtoupper($_REQUEST['type']) == "SETADVISOR") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $user_id = $data[0]->user_id;
    $advisor_code = $data[0]->advisor_code;
    $arr = AdvisorCode::setAdvisor($user_id, $advisor_code);
}


if (strtoupper($_REQUEST['type']) == "GETUSERADVISOR") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $user_id = $data[0]->user_id;
    $arr = AdvisorCode::getAdvisor($user_id);
}

if (strtoupper($_REQUEST['type']) == "REMOVEADVISOR") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $user_id = $data[0]->user_id;
    $arr = AdvisorCode::removeAdvisor($user_id);
}


echo json_encode($arr);
ErrorLog::log("RESPONSE", $arr);
