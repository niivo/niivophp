<?php

header('Content-Type: application/json; Charset=UTF-8');
require_once('../includes/app_top.php');
require_once('../includes/mysql.class.php');
require_once('../includes/global.inc.php');
require_once('../includes/functions_general.php');
require_once('../includes/classes/Holding.class.php');
require_once ('../includes/validation_msg.php');
require_once ('../includes/classes/ErrorLog.class.php');
require_once '../includes/classes/Mapping.class.php';
// require_once('../includes/classes/user.class.php');


ErrorLog::log("REQUEST URL", $_SERVER['REQUEST_URI']);
ErrorLog::log("REQUEST", $_REQUEST);
$arr = array("msg" => "no api found", "status" => "false");



if (strtoupper($_REQUEST['type']) == "HOLDINGAMOUNT") {
	   $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
	 $userid = $data[0]->userid;
	// print_r($pan); exit();

    $arr = Holding::HoldingValue($userid);
}
if (strtoupper($_REQUEST['type']) == "RECENTORDERS") {
	   $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
	 $userid = $data[0]->userid;
	// print_r($pan); exit();

    $arr = Holding::RecentOrders($userid);
}
if (strtoupper($_REQUEST['type']) == "INVEST") {
	   $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
	 $userid = $data[0]->userid;
	 
	  $category = $data[0]->category;
	  if(empty($category)){$category = "all";}
	 $arr = Holding::investment($userid,$category);
}
if (strtoupper($_REQUEST['type']) == "ALLINVEST") {
	   $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
	 $userid = $data[0]->userid;

    $arr = Holding::allInvest($userid);
}
if (strtoupper($_REQUEST['type']) == "WITHDRAW") {
	   $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
	 $folio_no = $data[0]->folio_no;
	 	 $scheme_code = $data[0]->BSE_SCHEME_CODE;
	 // print_r($userid);
    $arr = Holding::withdrawal($folio_no,  $scheme_code);
}

if (strtoupper($_REQUEST['type']) == "VIEWTRANX") {
	   $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
	 $folio = $data[0]->folio_no;
	 $scheme_code = $data[0]->scheme_code;

    $arr = Holding::ViewTransaction($folio , $scheme_code);
}

if (strtoupper($_REQUEST['type']) == "ORDERSSCREEN") {
	   $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
	 $folio = $data[0]->userid;

    $arr = Holding::OrdersScreen($folio);
}
if (strtoupper($_REQUEST['type']) == "PROFILE") {
	   $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
	 $userid = $data[0]->userid;

    $arr = Holding::user_details($userid);
}
echo json_encode($arr);

?>