<?php

header('Content-Type: application/json; Charset=UTF-8');
require_once('../includes/app_top.php');
require_once('../includes/mysql.class.php');
require_once('../includes/global.inc.php');
require_once('../includes/functions_general.php');
require_once('../includes/image_lib/image.class.php');
require_once('../includes/user.class.php');
require_once ('../includes/mailer.class.php');
require_once ('../includes/validation_msg.php');
require_once ('../includes/classes/Mandate.class.php');
require_once '../includes/config.php';
require_once ('../includes/classes/User.class.php');
require_once ('../includes/classes/ErrorLog.class.php');
require_once ('../includes/classes/MandateStatus.class.php');

$MANDATE    =   new Mandate;
$MANDATESTATUS    =   new MandateStatus;

//User::isToken();


$USER = new User();
//User::isActive();
//services/ws-mapping.php?type=COUNTRYCODE
//services/ws-mapping.php?type=ENTITYTAXCODE
//services/ws-mapping.php?type=INCOMECODE
//services/ws-mapping.php?type=NATIONALITYCODE
//services/ws-mapping.php?type=OCCUPATIONCODE
//services/ws-mapping.php?type=OTHERPROFILECODE
//services/ws-mapping.php?type=PEPCODE
//services/ws-mapping.php?type=POACODE
//services/ws-mapping.php?type=STATECODE
//services/ws-mapping.php?type=WEALTHSOURCECODE


ErrorLog::log("REQUEST URL", $_SERVER['REQUEST_URI']);
ErrorLog::log("REQUEST", $_REQUEST);
$arr = array("msg" => "no api found", "status" => "false");



if (strtoupper($_REQUEST['type']) == "MENDATE-REGISTER") {
    $data =$_POST;
	
    $arr = $MANDATE->mandateRegister($data);
}
if (strtoupper($_REQUEST['type']) == "MENDATE-AUTHURL") {
    $data =$_POST;
    $arr = $MANDATE->mandateAuthURL($data);
}
if (strtoupper($_REQUEST['type']) == "MANDATE-STATUS") {
    $data =$_POST;
    $arr = $MANDATESTATUS->mandateDetail($data);
}

if (strtoupper($_REQUEST['type']) == "MANDATE-SUCCESS") {
    $data =$_POST;
    $arr = $MANDATE->mandateStatus($data);
}


echo json_encode($arr);
ErrorLog::log("RESPONSE", $arr);
