<?php

header('Content-Type: application/json; Charset=UTF-8');
require_once('../includes/app_top.php');
require_once('../includes/mysql.class.php');
require_once('../includes/global.inc.php');
require_once('../includes/functions_general.php');
require_once('../includes/image_lib/image.class.php');

require_once('../includes/user.class.php');

require_once ('../includes/signature.class.php');
require_once ('../includes/mailer.class.php');
require_once ('../includes/validation_msg.php');
require_once '../includes/phpmailer/class.phpmailer.php';
require_once '../includes/classes/BSE_ORDER_API.class.php';

User::isToken();


$SIGNATURE = new SIGNATURE;
$USER = new User();

$BSI    =   new BSE_ORDER_API();
$resp= $BSI->orderEntry();
echo json_encode($resp);


exit;
        
User::isActive();

//services/ws-list.php?type=STATELIST
//services/ws-list.php?type=CITYLIST&data=[{"StateID":"1966"}]


function getLumpSumAmount($requiredAmount, $timeInYrs, $annualGrowthRate){
    $lumpSumAmount = $requiredAmount * pow((1 + $annualGrowthRate/100),(-1 * $timeInYrs));
    return $lumpSumAmount;
}

function getSipAmount($requiredAmount, $timeInYrs, $annualGrowthRate){
    $divisionFactor= 0;
    $timeInMths = $timeInYrs * 12;
    for($i = 1; $i <= $timeInMths; $i++){
        $divisionFactor += pow((1 + $annualGrowthRate/(12*100)), $i);
    }
    $sipAmt = $requiredAmount/$divisionFactor;
    return $sipAmt;
}

echo $sumAmount = getSipAmount(300000, 2, 10);
exit;

ErrorLog::log("REQUEST URL", $_SERVER['REQUEST_URI']);
ErrorLog::log("REQUEST", $_REQUEST);
$arr = array("msg" => "no api found", "status" => "false");



if (strtoupper($_REQUEST['type']) == "STATELIST") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $arr = Listing::stateList();
}

if (strtoupper($_REQUEST['type']) == "CITYLIST") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $StateID = $data[0]->StateID;
    $arr = Listing::cityList($StateID);
}





echo json_encode($arr);
ErrorLog::log("RESPONSE", $arr);
