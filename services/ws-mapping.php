<?php

header('Content-Type: application/json; Charset=UTF-8');
require_once('../includes/app_top.php');
require_once('../includes/mysql.class.php');
require_once('../includes/global.inc.php');
require_once('../includes/functions_general.php');
require_once('../includes/image_lib/image.class.php');
require_once('../includes/user.class.php');
require_once ('../includes/mailer.class.php');
require_once ('../includes/validation_msg.php');
require_once '../includes/phpmailer/class.phpmailer.php';
require_once '../includes/classes/User.class.php';
require_once '../includes/classes/ErrorLog.class.php';
//User::isToken();


$USER = new User();
//User::isActive();
//services/ws-mapping.php?type=COUNTRYCODE
//services/ws-mapping.php?type=ENTITYTAXCODE
//services/ws-mapping.php?type=INCOMECODE
//services/ws-mapping.php?type=NATIONALITYCODE
//services/ws-mapping.php?type=OCCUPATIONCODE
//services/ws-mapping.php?type=OTHERPROFILECODE
//services/ws-mapping.php?type=PEPCODE
//services/ws-mapping.php?type=POACODE
//services/ws-mapping.php?type=STATECODE
//services/ws-mapping.php?type=WEALTHSOURCECODE


ErrorLog::log("REQUEST URL", $_SERVER['REQUEST_URI']);
ErrorLog::log("REQUEST", $_REQUEST);
$arr = array("msg" => "no api found", "status" => "false");



if (strtoupper($_REQUEST['type']) == "COUNTRYCODE") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $arr = Mapping::countryCode();
}

if (strtoupper($_REQUEST['type']) == "ENTITYTAXCODE") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $arr = Mapping::entityTaxCode();
}

if (strtoupper($_REQUEST['type']) == "INCOMECODE") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $arr = Mapping::incomeCode();
}


if (strtoupper($_REQUEST['type']) == "NATIONALITYCODE") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $arr = Mapping::nationalityCode();
}

if (strtoupper($_REQUEST['type']) == "OCCUPATIONCODE") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $arr = Mapping::occupationCode();
}


if (strtoupper($_REQUEST['type']) == "OTHERPROFILECODE") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $arr = Mapping::otherProfileCode();
}


if (strtoupper($_REQUEST['type']) == "PEPCODE") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $arr = Mapping::pepCode();
}

if (strtoupper($_REQUEST['type']) == "POACODE") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $arr = Mapping::poaCode();
}

if (strtoupper($_REQUEST['type']) == "STATECODE") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $arr = Mapping::stateCode();
}

if (strtoupper($_REQUEST['type']) == "WEALTHSOURCECODE") {
    $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
    $arr = Mapping::wealthSourceCode();
}

echo json_encode($arr);
ErrorLog::log("RESPONSE", $arr);
