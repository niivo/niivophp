<?php
define("INVALIDCREDENTIAL", "अवैध अधिकारपत्रे");
define("NIIVOCODESENT", "तुमचा निवो सत्यापन संकेतांक पाठविला गेला आहे.");
define("MSGNOTSENT", "संदेश पाठविला नाही, कृपया पुन्हा प्रयत्न करा.");
define("MOBILEEXIST", "हा मोबाइल क्रमांक आधीच नोंदणीकृत केलेला आहे. कृपया दुसऱ्याने प्रयत्न करा.");
define("OTPVERIFIED", "ओटीपी यशस्वीरित्या सत्यापित केला");
define("ENTEROTP", "कृपया योग्य ओटीपी प्रविष्ट करा");
define("MOBILENOTEXIST", "हा मोबाइल क्रमांक अस्तित्वात नाही. कृपया वैध नंबरसह प्रयत्न करा");
define("LOGINSUCCESS", "यशस्वीपणे लॉगिन केले.");
define("MOBILEPASSNOMATCH", "मोबाईल आणि संकेतशब्द जुळत नाहीत");
define("SUCCESS", "यशस्वी");
define("PASSCHANGED", "संकेतशब्द यशस्वीरित्या बदलला");
define("OLDPASSINCORRECT", "जुना संकेतशब्द बरोबर नाही");
define("NOUSER", "वापरकर्ता अस्तित्वात नाही");
define("SOMETHINGWRONG", "अर्र, काहीतरी चुकले. \n कृपया पुन्हा प्रयत्न करा.");
define("INVALIDREQUEST", "अवैध विनंती");
define("SOMEERROR", "काही त्रुटी आहे. कृपया पुन्हा प्रयत्न करा.");
define("INVALIDIFSC", "अवैध आयएफएससी कोड");
define("INVALIDIFSC", "बँक सापडली नाही.  कृपया दुसरा बँक आयएफएससी निवडा.");
define("LANGUAGECHANGED", "भाषा यशस्वीरित्या बदलली");
define("SCHEMENOTEMPTY", "योजना संकेतांक रिक्त असू शकत नाही");
define("NORECORD", "रेकॉर्ड आढळले नाही");
define("ORDERCANCEL", "ऑर्डर यशस्वीरित्या रद्द करा");
define("FAILED", "अयशस्वी");
define("ORDERCONFIRM", "मागणीची पुष्टी करा");
define("RECORDEXIST", "रेकॉर्ड आधीच अस्तित्वात आहे");
define("NOSIPFOUND", "एसआयपीचा कोणताही हप्ता देय आढळला नाही");
define("NOADVISOREXIST", "सल्लागार संकेतांक अस्तित्वात नाही.");
define("SIPDUE", "[फंड_नाव] साठी रु.[रक्कम] चा एसआयपी हप्ता [देय_दिनांक] ला देय आहे");




define("STATUSTRUE", "true");
define("STATUSFALSE", "false");

define("ACCOUNTDEACTIVATED", "Your account is deactived by Charity team for review!");

define("LOGIN", "Successful Login");
define("USERUPDATED", "user updated successfully");
define("USERADDED", "Added successfully");

define("NOPAGE", "Page doesn't exist");
define("ALREADYEXIST", "Already exist");
define("USEREMAILEXIST", "This email address already registered");

define("FBEXIST", "This fb user already registered. Please try another one");
define("SUCCESSFULLYREGISTER", "Congratulations! You have successfully registered.");
define("RESETLINK", "Reset password link has been expired. Please try again.");
define("PASSWORD", "Please enter your password.");
define("CPASSWORD", "Please enter your confirm password.");




define("RESETLINK", "Reset password link has been expired. Please try again.");
//login error msg//
define("LOGINERROR", "Email and password do not match.");

//account validation msg
define("SUSPEND", "Your account is suspended.Please contact to admin.");
define("BLOCK", "Your account is blocked.Please contact to admin.");

define("PASSNOTMATCH", "Password and confirm password does not match.");

define("PASSSEND", "A password reset link was sent to your email address.");
define("PASSCHANGE", "Your password has been changed successfully.");
define("EMAILNOTEXIST", "Your email does not exist.");


