<?php
define("SUCCESS", "Success");
define("STATUSTRUE", "true");
define("STATUSFALSE", "false");
define("NORECORD", "Record not found");
define("ACCOUNTDEACTIVATED", "Your account is deactived by Charity team for review!");
define("INVALIDCREDENTIAL", "Invalid Credentials");
define("LOGIN", "Successful Login");
define("USERUPDATED", "user updated successfully");
define("USERADDED", "Added successfully");
define("NOUSER", "User doesn't exist");
define("NOPAGE", "Page doesn't exist");
define("ALREADYEXIST", "Already exist");
define("USEREMAILEXIST", "This email address already registered");
define("MOBILEEXIST", "This mobile number already registered. Please try another one");
define("FBEXIST", "This fb user already registered. Please try another one");
define("SUCCESSFULLYREGISTER", "Congratulations! You have successfully registered.");
define("RESETLINK", "Reset password link has been expired. Please try again.");
define("PASSWORD", "Please enter your password.");
define("CPASSWORD", "Please enter your confirm password.");
define("MOBILENOTEXIST", "This mobile number is nor exist. Please try with valid number");



// define("RESETLINK", "Reset password link has been expired. Please try again.");
//login error msg//
define("LOGINERROR", "Email and password do not match.");

//account validation msg
define("SUSPEND", "Your account is suspended.Please contact to admin.");
define("BLOCK", "Your account is blocked.Please contact to admin.");

define("PASSNOTMATCH", "Password and confirm password does not match.");

define("PASSSEND", "A password reset link was sent to your email address.");
define("PASSCHANGE", "Your password has been changed successfully.");
define("EMAILNOTEXIST", "Your email does not exist.");



define("NOCATEGORY", "Category does not exist.");
define("NOSUBCATEGORY", "Sub Category does not exist.");