<?php
define("INVALIDCREDENTIAL", "अमान्य क्रेडेन्शियल्स");
define("NIIVOCODESENT", "आपका निवो सत्यापन कोड भेजा जा चुका है।");
define("MSGNOTSENT", "संदेश नहीं भेजा गया, कृपया पुनः प्रयास करें।");
define("MOBILEEXIST", "यह मोबाइल नंबर पहले से पंजीकृत है। कृपया दूसरे से प्रयास करें");
define("OTPVERIFIED", "ओटीपी सत्यापन सफल।");
define("ENTEROTP", "कृपया सही ओटीपी डालें");
define("MOBILENOTEXIST", "यह मोबाइल नंबर मौजूद नहीं है। कृपया मान्य नंबर से प्रयास करें");
define("LOGINSUCCESS", "लॉगिन सफल।");
define("MOBILEPASSNOMATCH", "मोबाइल व पासवर्ड मैच नहीं करते");
define("SUCCESS", "सफल");
define("PASSCHANGED", "पासवर्ड सफलतापूर्वक बदला गया");
define("OLDPASSINCORRECT", "पुराना पासवर्ड सही नहीं है");
define("NOUSER", "उपयोगकर्ता मौजूद नहीं है");
define("SOMETHINGWRONG", "उफ, कुछ गलत हुआ।\nकृपया पुनः प्रयास करें।");
define("INVALIDREQUEST", "अमान्य अनुरोध");
define("SOMEERROR", "कोई त्रुटि हुई। कृपया पुनः प्रयास करें।");
define("INVALIDIFSC", "अमान्य आईएफएससी कोड");
define("INVALIDIFSC", "बैंक नहीं मिला। कृपया कोई अन्य बैंक आईएफएससी  चुनें।");
define("LANGUAGECHANGED", "भाषा सफलतापूर्वक बदली गयी");
define("SCHEMENOTEMPTY", "स्कीम कोड खाली नहीं रह सकता");
define("NORECORD", "रिकॉर्ड नहीं मिला");
define("ORDERCANCEL", "ऑर्डर सफलतापूर्वक रद्द करें");
define("FAILED", "विफल");
define("ORDERCONFIRM", "ऑर्डर की पुष्टि करें");
define("RECORDEXIST", "रिकॉर्ड पहले से मौजूद");
define("NOSIPFOUND", "कोई देय एसआईपी किश्त नहीं मिली");
define("NOADVISOREXIST", "सलाहकार कोड मौजूद नहीं है।");
define("SIPDUE", "[फ़ंड_नाम] के लिए रू.[राशि] की एसआईपी किश्त [भुगतान_तारीख] तक देय है");




define("STATUSTRUE", "true");
define("STATUSFALSE", "false");

define("ACCOUNTDEACTIVATED", "Your account is deactived by Charity team for review!");

define("LOGIN", "Successful Login");
define("USERUPDATED", "user updated successfully");
define("USERADDED", "Added successfully");

define("NOPAGE", "Page doesn't exist");
define("ALREADYEXIST", "Already exist");
define("USEREMAILEXIST", "This email address already registered");

define("FBEXIST", "This fb user already registered. Please try another one");
define("SUCCESSFULLYREGISTER", "Congratulations! You have successfully registered.");
define("RESETLINK", "Reset password link has been expired. Please try again.");
define("PASSWORD", "Please enter your password.");
define("CPASSWORD", "Please enter your confirm password.");




define("RESETLINK", "Reset password link has been expired. Please try again.");
//login error msg//
define("LOGINERROR", "Email and password do not match.");

//account validation msg
define("SUSPEND", "Your account is suspended.Please contact to admin.");
define("BLOCK", "Your account is blocked.Please contact to admin.");

define("PASSNOTMATCH", "Password and confirm password does not match.");

define("PASSSEND", "A password reset link was sent to your email address.");
define("PASSCHANGE", "Your password has been changed successfully.");
define("EMAILNOTEXIST", "Your email does not exist.");


