<?php

/**
* MySQLi Database Connection Class
* @access public
*/
class MySQLiDB {
    protected static $host;
    protected static $dbUser;
    protected static $dbPass;
    protected static $dbName;
    var $dbConn;
	protected static $charset;
    protected static $connectError;
    protected static $last_query;
    protected  $queryCount=0;
	protected $arrQueries	=	array();
   
                            
    function __construct ($host,$dbUser,$dbPass,$dbName,$charset="utf8") {
        self::$host		=	$host;
        self::$dbUser	=	$dbUser;
        self::$dbPass	=	$dbPass;
        self::$dbName	=	$dbName;
		self::$charset	=	$charset;
        $this->connectToDb();
    }
    function timer()
    {
        $time = explode(' ', microtime());
        return $time[0]+$time[1];
    }
    function __destruct() {
        if ($this->dbConn){
            $this->dbConn->close();
            self::$last_query="";
        }
    }
    
    private function connectToDb () {
        if (!isset($this->dbConn)){
            $this->dbConn   =   new mysqli(self::$host,
                                          self::$dbUser,
                                          self::$dbPass,self::$dbName);
			//$this->dbConn->set_charset(self::$charset);						  
            if ($this->dbConn->connect_errno) {
                die("Failed to connect to MySQL: (" . $this->dbConn->connect_errno . ") " . $this->dbConn->connect_error);
                $this->connectError=true;
            }
        }
    }
    
     function effectedRows(){
        return mysqli_affected_rows($this->dbConn);
    }
    
    function query($sql,$multi=false) {
       
        if ($multi)
            return $this->multiQuery ($sql);
        else{
            if ($this->dbConn){
                $beginning = $this->timer();    
                if (!$queryResource=mysqli_query($this->dbConn,$sql))
                {
					// echo $sql; exit();
                    throw new Exception("Query failed : ".$this->dbConn->error);
                }
                $timer_queries = round($this->timer()-$beginning,6);
                $this->queryCount++;
                array_push($this->arrQueries,array("sql"=>$sql,"time"=>$timer_queries));
                return new MySQLiDBResult($this,$queryResource,$sql);
            }
        }
    }
    
    function multiQuery($sql) {
        if ($this->dbConn){
            
            if (!$queryResource=mysqli_multi_query($this->dbConn,$sql))
            {
                throw new Exception("Query failed : ".$this->dbConn->error);
            }
			$this->queryCount++;
           	array_push($this->arrQueries,$sql);
            return new MySQLiDBResult($this,$queryResource,$sql);
        }
    }
    
	function num_queries(){
		return $this->queryCount;
	}
	
	function lastQueries(){
		return $this->arrQueries;
	}
	
    function escapeString($string){
                
        return mysqli_real_escape_string($this->dbConn,$string);
    }
	
	
    
} // end class

class MySQLiDBResult {
    var $mysql;
    var $query;
    var $rawQuery;

    function __construct(&$mysql,$queryRes,$sql) {
        //$this->mysql=& $mysql;
        //$this->query=$query;
        $this->query=$queryRes;
        $this->mysql=&$mysql;
        $this->rawQuery=$sql;
       
    }
    
    function size(){
        return $this->query->num_rows;
    }
    
    function fetchAll () {
        $result =   $this->query;
        if ($row=$result->fetch_all(MYSQLI_ASSOC)) {
            return $row;
        } else if ( $this->size() > 0 ) {
            $result->data_seek(0)   ;
            $result->free();
            return false;
        } else {
            return false;
        }
    }
    function fetch () {
        $result =   $this->query;
        if ($row=$result->fetch_array(MYSQLI_ASSOC)) {
            return $row;
        } else if ( $this->size() > 0 ) {
            mysqli_data_seek($result,0)   ;
           
            return false;
        } else {
           
            return false;
        }
    }
	
    function freeResult(){
        $result =   $this->query;
        mysqli_free_result($result);
    }
    
	function fetchRow() {
        $result =   $this->query;
        if ($row=$result->fetch_row()) {
            return $row;
        } else if ( $this->size() > 0 ) {
            mysqli_data_seek($result,0)   ;
           
            return false;
        } else {
           
            return false;
        }
    }
	
	function insertID(){
		return $this->mysql->dbConn->insert_id;
	}
	
    
    function lastSql($exit=false){
        if ($exit)
            die($this->rawQuery);
        else
            echo $this->rawQuery;
    }
    
   
    
}
?>
