<?php

class MailerClass {

    function company_sign_up($companyid) {

        global $db;
        global $ADMIN_MAIL;
        $post = array();
        $body = '';
        /* 	collect mail body	 */
        $sqlBody = " SELECT mail_body as bodys ,mail_keys as _keys,subject " .
                " FROM mail_body " .
                " where id=2 ";

        $rsObjBody = $db->query($sqlBody);
        if ($rsObjBody->size() > 0) {
            $rowBody = $rsObjBody->fetch();

            /* 	collect varables	 */
            $post['site_name'] = site_name;
            $post['bodys'] = $rowBody["bodys"];

            $sql = " SELECT *" .
                    " FROM company where id 	= " . $companyid;
            $rsObjRows = $db->query($sql);
            $rows = $rsObjRows->fetch();


            $post['u_email'] = $rows["email"];
            $post['u_fname'] = ucfirst($rows["username"]);
            //$post['reg_date'] = date('m-d-Y g:i A');


            /* 	collect key and vars	 */

            $sqlKey = " SELECT * FROM mail_keyword " .
                    " WHERE id IN (" . $rowBody["_keys"] . ")";
            $queryKey = $db->query($sqlKey);

            while ($key = $queryKey->fetch()) {
                $post['bodys'] = str_replace("[" . $key['key'] . "]", $key['vars'], $post['bodys']);
                $post['bodys'] = str_replace($key['vars'], $post[$key['vars']], $post['bodys']);
            }



            $this->send_mail($post['u_email'], $rowBody['subject'], $post['bodys'], "", "2");
            return true;
        }

        return false;
    }

    function user_sign_up($useriid) {

        global $db;
        global $ADMIN_MAIL;
        $post = array();
        $body = '';
        /* 	collect mail body	 */
        $sqlBody = " SELECT mail_body as bodys ,mail_keys as _keys,subject " .
                " FROM mail_body " .
                " where id=2 ";

        $rsObjBody = $db->query($sqlBody);
        if ($rsObjBody->size() > 0) {
            $rowBody = $rsObjBody->fetch();

            /* 	collect varables	 */
            $post['site_name'] = site_name;
            $post['bodys'] = $rowBody["bodys"];

            $sql = " SELECT *" .
                    " FROM users where userid 	= " . $useriid;
            $rsObjRows = $db->query($sql);
            $rows = $rsObjRows->fetch();


            $post['u_email'] = $rows["email"];
            $post['u_fname'] = ucfirst($rows["username"]);
            //$post['reg_date'] = date('m-d-Y g:i A');


            /* 	collect key and vars	 */

            $sqlKey = " SELECT * FROM mail_keyword " .
                    " WHERE id IN (" . $rowBody["_keys"] . ")";
            $queryKey = $db->query($sqlKey);

            while ($key = $queryKey->fetch()) {
                $post['bodys'] = str_replace("[" . $key['key'] . "]", $key['vars'], $post['bodys']);
                $post['bodys'] = str_replace($key['vars'], $post[$key['vars']], $post['bodys']);
            }



            $this->send_mail($post['u_email'], $rowBody['subject'], $post['bodys'], "", "2");


            return true;
        }

        return false;
    }

    function trip_assign_user($useriid) {

        global $db;
        global $ADMIN_MAIL;
        $post = array();
        $body = '';
        /* 	collect mail body	 */
        $sqlBody = " SELECT mail_body as bodys ,mail_keys as _keys,subject " .
                " FROM mail_body " .
                " where id=2 ";

        $rsObjBody = $db->query($sqlBody);
        if ($rsObjBody->size() > 0) {
            $rowBody = $rsObjBody->fetch();

            /* 	collect varables	 */
            $post['site_name'] = site_name;
            $post['bodys'] = $rowBody["bodys"];

            $sql = " SELECT *" .
                    " FROM users where userid 	= " . $useriid;
            $rsObjRows = $db->query($sql);
            $rows = $rsObjRows->fetch();


            $post['u_email'] = $rows["email"];
            $post['u_fname'] = ucfirst($rows["username"]);
            //$post['reg_date'] = date('m-d-Y g:i A');


            /* 	collect key and vars	 */

            $sqlKey = " SELECT * FROM mail_keyword " .
                    " WHERE id IN (" . $rowBody["_keys"] . ")";
            $queryKey = $db->query($sqlKey);

            while ($key = $queryKey->fetch()) {
                $post['bodys'] = str_replace("[" . $key['key'] . "]", $key['vars'], $post['bodys']);
                $post['bodys'] = str_replace($key['vars'], $post[$key['vars']], $post['bodys']);
            }



            $this->send_mail($post['u_email'], $rowBody['subject'], $post['bodys'], "", "2");


            return true;
        }

        return false;
    }

    function user_email_confirm($userid, $url = '') {
        global $db;
        global $ADMIN_MAIL;
        $post = array();

        /* 	collect mail body	 */
        $sqlBody = " select mail_body as body ,mail_keys as _keys,subject " .
                " FROM mail_body " .
                " where id=1 ";

        $rsObjBody = $db->query($sqlBody);
        if ($rsObjBody->size() > 0) {
            $rowBody = $rsObjBody->fetch();

            /* 	collect varables	 */
            $post['site_name'] = site_name;
            $post['body'] = $rowBody["body"];

            $post['confirm_email_link'] = '<a href="' . $url . '" style="background: rgb(0, 172, 223);padding: 10px;text-align: center;text-transform: uppercase;text-decoration:none;font-size: 16px;color: rgb(255, 255, 255);width: 250px;"> Confirm E-mail</a>'; //$url;

            $sql = "select firstname, email FROM users where id 	= " . $userid;

            $rsObjRows = $db->query($sql);
            $rows = $rsObjRows->fetch();
            $post['u_email'] = $rows["email"];
            $post['u_fname'] = $rows["firstname"];

            /* 	collect key and vars	 */

            $sqlKey = " SELECT * FROM mail_keyword " .
                    " WHERE id IN (" . $rowBody["_keys"] . ")";
            $queryKey = $db->query($sqlKey);

            while ($key = $queryKey->fetch()) {
                $post['body'] = str_replace("[" . $key['key'] . "]", $key['vars'], $post['body']);
                $post['body'] = str_replace($key['vars'], $post[$key['vars']], $post['body']);
            }
            $getMail = $this->send_mail($post['u_email'], $rowBody['subject'], $post['body'], "", "1");
            if ($getMail) {
                print_r($post['body']);
                die;
            } else {
                echo "dasda";
                die;
            }

            return true;
        }
        return false;
    }

    function admin_pw_reset_link($userid, $url) {
        global $db;
        global $ADMIN_MAIL;

        $post = array();

        /* 	collect mail body	 */
        $sqlBody = " select mail_body as body ,mail_keys as _keys,subject " .
                " FROM mail_body " .
                " where id=3 ";

        $rsObjBody = $db->query($sqlBody);

        if ($rsObjBody->size() > 0) {
            $rowBody = $rsObjBody->fetch();
            $userid = intval($userid);
            /* 	collect varables	 */
            $post['site_name'] = site_name;
            $post['body'] = $rowBody["body"];

            $post['pw_reset_url'] = '<a href="' . $url . '">Reset</a>'; //$url;

            $sql = "select name , email  " .
                    " FROM adminlogin where userno 	= '$userid'";



            $rsObjRows = $db->query($sql);
            $rows = $rsObjRows->fetch();

            $post['fname'] = strtoupper($rows["name"]);
            $post['_email'] = $rows["email"];
            $post['username'] = $rows["name"];

            /* 	collect key and vars	 */

            $sqlKey = " SELECT * FROM mail_keyword " .
                    " WHERE id IN (" . $rowBody["_keys"] . ")";
            $queryKey = $db->query($sqlKey);

            while ($key = $queryKey->fetch()) {
                //echo $key['key']."  k  - ".$key['vars']."  v  ".$post[$key['vars']]."  val<br>";


                $post['body'] = str_replace("[" . $key['key'] . "]", $key['vars'], $post['body']);
                $post['body'] = str_replace($key['vars'], $post[$key['vars']], $post['body']);
            }

            //echo $post['body'];

            $this->send_mail($post['_email'], $rowBody['subject'], $post['body']);

            return true;
        }

        return false;
    }

    function restro_pw_reset_link($userid, $url) {
        global $db;
        global $ADMIN_MAIL;

        $post = array();

        /* 	collect mail body	 */
        $sqlBody = " select mail_body as body ,mail_keys as _keys,subject " .
                " FROM mail_body " .
                " where id=5 ";

        $rsObjBody = $db->query($sqlBody);

        if ($rsObjBody->size() > 0) {
            $rowBody = $rsObjBody->fetch();
            $userid = intval($userid);
            /* 	collect varables	 */
            $post['site_name'] = site_name;
            $post['body'] = $rowBody["body"];

            $post['pw_reset_url'] = '<a href="' . $url . '">Reset</a>'; //$url;

            $sql = "select username , contact_email  " .
                    " FROM restaurant where id 	= '$userid'";



            $rsObjRows = $db->query($sql);
            $rows = $rsObjRows->fetch();

            $post['u_fname'] = strtoupper($rows["username"]);
            $post['_email'] = $rows["contact_email"];


            /* 	collect key and vars	 */

            $sqlKey = " SELECT * FROM mail_keyword " .
                    " WHERE id IN (" . $rowBody["_keys"] . ")";
            $queryKey = $db->query($sqlKey);

            while ($key = $queryKey->fetch()) {
                //echo $key['key']."  k  - ".$key['vars']."  v  ".$post[$key['vars']]."  val<br>";


                $post['body'] = str_replace("[" . $key['key'] . "]", $key['vars'], $post['body']);
                $post['body'] = str_replace($key['vars'], $post[$key['vars']], $post['body']);
            }



            $this->send_mail($post['_email'], $rowBody['subject'], $post['body']);

            return true;
        }

        return false;
    }

    function admin_pw_reset_confirm($userid) {
        global $db;
        global $ADMIN_MAIL;

        $post = array();

        /* 	collect mail body	 */
        $sqlBody = " select mail_body as body ,mail_keys as _keys,subject " .
                " FROM mail_body " .
                " where id=4 ";

        $rsObjBody = $db->query($sqlBody);
        if ($rsObjBody->size() > 0) {
            $rowBody = $rsObjBody->fetch();
            /* 	collect varables	 */
            $post['site_name'] = site_name;
            $post['body'] = $rowBody["body"];

            $sql = "select firstname , lastname , email " .
                    " FROM users where memberid 	= $userid";

            $rsObjRows = $db->query($sql);
            $rows = $rsObjRows->fetch();

            $post['fname'] = strtoupper($rows["firstname"] . " " . $rows["lastname"]);
            $post['_email'] = $rows["email"];
            $post['username'] = $rows["username"];

            /* 	collect key and vars	 */

            $sqlKey = " SELECT * FROM mail_keyword " .
                    " WHERE id IN (" . $rowBody["_keys"] . ")";
            $queryKey = $db->query($sqlKey);

            while ($key = $queryKey->fetch()) {
                //echo $key['key']."  k  - ".$key['vars']."  v  ".$post[$key['vars']]."  val<br>";


                $post['body'] = str_replace("[" . $key['key'] . "]", $key['vars'], $post['body']);
                $post['body'] = str_replace($key['vars'], $post[$key['vars']], $post['body']);
            }


            $this->send_mail($post['u_email'], $rowBody['subject'], $post['body'], "", "1");

            return true;
        }

        return false;
    }

    function signgupverificationmail($uid, $url) {
        global $db;
        global $ADMIN_MAIL;
        $post = array();

        /* 	collect mail body	 */
        $sqlBody = " select mail_body as body ,mail_keys as _keys,subject " .
                " FROM mail_body " .
                " where id=1 ";

        $rsObjBody = $db->query($sqlBody);
        if ($rsObjBody->size() > 0) {
            $rowBody = $rsObjBody->fetch();
            /* 	collect varables	 */
            $post['site_name'] = site_name;
            $post['signature'] = site_name;
            $post['body'] = $rowBody["body"];

            $post['confirm_email_link'] = $url;

            $sql = "select first_name , email FROM users where id ='$uid'";
            $rsObjRows = $db->query($sql);
            $rows = $rsObjRows->fetch();
            print_r($rows);
            $post['u_email'] = ($rows["email"]);
            $post['email'] = $rows["email"];

            /* 	collect key and vars	 */

            $sqlKey = " SELECT * FROM mail_keyword " .
                    " WHERE id IN (" . $rowBody["_keys"] . ")";
            $queryKey = $db->query($sqlKey);

            while ($key = $queryKey->fetch()) {
                //echo $key['key']."  k  - ".$key['vars']."  v  ".$post[$key['vars']]."  val<br>";


                $post['body'] = str_replace("[" . $key['key'] . "]", $key['vars'], $post['body']);
                $post['body'] = str_replace($key['vars'], $post[$key['vars']], $post['body']);
            }

            $this->send_mail($post['u_email'], $rowBody['subject'], $post['body'], "", "1");
            return true;
        }

        return false;
    }

    function pw_reset_link_user($userid, $url) {
        global $db;
        global $ADMIN_MAIL;

        $post = array();

        /* 	collect mail body	 */
        $sqlBody = " select mail_body as body ,mail_keys as _keys,subject " .
                " FROM mail_body " .
                " where id=3 ";

        $rsObjBody = $db->query($sqlBody);
        if ($rsObjBody->size() > 0) {
            $rowBody = $rsObjBody->fetch();
            /* 	collect varables	 */
            $post['site_name'] = site_name;
            $post['signature'] = site_name;

            $post['body'] = $rowBody["body"];

            $post['pw_reset_url'] = $url;

            $sql = "select * FROM users where userid ='$userid'";
            $rsObjRows = $db->query($sql);
            $rows = $rsObjRows->fetch();

            $post['u_fname'] = ($rows["name"]);
            $post['email'] = $rows["email"];

            /* 	collect key and vars	 */

            $sqlKey = " SELECT * FROM mail_keyword " .
                    " WHERE id IN (" . $rowBody["_keys"] . ")";
            $queryKey = $db->query($sqlKey);

            while ($key = $queryKey->fetch()) {
                //echo $key['key']."  k  - ".$key['vars']."  v  ".$post[$key['vars']]."  val<br>";


                $post['body'] = str_replace("[" . $key['key'] . "]", $key['vars'], $post['body']);
                $post['body'] = str_replace($key['vars'], $post[$key['vars']], $post['body']);
            }


            $this->send_mail($post['email'], $rowBody['subject'], $post['body'], "", "1");
            return true;
        }

        return false;
    }

    function status_notification($userid, $email_msg) {
        global $db;
        global $ADMIN_MAIL;

        $post = array();

        /* 	collect mail body	 */
        $sqlBody = " select mail_body as body ,mail_keys as _keys,subject " .
                " FROM mail_body " .
                " where id=9 ";

        $rsObjBody = $db->query($sqlBody);
        if ($rsObjBody->size() > 0) {
            $rowBody = $rsObjBody->fetch();
            /* 	collect varables	 */
            $post['site_name'] = site_name;
            $post['signature'] = site_name;

            $post['body'] = $rowBody["body"];

            $post['live-status'] = $email_msg;

            $sql = "select first_name , email FROM users where id ='$userid'";
            $rsObjRows = $db->query($sql);
            $rows = $rsObjRows->fetch();

            $post['u_fname'] = strtoupper($rows["first_name"]);
            $post['u_email'] = $rows["email"];

            /* 	collect key and vars	 */

            $sqlKey = " SELECT * FROM mail_keyword " .
                    " WHERE id IN (" . $rowBody["_keys"] . ")";
            $queryKey = $db->query($sqlKey);

            while ($key = $queryKey->fetch()) {
                //echo $key['key']."  k  - ".$key['vars']."  v  ".$post[$key['vars']]."  val<br>";


                $post['body'] = str_replace("[" . $key['key'] . "]", $key['vars'], $post['body']);
                $post['body'] = str_replace($key['vars'], $post[$key['vars']], $post['body']);
            }

            $this->send_mail($post['u_email'], $rowBody['subject'], $post['body'], "", "1");
            return true;
        }
        return false;
    }

    function reservation_sent($external_reference, $url) {
        global $db;
        global $ADMIN_MAIL;


        require_once ("user.class.php");
        $USER = new UserClass;
        $post = array();

        /* 	collect mail body	 */
        $sqlBody = " select mail_body as body ,mail_keys as _keys,subject " .
                " FROM mail_body " .
                " where id=10 ";

        $rsObjBody = $db->query($sqlBody);
        if ($rsObjBody->size() > 0) {
            $rowBody = $rsObjBody->fetch();
            /* 	collect varables	 */
            $post['site_name'] = site_name;
            $post['signature'] = site_name;
            $post['pw_reset_url'] = $url;
            $post['body'] = $rowBody["body"];
            $sql = "select * FROM reservation where external_reference ='$external_reference'";

            $rsObjRows = $db->query($sql);
            $rows = $rsObjRows->fetch();

            $sitter_id = $rows['user_to'];
            $userId = $rows['user_by'];

            $sitterdata = $USER->userInfo($sitter_id);

            $userdata = $USER->userInfo($userId);

            $start_date = date("l jS F Y", $rows['checkin']);
            $end_date = date("l jS F Y", $rows['checkout']);

            $post['u_fname'] = ($sitterdata["name"]);
            $post['u_email'] = ($sitterdata["email"]);
            $post['host-name'] = ($userdata["name"]);
            $post['host-email'] = ($userdata["email"]);
            $post['start-date'] = ($start_date);
            $post['end-date'] = ($end_date);


            /* 	collect key and vars	 */

            $sqlKey = " SELECT * FROM mail_keyword " .
                    " WHERE id IN (" . $rowBody["_keys"] . ")";
            $queryKey = $db->query($sqlKey);

            while ($key = $queryKey->fetch()) {
                //echo $key['key']."  k  - ".$key['vars']."  v  ".$post[$key['vars']]."  val<br>";


                $post['body'] = str_replace("[" . $key['key'] . "]", $key['vars'], $post['body']);
                $post['body'] = str_replace($key['vars'], $post[$key['vars']], $post['body']);
            }

            $this->send_mail($post['u_email'], $rowBody['subject'], $post['body'], "", "2");
            return true;
        }
        return false;
    }

    function reservation_accept($reservation_id) {
        global $db;
        global $ADMIN_MAIL;


        require_once ("user.class.php");
        $USER = new UserClass;
        $post = array();

        /* 	collect mail body	 */
        $sqlBody = " select mail_body as body ,mail_keys as _keys,subject " .
                " FROM mail_body " .
                " where id=11";

        $rsObjBody = $db->query($sqlBody);
        if ($rsObjBody->size() > 0) {
            $rowBody = $rsObjBody->fetch();
            /* 	collect varables	 */
            $post['site_name'] = site_name;
            $post['signature'] = site_name;

            $post['body'] = $rowBody["body"];
            $sql = "select * FROM reservation where id ='$reservation_id'";

            $rsObjRows = $db->query($sql);
            $rows = $rsObjRows->fetch();

            $sitter_id = $rows['user_to'];
            $userId = $rows['user_by'];

            $sitterdata = $USER->userInfo($sitter_id);

            $userdata = $USER->userInfo($userId);

            $start_date = date("l jS F Y", $rows['checkin']);
            $end_date = date("l jS F Y", $rows['checkout']);

            $post['investor-name'] = ($sitterdata["name"]);
            $post['investor-email'] = ($sitterdata["email"]);
            $post['host-name'] = ($userdata["name"]);
            $post['host-email'] = ($userdata["email"]);
            $post['start-date'] = ($start_date);
            $post['end-date'] = ($end_date);


            /* 	collect key and vars	 */

            $sqlKey = " SELECT * FROM mail_keyword " .
                    " WHERE id IN (" . $rowBody["_keys"] . ")";
            $queryKey = $db->query($sqlKey);

            while ($key = $queryKey->fetch()) {
                //echo $key['key']."  k  - ".$key['vars']."  v  ".$post[$key['vars']]."  val<br>";


                $post['body'] = str_replace("[" . $key['key'] . "]", $key['vars'], $post['body']);
                $post['body'] = str_replace($key['vars'], $post[$key['vars']], $post['body']);
            }

            $this->send_mail($post['host-email'], $rowBody['subject'], $post['body'], "", "2");
            return true;
        }
        return false;
    }

    function reservation_decline($reservation_id) {
        global $db;
        global $ADMIN_MAIL;


        require_once ("user.class.php");
        $USER = new UserClass;
        $post = array();

        /* 	collect mail body	 */
        $sqlBody = " select mail_body as body ,mail_keys as _keys,subject " .
                " FROM mail_body " .
                " where id=12";

        $rsObjBody = $db->query($sqlBody);
        if ($rsObjBody->size() > 0) {
            $rowBody = $rsObjBody->fetch();
            /* 	collect varables	 */
            $post['site_name'] = site_name;
            $post['signature'] = site_name;

            $post['body'] = $rowBody["body"];
            $sql = "select * FROM reservation where id ='$reservation_id'";

            $rsObjRows = $db->query($sql);
            $rows = $rsObjRows->fetch();

            $sitter_id = $rows['user_to'];
            $userId = $rows['user_by'];

            $sitterdata = $USER->userInfo($sitter_id);

            $userdata = $USER->userInfo($userId);

            $start_date = date("l jS F Y", $rows['checkin']);
            $end_date = date("l jS F Y", $rows['checkout']);

            $post['u_fname'] = ($sitterdata["name"]);

            $post['u_email'] = ($sitterdata["email"]);
            $post['host-name'] = ($userdata["name"]);
            $post['host-email'] = ($userdata["email"]);
            $post['start-date'] = ($start_date);
            $post['end-date'] = ($end_date);


            /* 	collect key and vars	 */

            $sqlKey = " SELECT * FROM mail_keyword " .
                    " WHERE id IN (" . $rowBody["_keys"] . ")";
            $queryKey = $db->query($sqlKey);

            while ($key = $queryKey->fetch()) {
                //echo $key['key']."  k  - ".$key['vars']."  v  ".$post[$key['vars']]."  val<br>";


                $post['body'] = str_replace("[" . $key['key'] . "]", $key['vars'], $post['body']);
                $post['body'] = str_replace($key['vars'], $post[$key['vars']], $post['body']);
            }

            $this->send_mail($post['host-email'], $rowBody['subject'], $post['body'], "", "2");
            return true;
        }
        return false;
    }

    function reservation_expire($reservation_id, $type) {
        global $db;
        global $ADMIN_MAIL;


        require_once ("user.class.php");
        $USER = new UserClass;
        $post = array();

        /* 	collect mail body	 */
        $sqlBody = " select mail_body as body ,mail_keys as _keys,subject " .
                " FROM mail_body " .
                " where id=15";

        $rsObjBody = $db->query($sqlBody);
        if ($rsObjBody->size() > 0) {
            $rowBody = $rsObjBody->fetch();
            /* 	collect varables	 */
            $post['site_name'] = site_name;
            $post['signature'] = site_name;

            $post['body'] = $rowBody["body"];
            $sql = "select * FROM reservation where id ='$reservation_id'";

            $rsObjRows = $db->query($sql);
            $rows = $rsObjRows->fetch();

            $sitter_id = $rows['user_to'];
            $userId = $rows['user_by'];

            $sitterdata = $USER->userInfo($sitter_id);

            $userdata = $USER->userInfo($userId);

            $start_date = date("l jS F Y", $rows['checkin']);
            $end_date = date("l jS F Y", $rows['checkout']);

            if ($type == '1') {
                $post['u_fname'] = ($sitterdata["name"]);
                $post['u_email'] = ($sitterdata["email"]);
                $post['host-name'] = ($userdata["name"]);
                $post['host-email'] = ($userdata["email"]);
            } else if ($type == '2') {
                $post['u_fname'] = ($userdata["name"]);
                $post['u_email'] = ($userdata["email"]);
                $post['host-name'] = ($sitterdata["name"]);
                $post['host-email'] = ($sitterdata["email"]);
            } else {
                $post['host-name'] = "Admin";
            }
            $post['start-date'] = ($start_date);
            $post['end-date'] = ($end_date);


            /* 	collect key and vars	 */

            $sqlKey = " SELECT * FROM mail_keyword " .
                    " WHERE id IN (" . $rowBody["_keys"] . ")";
            $queryKey = $db->query($sqlKey);

            while ($key = $queryKey->fetch()) {
                //echo $key['key']."  k  - ".$key['vars']."  v  ".$post[$key['vars']]."  val<br>";


                $post['body'] = str_replace("[" . $key['key'] . "]", $key['vars'], $post['body']);
                $post['body'] = str_replace($key['vars'], $post[$key['vars']], $post['body']);
            }

            $this->send_mail($post['host-email'], $rowBody['subject'], $post['body'], "", "2");
            return true;
        }
        return false;
    }

    function subscribe_blog($email) {
        global $db;
        global $ADMIN_MAIL;

        $post = array();

        /* 	collect mail body	 */
        $sqlBody = " select mail_body as body ,mail_keys as _keys,subject " .
                " FROM mail_body " .
                " where id=13";

        $rsObjBody = $db->query($sqlBody);
        if ($rsObjBody->size() > 0) {
            $rowBody = $rsObjBody->fetch();
            /* 	collect varables	 */
            $post['site_name'] = site_name;
            $post['signature'] = site_name;

            $post['body'] = $rowBody["body"];

            $post['host-email'] = ($email);

            /* 	collect key and vars	 */

            $sqlKey = " SELECT * FROM mail_keyword " .
                    " WHERE id IN (" . $rowBody["_keys"] . ")";
            $queryKey = $db->query($sqlKey);

            while ($key = $queryKey->fetch()) {
                //echo $key['key']."  k  - ".$key['vars']."  v  ".$post[$key['vars']]."  val<br>";


                $post['body'] = str_replace("[" . $key['key'] . "]", $key['vars'], $post['body']);
                $post['body'] = str_replace($key['vars'], $post[$key['vars']], $post['body']);
            }

            $this->send_mail($email, $rowBody['subject'], $post['body'], "", "3");
            return true;
        }
        return false;
    }

    function sending_photos($main_id, $sitter_id, $host_id) {
        global $db;
        global $ADMIN_MAIL;



        require_once ("user.class.php");
        $USER = new UserClass;
        $post = array();


        $sender_info = $USER->userInfo($sitter_id);
        $receiver_info = $USER->userInfo($host_id);

        $receiver_email = $receiver_info['email'];
        $receiver_name = $receiver_info['name'];

        $sender_email = $sender_info['email'];
        $sender_name = $sender_info['name'];

        /* 	collect mail body	 */
        $sqlBody = " select mail_body as body ,mail_keys as _keys,subject " .
                " FROM mail_body " .
                " where id=14";

        $rsObjBody = $db->query($sqlBody);
        if ($rsObjBody->size() > 0) {
            $rowBody = $rsObjBody->fetch();
            /* 	collect varables	 */
            $post['site_name'] = site_name;
            $post['signature'] = site_name;

            $post['body'] = $rowBody["body"];

            $post['host-name'] = $receiver_name;
            $post['host-email'] = $receiver_email;
            $post['u_fname'] = $sender_name;
            $post['u_email'] = $sender_email;

            $sql = "SELECT * FROM sending_images WHERE main_id = '" . $main_id . "' ";
            $results = $db->query($sql);
            while ($rows = $results->fetch()) {
                $image = $rows['image'];
                $post['photos'].="<a download href='" . base_path . $image . "'><img src='" . base_path . $image . "' width='300px;'></a>";
            }

            /* 	collect key and vars	 */

            $sqlKey = " SELECT * FROM mail_keyword " .
                    " WHERE id IN (" . $rowBody["_keys"] . ")";
            $queryKey = $db->query($sqlKey);

            while ($key = $queryKey->fetch()) {
                //echo $key['key']."  k  - ".$key['vars']."  v  ".$post[$key['vars']]."  val<br>";


                $post['body'] = str_replace("[" . $key['key'] . "]", $key['vars'], $post['body']);
                $post['body'] = str_replace($key['vars'], $post[$key['vars']], $post['body']);
            }

            $this->send_mail($post['host-email'], $rowBody['subject'], $post['body'], "", "4");
            return true;
        }
        return false;
    }

    function suport($post = array()) {

        global $db;

        $sqlBody = " select mail_body as body ,subject " .
                " FROM mail_body " .
                " where id=5 ";

        $rsObjBody = $db->query($sqlBody);



        if ($rsObjBody->size() > 0) {

            $rowBody = $rsObjBody->fetch();

            $post['bodys'] = stripslashes($rowBody['body']);

            $post['bodys'] = str_replace("[personal-name]", $post['name'], $post['bodys']);

            $post['bodys'] = str_replace("[personal-email]", $post['email'], $post['bodys']);

            $post['bodys'] = str_replace("[personal-comment]", $post['comment'], $post['bodys']);

            $post['bodys'] = str_replace("[personal-date]", $post['date'], $post['bodys']);

            //echo $post['bodys'];
            //die;

            $this->send_mail('', $post['subject'], $post['bodys']);

            return true;
        }

        return false;
    }

    function send_mail($mailto, $mailubject, $mailbody, $mailfrom = '', $mail_id, $smtp = false) {
        global $db;

        if ($smtp)
            self::send_mail_smtp($mailto, $mailubject, $mailbody, $mailfrom = '', $mail_id);
        else
            self::send_mail_php($mailto, $mailubject, $mailbody, $mailfrom = '', $mail_id);
    }

    function send_mail_php($mailto, $mailubject, $mailbody, $mailfrom = '', $mail_id) {
        global $db;

        global $ADMIN_MAIL, $ADMIN_MAIL2;

        $mailbody = stripslashes($mailbody);
        if ($mail_id > 0) {
            
        }
        $mailfrom = "soadii@gmail.com";
        $headers = "MIME-Version: 1.0" . "\n";

        $headers .= "Content-type: text/html; charset=iso-8859-1" . "\n";

        $headers .= "X-Priority: 1 (Higuest)\n";

        $headers .= "X-MSMail-Priority: High\n";

        $headers .= "Importance: High\n";

        $headers .= "From: <$mailfrom>" . "\n";

        $headers .= "Return-Path: <$mailfrom>" . "\n";


        $gMail = mail($mailto, $mailubject, $mailbody, $headers);
    }

   function send_mail_smtp($mailto, $mailubject, $mailbody, $mailfrom = '', $mail_id) {
        global $ADMIN_MAIL, $ADMIN_MAIL2;
        global $db;


        $mailbody = stripslashes($mailbody);
	
        if ($mailto == '')
            $mailto = $ADMIN_MAIL;

        if ($mailfrom == '')
            $mailfrom = $ADMIN_MAIL;


        $replytomail = $ADMIN_MAIL;

        $PHPMailer = new PHPMailer();
	
        //$PHPMailer->SMTPSecure='ssl';
	$PHPMailer->debug=false;
        $PHPMailer->CharSet = 'UTF-8';
        $PHPMailer->IsSMTP();        // telling the class to use SMTP
        $PHPMailer->SMTPDebug = 0;       // enables SMTP debug information (for testing)
        $PHPMailer->SMTPAuth = true;      // enable SMTP authentication
        $PHPMailer->Host = "mail.ninehertzindia.com"; // sets the SMTP server




        $PHPMailer->Port = 26;      // set the SMTP port for the GMAIL server
        $PHPMailer->Username = "test@ninehertzindia.com"; // SMTP account username
        $PHPMailer->Password = "test2015";    // SMTP account password


        $PHPMailer->AddAddress($mailto, "");
	
//        $PHPMailer->SetFrom($mailfrom, '');
//        $PHPMailer->AddReplyTo($mailfrom, "");
        $PHPMailer->Subject = $mailubject;
	
        $PHPMailer->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
        $PHPMailer->MsgHTML($mailbody);
         $aa = "Mailer Error: " . $this->ErrorInfo;




        if (!$PHPMailer->Send()) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

}
