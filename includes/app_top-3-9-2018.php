<?php

ob_start();
session_start();

ini_set('display_errors', 1);
error_reporting(E_ERROR);
session_cache_expire(10);
ini_set('session.gc_maxlifetime', 24 * 60 * 60);
ini_set('session.gc_probability', 1);
ini_set('session.gc_divisor', 1);
$currenturl = $_SERVER['PHP_SELF'];
$parts = Explode('/', $currenturl);
$CURRENT_PAGE = $parts[count($parts) - 1];

$currenturi = $_SERVER['REQUEST_URI'];
$uriparts = Explode('/', $currenturi);
$CURRENT_PAGE_URI = $uriparts[count($uriparts) - 1];


define("PHP_SELF", $CURRENT_PAGE);
define("REQUEST_URI", $CURRENT_PAGE_URI);

define("MSG_ATT_SESSION_OFF", "Please Contact your Store Manager");
define("REGULAR_HOUR", "40.00");
define("REGULAR_HOUR_TOTAL", "80.00");
define("DM", "-1"); //DEVELOPMEN MODE 1=>on 0=>OFF ,-1 =>testing
define("SALT", "soadii!@#321");


define("GOOGLE_API_KEY", "AIzaSyBe-p0tq9HSfHRV1zgJZ0d9VmzSyBndHYY");
define("GOOGLE_GCM_URL", "https://fcm.googleapis.com/fcm/send");

$attandenceTimeAddMinute = 5;
include 'fb.php';

function __autoload($class) {
    if (file_exists("../../includes/classes/$class.class.php")) {
        include_once "../../includes/classes/$class.class.php";
    }
    if (file_exists("../includes/classes/$class.class.php")) {
        include_once "../includes/classes/$class.class.php";
    }
    if (file_exists("/includes/classes/$class.class.php")) {
        include_once "/includes/classes/$class.class.php";
    }
    if (file_exists("./includes/classes/$class.class.php")) {
        include_once "./includes/classes/$class.class.php";
    }
}

function fheader($url) {
    $url = admin_path . $url;

//    if (ereg("Microsoft", $_SERVER['SERVER_SOFTWARE'])) {
//        header("Refresh: 0; URL=$url");
//    } else {
//        header("Location: $url");
//    }
    header("Location: $url");
    exit();
}

function cheader($url) {

    $url = base_path . $url;
//    if (ereg("Microsoft", $_SERVER['SERVER_SOFTWARE'])) {
//        header("Refresh: 0; URL=$url");
//    } else {
//        header("Location: $url");
//    }
       header("Location: $url");
    exit();
}

if (@$PageTitle == "")
    $PageTitle = $lang['SITE_TITLE'];

$All_timer_Condition = false;
