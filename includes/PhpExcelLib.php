<?php 
//if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 


/**
 * CodeIgniter CSV Import Class
 *
 * This library will help import a CSV file into
 * an associative array.
 * 
 * This library treats the first row of a CSV file
 * as a column header row.
 * 
 *
 * @package         CodeIgniter
 * @subpackage      Libraries
 * @category        Libraries
 * @author          Aman jain
 */

class PhpExcelLib {
    public $objPHPExcel;

    function PhpExcelLib()
    {
        require_once '../includes/phpexcel/Classes/PHPExcel.php';
        
        require_once  '../includes/phpexcel/Classes/PHPExcel/IOFactory.php';
        $this->ci          = &get_instance();
        $this->ci->load->library("session");
        $this->objPHPExcel = new PHPExcel();
        $this->objPHPExcel->getProperties()->setCreator("Pocketinns")
							 ->setLastModifiedBy("Aman Jain")
							 ->setTitle("listing csv")
							 ->setSubject("listing csv")
							 ->setDescription("listing csv")
							 ->setKeywords("listing csv php")
							 ->setCategory("listing csv");
      
    }
    public function init()
    { 
          return $this->objPHPExcel;
    } 
    public function addDropdownLong($column,$options=[],$till=250){
          
            
            $this->objPHPExcel->setActiveSheetIndex(0);// Drop down in sheet 0
            $objValidation = $this->objPHPExcel->getSheet(0)->getCell("{$column}2")->getDataValidation();
            $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
            $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
            $objValidation->setAllowBlank(false);
            $objValidation->setShowInputMessage(true);
            $objValidation->setShowErrorMessage(true);
            $objValidation->setShowDropDown(true);
            $objValidation->setErrorTitle('Input error');
            $objValidation->setError('Value is not in list.');
            $objValidation->setFormula1('List!$A$1:$A$'.count($options));
            for($i=2;$i<=$till;$i++){
                    $this->objPHPExcel->getSheet(0)->getCell("$column".$i)->setDataValidation(clone $objValidation);
            }
    }
    public function addDropdown($column,$options=[],$till=250){
          
            $options= implode(",",$options);
            $objValidation = $this->objPHPExcel->getSheet(0)->getCell( "{$column}2")->getDataValidation();
            $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
            $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
            $objValidation->setAllowBlank(false);
            $objValidation->setShowInputMessage(true);
            $objValidation->setShowErrorMessage(true);
            $objValidation->setShowDropDown(true);
            $objValidation->setErrorTitle('Input error');
            $objValidation->setError('Value is not in list.');
            $objValidation->setPromptTitle('Pick from list');
            $objValidation->setPrompt('Please pick a value from the drop-down list.');
            $objValidation->setFormula1('"'.$options.'"');
            $this->objPHPExcel->getSheet(0)->setTitle('Simple');
            for($i=2;$i<=$till;$i++){
                    $this->objPHPExcel->getSheet(0)->getCell("$column".$i)->setDataValidation(clone $objValidation);
            }
    }  
    public function save($path="/upload/listing.xls"){
        $objWriter = PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel5');
        $objWriter->save(str_replace('.php', '.xlsx', FCPATH.$path));
    }
    
    public function read($path="/upload/listing_csv.xlsx"){
        require_once '../includes/phpexcel/Classes/PHPExcel.php';
        
        require_once  '../includes/phpexcel/Classes/PHPExcel/IOFactory.php';
                $inputFileName =$path;

                //  Read your Excel workbook
                try {
                    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                    $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel   = $objReader->load($inputFileName);
                } catch(Exception $e) {
                    die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
                }

                //  Get worksheet dimensions
                $sheet = $objPHPExcel->getSheet(0); 
                $highestRow = $sheet->getHighestRow(); 
                $highestColumn = $sheet->getHighestColumn();

                //  Loop through each row of the worksheet in turn
                for ($row = 1; $row <= $highestRow; $row++){ 
                    //  Read a row of data into an array
                    $rowData[] = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                    NULL,
                                                    TRUE,
                                                    FALSE);
                    //  Insert row data array into your database of choice here
                    
                  
                }
                  
                return $rowData;
    }
}