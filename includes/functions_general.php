<?php
function GetMonthNo($monthname) {
    switch ($monthname) {
        case "Jan":
            return "01";
            break;
        case "Feb":
            return "02";
            break;
        case "Mar":
            return "03";
            break;
        case "Apr":
            return "04";
            break;
        case "May":
            return "05";
            break;
        case "Jun":
            return "06";
            break;
        case "Jul":
            return "07";
            break;
        case "Aug":
            return "08";
            break;
        case "Sep":
            return "09";
            break;
        case "Oct":
            return "10";
            break;
        case "Nov":
            return "11";
            break;
        case "Dec":
            return "12";
            break;
    }
}
//for date format ("2015-05-22") returns 22 May 2015.

function iso2ts ($iso, $hour=12, $min=0, $sec=0) 
  {
    $d = substr($iso, 8, 2);
    $m = substr($iso, 5, 2);
    $y = substr($iso, 0, 4);
    return mktime($hour, $min, $sec, $m, $d, $y);
  }
     
    // iso2http: HTTP "Last-Modified" format (Fri, 22 May 2015 )
   function iso2http ($iso, $hour=12, $min=0, $sec=0)
  { 
    return gmdate("D, d M Y ", iso2ts($iso, $hour, $min, $sec));	
  }
///////date format
  
  
  
  //date difference
  
function diffDate($datetime1,$datetime2){
  

$difference = $datetime1->diff($datetime2);

if($difference->y){
$date =             $difference->y.' years, ' 
                   .$difference->m.' months, ' 
                   .$difference->d.' days';    
}else if($difference->m){
    $date =         $difference->m.' months, ' 
                   .$difference->d.' days';
}else{
    $date =         $difference->d.' days';
}


                   return $date;
}
  //date difference
  
  
function sqlDate($date) {
    $dtDate = explode("-", $date);
    $dtDate = $dtDate[2] . "-" . $dtDate[0] . "-" . $dtDate[1];
    return $dtDate;
}

function sqlDateTime($date) {

    $dtDateTime = explode(" ", $date);
    $dtDate = explode("-", $dtDateTime[0]);
    if ($dtDateTime[2]) {
        $dtDate = $dtDate[2] . "-" . $dtDate[0] . "-" . $dtDate[1] . " " . $dtDateTime[1] . ":00" . " " . $dtDateTime[2];
    } else {
        $dtDate = $dtDate[2] . "-" . $dtDate[0] . "-" . $dtDate[1] . " " . $dtDateTime[1] . ":00";
    }
    return $dtDate;
}

function FormatDate($date) {
    global $date_format;
    return date($date_format, strtotime($date));
}

function FormatDateMonth($date) {
    global $date_format_month;
    return date($date_format_month, strtotime($date));
}

function FormatTime($date) {
    global $time_format;
    return date($time_format, strtotime($date));
}

function nowDate() {
    return date("Y-m-d");
}

function nowDateTime() {
    return date("Y-m-d H:i:s");
}

function hoursToHourMinite($hours) {
    $hourInfo = explode(".", $hours);
    $hmiinit = ($hourInfo[0] * 60);
    $minites = round(($hours * 60) - $hmiinit);
    if ($minites <= 9)
        $minites = "0" . $minites;
    if ($minites > 0)
        $hourminit = $hourInfo[0] . ":" . $minites;
    else
        $hourminit = $hourInfo[0] . ":" . $minites;
    return $hourminit;
}

function nowTime() {
    return date("H:i:s");
}

function FormatDateTime($date) {
    global $date_time_format;
    return date($date_time_format, strtotime($date));
}

function convert24to12Hours($time, $sec = 0) {
    if ($sec == 0)
        $format = "g:i A";
    else
        $format = "g:i:s A";

    $time = date($format, strtotime($time));
    return $time;
}

function convert12to24Hours($time, $sec = 0) {
    if ($sec == 0)
        $format = "H:i";
    else
        $format = "H:i:s";

    $time = date($format, strtotime($time));
    return $time;
}

function convert12to24Date($time, $sec = 0) {

    $format = "Y-m-d H:i:s";
    $time = date($format, strtotime($time));
    return $time;
}

function FormatNumber($val) {
    //string number_format ( float $number , int $decimals , string $dec_point , string $thousands_sep )
    return number_format($val, 2, '.', ',');
}

function dateAdd($tp, $interval, $today) {

    if (empty($today))
        $today_day = date("Y-m-d");
    else
        $today_day = $today;
    switch ($tp) {
        case "d":
            $by = $interval . " days";
            break;
        case "m":
            $by = $interval . " month";
            break;
        case "y":
            $by = $interval . " year";
            break;
        case "w":
            $by = $interval . " week";
            break;
    }
    $date = strtotime(date("Y-m-d", strtotime($today_day)) . $by);
    $date = date('Y-m-d', $date);
    return $date;
}

function dateDiff($interval = "", $start, $end) {
    $tp = strtoupper($interval);
    if (empty($end))
        $end = date("Y-m-d");
    $sdate = strtotime($start);
    $edate = strtotime($end);
    switch ($tp) {
        case "Y":
            $totalDays = date("z", $sdate) + 1;
            $second_diff = 3600 * 24 * 365;
            break;
        case "M":
            $lastday = date("t", $sdate);
            $second_diff = 3600 * 24 * $lastday;
            break;
        case "W":
            $second_diff = 3600 * 24 * 7;
            break;
        case "D":
            $second_diff = 3600 * 24;
            break;
    }
    $date_diff = $edate - $sdate;
    $diff = ($date_diff / $second_diff);
    if ($diff > 0 && $diff < 1)
        $diff = 1;
    else
        $diff = floor($diff);

    return $diff;
}

function make_page($total_items, $items_per_page, $p) {
    if (($total_items % $items_per_page) != 0) {
        $maxpage = ($total_items) / $items_per_page + 1;
    } else {
        $maxpage = ($total_items) / $items_per_page;
    }
    $maxpage = (int) $maxpage;
    if ($maxpage <= 0) {
        $maxpage = 1;
    }
    if ($p > $maxpage) {
        $p = $maxpage;
    } elseif ($p < 1) {
        $p = 1;
    }
    $start = ($p - 1) * $items_per_page;
    return Array($start, $p, $maxpage);
}

function randomcode($len = "6") {
    $code = NULL;
    for ($i = 0; $i < $len; $i++) {
        $char = chr(rand(48, 122));
        while (!ereg("[a-zA-Z0-9]", $char)) {
            if ($char == $lchar) {
                continue;
            }
            $char = chr(rand(48, 90));
        }
        $pass .= $char;
        $lchar = $char;
    }
    return $pass;
}

function is_email_address($email) {
    $regexp = "/^[a-z0-9]+([_\\.-][a-z0-9]+)*@([a-z0-9]+([\.-][a-z0-9]+)*)+\\.[a-z]{2,}$/i";
    if (!preg_match($regexp, $email)) {
        return false;
    } else {
        return true;
    }
}

if (!function_exists('str_ireplace')) {

    function str_ireplace($search, $replace, $subject) {
        $search = preg_quote($search, "/");
        return preg_replace("/" . $search . "/i", $replace, $subject);
    }

}
if (!function_exists('htmlspecialchars_decode')) {

    function htmlspecialchars_decode($text, $ent_quotes = "") {
        $text = str_replace("&quot;", "\"", $text);
        $text = str_replace("&#039;", "'", $text);
        $text = str_replace("&lt;", "<", $text);
        $text = str_replace("&gt;", ">", $text);
        $text = str_replace("&amp;", "&", $text);
        return $text;
    }

}
if (!function_exists('str_split')) {

    function str_split($string, $split_length = 1) {
        $count = strlen($string);
        if ($split_length < 1) {
            return false;
        } elseif ($split_length > $count) {
            return array($string);
        } else {
            $num = (int) ceil($count / $split_length);
            $ret = array();
            for ($i = 0; $i < $num; $i++) {
                $ret[] = substr($string, $i * $split_length, $split_length);
            }
            return $ret;
        }
    }

}

function security($value) {
    if (is_array($value)) {
        $value = array_map('security', $value);
    } else {
        if (!get_magic_quotes_gpc()) {
            $value = htmlspecialchars($value, ENT_QUOTES);
        } else {
            $value = htmlspecialchars(stripslashes($value), ENT_QUOTES);
        }
        $value = str_replace("\\", "\\\\", $value);
    }
    return $value;
}

function clean($string) {
   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}

function getPagingHtml($count, $limit, $currentPage, $url) {

	if ($count > 0) {

		$p;

		$maxDisplayedPages = 7;

		//var n = 1;



		$strPaging = "";

		$doneDots = false;

		$lastPage = ceil($count / $limit);

		$firstDisplayedPage = min($lastPage - ($maxDisplayedPages - 2), max($currentPage - ($maxDisplayedPages / 2) + 1, 1));

		$lastDisplayedPage;

		if ($firstDisplayedPage == 1)
			$lastDisplayedPage = min($maxDisplayedPages - 1, $lastPage);
		else
			$lastDisplayedPage = min($firstDisplayedPage + $maxDisplayedPages - 2, $lastPage);



		if ($currentPage > 1) {

			$strPaging.="<a class='dbl_prev' href='" . $url . "&pageindex=1'><img src='images/pre2.png' border='0' /></a>&nbsp;";

			$strPaging.="<a class='prev' href='" . $url . "&pageindex=" . ($currentPage - 1) . "'><img src='images/pre.png' border='0' /></a>&nbsp;";
		} else {



			$strPaging.="&nbsp;";
		}

		for ($p = 1; $p <= $lastPage; $p++) {

			if ($currentPage == $p) {

				$strPaging.= "<span class='text-red'><b>$p</b></span> ";
			} else {

				if ($p == 1 || $p == $lastPage || ($p >= $firstDisplayedPage && $p <= $lastDisplayedPage)) {

					$strPaging.= "<a class='page' href='" . $url . "&pageindex=" . $p . "'>$p</a> ";

					$doneDots = false;
				} else if (!$doneDots) {

					$strPaging.= "<span class='page'>... </span>";

					$doneDots = true;
				}
			}
		}



		if ($currentPage < ($count / $limit)) {

			$strPaging.= "<a class='next' href='" . $url . "&pageindex=" . ($currentPage + 1) . "'><img src='images/next.png' border='0' /></a>";



			$strPaging.= "<a class='dbl_next' href='" . $url . "&pageindex=" . ($lastPage) . "'><img src='images/next2.png' border='0' /></a>";
		} else {

			$strPaging.= "&nbsp;";
		}
	}

	return $strPaging;
}

function farentgetPagingHtml($count, $limit, $currentPage, $url) {
    if ($count > 0) {
        $p;
        $maxDisplayedPages = 7;
        //var n = 1;

        $strPaging = "";
        $doneDots = false;
        $lastPage = ceil($count / $limit);
        $firstDisplayedPage = min($lastPage - ($maxDisplayedPages - 2), max($currentPage - ($maxDisplayedPages / 2) + 1, 1));
        $lastDisplayedPage;
        if ($firstDisplayedPage == 1)
            $lastDisplayedPage = min($maxDisplayedPages - 1, $lastPage);
        else
            $lastDisplayedPage = min($firstDisplayedPage + $maxDisplayedPages - 2, $lastPage);

        if ($currentPage > 1) {
            $strPaging.="<a href='" . $url . "&pageindex=1' class='pagingarow'>«</a>&nbsp;";
            $strPaging.="<a href='" . $url . "&pageindex=" . ($currentPage - 1) . "' class='pagingarow'>‹</a>&nbsp;";
        } else {

            $strPaging.="&nbsp;";
        }
        for ($p = 1; $p <= $lastPage; $p++) {
            if ($currentPage == $p) {
                $strPaging.= "<span class='text-red'><b>$p</b></span> ";
            } else {
                if ($p == 1 || $p == $lastPage || ($p >= $firstDisplayedPage && $p <= $lastDisplayedPage)) {
                    $strPaging.= "<a class='page' href='" . $url . "&pageindex=" . $p . "'>$p</a> ";
                    $doneDots = false;
                } else if (!$doneDots) {
                    $strPaging.= "<span class='page'>... </span>";
                    $doneDots = true;
                }
            }
        }

        if ($currentPage < ($count / $limit)) {
            $strPaging.= "<a href='" . $url . "&pageindex=" . ($currentPage + 1) . "' class='pagingarow'>›</a>";

            $strPaging.= "<a href='" . $url . "&pageindex=" . ($lastPage) . "' class='pagingarow'>»</a>";
        } else {
            $strPaging.= "&nbsp;";
        }
    }
    return $strPaging;
}

function getSecureFormat($str) {
    $result = "";
    for ($i = 0; $i < strlen($str); $i++)
        $result.="*";

    return $result;
}

function MonthDays($someMonth, $someYear) {
    return date("t", strtotime($someYear . "-" . $someMonth . "-01"));
}

function shortString($str, $num = 50) {
    if (strlen($str) > $num)
        return substr($str, 0, $num) . "...";
    else
        return substr($str, 0, $num);
}

function IsInt($int) {

    // First check if it's a numeric value as either a string or number
    if (is_numeric($int) === TRUE) {

        // It's a number, but it has to be an integer
        if ((int) $int == $int) {

            return TRUE;

            // It's a number, but not an integer, so we fail
        } else {

            return FALSE;
        }

        // Not a number
    } else {

        return FALSE;
    }
}

function timeDiff($firstTime, $lastTime) {
    $firstTime = strtotime($firstTime);
    $lastTime = strtotime($lastTime);

    $timeDiff = $lastTime - $firstTime;
    $hours = round((($timeDiff % 604800) % 86400) / 3600, 2);
    return $hours;
}

function getSundayAndSaturday($cdate) {
    $getsunday = '';

    $getsunday = date("Y-m-d", strtotime("LAST tuesday", strtotime($cdate)));
    //$timeInfo['saturday']	=	date($date_format,strtotime("NEXT SATURDAY",strtotime($cdate)));

    return $getsunday;
}

function sendPDF($to, $subject, $message, $from, $file) {
    $PHP_MAILER = new PHPMailer(true);
    $PHP_MAILER->SetFrom($from);
    $PHP_MAILER->AddReplyTo($from);
    $address = $to;
    $PHP_MAILER->AddAddress($to);

    $PHP_MAILER->Subject = $subject;
    $PHP_MAILER->MsgHTML($message);
    $PHP_MAILER->AddAttachment(realpath($file));   // attachment

    if (!$PHP_MAILER->Send()) {
        // echo "Mailer Error: " . $mail->ErrorInfo;
        return false;
    } else {
        return true;
    }
}

function sprintf1($value) {
    if ($value != 0) {
        $value = round($value, 2);
        return sprintf("%.2f", $value);
    } else {
        return "";
    }
}

// get location data using geoplugin

/*** 4d818940b0227a7ba8b77b0a975e8ec83d967c3e2042136b9a98bb6d6cbfd286
//http://api.ipinfodb.com/v3/ip-city/?key=4d818940b0227a7ba8b77b0a975e8ec83d967c3e2042136b9a98bb6d6cbfd286&ip=201.252.86.162&format=json ****/

function getGeoPluginData() {
    
	$locationDataArray = array();
	$client = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote = $_SERVER['REMOTE_ADDR'];

    if (filter_var($client, FILTER_VALIDATE_IP)) {
        $ip = $client;
    } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
        $ip = $forward;
    } else {
        $ip = $remote;
    }
//echo $ip; 
//    $ip = '190.2.8.65';
//	$ip = '122.160.152.0';
	$geoDataArray = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));

	if(empty($geoDataArray->geoplugin_city)) {

		$ipiDataArray = @json_decode(file_get_contents("http://api.ipinfodb.com/v3/ip-city/?key=4d818940b0227a7ba8b77b0a975e8ec83d967c3e2042136b9a98bb6d6cbfd286&ip=" . $ip . "&format=json"));
		$locationDataArray['geoplugin_city'] = str_replace("ā", "a", $ipiDataArray->cityName);
		$locationDataArray['geoplugin_region'] = str_replace("ā", "a", $ipiDataArray->regionName);
		$locationDataArray['geoplugin_countryName'] = str_replace("ā", "a", $ipiDataArray->countryName);
		$locationDataArray['geoplugin_latitude'] = $ipiDataArray->latitude;
		$locationDataArray['geoplugin_longitude'] = $ipiDataArray->longitude;
	} else {
		$locationDataArray['geoplugin_city'] = str_replace("ā", "a", $geoDataArray->geoplugin_city);
		$locationDataArray['geoplugin_region'] = str_replace("ā", "a", $geoDataArray->geoplugin_region);
		$locationDataArray['geoplugin_countryName'] = str_replace("ā", "a", $geoDataArray->geoplugin_countryName);
		$locationDataArray['geoplugin_latitude'] = $geoDataArray->geoplugin_latitude;
		$locationDataArray['geoplugin_longitude'] = $geoDataArray->geoplugin_longitude;
	}

	return $locationDataArray;
    //return @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
}

function getTimeInfo($oldTime, $newTime, $timeType = "x") {

        global $db;
        $timeCalc = strtotime($newTime) - strtotime($oldTime);
        if ($timeType == "x") {
            if ($timeCalc > 60) {
                $timeType = "i";
            }
            if ($timeCalc > (60 * 60)) {
                $timeType = "h";
            }
            if ($timeCalc > (60 * 60 * 24)) {
                $timeType = "d";
            }
            if ($timeCalc > (60 * 60 * 24 * 7)) {
                $timeType = "w";
            }
            if ($timeCalc > (60 * 60 * 24 * 7)) {
                $timeType = "w";
            }
        }
        if ($timeType == "s") {
            
        }
        if ($timeType == "i") {

            if (round($timeCalc / 60) <= 1)
                $timeCalc = round($timeCalc / 60) . " min ago";
            else
                $timeCalc = round($timeCalc / 60) . " mins ago";
        }
        elseif ($timeType == "h") {

            if (round($timeCalc / 60 / 60) <= 1) {
                $timeCalc = round($timeCalc / 60 / 60) . " hour ago";
            } else {
                $timeCalc = round($timeCalc / 60 / 60) . " hours ago";
            }
        } elseif ($timeType == "d") {

            if (round($timeCalc / 60 / 60 / 24) <= 1) {
                $timeCalc = round($timeCalc / 60 / 60 / 24) . " day ago";
            } else {
                $timeCalc = round($timeCalc / 60 / 60 / 24) . " days ago";
            }
        } elseif ($timeType == "w") {

            if (round($timeCalc / 60 / 60 / 24 / 7) <= 1) {
                $timeCalc = round($timeCalc / 60 / 60 / 24 / 7) . " week ago";
            } else {
                $timeCalc = round($timeCalc / 60 / 60 / 24 / 7) . " weeks ago";
            }
        } else {
            $timeCalc = " Just now";
        }
        return $timeCalc;
    }

?>