<?php
define("INVALIDCREDENTIAL", "Invalid Credentials");
define("NIIVOCODESENT", "Your Niivo verification code has been sent.");
define("MSGNOTSENT", "Message not sent,Please try again.");
define("MOBILEEXIST", "This mobile number already registered. Please try another one");
define("OTPVERIFIED", "Otp verified successfully.");
define("ENTEROTP", "Please enter correct otp");
define("MOBILENOTEXIST", "This mobile number is nor exist. Please try with valid number");
define("LOGINSUCCESS", "Login successfully.");
define("MOBILEPASSNOMATCH", "Mobile and password does not match");
define("SUCCESS", "Success");
define("PASSCHANGED", "Password Successfully Changed");
define("OLDPASSINCORRECT", "Old password is not correct");
define("NOUSER", "User doesn't exist");
define("SOMETHINGWRONG", "Oops, something went wrong.\n Please try again.");
define("INVALIDREQUEST", "invalid Request");
define("SOMEERROR", "There is some error.Please try again.");
define("INVALIDIFSC", "Invalid IFSC code");
define("NOBANKFOUND", "Bank not found. Please select another bank IFSC.");
define("LANGUAGECHANGED", "Language Successfully Changed");
define("SCHEMENOTEMPTY", "Scheme Code can't be empty");
define("NORECORD", "Record not found");
define("ORDERCANCEL", "Order Cancel Successfully");
define("FAILED", "Failed");
define("ORDERCONFIRM", "Order Confirm");
define("RECORDEXIST", "Record already exist");
define("NOSIPFOUND", "No SIP installment due found");
define("NOADVISOREXIST", "Advisor Code doesn't exist.");
define("SIPDUE", "SIP Installment due for [fund_name] of Rs.[amount] by [due_date]");




define("STATUSTRUE", "true");
define("STATUSFALSE", "false");

define("ACCOUNTDEACTIVATED", "Your account is deactived by Charity team for review!");

define("LOGIN", "Successful Login");
define("USERUPDATED", "user updated successfully");
define("USERADDED", "Added successfully");

define("NOPAGE", "Page doesn't exist");
define("ALREADYEXIST", "Already exist");
define("USEREMAILEXIST", "This email address already registered");

define("FBEXIST", "This fb user already registered. Please try another one");
define("SUCCESSFULLYREGISTER", "Congratulations! You have successfully registered.");
define("RESETLINK", "Reset password link has been expired. Please try again.");
define("PASSWORD", "Please enter your password.");
define("CPASSWORD", "Please enter your confirm password.");




define("RESETLINK", "Reset password link has been expired. Please try again.");
//login error msg//
define("LOGINERROR", "Email and password do not match.");

//account validation msg
define("SUSPEND", "Your account is suspended.Please contact to admin.");
define("BLOCK", "Your account is blocked.Please contact to admin.");

define("PASSNOTMATCH", "Password and confirm password does not match.");

define("PASSSEND", "A password reset link was sent to your email address.");
define("PASSCHANGE", "Your password has been changed successfully.");
define("EMAILNOTEXIST", "Your email does not exist.");


