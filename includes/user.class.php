<?php

class UserClass {

    function isUser($userid) {
        global $db;
        $sql = "SELECT userid FROM users WHERE userid  = '$userid' ";
        $query = $db->query($sql);

        if ($query->size() > 0)
            return true;
        else
            return false;
    }

    function validateUsername($username) {
        global $db;
        $sql = "SELECT * FROM users WHERE username  = $username ";
        $query = $db->query($sql);

        if ($query->size() > 0)
            return true;
        else
            return false;
    }

    function checkPhoneExist($mobile, $id) {
        global $db;
        $isValid = TRUE;


        $where = " AND 1 ";
        if ($mobile != '') {
            if ($id != '') {
                $where .= " AND userid != '" . $id . "' ";
            }
            $sql = " SELECT * FROM users WHERE " .
                    " mobile = '" . $mobile . "' " . $where;

            $result = $db->query($sql);
            if ($result->size() > 0) {
                return false;
            } else {
                return true;
            }
        }

        return true;
    }
    function checkFbExist($fb_id, $id) {
        global $db;
        $isValid = TRUE;


        $where = " AND 1 ";
        if ($fb_id != '') {
            if ($id != '') {
                $where .= " AND userid != '" . $id . "' ";
            }
            $sql = " SELECT * FROM users WHERE " .
                    " fb_id = '" . $fb_id . "' " . $where;

            $result = $db->query($sql);
            if ($result->size() > 0) {
                return false;
            } else {
                return true;
            }
        }

        return true;
    }

    /**
     * (Made by Sumit Mathur for PHP 4, PHP 5)<br/>
     * Parse information about an user.
     * @param int $user_id <p>  An intger id of an User  </p>
     * @return Array An array containg user's information
     */
    function userInfo($user_id) {
        global $db;
        $data = array();
        $user_id = intval($user_id);

        $sql = "SELECT * FROM users WHERE id = " . $user_id;
        $que = $db->query($sql);
        $row = $que->fetch();
        $data['userid'] = $row['id'];
        $data['email'] = $row['email'];
        $data['name'] = ucfirst($row['first_name'] . " " . $row['last_name']);
        $data['username'] = $row['username'];
        $data['fname'] = $row['first_name'];
        $data['lname'] = $row['last_name'];
        $data['phone_number'] = $row['phone_number'];
        $data['address'] = $row['address'];
        $data['country_id'] = $row['country_id'];
        $data['state_id'] = $row['state_id'];
        $data['city'] = $row['city'];
        $data['gender'] = $row['gender'];
        $data['hear_about_us'] = $row['hear_about_us'];
        $data['is_sitter'] = $row['is_sitter'];
        $data['type'] = $row['type'];
        $data['dtdate'] = $row['dtdate'];
        $data['mail_verify'] = $row['email_verify'];
        $data['status'] = $row['status'];
        $data['lastlogin'] = $row['lastlogin'];
        $data['stepcompleted'] = $row['step_completed'];
        $data['facebook'] = $row['facebook'];
        $data['twitter'] = $row['twitter'];
        $data['last_modified'] = $row['last_modified'];
        $data['unsubscribe'] = $row['unsubscribe'];
        $data['otp_verify'] = $row['otp_verify'];
        
        $data['fb_id'] = $row['fb_id'];
        $data['phone_interview'] = $row['phone_interview'];
        $data['google'] = $row['google'];
        if ($row['pic'] != "") {
            if (strpos($row['pic'], "http://")) {
                $image = base_path . $row['pic'];
                $thumb = base_path . $row['thumb_image'];
            } else {
                $image = $row['pic'];
                $thumb = $row['thumb_image'];
            }
        } else {
            $image = base_path . "images/host-no-img.jpg";
            $thumb = base_path . "images/host-no-img.jpg";
        }
        $data['image'] = $image;
        $data['thumb_image'] = $thumb;

        $data['lat'] = $row['lat'];
        $data['lon'] = $row['lon'];

        return $data;
    }

    /**
     * (Made by shruti kalwar for PHP 4, PHP 5)<br/>
     * Parse total users from users or today's users.
     * @param int string $tp <p>  A string containing true or false condition  </p>
     * @return  int A integer counting of users.
     */
    function calculateMonth($startDate, $endDate) {
        $ts1 = strtotime($endDate);
        $ts2 = strtotime($startDate);

        $year1 = date('Y', $ts1);
        $year2 = date('Y', $ts2);

        $month1 = date('m', $ts1);
        $month2 = date('m', $ts2);
        $diff = floor(abs((($year2 - $year1) * 12) + ($month2 - $month1)) / 12);
        return $diff;
    }

    function checkBusinessOwnerEmailExist($email, $id) {
        global $db;
        $isValid = TRUE;
        $where = " AND 1 ";
        if (is_email_address($email) && $email != "") {
            if ($id != '') {
                $where = " AND userid != '" . $id . "' ";
            }
            $sql = " SELECT * FROM business_owner WHERE " .
                    " upper(email) = '" . strtoupper($email) . "' " . $where;

            $result = $db->query($sql);
            if ($result->size() > 0) {
//                $this->msg = "The email address entered is already registered";
                unset($result);
                return false;
            }
        }
        return true;
    }

    function checkUsernameExist($username, $id) {
        global $db;
        $isValid = TRUE;
        $where = " AND 1 ";
        if ($username != "") {
            if ($id != '') {
                $where = " AND userid != '" . $id . "' ";
            }
            $sql = " SELECT * FROM business_owner WHERE " .
                    " upper(email) = '" . strtoupper($username) . "' " . $where;

            $result = $db->query($sql);
            if ($result->size() > 0) {
//                $this->msg = "The email address entered is already registered";
                unset($result);
                return false;
            }
        }
        return true;
    }

    function checkBusinessOwnerExist($username, $id) {
        global $db;
        $isValid = TRUE;
        $where = " AND 1 ";
        
      
        if ($username != "") {
            if ($id != '') {
                $where .= " AND id != '" . $id  . "' ";
            }
             $sql = " SELECT * FROM business_page WHERE " .
                    " upper(username) = '" . strtoupper($username) . "' " . $where;

            $result = $db->query($sql);
            if ($result->size() > 0) {
//                $this->msg = "The email address entered is already registered";
                unset($result);
                return false;
            }
        }
        return true;
    }

    function getUserName($userid) {
        global $db;
        $count = 0;

        $sql = "SELECT * FROM users WHERE userid = '" . $userid . "' ";
        $res = $db->query($sql);
        $rs = $res->fetch();
        if ($res->size() > 0) {
            $title = $rs['username'];
        }
        return $title;
    }

    function totalUser($tp = false) {
        global $db;
        $count = 0;
        if ($tp == false)
            $sql = "select userid from users";
        else {
            $today = date("Y-m-d", strtotime("today"));
            $sql = "SELECT userid FROM users where dtdate  LIKE '%" . $today . "%'";
        }

        $res = $db->query($sql);
        if ($res->size() > 0) {
            $count = $res->size();
        }
        return $count;
    }

    /**
     * (Made by shruti kalwar for PHP 4, PHP 5)<br/>
     * Parse active or deactive users.
     * @param int string $tp <p>  A string containing true or false condition  </p>
     * @return  int A integer counting of active and deactive users.
     */
    function totalStore($tp = false) {
        global $db;
        $count = 0;
        if ($tp == false)
            $sql = "select id from store";
        else {
            $today = date("Y-m-d", strtotime("today"));
            $sql = "SELECT id FROM store where date_created LIKE '%" . $today . "%'";
        }

        $res = $db->query($sql);
        if ($res->size() > 0) {
            $count = $res->size();
        }
        return $count;
    }

    function userStatus($tp = false) {
        global $db;
        $count = 0;
        if ($tp == false) {
            $sql = "select id from users where status='1'";
        } else {
            $sql = "SELECT id FROM users where status='0'";
        }

        $res = $db->query($sql);
        if ($res->size() > 0) {
            $count = $res->size();
        }
        return $count;
    }

    /**
     * (Made by Sumit Mathur for PHP 4, PHP 5)<br/>
     * Parse server detail if exists.
     * @return String it return real path for file.
     */
    function get_real_path() {
        return dirname($this->get_server_var('SCRIPT_FILENAME')) . "/";
    }

    /**
     * (Made by Sumit Mathur for PHP 4, PHP 5)<br/>
     * Parse image and return a true or a fake image.
     * @param string $image string containg image or not.
     * @return String it return real path for file.
     */
    protected function check_real_image($image) {
        return $image ? $image : "fake.jpg";
    }

    /**
     * (Made by Sumit Mathur for PHP 4, PHP 5)<br/>
     * Parse server detail if exists.
     * @param String $id <p>  A string containing any server variable </p>
     * @return String if exists otherwise it return blank value.
     */
    function get_server_var($id) {
        return isset($_SERVER[$id]) ? $_SERVER[$id] : '';
    }

    function chkuserlogin() {
        if (!isset($_SESSION[md5('user')])) {
            cheader('signup.php');
        }
    }

    //for get the page title
    function getPageTitle($title) {
        global $db;
        global $lang;
        $sql_page = "select * from html_pages where pagecode='" . $title . "'";
        $exeQuery = $db->query($sql_page);
        if ($exeQuery->size() > 0) {
            $pagedata = $exeQuery->fetch();
            return $pagedata['page_title'];
        } else {
            if (isset($lang[$title]) && $lang[$title] != "") {
                return $lang[$title];
            } else {
                return $lang['SITE_TITLE'];
            }
        }
    }

    function getlatlon($address) {
        return $json = @json_decode(file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=" . rawurlencode($address)));
    }

    /* Added by amarat */

    function getStoreById($id) {
        global $db;
        $store_name = '';
        $sql_store = "select * from store where id='" . $id . "'";
        $exeQuery = $db->query($sql_store);
        if ($exeQuery->size() > 0) {
            $storedata = $exeQuery->fetch();
            $store_name = $storedata['store_name'];
        }
        return $store_name;
    }

    function getRadiousLatLon($lat, $lon, $d = 250, $unit = "K") {
        $r = 3959; // earth redious
        if (strtoupper($unit) == "K")
            $d = round($d / 1.60934);
        $arr = array();
        $latN = rad2deg(asin(sin(deg2rad($lat)) * cos($d / $r) + cos(deg2rad($lat)) * sin($d / $r) * cos(deg2rad(0))));
        $latS = rad2deg(asin(sin(deg2rad($lat)) * cos($d / $r) + cos(deg2rad($lat)) * sin($d / $r) * cos(deg2rad(180))));
        $lonE = rad2deg(deg2rad($lon) + atan2(sin(deg2rad(90)) * sin($d / $r) * cos(deg2rad($lat)), cos($d / $r) - sin(deg2rad($lat)) * sin(deg2rad($latN))));
        $lonW = rad2deg(deg2rad($lon) + atan2(sin(deg2rad(270)) * sin($d / $r) * cos(deg2rad($lat)), cos($d / $r) - sin(deg2rad($lat)) * sin(deg2rad($latN))));
        $arr['latN'] = $latN;
        $arr['latS'] = $latS;
        $arr['lonE'] = $lonE;
        $arr['lonW'] = $lonW;
        return $arr;
    }
    
    function getTimeInfo($oldTime, $newTime, $timeType) {

        global $db;

        $oldTime = date("Y-m-d H:i:s", strtotime($oldTime));

        $timeCalc = strtotime($newTime) - strtotime($oldTime);

        if ($timeType == "x") {

            if ($timeCalc > 60) {

                $timeType = "m";
            }

            if ($timeCalc > (60 * 60)) {

                $timeType = "h";
            }

            if ($timeCalc > (60 * 60 * 24)) {

                $timeType = "d";
            }
        }

        if ($timeType == "s") {
            
        }

        if ($timeType == "m") {



            if (round($timeCalc / 60) <= 1)
                $timeCalc = round($timeCalc / 60) . " min ago";
            else
                $timeCalc = round($timeCalc / 60) . " mins ago";
        }

        elseif ($timeType == "h") {



            if (round($timeCalc / 60 / 60) <= 1)
                $timeCalc = round($timeCalc / 60 / 60) . " hour ago";
            else
                $timeCalc = round($timeCalc / 60 / 60) . " hours ago";
        }

        elseif ($timeType == "d") {





            if (round($timeCalc / 60 / 60 / 24) <= 1)
                $timeCalc = round($timeCalc / 60 / 60 / 24) . " day ago";
            else
                $timeCalc = round($timeCalc / 60 / 60 / 24) . " days ago";
        }else {

            $timeCalc .= " sec ago";
        }

        return $timeCalc;
    }
    
    function navValue($schemeCode){
        global $db;
        $price=0;
        $sql    =   "select NAV from view_niivo_master where STARMF_SCHEME_CODE='".strtoupper($schemeCode)."'";
        $result =   $db->query($sql);
        if ($result->size()>0){
            $rs =   $result->fetch();
            $price  =   $rs['NAV'];
        }
        return $price;
        
    }

}

?>