<?php

class MandateStatus {
//    protected $SOAP_URL     =   "http://bsestarmfdemo.bseindia.com/MFUploadService/MFUploadService.svc/Basic";
//    protected $API_USER     =   "1707101";
//    protected $API_PASS     =   "@123456";
//    protected $API_MEMBERID =   "17071";
        var     $PASS_key       =   "";
//    protected $GET_PASSWORD_ACTION  =   '<wsa:Action>http://bsestarmfdemo.bseindia.com/2016/01/IMFUploadService/getPassword</wsa:Action>';
//    protected $UPLOAD_ACTION  =   '<wsa:Action>http://bsestarmfdemo.bseindia.com/2016/01/IMFUploadService/MFAPI</wsa:Action>';

    protected $SOAP_URL     =   "http://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Basic";
    
    protected $SOAP_URL_MENDATE     =   "https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure";
    
    protected $API_USER     =   API_USER;
    protected $API_PASS     =   API_PASS;
    protected $API_MEMBERID =   API_MEMBER;
    protected $GET_PASSWORD_ACTION  =   '<wsa:Action>http://www.bsestarmf.in/2016/01/IStarMFWebService/GetAccessToken</wsa:Action>';
    protected $MANDATE_ACTION   =   "<wsa:Action>http://www.bsestarmf.in/2016/01/IStarMFWebService/MandateDetails</wsa:Action>";




    private function soapHeader($tp) {
        
        $headerStr  ='<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="http://www.bsestarmf.in/2016/01/" xmlns:star="http://schemas.datacontract.org/2004/07/StarMFWebService"><soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">';
        $headerStr .= ($tp == "PASS") ? $this->GET_PASSWORD_ACTION : $this->MANDATE_ACTION;
        $headerStr .= '<wsa:To>http://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Basic</wsa:To>';
        $headerStr .= '</soap:Header><soap:Body>';
        return $headerStr;
    }
    
    private function  soapFooter(){
        $footerStr   ='</soap:Body></soap:Envelope>';
        return $footerStr;
    }
    
    
    function sendRequest($xml_string){
        $headers = array(
                        "Content-type: application/soap+xml;charset=\"utf-8\"",
                    ); //SOAPAction: your op URL
        
           $url = $this->SOAP_URL;
           
           $xml_post_string    =   $xml_string;
           
	$this->post_log($xml_string . "\n"); 
            // PHP cURL  for https connection with auth
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            // converting
            $response = curl_exec($ch); 
            curl_close($ch);
        
            $this->post_log($response . "\n"); 
//            $xml = new SimpleXMLElement($response);
//            $xml->registerXPathNamespace("soap", "http://www.w3.org/2003/05/soap-envelope");
//            $body = $xml->xpath("//soap:Body");
            return $response;
    }
    
    
    function __construct($passKey="ninehertz123") {
        $this->PASS_key =   $passKey;
        
    }
    
    function getAccessToken(){
        $str = self::soapHeader("PASS");
        $str    .=   "<ns:GetAccessToken><ns:Param>".         
                    "<star:MemberId>".$this->API_MEMBERID."</star:MemberId>".
                    "<star:PassKey>".$this->PASS_key."</star:PassKey>".
                    "<star:Password>".$this->API_PASS."</star:Password>".  
                    "<star:RequestType>Mandate</star:RequestType>".
                    "<star:UserId>".$this->API_USER."</star:UserId>".     
                    "</ns:Param></ns:GetAccessToken>";
        $str .= self::soapFooter();
        
//        echo $str;
//        exit;
        $resp = self::sendRequest($str);
		
        $body = self::parseXML($resp);
       
        if ($body['status'] == 100)           
			return $body['pass'];
    }
    
    function mandateDetail($clientCode,$mandateID){
        global $db;
        $fromDate  =   date("d/m/Y",  strtotime("-1 Month"));
        
        $toDate  =   date("d/m/Y",  strtotime("-1 Days"));
        $tempPass = self::getAccessToken();
        
        if ($tempPass != '') {
            $orderParam->ClientCode = $clientCode;
            $orderParam->EncryptedPassword = $tempPass;
            $orderParam->FromDate = $fromDate;
            $orderParam->MandateId = $mandateID;
            $orderParam->MemberCode = $this->API_MEMBERID;
            $orderParam->ToDate = $toDate;;
            
            $str = self::soapHeader("");
            $str .= " <ns:MandateDetails><ns:Param>";
            foreach ($orderParam as $k => $v) {
                $str .= "<star:" . $k . ">" . $v . "</star:" . $k . ">";
            }
            $str .= "</ns:Param></ns:MandateDetails>";
            $str .= self::soapFooter();          
           
            $resp = self::sendRequest($str);
			
			
           
            $resp   = self::parseXMLMandate($resp);
            
        }
        
        return $resp;
    }
    
 
    function parseXML($xmlString,$tp="MANDATE") {
        $resp = array();
        $p = xml_parser_create();
        xml_parse_into_struct($p, $xmlString, $vals, $index);
        xml_parser_free($p);
        // echo "\nVals array\n";
//         print_r($vals);
//         exit;


        foreach ($vals as $val) {
         
            if ($val['tag'] == "B:RESPONSESTRING") {
                $resp['pass'] = $val['value'];
            }
            if ($val['tag'] == "B:STATUS") {
                $resp['status'] = $val['value'];
            }
        }

        return $resp;
    }
    
    function parseXMLMandate($xmlString) {
        $resp = array();
        $p = xml_parser_create();
        xml_parse_into_struct($p, $xmlString, $vals, $index);
        xml_parser_free($p);
        // echo "\nVals array\n";
//         print_r($vals);
//         exit;


        foreach ($vals as $val) {
            switch ($val['tag']){
                case "B:MESSAGE";
                    $resp['msg'] = $val['value'];
                    break;
                case "B:STATUS";
                    if($val['level']==7)
                        $resp['mandate_status'] = $val['value'];
                    else
                        $resp['status'] = $val['value'];
                    break;
                case "B:AMOUNT";
                    $resp['amount'] = $val['value'];
                    break;
                case "B:APPROVEDDATE";
                    $resp['approve_date'] = $val['value'];
                    break;
                case "B:BANKACCNO";
                    $resp['account_no'] = $val['value'];
                    break;
                case "B:BANKBRANCH";
                    $resp['bank_branch'] = $val['value'];
                    break;
                case "B:CLIENTCODE";
                    $resp['client_code'] = $val['value'];
                    break;
                case "B:CLIENTNAME";
                    $resp['client_name'] = $val['value'];
                    break;
                case "B:MANDATEID";
                    $resp['mandateId'] = $val['value'];
                    break;
                case "B:MANDATETYPE";
                    $resp['mandate_type'] = $val['value'];
                    break;
                case "B:REGNDATE";
                    $resp['register_date'] = $val['value'];
                    break;
                case "B:REMARKS";
                    $resp['remarks'] = $val['value'];
                    break;
                case "B:UMRNNO";
                    $resp['umrn_no'] = $val['value'];
                    break;
            }
                
            
        }

        return $resp;
    }
    
	
	  //-------- log
   public function post_log($data)
    {
        $log_filename =  "../uploads/BSE_APIs";
        if (!file_exists($log_filename))
        {
            // create directory/folder uploads.
            mkdir($log_filename, 0777, true);
            chmod($log_filename, 0777);
        }

        if (!file_exists($log_filename . "/" . date('Y')))
        {
            // create directory/folder uploads.
            mkdir($log_filename . "/" . date('Y'), 0777, true);
            chmod($log_filename . "/" . date('Y'), 0777);
        }

        if (!file_exists($log_filename . "/" . date('Y') . "/" . date('M')))
        {
            // create directory/folder uploads.
            mkdir($log_filename . "/" . date('Y') . "/" . date('M'), 0777, true);
            chmod($log_filename . "/" . date('Y') . "/" . date('M'), 0777);
        }

        //$data = date("Y-m-d H:i:s")." - ".$data;
        $log_file_data = $log_filename . "/" . date('Y') . "/" . date('M') . '/' . date('d-M-Y') . '.log';
        file_put_contents($log_file_data, $data . "\n", FILE_APPEND);
        chmod($log_file_data, 0777);
    }
}
