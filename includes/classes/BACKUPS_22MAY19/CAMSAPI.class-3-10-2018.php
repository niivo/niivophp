<?php

class CAMSAPI {

    protected $SOAP_URL = "http://www.camskra.com/services_kycenquiry.asmx";
  
    protected $KRA_CODE = "PUNEETWS";
    protected $KRA_PASS = "kra$36369";
    var $PASS_key = "";
    
    

    private function soapHeader() {
        $headerStr = '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cam="https://camskra.com/">';

        $headerStr .= '<soap:Header /><soap:Body>';
        return $headerStr;
    }

    
    function parseXML($xmlString) {
        $resp = array();
        $p = xml_parser_create();
        xml_parse_into_struct($p, $xmlString, $vals, $index);
        xml_parser_free($p);
        $arr    =   array("CAMSKRA","CVLKRA","NDMLKRA","DOTEXKRA","KARVYKRA");
        // echo "\nVals array\n";
        // print_r($vals);


        foreach ($vals as $val) {
           if ($val['tag'] == "GETPASSWORDRESULT") {
                $resp['pass'] = $val['value'];
            }
            if ($val['tag'] == "APP_PAN_NO") {
                $resp['pancard_no'] = $val['value'];
            }
            if ($val['tag'] == "APP_NAME") {
                $resp['name'] = $val['value'];
            }
            if (in_array($val['tag'], $arr)){
                $resp['kra_detail'][$val['tag']]=$val['value'];
            }
        }

        return $resp;
    }
    
    function parseXMLDownload($xmlString) {
        $resp = array();
        $p = xml_parser_create();
        xml_parse_into_struct($p, $xmlString, $vals, $index);
        xml_parser_free($p);
        $arr    =   array("CAMSKRA","CVLKRA","NDMLKRA","DOTEXKRA","KARVYKRA");
        // echo "\nVals array\n";
        // print_r($vals);


        foreach ($vals as $val) {
        
//           if ($val['tag'] == "GETPASSWORDRESULT") {
//                $resp['pass'] = $val['value'];
//            }
//            if ($val['tag'] == "APP_PAN_NO") {
//                $resp['pancard_no'] = $val['value'];
//            }
//            if ($val['tag'] == "APP_NAME") {
//                $resp['name'] = $val['value'];
//            }
//            if (in_array($val['tag'], $arr)){
//                $resp['kra_detail'][$val['tag']]=$val['value'];
//            }
            if (is_null($val['value']))
                $resp[$val['tag']]="";
            else
                $resp[$val['tag']]=$val['value'];
        }

        return $resp;
    }

    private function soapFooter() {
        $footerStr = '</soap:Body></soap:Envelope>';
        return $footerStr;
    }

    function sendRequest($xml_string) {
        $headers = array(
            "Content-type: text/xml",
        ); //SOAPAction: your op URL

        $url = $soapUrl;
        $xml_post_string = $xml_string;
// print_r($xml_string);
//        exit;
        // PHP cURL  for https connection with auth
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $this->SOAP_URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // converting
        $response = curl_exec($ch);

        curl_close($ch);


        return $response;
    }

    function getPassword() {
        $str = self::soapHeader();
        $str .= "<cam:GetPassword>" .
                "<cam:PASSWORD>".$this->KRA_PASS. "</cam:PASSWORD>" .
                "<cam:PASSKEY>" . $this->PASS_key . "</cam:PASSKEY>" .
                "</cam:GetPassword>";
        $str .= self::soapFooter();
        
        $resp = self::sendRequest($str);
       
        
        $body = self::parseXML($resp);
        if ($body['pass'])           
			return $body['pass'];
    }

    function __construct($passKey = "ninehertz123") {
        $this->PASS_key = $passKey;
    }

    function makeOrderStringXml($orders) {
        $str = "";
        foreach ($orders as $v) {
            $str .= "<arr:string>" . $v . "</arr:string>";
        }
        return $str;
    }

    function panVerify($rs) {
        global $db;

        $status = false;
        $panInq = new stdClass();
        $summRec = new stdClass();

       try {
         
            $panCard = $rs['pancard_no'];

            $tempPass = self::getPassword();
           
            
         /*
          <cam:VerifyPANDetails>
         <!--Optional:-->
         <cam:InputXML>
            <APP_REQ_ROOT>
			<APP_PAN_INQ>
          * <APP_PAN_NO>AVAPG5054F</APP_PAN_NO>
			<APP_PAN_DOB></APP_PAN_DOB>
			<APP_IOP_FLG>IE</APP_IOP_FLG>
			<APP_POS_CODE>PS</APP_POS_CODE>
			</APP_PAN_INQ>
			<APP_SUMM_REC>
			<APP_OTHKRA_CODE>PUNEETWS</APP_OTHKRA_CODE>
			<APP_OTHKRA_BATCH>PANCHECK 28-09-2018</APP_OTHKRA_BATCH>
			<APP_REQ_DATE>28-09-2018 02:12:00</APP_REQ_DATE>
			<APP_TOTAL_REC>1</APP_TOTAL_REC>
			</APP_SUMM_REC>
		</APP_REQ_ROOT>
         </cam:InputXML>
          */
            if ($tempPass != '') {
                $panInq->APP_PAN_NO = $panCard;
                $panInq->APP_PAN_DOB = "";
                $panInq->APP_IOP_FLG = "IE";
                $panInq->APP_POS_CODE = "PS";
                
                $summRec->APP_OTHKRA_CODE = $this->KRA_CODE;
                $summRec->APP_OTHKRA_BATCH = "PANCHECK ".date("m-d-Y");
                $summRec->APP_REQ_DATE       = date("m-d-Y h:i:s");
                $summRec->APP_TOTAL_REC = "1";
                
      

                $str = self::soapHeader();
                $str    .= "<cam:VerifyPANDetails> <cam:InputXML><APP_REQ_ROOT>";
                $str    .=   "<APP_PAN_INQ>";
                foreach ($panInq as $k => $v) {
                    $str .= "<" . $k . ">" . $v . "</" . $k . ">";
                }
                $str.="</APP_PAN_INQ>";
                $str.="<APP_SUMM_REC>";
                
                foreach ($summRec as $k => $v) {
                    $str .= "<" . $k . ">" . $v . "</" . $k . ">";
                }
                $str    .="</APP_SUMM_REC>";
                $str    .= "</APP_REQ_ROOT></cam:InputXML>";
               
                $str    .= "<cam:USERNAME>".$this->KRA_CODE."</cam:USERNAME>";
                $str    .= "<cam:POSCOD>PS</cam:POSCOD>";
                $str    .= "<cam:PASSWORD>".$tempPass."</cam:PASSWORD>";
                $str    .= "<cam:PASSKEY>".$this->PASS_key."</cam:PASSKEY>";
                $str    .= "</cam:VerifyPANDetails>";
                
                $str    .= self::soapFooter();
                
              
                $resp = self::sendRequest($str);
				
                $body = self::parseXML($resp);
               
                $kycStatus  =   0;
              
                if (!empty($body['kra_detail'])) {
                    foreach($body['kra_detail'] as $val){
                        
                        if ($val=="01" || $val=="02" || $val=="03"){
                            $kycStatus  =   1;
                            break 1; 
                        }
                    }
                    if ($kycStatus)
                        $panData    =   self::getPanDetails($rs);
                    
                    $status="true";
                    $msg = "Success";
                    
                } else {
                    $msg = $body['pass'];
                }
                
                $data   =   array("pancard_no"=>$body['pancard_no'],"name"=>$body['name'],"kyc_status" => $kycStatus,"pan_data"=>$panData);
                $arr = array("status" => $status,"data"=>$data , "msg" => $msg);

                return $arr;
            }
        } catch (Exception $e) {
            return array("status" => $status, "msg" => SOMETHINGWRONG, "retry" => "1", "order_id" => "", "bse_order_no" => "", "error" => $e->getMessage());
       }
    }

    function getPanDetails($rs){
        
        global $db;

        $status = false;
        $panInq = new stdClass();
        $summRec = new stdClass();

       try {
         
            $panCard = $rs['pancard_no'];

            $tempPass = self::getPassword();
            if ($tempPass != '') {
                $panInq->APP_PAN_NO = $panCard;
                $panInq->APP_PAN_DOB = "";
                $panInq->APP_IOP_FLG = "IE";
                $panInq->APP_POS_CODE = "RS";
                
                $summRec->APP_OTHKRA_CODE = $this->KRA_CODE;
                $summRec->APP_OTHKRA_BATCH = "PANCHECK ".date("m-d-Y");
                $summRec->APP_REQ_DATE       = date("m-d-Y h:i:s");
                $summRec->APP_TOTAL_REC = "1";
                
      

                $str = self::soapHeader();
                $str    .= "<cam:DownloadPANDetails> <cam:InputXML><APP_REQ_ROOT>";
                $str    .=   "<APP_PAN_INQ>";
                foreach ($panInq as $k => $v) {
                    $str .= "<" . $k . ">" . $v . "</" . $k . ">";
                }
                $str.="</APP_PAN_INQ>";
                $str.="<APP_SUMM_REC>";
                
                foreach ($summRec as $k => $v) {
                    $str .= "<" . $k . ">" . $v . "</" . $k . ">";
                }
                $str    .="</APP_SUMM_REC>";
                $str    .= "</APP_REQ_ROOT></cam:InputXML>";
               
                $str    .= "<cam:USERNAME>".$this->KRA_CODE."</cam:USERNAME>";
                $str    .= "<cam:POSCOD>PS</cam:POSCOD>";
                $str    .= "<cam:PASSWORD>".$tempPass."</cam:PASSWORD>";
                $str    .= "<cam:PASSKEY>".$this->PASS_key."</cam:PASSKEY>";
                $str    .= "</cam:DownloadPANDetails>";
                
                $str    .= self::soapFooter();
                
              
                $resp = self::sendRequest($str);
				
                $body = self::parseXMLDownload($resp);
               
               
//                print_r($body);
//                exit;
                
//                $data   =   array("pancard_no"=>$body['pancard_no'],"name"=>$body['name'],"kyc_status" => $kycStatus);
//                $arr = array("status" => $status,"data"=>$data , "msg" => $msg);

                return $body;
            }
        } catch (Exception $e) {
            return array("status" => $status, "msg" => SOMETHINGWRONG, "retry" => "1", "order_id" => "", "bse_order_no" => "", "error" => $e->getMessage());
       }
    }
   
}
