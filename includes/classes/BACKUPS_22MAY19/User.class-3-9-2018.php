<?php

class User extends Models {

    protected static $table = "users";

    public static function isUser($userid) {
        global $db;
        return $db->query("select count(userid) as counter from users where userid='$userid' AND status = '1' ")->fetch()['counter'];
    }

    public static function getEmailUser($email) {
        global $db;
        return $db->query("select userid from users where email = '$email'")->fetch()['userid'];
    }

    public static function getMobileUser($mobile) {
        global $db;

        return $db->query("select userid from users where mobile = '$mobile'")->fetch()['userid'];
    }

    public static function totalUser($tp = false) {
        global $db;
        $count = 0;
        if ($tp == false)
            $sql = "select userid from users where 1 ";
        else {
            $today = date("Y-m-d", strtotime("today"));
            $sql = "SELECT userid FROM users where dtdate like '%" . $today . "%' ";
        }

        $res = $db->query($sql);
        if ($res->size() > 0) {
            $count = $res->size();
        }
        return $count;
    }

    public static function isActive() {
//        global $db;
//        $data = json_decode(str_replace("\\", "", urldecode($_GET['data'])));
//
//        $rs = (array) $data[0];
//        if ($rs["userid"] != "") {
//            $userid = $rs["userid"];
//        } else {
//            $rs = (array) $_POST;
//            $userid = $rs["userid"];
//        }
//        if ($userid != "") {
//            $isUser = self::isUser($userid);
//            if (!$isUser) {
//                echo json_encode(array("status" => "deactive", "msg" => "Your account is deactivated by Niivo team for review"));
//                exit;
//            }
//        }
    }

    public static function uploadUserImage($files, $userid, $type = '') {

        global $db;
        $isCreatedUser = ("../uploads/users");
        $isCreatedUserid = ("../uploads/users/$userid");



        if (!file_exists($isCreatedUser)) {
            @mkdir($isCreatedUser, 0777, true);
        }
        if (!file_exists($isCreatedUserid)) {
            @mkdir($isCreatedUserid, 0777, true);
        }



        $file_name = time() . str_replace(array(" ", "'", "\'", "_"), "", $_FILES['image']['name']);
        $IMAGE = new UPLOAD_IMAGE();
        $IMAGE->init($_FILES['image']['tmp_name']);
        move_uploaded_file($_FILES['image']['tmp_name'], "../uploads/users/$userid/$file_name");
        $file_namethumb = explode(".", $file_name);
        $file_nameExt = end($file_namethumb);
        $file_namethumb = $file_namethumb[0] . "-thumb" . ".$file_nameExt";
        $IMAGE->resizeImage(200, 200, 'crop');
        $IMAGE->saveImage("../uploads/users/$userid/$file_namethumb", 75);

        $main_path = "uploads/users/$userid/$file_name";
        $thumb_path = "uploads/users/$userid/$file_namethumb";

        unset($IMAGE);

        if ($type == "Q") {
            User::save(array("qr_image" => "uploads/users/$userid/$file_name"), "userid = $userid");
        } else {

            $sql = "update users set image='" . $main_path . "',thumb_image='" . $thumb_path . "' where userid='" . $userid . "'";
            $re = $db->query($sql);
        }


        return true;
    }

    function checkPhoneExist($mobile, $id) {
        global $db;
        $isValid = TRUE;

        $where = " AND 1 ";
        if ($mobile != '') {
            if ($id != '') {
                $where .= " AND userid != '" . $id . "' ";
            }
            $sql = " SELECT * FROM users WHERE " .
                    " mobile = '" . $mobile . "' " . $where;

            $result = $db->query($sql);
            if ($result->size() > 0) {
                return false;
            } else {
                return true;
            }
        }
        return true;
    }

    public static function sendOtp($rs) {

        global $db;

        $status = STATUSFALSE;
        $data = array();
        $userid = "";

        require_once 'sms.php';

        $mobile = addslashes(utf8_encode($rs->mobile));
        $password = addslashes(utf8_encode($rs->password));
        $default_lang = addslashes(utf8_encode($rs->default_lang));
        $otp = rand(1000, 9999);
        if ($mobile == '') {
            $msg = INVALIDCREDENTIAL;
        } else {
            $sql = "select * from users where mobile = '" . $mobile . "' ";
            $res = $db->query($sql);
            if ($res->size() == 0) {

                $sql1 = " INSERT INTO  users SET "
                        . " mobile    =   '" . $mobile . "',"
                        . " password    =   '" . md5($password) . "',"
                        . " otp    =   '" . $otp . "',"
                        . " default_lang    =   '" . $default_lang . "',"
                        . " dtdate = '" . nowDateTime() . "' "
                        . " ";
                $db->query($sql1);
                $data['otp'] = "$otp";
                $data['mobile'] = $mobile;

                $sms = sms($mobile, $otp);
                if ($sms == "true") {
                    $msg = NIIVOCODESENT;
                    $status = STATUSTRUE;
                } else {
                    $msg = MSGNOTSENT;
                }
            } else {
                $msg = MOBILEEXIST;
            }
        }


        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

    public static function otpVerify($rs) {

        global $db;

        $status = STATUSFALSE;
        $user_data = array();
        $userid = "";

        $mobile = addslashes(utf8_encode($rs->mobile));
        $otp = addslashes(utf8_encode($rs->otp));
        $device_token = addslashes(utf8_encode($rs->device_token));
        $default_lang = addslashes(utf8_encode($rs->default_lang));

        $sql = " SELECT * FROM users WHERE " .
                " mobile = '" . $mobile . "'  AND otp = '" . $otp . "' ";

        $result = $db->query($sql);
        if ($result->size() > 0) {
            $rs = $result->fetch();
            $userid = $rs['userid'];
            $sql1 = " UPDATE users SET "
                    . " status    =   '1',"
                    . " otp_verify    =   '1',"
                    . " default_lang    =   '" . $default_lang . "',"
                    . " last_login = '" . nowDateTime() . "' "
                    . " WHERE userid = '" . $userid . "'"
                    . " ";
            $db->query($sql1);

            $user_info = User::getUserinfo($userid);
            $user_data = $user_info['data'];

            UserDevices::remove("device_token='$device_token'");
            UserDevices::addDevice($userid, $device_token, "ANDROID");

            $status = STATUSTRUE;
            $msg = OTPVERIFIED;
        } else {
            $msg = ENTEROTP;
        }


        $arr = array("status" => $status, "msg" => $msg, "data" => $user_data);
        return $arr;
    }

    public static function login($rs) {

        global $db;

        $status = STATUSFALSE;
        $user_data = array();
        $userid = "";

        $mobile = addslashes(utf8_encode($rs->mobile));
        $password = addslashes(utf8_encode($rs->password));
        $device_token = addslashes(utf8_encode($rs->device_token));
        $default_lang = addslashes(utf8_encode($rs->default_lang));

        $sql = " SELECT * FROM users WHERE " .
                " mobile = '" . $mobile . "'  AND password = '" . md5($password) . "' ";

        $result = $db->query($sql);
        if ($result->size() > 0) {
            $rs = $result->fetch();
            $userid = $rs['userid'];
            $sql1 = " UPDATE users SET "
                    . " default_lang = '" . $default_lang . "', "
                    . " last_login = '" . nowDateTime() . "' "
                    . " WHERE userid = '" . $userid . "'"
                    . " ";
            $db->query($sql1);

            $user_info = User::getUserinfo($userid);
            $user_data = $user_info['data'];

            UserDevices::remove("device_token='$device_token'");
            UserDevices::addDevice($userid, $device_token, "ANDROID");

            $status = STATUSTRUE;
            $msg = LOGINSUCCESS;
        } else {
            $msg = MOBILEPASSNOMATCH;
        }


        $arr = array("status" => $status, "msg" => $msg, "data" => $user_data);
        return $arr;
    }

    public static function reSendOtp($rs) {
        global $db;

        $status = STATUSFALSE;
        $data = array();
        $userid = "";


        $mobile = addslashes(utf8_encode($rs->mobile));
        $otp = rand(1000, 9999);

        require_once 'sms.php';



        if ($mobile == '') {
            $msg = INVALIDCREDENTIAL;
        } else {
            $sql = "select * from users where mobile = '" . $mobile . "' ";
            $res = $db->query($sql);
            if ($res->size() > 0) {

                $rs = $res->fetch();

                $sql1 = " UPDATE users SET "
                        . " otp    =   '" . $otp . "', "
                        . " otp_verify    =   '0' "
                        . " WHERE userid = '" . $rs['userid'] . "' ";
                $db->query($sql1);

                $data['otp'] = "$otp";
                $data['mobile'] = $mobile;

                $sms = sms($mobile, $otp);
                if ($sms == "true") {
                    $msg = NIIVOCODESENT;
                    $status = STATUSTRUE;
                } else {
                    $msg = MSGNOTSENT;
                }
            } else {
                $msg = MOBILENOTEXIST;
            }
        }


        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

    public static function getUserinfo($userid) {
        global $db;

        $data = array();
        $status = STATUSFALSE;
        $userid = intval($userid);
        if ($userid == '' && $userid <= 0) {
            $msg = INVALIDCREDENTIAL;
//        } else if (User::isUser($userid) == false) {
//            $msg = NOUSER;
        } else {
            $sql = "SELECT * FROM users WHERE userid = " . $userid;
            $que = $db->query($sql);
            $row = $que->fetch();
            $row['wealthSourceCode'] = $row['wealth_source'];
            $row['occupationType'] = $row['occupation_type'];

            $data = $row;
            $msg = SUCCESS;
            $status = STATUSTRUE;
        }
        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

    function logout($rs) {
        global $db;
        $status = STATUSFALSE;

        $userid = intval($rs->userid);
        $device_token = addslashes(utf8_encode($rs->device_token));

        if ($userid != "") {
            UserDevices::remove("device_token='$device_token'");

            $msg = SUCCESS;
            $status = STATUSTRUE;
        } else {
            $msg = "Invalid credential";
        }

        $arr = array("msg" => $msg, "status" => $status);
        return $arr;
    }

    public static function changePassword($userid, $oldpass, $pass) {

        global $db;
        $status = STATUSFALSE;

        if ($userid == '' || $oldpass == '' || $pass == '') {
            $msg = INVALIDCREDENTIAL;
        } else if (User::isUser($userid) == false) {
            $msg = NOUSER;
        } else {

            $sql = " SELECT * FROM users WHERE " .
                    " userid    =   '" . $userid . "' " .
                    " and password  =   '" . md5($oldpass) . "'";
            $result1 = $db->query($sql);

            if ($result1->size() > 0) {

                $sql1 = " UPDATE users SET " .
                        " password    =   '" . md5($pass) . "'  WHERE " .
                        " userid  =   '" . $userid . "'";
                $db->query($sql1);
                $msg = PASSCHANGED;
                $status = "true";
            } else
                $msg = OLDPASSINCORRECT;
        }
        $arr = array("status" => $status, "msg" => $msg);
        return $arr;
    }

    public static function forgotPassword($rs) {
        global $db;

        $status = STATUSFALSE;
        $data = array();
        $userid = "";

        require_once 'sms.php';

        $mobile = addslashes(utf8_encode($rs->mobile));

        $otp = rand(1000, 9999);
        if ($mobile == '') {
            $msg = INVALIDCREDENTIAL;
        } else {
            $sql = "select * from users where mobile = '" . $mobile . "' ";
            $res = $db->query($sql);
            if ($res->size() > 0) {

                $rs = $res->fetch();

                $sql1 = " UPDATE users SET "
                        . " otp    =   '" . $otp . "' "
                        . " WHERE userid = '" . $rs['userid'] . "' ";
                $db->query($sql1);

                $data['otp'] = "$otp";
                $data['mobile'] = $mobile;


                $sms = sms($mobile, $otp);
                $msg = NIIVOCODESENT;
                $status = STATUSTRUE;
            } else {
                $msg = MOBILENOTEXIST;
            }
        }
        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

    public static function passwordChange($mobile, $pass, $otp) {

        global $db;
        $status = STATUSFALSE;

        if ($mobile == '' || $mobile == '') {
            $msg = INVALIDCREDENTIAL;
//        } else if (User::isUser($userid) == false) {
//            $msg = NOUSER;
        } else {

            $sql = " SELECT * FROM users WHERE " .
                    " mobile    =   '" . $mobile . "' " .
                    " and otp  =   '" . $otp . "'";
            $result1 = $db->query($sql);

            if ($result1->size() > 0) {

                $sql1 = " UPDATE users SET " .
                        " password    =   '" . md5($pass) . "',"
                        . " otp    =   '' "
                        . " WHERE " .
                        " mobile  =   '" . $mobile . "'";
                $db->query($sql1);
                $msg = PASSCHANGED;
                $status = "true";
            } else
                $msg = ENTEROTP;
        }
        $arr = array("status" => $status, "msg" => $msg);
        return $arr;
    }

    function updateProfile($rs) {
        global $db;
        $status = "false";
        $user_detail = array();
        $BSE = new BSE_API;

        $userid = $rs['userid'];

        if ($userid == '') {
            $msg = INVALIDCREDENTIAL;
        } else if (User::isUser($userid) == false) {
            $msg = NOUSER;
        } else {
            $str = "";
            $strFatca = "";

            $str .= $userid . "|SI|01|";
            $str .= $rs['occupation_code'] . "|";
            $str .= $rs['name'] . "|||";
            $str .= self::convertDate($rs['dob']) . "|";
            $str .= $rs['gender'] . "||";
            $str .= $rs['pencard'] . "||||P||||||";
            $str .= $rs['bank_type'] . "|";
            $str .= $rs['account_no'] . "||";
            $str .= $rs['ifsc'] . "|Y|||||||||||||||||||||";
            $str .= $rs['account_name'] . "|";
            $str .= $rs['add1'] . "|";
            $str .= $rs['add2'] . "|";
            $str .= $rs['add3'] . "|";
            $str .= $rs['city'] . "|";
            $str .= $rs['state_code'] . "|";
            $str .= $rs['pin'] . "|";
            $str .= $rs['country'] . "|||||";
            $str .= $rs['email'] . "|P|01|||||||||||||||";
            $str .= $rs['mobile'] . "";

            $strFatca .= $rs['pencard'] . "||";
            $strFatca .= $rs['name'] . "|";
            $strFatca .= "|||";
            $strFatca .= "01|E|1|" . $rs['city'];
            $strFatca .= "|IN|IN|";
            $strFatca .= $rs['pencard'] . "|C||||||||||";
            $strFatca .= $rs['wealthSourceCode'] . "||";
            $strFatca .= $rs['anualIncomeCode'] . "|||N|";
            $strFatca .= $rs['occupation_code'] . "|";
            $strFatca .= $rs['occupationType'] . "|||||||||||B|N||||||||||||||||||||||||||Y|N|";
            $strFatca .= $rs['aadhar_no'] . "|N|";
            $strFatca .= $_SERVER['REMOTE_ADDR'] . "#" . date("d-m-y") . ":" . date("h:i") . "||";
            //  echo $str;
            //   exit;
            try {
                $resp = $BSE->uploadClient($str);

                if ($resp['status'] == 'false') {
                    return array("status" => $status, "msg" => $resp['msg'], "retry" => "1", "data" => $user_detail);
                }
                $resp = $BSE->FATCAuploadClient($strFatca);
                if ($resp['status'] == 'false') {
                    return array("status" => $status, "msg" => $resp['msg'], "retry" => "1", "data" => $user_detail);
                }
            } catch (Exception $e) {
                return array("status" => $status, "msg" => SOMETHINGWRONG, "retry" => "1", "data" => $user_detail);
            }


            if ($resp['status'] == "true") {
                $sql = "update users set "
                        . "name='" . $rs['name'] . "',"
                        . "dob='" . $rs['dob'] . "',"
                        . "mobile='" . $rs['mobile'] . "',"
                        . "gender='" . $rs['gender'] . "',"
                        . "occupation_code='" . $rs['occupation_code'] . "',"
                        . "occupation='" . $rs['occupation'] . "',"
                        . "address1='" . $rs['add1'] . "',"
                        . "address2='" . $rs['add2'] . "',"
                        . "address3='" . $rs['add3'] . "',"
                        . "state='" . $rs['state'] . "',"
                        . "state_code='" . $rs['state_code'] . "',"
                        . "city='" . $rs['city'] . "',"
                        . "pin='" . $rs['pin'] . "',"
                        . "country='" . $rs['country'] . "',"
                        . "pencard='" . $rs['pencard'] . "',"
                        . "bank_ac_type='" . $rs['bank_type'] . "',"
                        . "account_no='" . $rs['account_no'] . "',"
                        . "ifsc='" . $rs['ifsc'] . "',"
                        . "bank_name='" . $rs['bank_name'] . "',"
                        . "bank_id='" . $rs['bank_id'] . "',"
                        . "account_name='" . $rs['account_name'] . "',"
                        . "father_name='" . $rs['father_name'] . "',"
                        . "mother_name='" . $rs['mother_name'] . "',"
                        . "wealth_source='" . $rs['wealthSourceCode'] . "',"
                        . "anualIncomeCode='" . $rs['anualIncomeCode'] . "',"
                        . "occupation_type='" . $rs['occupationType'] . "',"
                        . "aadhar_no='" . $rs['aadhar_no'] . "',"
                        . "email='" . $rs['email'] . "',"
                        . "nominee_name='" . $rs['nominee_name'] . "',"
                        . "nominee_dob='" . $rs['nominee_dob'] . "',"
                        . "nominee_relationship='" . $rs['nominee_relationship'] . "',"
                        . "nominee_address='" . $rs['nominee_address'] . "',"
                        . "pancard_verify='1',"
                        . "aadhar_verify='" . $rs['is_aadhar_verify'] . "'"
                        . "where userid='" . $userid . "'";
                $res = $db->query($sql);

                if ($_FILES['image']['name'] != "") {
                    if (!User::uploadUserImage($image, $userid)) {
                        $msg = "User added succesfully but media image not uploaded";
                    }
                }
                $user_info = User::getUserinfo($userid);
                $user_detail = $user_info['data'];
                $status = "true";
                $msg = SUCCESS;
            } else {
                $msg = $resp['msg'];
            }
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $user_detail, "retry" => "0");
        return $arr;
    }

    function convertDate($dt) {
        $dtArr = explode("-", $dt);
        $date = $dtArr[2] . "/" . $dtArr[1] . "/" . $dtArr[0];

        return $date;
    }

    function convertDateHyphen($dt) {
        $dtArr = explode("-", $dt);
        $date = $dtArr[2] . "-" . $dtArr[1] . "-" . $dtArr[0];

        return $date;
    }

    public static function totalUserOrder($userid) {
        global $db;
        $count = 0;
        $sql = "SELECT * FROM orders where userid = '" . $userid . "' ";
        $res = $db->query($sql);
        if ($res->size() > 0) {
            $count = $res->size();
        }
        return $count;
    }

    public static function withdrawOtp($userid) {

        global $db;

        $status = STATUSFALSE;
        $data = array();

        require_once 'sms.php';

        if ($userid == '') {
            $msg = INVALIDCREDENTIAL;
        } else {
            $sql = "SELECT * FROM users WHERE userid = '" . $userid . "' ";
            $que = $db->query($sql);
            $row = $que->fetch();
            if ($que->size() > 0) {

                $mobile = $row['mobile'];
                $otp = rand(1000, 9999);
                $sms = sms($mobile, $otp);
                if ($sms == "true") {
                    $msg = NIIVOCODESENT;
                    $status = STATUSTRUE;
                } else {
                    $msg = MSGNOTSENT;
                }
            } else {
                $msg = NOUSER;
            }
        }


        $arr = array("status" => $status, "msg" => $msg, "otp" => $otp);
        return $arr;
    }

    function jsonUpload($rs) {
        global $db;
        $status = "false";
        $userid = $rs['userid'];
        $jsonFile = $_FILES['json'];

        $isCreatedUser = ("../uploads/json/");
        $filePath = ("uploads/json/");

        if (!file_exists($isCreatedUser)) {
            @mkdir($isCreatedUser, 0777, true);
        }

        if ($jsonFile['tmp_name'] == '' or $userid == "") {
            $msg = INVALIDREQUEST;
        } else {
            $fileName = $userid . ".json";
            move_uploaded_file($jsonFile['tmp_name'], $isCreatedUser . $fileName);
            $db->query("update users set aof_json='" . $filePath . $fileName . "', image_upload_status='PENDING' where userid=" . $userid);

            $input_file = "/var/www/html/" . $filePath . $fileName;

            $nowdate = date("dmY");
            $member_code = API_MEMBER;
            $client_code = $userid;

            $output_file = "/var/www/html/".$filePath."/" . $member_code . $client_code . $nowdate . ".tiff";
            move_uploaded_file($output_file, $isCreatedUser . $fileName);
            //echo "/niivo/bin/niivo-image-utils.sh $input_file $output_file";
			//exit; 
            $result = shell_exec("/niivo/bin/niivo-image-utils.sh $input_file $output_file");
			//echo $result; 
			//exit;
            if ($result == 0) {
                $IU = new FileUpload;
                $arr = $IU->uploadFile($client_code, $output_file);
                $status = "true";
                $msg = SUCCESS;
            } else {
                $msg = SOMEERROR;
            }
        }
        $arr = array("status" => $status, "msg" => $msg, "data" => $arr);
        return $arr;
    }

    function verifyIFSC($ifscCode) {
        global $db;
        $data1 = array();
        $status = "false";
        if (empty($ifscCode)) {
            return array("status" => $status, "msg" => "Blank IFSC code");
        } else {
            $data = file_get_contents("https://ifsc.razorpay.com/" . strtoupper($ifscCode));
            $json = json_decode($data);
            if (count($json) <= 0)
                return array("status" => $status, "msg" => INVALIDIFSC);
            else {
                $sql = "select bank_id,razorpay_name as name from bank_list where UPPER(razorpay_name)='" . strtoupper($json->BANK) . "'";
                $result = $db->query($sql);
                if ($result->size() > 0) {
                    // print_r($json);
                    $rs = $result->fetch();
                    $rs['branch'] = $json->BRANCH;
                    $rs['address'] = $json->ADDRESS;
                    $rs['city'] = $json->CITY;
                    $rs['dist'] = $json->DISTRICT;
                    $rs['state'] = $json->STATE;
                    $rs['contact'] = $json->CONTACT;
                    $rs['ifsc'] = $json->IFSC;
                    $data1 = $rs;
                    $status = "true";
                    $msg = SUCCESS;
                } else {
                    $msg = NOBANKFOUND;
                }
            }
        }
        $arr = array("status" => $status, "msg" => $msg, "data" => $data1);
        return $arr;
    }

    public static function xmlUpload($clintCode, $str, $type) {
        $isCreatedUserid = ("../uploads/request/");
        if (!file_exists($isCreatedUserid)) {
            @mkdir($isCreatedUserid, 0777, true);
        }

        $xml_file = $type . "_" . time() . "_" . $clintCode . ".xml";
        $f = fopen("$isCreatedUserid/$xml_file", "w");
        fwrite($f, $str);
        fclose($f);
        return $xml_file;
    }

    public static function setLanguage($userid, $default_lang) {

        global $db;
        $status = STATUSFALSE;

        if ($userid == '' || $userid <= 0) {
            $msg = INVALIDCREDENTIAL;
        } else {

            $sql = " SELECT * FROM users WHERE " .
                    " userid    =   '" . $userid . "' "
                    . "";
            $result1 = $db->query($sql);
            if ($result1->size() > 0) {
                $sql1 = " UPDATE users SET " .
                        " default_lang    =   '" . $default_lang . "' "
                        . " WHERE " .
                        " userid    =   '" . $userid . "' ";
                $db->query($sql1);
                $msg = LANGUAGECHANGED;
                $status = "true";
            } else
                $msg = NOUSER;
        }
        $arr = array("status" => $status, "msg" => $msg);
        return $arr;
    }

}
