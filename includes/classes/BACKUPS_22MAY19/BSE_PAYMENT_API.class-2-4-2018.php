<?php

class BSE_PAYMENT_API {

    protected $SOAP_URL = "http://www.bsestarmf.in/StarMFPaymentGatewayService/StarMFPaymentGatewayService.svc/Basic";
//    protected $API_USER = "1438201";
//    protected $API_MEMBER = "14382";
//    protected $API_PASS = "@123456";
//    
    protected $API_USER = API_USER;
    protected $API_PASS = API_PASS;
    protected $API_MEMBER = API_MEMBER;
    var $PASS_key = "";
    protected $GET_PASSWORD_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://tempuri.org/IStarMFPaymentGatewayService/GetPassword</wsa:Action>';
    protected $PAYMNET_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://tempuri.org/IStarMFPaymentGatewayService/PaymentGatewayAPI</wsa:Action>';

    private function soapHeaderPassword($tp) {
        $headerStr = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:tem="http://tempuri.org/" xmlns:star="http://schemas.datacontract.org/2004/07/StarMFPaymentGatewayService">';
        $headerStr .= '<soap:Header>';

        $headerStr .= ($tp == "PASS") ? $this->GET_PASSWORD_ACTION : $this->PAYMNET_ACTION;
        $headerStr .= '<wsa:To xmlns:wsa="http://www.w3.org/2005/08/addressing">http://www.bsestarmf.in/StarMFPaymentGatewayService/StarMFPaymentGatewayService.svc/Basic</wsa:To>';
        $headerStr .= '</soap:Header><soap:Body>';
        return $headerStr;
    }

    private function soapHeaderPayment($tp) {
        $headerStr = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:tem="http://tempuri.org/" xmlns:star="http://schemas.datacontract.org/2004/07/StarMFPaymentGatewayService" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays">';
        $headerStr .= '<soap:Header>';

        $headerStr .= ($tp == "PASS") ? $this->GET_PASSWORD_ACTION : $this->PAYMNET_ACTION;
        $headerStr .= '<wsa:To xmlns:wsa="http://www.w3.org/2005/08/addressing">http://www.bsestarmf.in/StarMFPaymentGatewayService/StarMFPaymentGatewayService.svc/Basic</wsa:To>';
        $headerStr .= '</soap:Header><soap:Body>';
        return $headerStr;
    }

    function parseXML($xmlString) {
        $resp = array();
        $p = xml_parser_create();
        xml_parse_into_struct($p, $xmlString, $vals, $index);
        xml_parser_free($p);
        // echo "\nVals array\n";
        // print_r($vals);
		


        foreach ($vals as $val) {
            if ($val['tag'] == "B:RESPONSESTRING") {
                $resp['pass'] = $val['value'];
            }
            if ($val['tag'] == "B:STATUS") {
                $resp['status'] = $val['value'];
            }
        }

        return $resp;
    }

    private function soapFooter() {
        $footerStr = '</soap:Body></soap:Envelope>';
        return $footerStr;
    }

    function sendRequest($xml_string) {
        $headers = array(
            "Content-type: application/soap+xml;charset=\"utf-8\"",
        ); //SOAPAction: your op URL

        $url = $soapUrl;
        $xml_post_string = $xml_string;
// print_r($xml_string);
//        exit;
        // PHP cURL  for https connection with auth
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $this->SOAP_URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // converting
        $response = curl_exec($ch);

        curl_close($ch);


        return $response;
    }

    function getPassword() {
        $str = self::soapHeaderPassword("PASS");
        $str .= "<tem:GetPassword>" .
                "<tem:Param>" .
                "<star:MemberId>" . $this->API_MEMBER . "</star:MemberId>" .
                "<star:PassKey>" . $this->PASS_key . "</star:PassKey>" .
                "<star:Password>" . $this->API_PASS . "</star:Password>" .
                "<star:UserId>" . $this->API_USER . "</star:UserId>" .
                "</tem:Param>" .
                "</tem:GetPassword>";
        $str .= self::soapFooter();
        $resp = self::sendRequest($str);
        $body = self::parseXML($resp);
        if ($body['status'] == 100)           
			return $body['pass'];
    }

    function __construct($passKey = "ninehertz123") {
        $this->PASS_key = $passKey;
    }

    function makeOrderStringXml($orders) {
        $str = "";
        foreach ($orders as $v) {
            $str .= "<arr:string>" . $v . "</arr:string>";
        }
        return $str;
    }

    function payOrder($rs, $order_id) {
        global $db;

        $status = false;
        $orderType = "NEW";
//        $localOrder = self::saveLocalOrder($rs);
//        if ($localOrder['stauts'] === false)
//            return array("status" => $status, "order_no" => $localOrder['id'], "msg" => $localOrder['msg'], "retry" => "1",);

        $orderParam = new stdClass();
       try {

            $transNo = time();
            $clintCode = $rs['userid'];

            $user_data = User::getUserinfo($clintCode);

            $user_info = $user_data['data'];
            $account_no = $user_info['account_no'];
            $ifsc = strtoupper($user_info['ifsc']);
            $BankID = strtoupper($user_info['bank_id']);
            $orders = explode(",", $rs['orders']);
            $amount = $rs['amount'];

            $paymemt_type = $rs['paymemt_type'];
            $sip_registration_no = $rs['sip_registration_no'];
			$due_date   =   $rs['due_date'];

            if ($paymemt_type == 'SIP' && $sip_registration_no != '') {

                $bse_order_no = BSE_PAYMENT_API::sipOrderDetail($sip_registration_no,$due_date);
				
		        $orders = array($bse_order_no);
                if ($bse_order_no == '') {
                    return array("status" => $status, "msg" => "Oops, something went wrong.\n Please try again.", "retry" => "1", "order_id" => "", "bse_order_no" => "", "error" => "Bse order number not found.");
                }
            }
            $tempPass = self::getPassword();
			//print_r($bse_order_no);
			//exit;
            if ($tempPass != '') {
                $orderParam->AccNo = $account_no;
                $orderParam->BankID = $BankID;
                $orderParam->ClientCode = $clintCode;
                $orderParam->EncryptedPassword = $tempPass;
                $orderParam->IFSC = $ifsc;
                $orderParam->LogOutURL = "http://172.104.187.141/services/thank-you.php";
                $orderParam->MemberCode = $this->API_MEMBER;
                $orderParam->Mode = "DIRECT";
                $orderParam->Orders = self::makeOrderStringXml($orders);
                $orderParam->TotalAmount = $amount;

                $str = self::soapHeaderPayment("");
                $str .= "<tem:PaymentGatewayAPI> <tem:Param>";
                foreach ($orderParam as $k => $v) {
                    $str .= "<star:" . $k . ">" . $v . "</star:" . $k . ">";
                }
                $str .= " </tem:Param></tem:PaymentGatewayAPI>";
                $str .= self::soapFooter();
							
                $resp = self::sendRequest($str);
				
                $body = self::parseXML($resp);
				

                if ($body['status'] == 100) {
                    $msg = $body['pass'];
                    $status = true;
                    self::updateLocalOrder($orders, -1, "");
                } else {
                    $msg = $body['pass'];
                }
                $arr = array("status" => $status, "retry" => "0", "msg" => $msg);

                return $arr;
            }
        } catch (Exception $e) {
            return array("status" => $status, "msg" => "Oops, something went wrong.\n Please try again.", "retry" => "1", "order_id" => "", "bse_order_no" => "", "error" => $e->getMessage());
       }
    }

    function updateLocalOrder($orders, $order_status, $reason = "") {
        global $db;
        $status = false;
        foreach ($orders as $order) {
            $sql = "update orders set " .
                    "payment_status=" . $order_status . "," .
                    "failed_reson='" . $reason . "' where bse_order_no=" . $order;

            $result = $db->query($sql);
        }
        $id = $result->insertID();
        return array("msg" => "Success", "status" => true, "id" => $id, "order_id" => $orderID);
    }

    public static function sipOrderDetail($sip_registration_no,$due_date) {
        global $db;

        require_once 'Child_Order.class.php';
        $BSE_CC = new Child_Order();

        $sql = "SELECT * FROM orders WHERE sip_registration_no = '" . $sip_registration_no . "' ";
		

        $que = $db->query($sql);

        if ($que->size() > 0) {
            $row = $que->fetch();
            $id = $row['id'];
            $send_data = array("order_id" => $id,"due_date"=>$due_date);
            $arr = $BSE_CC->orderDetail($send_data);

            if ($arr['resp']['order_no'] != '' && $arr['resp']['order_no'] != null) {
                $resp = $arr['resp'];
                $bse_order_no = $resp['order_no'];
            }
        }

        return $bse_order_no;
    }

}
