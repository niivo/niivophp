<?php

class Intermediate extends Models {

    public static function equityFunds($sub_section) {
        global $db;

        $data = array();
        $status = "false";
        if ($sub_section == '') {
            $msg = INVALIDCREDENTIAL;
        } else {
            if ($sub_section == 'Large') {
                $sql = "SELECT * FROM view_niivo_master WHERE FUND_CLASS='Equity' AND FUND_CATEGORIZATION='Large Cap' AND SHARE_CLASS='RG' order by SCHEME_NAME ";
            } else if ($sub_section == 'Mid') {
                $sql = "SELECT * FROM view_niivo_master WHERE FUND_CLASS='Equity' AND FUND_CATEGORIZATION='Small & Mid Cap' AND SHARE_CLASS='RG' order by SCHEME_NAME";
            } else if ($sub_section == 'Thematic') {
                $sql = "SELECT * FROM view_niivo_master WHERE FUND_CLASS='Equity' AND FUND_CATEGORIZATION='Thematic Fund' AND SHARE_CLASS='RG' order by SCHEME_NAME";
            } else if ($sub_section == 'Broad') {
                $sql = "SELECT * FROM view_niivo_master WHERE FUND_CLASS='Equity' AND FUND_CATEGORIZATION='Broad Market Fund' AND SHARE_CLASS='RG' order by SCHEME_NAME";
            }


            $que = $db->query($sql);
            if ($que->size() > 0) {
                while ($row = $que->fetch()) {
                    $row['STARMF_SCHEME_NAME'] = utf8_encode($row['STARMF_SCHEME_NAME']);
                    $state[] = $row;
                }
                $data = $state;
                $msg = SUCCESS;
                $status = "true";
            } else {
                $msg = NORECORD;
            }
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

    public static function debtFunds() {
        global $db;

        $data = array();
        $status = "false";

        $sql = "SELECT * FROM view_niivo_master WHERE FUND_CLASS='Fixed Income' AND FUND_CATEGORIZATION='Debt Funds' AND SHARE_CLASS='RG' order by SCHEME_NAME";
        $que = $db->query($sql);

        if ($que->size() > 0) {
            while ($row = $que->fetch()) {
                $row['STARMF_SCHEME_NAME'] = utf8_encode($row['STARMF_SCHEME_NAME']);
                $state[] = $row;
            }
            $data = $state;
            $msg = SUCCESS;
            $status = "true";
        } else {
            $msg = NORECORD;
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data);

        return $arr;
    }

    public static function balancedFunds($sub_section) {
        global $db;

        $data = array();
        $status = "false";
        if ($sub_section == '') {
            $msg = INVALIDCREDENTIAL;
        } else {
            if ($sub_section == 'Equity') {
                $sql = "SELECT * FROM view_niivo_master WHERE FUND_CLASS='Equity' AND FUND_CATEGORIZATION='Balanced Fund' AND SHARE_CLASS='RG' order by SCHEME_NAME";
            } else if ($sub_section == 'Debt') {
                $sql = "SELECT * FROM view_niivo_master WHERE FUND_CLASS IN ('Fixed Income','Mixed Allocation') AND FUND_CATEGORIZATION='Balanced Fund' AND SHARE_CLASS='RG' order by SCHEME_NAME";
            }

            $que = $db->query($sql);
            if ($que->size() > 0) {
                while ($row = $que->fetch()) {
                    $row['STARMF_SCHEME_NAME'] = utf8_encode($row['STARMF_SCHEME_NAME']);
                    $state[] = $row;
                }
                $data = $state;
                $msg = SUCCESS;
                $status = "true";
            } else {
                $msg = NORECORD;
            }
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

    public static function taxSavingFunds() {
        global $db;

        $data = array();
        $status = "false";

        $sql = "SELECT * FROM view_niivo_master WHERE FUND_CLASS='Equity' AND FUND_CATEGORIZATION='Tax Saving' AND SHARE_CLASS='RG' order by SCHEME_NAME";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            while ($row = $que->fetch()) {
                $row['STARMF_SCHEME_NAME'] = utf8_encode($row['STARMF_SCHEME_NAME']);
                $state[] = $row;
            }
            $data = $state;
            $msg = SUCCESS;
            $status = "true";
        } else {
            $msg = NORECORD;
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

}
