<?php

class Verifiation extends Models {

    protected static $table = "users";

    public static function isVerifiation($userid) {
        global $db;
        return $db->query("select count(userid) as counter from users where userid='$userid' AND status = '1' ")->fetch()['counter'];
    }

    public static function isPanCardAdded($userid) {
        global $db;
        return $db->query("select count(userid) as counter from user_pancard_details where userid='$userid' ")->fetch()['counter'];
    }

    public static function isAddressAdded($userid) {
        global $db;
        return $db->query("select count(userid) as counter from user_address_details where userid='$userid' ")->fetch()['counter'];
    }

    public static function isIdentityAdded($userid) {
        global $db;
        return $db->query("select count(userid) as counter from user_identity_details where userid='$userid' ")->fetch()['counter'];
    }

    public static function isNomineeAdded($userid) {
        global $db;
        return $db->query("select count(userid) as counter from user_nominee_details where userid='$userid' ")->fetch()['counter'];
    }

    public static function isBankAdded($userid) {
        global $db;
        return $db->query("select count(userid) as counter from user_bank_details where userid='$userid' ")->fetch()['counter'];
    }

    public static function uploadPancardImage($files, $userid, $type = '') {

        global $db;
        $isCreatedVerifiation = ("../uploads/users");
        $isCreatedVerifiationid = ("../uploads/users/$userid");
        $isCreatedPancard = ("../uploads/users/$userid/pancard");


        if (!file_exists($isCreatedVerifiation)) {
            @mkdir($isCreatedVerifiation, 0777, true);
        }
        if (!file_exists($isCreatedVerifiationid)) {
            @mkdir($isCreatedVerifiationid, 0777, true);
        }
        if (!file_exists($isCreatedPancard)) {
            @mkdir($isCreatedPancard, 0777, true);
        }

        $file_name = randomcode(5) . time() . str_replace(array(" ", "'", "\'", "_"), "", $_FILES['pancard_image']['name']);
        $IMAGE = new UPLOAD_IMAGE();
        $IMAGE->init($_FILES['pancard_image']['tmp_name']);
        move_uploaded_file($_FILES['pancard_image']['tmp_name'], "../uploads/users/$userid/pancard/$file_name");

        $main_path = "uploads/users/$userid/pancard/$file_name";

        unset($IMAGE);

        $sql = "UPDATE users SET "
                . " pancard_image = 'uploads/users/$userid/pancard/$file_name' "
                . " WHERE userid =  '" . $userid . "' "
                . " ";
        $result = $db->query($sql);

        return true;
    }

    public static function uploadAdressFrontImage($files, $userid, $type = '') {

        global $db;
        $isCreatedVerifiation = ("../uploads/users");
        $isCreatedVerifiationid = ("../uploads/users/$userid");
        $isCreatedaddress = ("../uploads/users/$userid/address");


        if (!file_exists($isCreatedVerifiation)) {
            @mkdir($isCreatedVerifiation, 0777, true);
        }
        if (!file_exists($isCreatedVerifiationid)) {
            @mkdir($isCreatedVerifiationid, 0777, true);
        }
        if (!file_exists($isCreatedaddress)) {
            @mkdir($isCreatedaddress, 0777, true);
        }

        $file_name = randomcode(5) . time() . str_replace(array(" ", "'", "\'", "_"), "", $_FILES['front_image']['name']);
        $IMAGE = new UPLOAD_IMAGE();
        $IMAGE->init($_FILES['front_image']['tmp_name']);
        move_uploaded_file($_FILES['front_image']['tmp_name'], "../uploads/users/$userid/address/$file_name");

        $main_path = "uploads/users/$userid/address/$file_name";

        unset($IMAGE);

        $sql = "UPDATE user_address_details SET "
                . " front_image = 'uploads/users/$userid/address/$file_name' "
                . " WHERE userid =  '" . $userid . "' "
                . " ";
        $result = $db->query($sql);
        return true;
    }

    public static function uploadAdressBackImage($files, $userid, $type = '') {

        global $db;
        $isCreatedVerifiation = ("../uploads/users");
        $isCreatedVerifiationid = ("../uploads/users/$userid");
        $isCreatedaddress = ("../uploads/users/$userid/address");


        if (!file_exists($isCreatedVerifiation)) {
            @mkdir($isCreatedVerifiation, 0777, true);
        }
        if (!file_exists($isCreatedVerifiationid)) {
            @mkdir($isCreatedVerifiationid, 0777, true);
        }
        if (!file_exists($isCreatedaddress)) {
            @mkdir($isCreatedaddress, 0777, true);
        }

        $file_name = randomcode(5) . time() . str_replace(array(" ", "'", "\'", "_"), "", $_FILES['back_image']['name']);
        $IMAGE = new UPLOAD_IMAGE();
        $IMAGE->init($_FILES['back_image']['tmp_name']);
        move_uploaded_file($_FILES['back_image']['tmp_name'], "../uploads/users/$userid/address/$file_name");

        $main_path = "uploads/users/$userid/address/$file_name";

        unset($IMAGE);

        $sql = "UPDATE user_address_details SET "
                . " back_image = 'uploads/users/$userid/address/$file_name' "
                . " WHERE userid =  '" . $userid . "' "
                . " ";
        $result = $db->query($sql);
        return true;
    }

    public static function uploadIdentityPhoto($files, $userid, $type = '') {

        global $db;
        $isCreatedVerifiation = ("../uploads/users");
        $isCreatedVerifiationid = ("../uploads/users/$userid");
        $isCreatedidentity = ("../uploads/users/$userid/identity");


        if (!file_exists($isCreatedVerifiation)) {
            @mkdir($isCreatedVerifiation, 0777, true);
        }
        if (!file_exists($isCreatedVerifiationid)) {
            @mkdir($isCreatedVerifiationid, 0777, true);
        }
        if (!file_exists($isCreatedidentity)) {
            @mkdir($isCreatedidentity, 0777, true);
        }

        $file_name = randomcode(5) . time() . str_replace(array(" ", "'", "\'", "_"), "", $_FILES['photo']['name']);
        $IMAGE = new UPLOAD_IMAGE();
        $IMAGE->init($_FILES['photo']['tmp_name']);
        move_uploaded_file($_FILES['photo']['tmp_name'], "../uploads/users/$userid/identity/$file_name");

        $main_path = "uploads/users/$userid/identity/$file_name";

        unset($IMAGE);

        $sql = "UPDATE user_identity_details SET "
                . " photo = 'uploads/users/$userid/identity/$file_name' "
                . " WHERE userid =  '" . $userid . "' "
                . " ";
        $result = $db->query($sql);
        return true;
    }

    public static function uploadChequelLeafPhoto($files, $userid, $type = '') {

        global $db;
        $isCreatedVerifiation = ("../uploads/users");
        $isCreatedVerifiationid = ("../uploads/users/$userid");
        $isCreatedbank = ("../uploads/users/$userid/bank");


        if (!file_exists($isCreatedVerifiation)) {
            @mkdir($isCreatedVerifiation, 0777, true);
        }
        if (!file_exists($isCreatedVerifiationid)) {
            @mkdir($isCreatedVerifiationid, 0777, true);
        }
        if (!file_exists($isCreatedbank)) {
            @mkdir($isCreatedbank, 0777, true);
        }

        $file_name = randomcode(5) . time() . str_replace(array(" ", "'", "\'", "_"), "", $_FILES['cheque_leaf']['name']);
        $IMAGE = new UPLOAD_IMAGE();
        $IMAGE->init($_FILES['cheque_leaf']['tmp_name']);
        move_uploaded_file($_FILES['cheque_leaf']['tmp_name'], "../uploads/users/$userid/bank/$file_name");

        $main_path = "uploads/users/$userid/bank/$file_name";

        unset($IMAGE);

        $sql = "UPDATE user_bank_details SET "
                . " cheque_leaf = 'uploads/users/$userid/bank/$file_name' "
                . " WHERE userid =  '" . $userid . "' "
                . " ";
        $result = $db->query($sql);
        return true;
    }

    public static function uploadDigitalSignaturePhoto($files, $userid, $type = '') {

        global $db;
        $isCreatedVerifiation = ("../uploads/users");
        $isCreatedVerifiationid = ("../uploads/users/$userid");
        $isCreatedbank = ("../uploads/users/$userid/bank");


        if (!file_exists($isCreatedVerifiation)) {
            @mkdir($isCreatedVerifiation, 0777, true);
        }
        if (!file_exists($isCreatedVerifiationid)) {
            @mkdir($isCreatedVerifiationid, 0777, true);
        }
        if (!file_exists($isCreatedbank)) {
            @mkdir($isCreatedbank, 0777, true);
        }

        $file_name = randomcode(5) . time() . str_replace(array(" ", "'", "\'", "_"), "", $_FILES['digital_signature']['name']);
        $IMAGE = new UPLOAD_IMAGE();
        $IMAGE->init($_FILES['digital_signature']['tmp_name']);
        move_uploaded_file($_FILES['digital_signature']['tmp_name'], "../uploads/users/$userid/bank/$file_name");

        $main_path = "uploads/users/$userid/bank/$file_name";

        unset($IMAGE);

        $sql = "UPDATE user_bank_details SET "
                . " digital_signature = 'uploads/users/$userid/bank/$file_name' "
                . " WHERE userid =  '" . $userid . "' "
                . " ";
        $result = $db->query($sql);
        return true;
    }

    public static function addPanCard($rs) {

        global $db;

        $status = "false";
        $data = array();
        $user_data = array();


        $userid = intval($rs['userid']);
        $pancard_type = addslashes(utf8_encode($rs['pancard_type']));
        $pencard = addslashes(utf8_encode($rs['pencard']));
        $name = addslashes(utf8_encode($rs['name']));
        $dob = addslashes(utf8_encode($rs['dob']));
        $father_name = addslashes(utf8_encode($rs['father_name']));
        $entity_type = addslashes(utf8_encode($rs['entity_type']));
        $pancard_image = addslashes(utf8_encode($rs['pancard_image']));

//        $pancard_image = $_FILES['pancard_image'];


        if ($userid == '' || $pencard == '') {
            $msg = INVALIDCREDENTIAL;
        } else if (User::isUser($userid) == false) {
            $msg = NOUSER;
        } else {

            $sql = "UPDATE users SET "
                    . " pancard_type = '" . $pancard_type . "', "
                    . " pencard = '" . $pencard . "', "
                    . " name = '" . $name . "', "
                    . " dob = '" . $dob . "', "
                    . " father_name = '" . $father_name . "', "
                    . " entity_type = '" . $entity_type . "', "
                    . " pancard_image = '" . $pancard_image . "', "
                    . " verification_step = '1' "
                    . " WHERE userid =  '" . $userid . "' "
                    . " ";
            $result = $db->query($sql);

            $status = STATUSTRUE;
            $msg = USERADDED;

            $user_info = User::getUserinfo($userid, $userid);
            $data = $user_info['data'];
        }
        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

    public static function addAddress($rs) {

        global $db;

        $status = "false";
        $data = array();
        $user_data = array();


        $userid = intval($rs['userid']);
        $residency = addslashes(utf8_encode($rs['residency_type']));
        $ca_pincode = addslashes(utf8_encode($rs['ca_pincode']));
        $ca_address = addslashes(utf8_encode($rs['ca_address']));
        $ca_city = addslashes(utf8_encode($rs['ca_city']));
        $ca_state = addslashes(utf8_encode($rs['ca_state']));
        $ca_country = addslashes(utf8_encode($rs['ca_country']));
        $pa_pincode = addslashes(utf8_encode($rs['pa_pincode']));
        $pa_address = addslashes(utf8_encode($rs['pa_address']));
        $pa_city = addslashes(utf8_encode($rs['pa_city']));
        $pa_state = addslashes(utf8_encode($rs['pa_state']));
        $pa_country = addslashes(utf8_encode($rs['pa_country']));
        $email = addslashes(utf8_encode($rs['email']));
        $address_proof_type = addslashes(utf8_encode($rs['address_proof_type']));
        $front_image = addslashes(utf8_encode($rs['front_image']));
        $back_image = addslashes(utf8_encode($rs['back_image']));

//        $front_image = $_FILES['front_image'];
//        $back_image = $_FILES['back_image'];


        if ($userid == '' || $residency == '') {
            $msg = INVALIDCREDENTIAL;
        } else if (User::isUser($userid) == false) {
            $msg = NOUSER;
        } else {

            $sql = "SELECT * FROM user_address_details WHERE userid = '" . $userid . "' ";
            $que = $db->query($sql);
            if ($que->size() == 0) {

                $sql = "INSERT INTO user_address_details SET "
                        . " userid = '" . $userid . "',"
                        . " residency_type = '" . $residency . "',"
                        . " ca_pincode = '" . $ca_pincode . "',"
                        . " ca_address = '" . $ca_address . "',"
                        . " ca_city = '" . $ca_city . "',"
                        . " ca_state = '" . $ca_state . "',"
                        . " ca_country = '" . $ca_country . "',"
                        . " pa_pincode = '" . $pa_pincode . "',"
                        . " pa_address = '" . $pa_address . "',"
                        . " pa_city = '" . $pa_city . "',"
                        . " pa_state = '" . $pa_state . "',"
                        . " pa_country = '" . $pa_country . "',"
                        . " email = '" . $email . "',"
                        . " address_proof_type = '" . $address_proof_type . "',"
                        . " front_image = '" . $front_image . "',"
                        . " back_image = '" . $back_image . "',"
                        . " dtdate = '" . nowDateTime() . "'"
                        . " ";
                $result = $db->query($sql);
                $sql = "UPDATE users SET "
                        . " verification_step = '2', "
                        . " ekyc_steps = 'ADDRESS' "
                        . " WHERE userid =  '" . $userid . "' "
                        . " ";
                $result = $db->query($sql);
            } else {
                $sql = "UPDATE user_address_details SET "
                        . " residency_type = '" . $residency . "',"
                        . " ca_pincode = '" . $ca_pincode . "',"
                        . " ca_address = '" . $ca_address . "',"
                        . " ca_city = '" . $ca_city . "',"
                        . " ca_state = '" . $ca_state . "',"
                        . " ca_country = '" . $ca_country . "',"
                        . " pa_pincode = '" . $pa_pincode . "',"
                        . " pa_address = '" . $pa_address . "',"
                        . " pa_city = '" . $pa_city . "',"
                        . " pa_state = '" . $pa_state . "',"
                        . " pa_country = '" . $pa_country . "',"
                        . " email = '" . $email . "',"
                        . " address_proof_type = '" . $address_proof_type . "',"
                        . " front_image = '" . $front_image . "',"
                        . " back_image = '" . $back_image . "',"
                        . " dtdate = '" . nowDateTime() . "'"
                        . " WHERE userid = '" . $userid . "' "
                        . "";
                $result = $db->query($sql);
                $sql = "UPDATE users SET "
                        . " verification_step = '2', "
                        . " ekyc_steps = 'ADDRESS' "
                        . " WHERE userid =  '" . $userid . "' "
                        . " ";
                $result = $db->query($sql);
            }


            $message = SUCCESS;

            $status = STATUSTRUE;
            $msg = USERADDED;

            $user_info = User::getUserinfo($userid, $userid);
            $data = $user_info['data'];
        }
        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

    public static function addIdentity($rs) {

        global $db;

        $status = "false";
        $data = array();
        $user_data = array();


        $userid = intval($rs['userid']);
        $birth_country = addslashes(utf8_encode($rs['birth_country']));
        $birth_place = addslashes(utf8_encode($rs['birth_place']));
        $address_type = addslashes(utf8_encode($rs['address_type']));
        $gross_annual_income = addslashes(utf8_encode($rs['gross_annual_income']));
        $occupation = addslashes(utf8_encode($rs['occupation']));
        $politically_exposed_person = addslashes(utf8_encode($rs['politically_exposed_person']));
        $tax_residency_country = addslashes(utf8_encode($rs['tax_residency_country']));
        $wealth_source = addslashes(utf8_encode($rs['wealth_source']));
        $marital_status = addslashes(utf8_encode($rs['marital_status']));
        $no_of_children = addslashes(utf8_encode($rs['no_of_children']));
        $salary_dependent = addslashes(utf8_encode($rs['salary_dependent']));
        $gender = addslashes(utf8_encode($rs['gender']));
        $nationality = addslashes(utf8_encode($rs['nationality']));
        $country = addslashes(utf8_encode($rs['country']));
        $tpin = addslashes(utf8_encode($rs['tpin']));
        $photo = addslashes(utf8_encode($rs['photo']));

//        $photo = $_FILES['photo'];



        if ($userid == '') {
            $msg = INVALIDCREDENTIAL;
        } else if (User::isUser($userid) == false) {
            $msg = NOUSER;
        } else {

            $sql = "SELECT * FROM user_identity_details WHERE userid = '" . $userid . "' ";
            $que = $db->query($sql);
            if ($que->size() == 0) {

                $sql = "INSERT INTO user_identity_details SET "
                        . " userid = '" . $userid . "',"
                        . " birth_country = '" . $birth_country . "',"
                        . " birth_place = '" . $birth_place . "',"
                        . " address_type = '" . $address_type . "',"
                        . " gross_annual_income = '" . $gross_annual_income . "',"
                        . " occupation = '" . $occupation . "',"
                        . " politically_exposed_person = '" . $politically_exposed_person . "',"
                        . " tax_residency_country = '" . $tax_residency_country . "',"
                        . " wealth_source = '" . $wealth_source . "',"
                        . " marital_status = '" . $marital_status . "',"
                        . " no_of_children = '" . $no_of_children . "',"
                        . " gender = '" . $gender . "',"
                        . " nationality = '" . $nationality . "',"
                        . " country = '" . $country . "',"
                        . " tpin = '" . $tpin . "',"
                        . " photo = '" . $photo . "',"
                        . " dtdate = '" . nowDateTime() . "'"
                        . " ";
                $result = $db->query($sql);
                $sql = "UPDATE users SET "
                        . " verification_step = '3', "
                        . " ekyc_steps = 'IDENTITY' "
                        . " WHERE userid =  '" . $userid . "' "
                        . " ";
                $result = $db->query($sql);
            } else {
                $sql = "UPDATE user_identity_details SET "
                        . " birth_country = '" . $birth_country . "',"
                        . " birth_place = '" . $birth_place . "',"
                        . " address_type = '" . $address_type . "',"
                        . " gross_annual_income = '" . $gross_annual_income . "',"
                        . " occupation = '" . $occupation . "',"
                        . " politically_exposed_person = '" . $politically_exposed_person . "',"
                        . " tax_residency_country = '" . $tax_residency_country . "',"
                        . " wealth_source = '" . $wealth_source . "',"
                        . " marital_status = '" . $marital_status . "',"
                        . " no_of_children = '" . $no_of_children . "',"
                        . " gender = '" . $gender . "',"
                        . " nationality = '" . $nationality . "',"
                        . " country = '" . $country . "',"
                        . " tpin = '" . $tpin . "',"
                        . " photo = '" . $photo . "',"
                        . " dtdate = '" . nowDateTime() . "'"
                        . " WHERE userid = '" . $userid . "' "
                        . " ";
                $result = $db->query($sql);
                $sql = "UPDATE users SET "
                        . " verification_step = '3', "
                        . " ekyc_steps = 'IDENTITY' "
                        . " WHERE userid =  '" . $userid . "' "
                        . " ";
                $result = $db->query($sql);
            }

            $message = SUCCESS;

            $status = STATUSTRUE;
            $msg = USERADDED;
            $user_info = User::getUserinfo($userid, $userid);
            $data = $user_info['data'];
        }
        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

    public static function addNominee($rs) {

        global $db;

        $status = "false";
        $data = array();
        $user_data = array();


        $userid = intval($rs['userid']);
        $nominee_name = addslashes(utf8_encode($rs['nominee_name']));
        $nominee_relationship = addslashes(utf8_encode($rs['nominee_relationship']));
        $nominee_address = addslashes(utf8_encode($rs['nominee_address']));

        if ($userid == '') {
            $msg = INVALIDCREDENTIAL;
        } else if (User::isUser($userid) == false) {
            $msg = NOUSER;
        } else {

            $sql = "SELECT * FROM user_nominee_details WHERE userid = '" . $userid . "' ";
            $que = $db->query($sql);
            if ($que->size() == 0) {

                $sql = "INSERT INTO user_nominee_details SET "
                        . " userid = '" . $userid . "',"
                        . " nominee_name = '" . $nominee_name . "',"
                        . " nominee_relationship = '" . $nominee_relationship . "',"
                        . " nominee_address = '" . $nominee_address . "',"
                        . " dtdate = '" . nowDateTime() . "'"
                        . " ";
                $result = $db->query($sql);
                $sql = "UPDATE users SET "
                        . " verification_step = '4',"
                        . " ekyc_steps = 'NOMINEE' "
                        . " WHERE userid =  '" . $userid . "' "
                        . " ";
                $result = $db->query($sql);
            } else {
                $sql = "UPDATE user_nominee_details SET "
                        . " nominee_name = '" . $nominee_name . "',"
                        . " nominee_relationship = '" . $nominee_relationship . "',"
                        . " nominee_address = '" . $nominee_address . "',"
                        . " dtdate = '" . nowDateTime() . "'"
                        . " WHERE userid = '" . $userid . "' "
                        . " ";
                $result = $db->query($sql);
                $sql = "UPDATE users SET "
                        . " verification_step = '4',"
                        . " ekyc_steps = 'NOMINEE' "
                        . " WHERE userid =  '" . $userid . "' "
                        . " ";
                $result = $db->query($sql);
            }
            $message = SUCCESS;

            $status = STATUSTRUE;
            $msg = USERADDED;
            $user_info = User::getUserinfo($userid, $userid);
            $data = $user_info['data'];
        }
        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

    public static function addBank($rs) {

        global $db;

        $status = "false";
        $data = array();
        $user_data = array();


        $userid = intval($rs['userid']);

        $account_type = addslashes(utf8_encode($rs['account_type']));
        $account_holder_name = addslashes(utf8_encode($rs['account_holder_name']));
        $account_number = addslashes(utf8_encode($rs['account_number']));
        $ifsc_code = addslashes(utf8_encode($rs['ifsc_code']));
        $cheque_leaf = addslashes(utf8_encode($rs['cheque_leaf']));
        $digital_signature = addslashes(utf8_encode($rs['digital_signature']));

//        $cheque_leaf = $_FILES['cheque_leaf'];
//        $digital_signature = $_FILES['digital_signature'];



        if ($userid == '') {
            $msg = INVALIDCREDENTIAL;
        } else if (User::isUser($userid) == false) {
            $msg = NOUSER;
        } else {

            $sql = "SELECT * FROM user_bank_details WHERE userid = '" . $userid . "' ";
            $que = $db->query($sql);
            if ($que->size() == 0) {

                $sql = "INSERT INTO user_bank_details SET "
                        . " userid = '" . $userid . "',"
                        . " account_type = '" . $account_type . "',"
                        . " account_holder_name = '" . $account_holder_name . "',"
                        . " account_number = '" . $account_number . "',"
                        . " ifsc_code = '" . $ifsc_code . "',"
                        . " cheque_leaf = '" . $cheque_leaf . "',"
                        . " digital_signature = '" . $digital_signature . "',"
                        . " dtdate = '" . nowDateTime() . "'"
                        . " ";
                $result = $db->query($sql);
                $sql = "UPDATE users SET "
                        . " verification_step = '5' "
                        . " WHERE userid =  '" . $userid . "' "
                        . " ";
                $result = $db->query($sql);
            } else {
                $sql = "UPDATE user_bank_details SET "
                        . " userid = '" . $userid . "',"
                        . " account_type = '" . $account_type . "',"
                        . " account_holder_name = '" . $account_holder_name . "',"
                        . " account_number = '" . $account_number . "',"
                        . " ifsc_code = '" . $ifsc_code . "',"
                        . " cheque_leaf = '" . $cheque_leaf . "',"
                        . " digital_signature = '" . $digital_signature . "',"
                        . " dtdate = '" . nowDateTime() . "'"
                        . " WHERE userid = '" . $userid . "' "
                        . " ";
                $result = $db->query($sql);
                $sql = "UPDATE users SET "
                        . " verification_step = '5' "
                        . " WHERE userid =  '" . $userid . "' "
                        . " ";
                $result = $db->query($sql);
            }
            $message = SUCCESS;

            $status = STATUSTRUE;
            $msg = USERADDED;
            $user_info = User::getUserinfo($userid, $userid);
            $data = $user_info['data'];
        }
        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

    public static function user_nominee_details($userid) {
        global $db;

        $data = array();
        $sql = "SELECT * FROM user_nominee_details WHERE userid = '" . $userid . "' ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            $row = $que->fetch();
            $data = $row;
        }
        return $data;
    }
    public static function user_identity_details($userid) {
        global $db;

        $data = array();
        $sql = "SELECT * FROM user_identity_details WHERE userid = '" . $userid . "' ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            $row = $que->fetch();
            $data = $row;
        }
        return $data;
    }

}
