<?php

class Listing extends Models {

    public static function stateList() {
        global $db;

        $data = array();
        $status = "false";

        $sql = "SELECT * FROM state WHERE CountryID = '104' ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            while ($row = $que->fetch()) {
                $state[] = $row;
            }
            $data = $state;
            $msg = SUCCESS;
            $status = "true";
        } else {
            $msg = NORECORD;
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

    public static function cityList($StateID) {
        global $db;

        $data = array();
        $status = "false";

        $sql = "SELECT * FROM city WHERE StateID = '" . $StateID . "' ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            while ($row = $que->fetch()) {

                $state[] = $row;
            }
            $data = $state;
            $msg = SUCCESS;
            $status = "true";
        } else {
            $msg = NORECORD;
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

    public static function longTerm($SCHEME_CODE) {
        global $db;

        $data = array();
        $status = "false";

        $sql = "SELECT LT.*,SIP_DATES,SIP_MIN_INSTALLMENT_AMT,SIP_MULTPL_AMT,SIP_MIN_INSTALLMENT_NUM,SIP_MAX_INSTALLMENT_NUM FROM long_term LT left join sip_master SM on LT.ISIN=SM.ISIN WHERE AMFI_SCHEME_CODE != '' ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            while ($row = $que->fetch()) {
                $row['STARMF_SCHEME_NAME'] = utf8_encode($row['STARMF_SCHEME_NAME']);
                $state[] = $row;
            }
            $data = $state;
            $msg = SUCCESS;
            $status = "true";
        } else {
            $msg = NORECORD;
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

    public static function mostPersistent($SCHEME_CODE) {
        global $db;

        $data = array();
        $status = "false";

        $sql = "SELECT MP.*,SIP_DATES,SIP_MIN_INSTALLMENT_AMT,SIP_MULTPL_AMT,SIP_MIN_INSTALLMENT_NUM,SIP_MAX_INSTALLMENT_NUM FROM most_persistent MP left join sip_master SM on SM.ISIN=MP.ISIN WHERE AMFI_SCHEME_CODE != '' ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            while ($row = $que->fetch()) {
                $row['STARMF_SCHEME_NAME'] = utf8_encode($row['STARMF_SCHEME_NAME']);
                $state[] = $row;
            }
            $data = $state;
            $msg = SUCCESS;
            $status = "true";
        } else {
            $msg = NORECORD;
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

    public static function riskAverse($SCHEME_CODE) {
        global $db;

        $data = array();
        $status = "false";

        $sql = "SELECT RA.*,SIP_DATES,SIP_MIN_INSTALLMENT_AMT,SIP_MULTPL_AMT,SIP_MIN_INSTALLMENT_NUM,SIP_MAX_INSTALLMENT_NUM FROM risk_averse RA left join sip_master SM on SM.ISIN=RA.ISIN WHERE AMFI_SCHEME_CODE != '' ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            while ($row = $que->fetch()) {
                $row['STARMF_SCHEME_NAME'] = utf8_encode($row['STARMF_SCHEME_NAME']);
                $state[] = $row;
            }
            $data = $state;
            $msg = SUCCESS;
            $status = "true";
        } else {
            $msg = NORECORD;
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

    public static function riskTaker($SCHEME_CODE) {
        global $db;

        $data = array();
        $status = "false";

        $sql = "SELECT RT.*,SIP_DATES,SIP_MIN_INSTALLMENT_AMT,SIP_MULTPL_AMT,SIP_MIN_INSTALLMENT_NUM,SIP_MAX_INSTALLMENT_NUM FROM risk_taker RT left join sip_master SM on SM.ISIN=RT.ISIN WHERE AMFI_SCHEME_CODE != '' ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            while ($row = $que->fetch()) {
                $row['STARMF_SCHEME_NAME'] = utf8_encode($row['STARMF_SCHEME_NAME']);
                $state[] = $row;
            }
            $data = $state;
            $msg = SUCCESS;
            $status = "true";
        } else {
            $msg = NORECORD;
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

    public static function schemeDetail($SCHEME_CODE) {
        global $db;

        $data = array();
        $status = "false";

        $sql = "SELECT * FROM view_niivo_master WHERE STARMF_SCHEME_CODE = '" . $SCHEME_CODE . "' ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            $row = $que->fetch();
            $row['STARMF_SCHEME_NAME'] = utf8_encode($row['STARMF_SCHEME_NAME']);

            $data = $row;
            $msg = SUCCESS;
            $status = "true";
        } else {
            $msg = NORECORD;
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

    public static function biggnersList($fund_category, $sip_flag) {
        global $db;

        $data = array();
        $status = "false";
        $growth = "0";
        if ($fund_category > 0) {
            $sql1 = "SELECT * FROM fund_category WHERE id = '" . $fund_category . "' ";
            $que1 = $db->query($sql1);
            $row1 = $que1->fetch();
            if ($que1->size() > 0) {
                $sql_text = $row1['sql_text'];
                $growth = $row1['growth'];

                if ($sip_flag == 'Y') {
                    $where = " AND STARMF_SIP_FLAG = 'Y' ";
                }
                
                //this condition is for temprory to avoid CAMS scheme, 
                //$where .=" and STARMF_RTA_CODE!='CAMS' ";

                switch ($fund_category) {
                    case "1":
                        $sql = "SELECT * FROM view_niivo_master n WHERE n.FUND_CLASS='Fixed Income' AND n.RISK_AVERSION_SCORE <> 'N.A.' AND n.SHARE_CLASS='RG' ". $where. " ORDER BY cast(n.RISK_AVERSION_SCORE as decimal(6,3)) DESC LIMIT 3";
                        $growth = "6";
                        break;
                    case "2":
                        $sql = "SELECT * FROM view_niivo_master n WHERE n.FUND_CLASS='Fixed Income' AND n.RISK_AVERSION_SCORE <> 'N.A.' AND n.SHARE_CLASS='RG' ". $where. "  ORDER BY cast(n.RISK_AVERSION_SCORE as decimal(6,3)) DESC LIMIT 3 ";
                        $growth = "6";
                        break;
                    case "3":
                        $sql = "SELECT * FROM view_niivo_master n WHERE n.FUND_CATEGORIZATION='Tax Saving' AND n.LONG_TERM_SCORE <> 'N.A.' AND n.SHARE_CLASS='RG' ". $where. "  ORDER BY cast(n.LONG_TERM_SCORE as decimal(6,3)) DESC LIMIT 3";
                        $growth = "10";
                        break;
                    case "4":
                        $sql = "SELECT * FROM view_niivo_master n WHERE n.FUND_CLASS='Equity' AND n.PERSISTENCE_SCORE <> 'N.A.' AND n.SHARE_CLASS='RG' ". $where. "  ORDER BY cast(n.PERSISTENCE_SCORE as decimal(6,3)) DESC LIMIT 3";
                        $growth = "10";
                        break;
                    case "5":
                        $sql = "SELECT * FROM view_niivo_master n WHERE n.FUND_CLASS='Equity' AND n.PERSISTENCE_SCORE <> 'N.A.' AND n.SHARE_CLASS='RG' ". $where. "  ORDER BY cast(n.PERSISTENCE_SCORE as decimal(6,3)) DESC LIMIT 3";
                        $growth = "10";
                        break;
                    case "6":
                        $sql = "SELECT * FROM view_niivo_master n WHERE n.FUND_CLASS ='Mixed Allocation' AND n.PERSISTENCE_SCORE <> 'N.A.' AND n.SHARE_CLASS='RG' ". $where. "  ORDER BY cast(n.PERSISTENCE_SCORE as decimal(6,3)) DESC LIMIT 3";
                        $growth = "8";
                        break;
                    case "7":
                        $sql = "SELECT * FROM view_niivo_master n WHERE n.FUND_CLASS='Mixed Allocation' AND n.PERSISTENCE_SCORE <> 'N.A.' AND n.SHARE_CLASS='RG' ". $where. "  ORDER BY cast(n.PERSISTENCE_SCORE as decimal(6,3)) DESC LIMIT 3";
                        $growth = "8";
                        break;
                    case "8":
                        $sql = "SELECT * FROM view_niivo_master n WHERE n.FUND_CLASS='Fixed Income' AND n.RISK_AVERSION_SCORE <> 'N.A.' AND n.SHARE_CLASS='RG' ". $where. "  ORDER BY cast(n.RISK_AVERSION_SCORE as decimal(6,3)) DESC LIMIT 3";
                        $growth = "6";
                        break;
                    case "9":
                        $sql = "SELECT * FROM view_niivo_master n WHERE n.FUND_CLASS='Equity' AND n.PERSISTENCE_SCORE <> 'N.A. ' AND n.SHARE_CLASS='RG' ". $where. "  ORDER BY cast(n.PERSISTENCE_SCORE as decimal(6,3)) DESC LIMIT 3";
                        $growth = "10";
                        break;
                    default:
                        $sql = "SELECT * FROM view_niivo_master n WHERE
n.FUND_CLASS='Equity' AND n.PERSISTENCE_SCORE
<> 'N.A.' AND n.SHARE_CLASS='RG'
$where
ORDER BY
cast(n.PERSISTENCE_SCORE as decimal(6,3)) DESC LIMIT 3 ";
                }


                $que = $db->query($sql);
                if ($que->size() > 0) {
                    while ($row = $que->fetch()) {
                        $row['STARMF_SCHEME_NAME'] = utf8_encode($row['STARMF_SCHEME_NAME']);
                        $state[] = $row;
                    }
                    $data = $state;
                    $msg = SUCCESS;
                    $status = "true";
                } else {
                    $msg = NORECORD;
                }
            } else {
                $msg = INVALIDCREDENTIAL;
            }
        } else {
            $msg = INVALIDCREDENTIAL;
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data, "growth" => $growth);
        return $arr;
    }

    public static function fundCategory() {
        global $db;

        $data = array();
        $status = "false";

        $sql = "SELECT * FROM fund_category WHERE 1 ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            while ($row = $que->fetch()) {
                $data[] = $row;
            }
            $msg = SUCCESS;
            $status = "true";
        } else {
            $msg = NORECORD;
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

}
