<?php

class Verifiation extends Models {

    protected static $table = "users";

    public static function isVerifiation($userid) {
        global $db;
        return $db->query("select count(userid) as counter from users where userid='$userid' AND status = '1' ")->fetch()['counter'];
    }

    public static function isPanCardAdded($userid) {
        global $db;
        return $db->query("select count(userid) as counter from user_pancard_details where userid='$userid' ")->fetch()['counter'];
    }

    public static function isAddressAdded($userid) {
        global $db;
        return $db->query("select count(userid) as counter from user_address_details where userid='$userid' ")->fetch()['counter'];
    }

    public static function isIdentityAdded($userid) {
        global $db;
        return $db->query("select count(userid) as counter from user_identity_details where userid='$userid' ")->fetch()['counter'];
    }

    public static function isNomineeAdded($userid) {
        global $db;
        return $db->query("select count(userid) as counter from user_nominee_details where userid='$userid' ")->fetch()['counter'];
    }
    
    public static function isBankAdded($userid) {
        global $db;
        return $db->query("select count(userid) as counter from user_bank_details where userid='$userid' ")->fetch()['counter'];
    }

    public static function uploadPancardImage($files, $userid, $type = '') {

        global $db;
        $isCreatedVerifiation = ("../uploads/users");
        $isCreatedVerifiationid = ("../uploads/users/$userid");
        $isCreatedPancard = ("../uploads/users/$userid/pancard");


        if (!file_exists($isCreatedVerifiation)) {
            @mkdir($isCreatedVerifiation, 0777, true);
        }
        if (!file_exists($isCreatedVerifiationid)) {
            @mkdir($isCreatedVerifiationid, 0777, true);
        }
        if (!file_exists($isCreatedPancard)) {
            @mkdir($isCreatedPancard, 0777, true);
        }

        $file_name = randomcode(5) . time() . str_replace(array(" ", "'", "\'", "_"), "", $_FILES['pan_card_image']['name']);
        $IMAGE = new UPLOAD_IMAGE();
        $IMAGE->init($_FILES['pan_card_image']['tmp_name']);
        move_uploaded_file($_FILES['pan_card_image']['tmp_name'], "../uploads/users/$userid/pancard/$file_name");

        $main_path = "uploads/users/$userid/pancard/$file_name";

        unset($IMAGE);

        $sql = "UPDATE user_pancard_details SET "
                . " pan_card_image = 'uploads/users/$userid/pancard/$file_name' "
                . " WHERE userid =  '" . $userid . "' "
                . " ";
        $result = $db->query($sql);

        return true;
    }

    public static function uploadAdressFrontImage($files, $userid, $type = '') {

        global $db;
        $isCreatedVerifiation = ("../uploads/users");
        $isCreatedVerifiationid = ("../uploads/users/$userid");
        $isCreatedaddress = ("../uploads/users/$userid/address");


        if (!file_exists($isCreatedVerifiation)) {
            @mkdir($isCreatedVerifiation, 0777, true);
        }
        if (!file_exists($isCreatedVerifiationid)) {
            @mkdir($isCreatedVerifiationid, 0777, true);
        }
        if (!file_exists($isCreatedaddress)) {
            @mkdir($isCreatedaddress, 0777, true);
        }

        $file_name = randomcode(5) . time() . str_replace(array(" ", "'", "\'", "_"), "", $_FILES['front_image']['name']);
        $IMAGE = new UPLOAD_IMAGE();
        $IMAGE->init($_FILES['front_image']['tmp_name']);
        move_uploaded_file($_FILES['front_image']['tmp_name'], "../uploads/users/$userid/address/$file_name");

        $main_path = "uploads/users/$userid/address/$file_name";

        unset($IMAGE);

        $sql = "UPDATE user_address_details SET "
                . " front_image = 'uploads/users/$userid/address/$file_name' "
                . " WHERE userid =  '" . $userid . "' "
                . " ";
        $result = $db->query($sql);
        return true;
    }

    public static function uploadAdressBackImage($files, $userid, $type = '') {

        global $db;
        $isCreatedVerifiation = ("../uploads/users");
        $isCreatedVerifiationid = ("../uploads/users/$userid");
        $isCreatedaddress = ("../uploads/users/$userid/address");


        if (!file_exists($isCreatedVerifiation)) {
            @mkdir($isCreatedVerifiation, 0777, true);
        }
        if (!file_exists($isCreatedVerifiationid)) {
            @mkdir($isCreatedVerifiationid, 0777, true);
        }
        if (!file_exists($isCreatedaddress)) {
            @mkdir($isCreatedaddress, 0777, true);
        }

        $file_name = randomcode(5) . time() . str_replace(array(" ", "'", "\'", "_"), "", $_FILES['back_image']['name']);
        $IMAGE = new UPLOAD_IMAGE();
        $IMAGE->init($_FILES['back_image']['tmp_name']);
        move_uploaded_file($_FILES['back_image']['tmp_name'], "../uploads/users/$userid/address/$file_name");

        $main_path = "uploads/users/$userid/address/$file_name";

        unset($IMAGE);

        $sql = "UPDATE user_address_details SET "
                . " back_image = 'uploads/users/$userid/address/$file_name' "
                . " WHERE userid =  '" . $userid . "' "
                . " ";
        $result = $db->query($sql);
        return true;
    }

    public static function uploadIdentityPhoto($files, $userid, $type = '') {

        global $db;
        $isCreatedVerifiation = ("../uploads/users");
        $isCreatedVerifiationid = ("../uploads/users/$userid");
        $isCreatedidentity = ("../uploads/users/$userid/identity");


        if (!file_exists($isCreatedVerifiation)) {
            @mkdir($isCreatedVerifiation, 0777, true);
        }
        if (!file_exists($isCreatedVerifiationid)) {
            @mkdir($isCreatedVerifiationid, 0777, true);
        }
        if (!file_exists($isCreatedidentity)) {
            @mkdir($isCreatedidentity, 0777, true);
        }

        $file_name = randomcode(5) . time() . str_replace(array(" ", "'", "\'", "_"), "", $_FILES['photo']['name']);
        $IMAGE = new UPLOAD_IMAGE();
        $IMAGE->init($_FILES['photo']['tmp_name']);
        move_uploaded_file($_FILES['photo']['tmp_name'], "../uploads/users/$userid/identity/$file_name");

        $main_path = "uploads/users/$userid/identity/$file_name";

        unset($IMAGE);

        $sql = "UPDATE user_identity_details SET "
                . " photo = 'uploads/users/$userid/identity/$file_name' "
                . " WHERE userid =  '" . $userid . "' "
                . " ";
        $result = $db->query($sql);
        return true;
    }

    public static function uploadChequelLeafPhoto($files, $userid, $type = '') {

        global $db;
        $isCreatedVerifiation = ("../uploads/users");
        $isCreatedVerifiationid = ("../uploads/users/$userid");
        $isCreatedbank = ("../uploads/users/$userid/bank");


        if (!file_exists($isCreatedVerifiation)) {
            @mkdir($isCreatedVerifiation, 0777, true);
        }
        if (!file_exists($isCreatedVerifiationid)) {
            @mkdir($isCreatedVerifiationid, 0777, true);
        }
        if (!file_exists($isCreatedbank)) {
            @mkdir($isCreatedbank, 0777, true);
        }

        $file_name = randomcode(5) . time() . str_replace(array(" ", "'", "\'", "_"), "", $_FILES['cheque_leaf']['name']);
        $IMAGE = new UPLOAD_IMAGE();
        $IMAGE->init($_FILES['cheque_leaf']['tmp_name']);
        move_uploaded_file($_FILES['cheque_leaf']['tmp_name'], "../uploads/users/$userid/bank/$file_name");

        $main_path = "uploads/users/$userid/bank/$file_name";

        unset($IMAGE);

        $sql = "UPDATE user_bank_details SET "
                . " cheque_leaf = 'uploads/users/$userid/bank/$file_name' "
                . " WHERE userid =  '" . $userid . "' "
                . " ";
        $result = $db->query($sql);
        return true;
    }

    public static function uploadDigitalSignaturePhoto($files, $userid, $type = '') {

        global $db;
        $isCreatedVerifiation = ("../uploads/users");
        $isCreatedVerifiationid = ("../uploads/users/$userid");
        $isCreatedbank = ("../uploads/users/$userid/bank");


        if (!file_exists($isCreatedVerifiation)) {
            @mkdir($isCreatedVerifiation, 0777, true);
        }
        if (!file_exists($isCreatedVerifiationid)) {
            @mkdir($isCreatedVerifiationid, 0777, true);
        }
        if (!file_exists($isCreatedbank)) {
            @mkdir($isCreatedbank, 0777, true);
        }

        $file_name = randomcode(5) . time() . str_replace(array(" ", "'", "\'", "_"), "", $_FILES['digital_signature']['name']);
        $IMAGE = new UPLOAD_IMAGE();
        $IMAGE->init($_FILES['digital_signature']['tmp_name']);
        move_uploaded_file($_FILES['digital_signature']['tmp_name'], "../uploads/users/$userid/bank/$file_name");

        $main_path = "uploads/users/$userid/bank/$file_name";

        unset($IMAGE);

        $sql = "UPDATE user_bank_details SET "
                . " digital_signature = 'uploads/users/$userid/bank/$file_name' "
                . " WHERE userid =  '" . $userid . "' "
                . " ";
        $result = $db->query($sql);
        return true;
    }

    public static function addPanCard($rs) {

        global $db;

        $status = "false";
        $data = array();
        $user_data = array();


        $userid = intval($rs['userid']);
        $pan_card_number = addslashes(utf8_encode($rs['pan_card_number']));
        $dob = addslashes(utf8_encode($rs['dob']));
        $name = addslashes(utf8_encode($rs['name']));
        $father_name = addslashes(utf8_encode($rs['father_name']));
        $mother_name = addslashes(utf8_encode($rs['mother_name']));

        $pan_card_image = $_FILES['pan_card_image'];


        if ($userid == '' || $pan_card_number == '') {
            $msg = INVALIDCREDENTIAL;
        } else if (User::isUser($userid) == false) {
            $msg = NOUSER;
        } else {

            $sql = "SELECT * FROM user_pancard_details WHERE userid = '" . $userid . "' ";
            $que = $db->query($sql);
            if ($que->size() == 0) {

                $sql = "INSERT INTO user_pancard_details SET "
                        . " userid = '" . $userid . "',"
                        . " pan_card_number = '" . $pan_card_number . "',"
                        . " dob = '" . $dob . "',"
                        . " name = '" . $name . "',"
                        . " father_name = '" . $father_name . "',"
                        . " mother_name = '" . $mother_name . "',"
                        . " dtdate = '" . nowDateTime() . "'"
                        . " ";
                $result = $db->query($sql);
                $sql = "UPDATE users SET "
                        . " verification_step = '1' "
                        . " WHERE userid =  '" . $userid . "' "
                        . " ";
                $result = $db->query($sql);



                if ($_FILES['pan_card_image']['tmp_name'] != "") {
                    if (!Verifiation::uploadPancardImage($pan_card_image, $userid)) {
                        
                    }
                }

                $message = "Success";

                $status = STATUSTRUE;
                $msg = USERADDED;
            } else {
                $msg = ALREADYEXIST;
            }
            $user_info = User::getUserinfo($userid, $userid);
            $data = $user_info['data'];
        }
        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

    public static function addAddress($rs) {

        global $db;

        $status = "false";
        $data = array();
        $user_data = array();


        $userid = intval($rs['userid']);
        $residency = addslashes(utf8_encode($rs['residency']));
        $pincode = addslashes(utf8_encode($rs['pincode']));
        $address = addslashes(utf8_encode($rs['address']));
        $city = addslashes(utf8_encode($rs['city']));
        $state = addslashes(utf8_encode($rs['state']));
        $email = addslashes(utf8_encode($rs['email']));
        $address_proof_type = addslashes(utf8_encode($rs['address_proof_type']));

        $front_image = $_FILES['front_image'];
        $back_image = $_FILES['back_image'];


        if ($userid == '' || $residency == '') {
            $msg = INVALIDCREDENTIAL;
        } else if (User::isUser($userid) == false) {
            $msg = NOUSER;
        } else {

            $sql = "SELECT * FROM user_address_details WHERE userid = '" . $userid . "' ";
            $que = $db->query($sql);
            if ($que->size() == 0) {

                $sql = "INSERT INTO user_address_details SET "
                        . " userid = '" . $userid . "',"
                        . " residency = '" . $residency . "',"
                        . " pincode = '" . $pincode . "',"
                        . " address = '" . $address . "',"
                        . " city = '" . $city . "',"
                        . " state = '" . $state . "',"
                        . " email = '" . $email . "',"
                        . " address_proof_type = '" . $address_proof_type . "',"
                        . " dtdate = '" . nowDateTime() . "'"
                        . " ";
                $result = $db->query($sql);
                $sql = "UPDATE users SET "
                        . " verification_step = '2' "
                        . " WHERE userid =  '" . $userid . "' "
                        . " ";
                $result = $db->query($sql);



                if ($_FILES['front_image']['tmp_name'] != "") {
                    if (!Verifiation::uploadAdressFrontImage($front_image, $userid)) {
                        
                    }
                }
                if ($_FILES['back_image']['tmp_name'] != "") {
                    if (!Verifiation::uploadAdressBackImage($back_image, $userid)) {
                        
                    }
                }

                $message = "Success";

                $status = STATUSTRUE;
                $msg = USERADDED;
            } else {
                $msg = ALREADYEXIST;
            }
            $user_info = User::getUserinfo($userid, $userid);
            $data = $user_info['data'];
        }
        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

    public static function addIdentity($rs) {

        global $db;

        $status = "false";
        $data = array();
        $user_data = array();


        $userid = intval($rs['userid']);
        $birth_country = addslashes(utf8_encode($rs['birth_country']));
        $birth_place = addslashes(utf8_encode($rs['birth_place']));
        $address_type = addslashes(utf8_encode($rs['address_type']));
        $gross_annual_income = addslashes(utf8_encode($rs['gross_annual_income']));
        $occupation = addslashes(utf8_encode($rs['occupation']));
        $politically_exposed_person = addslashes(utf8_encode($rs['politically_exposed_person']));
        $tax_residency_country = addslashes(utf8_encode($rs['tax_residency_country']));
        $wealth_source = addslashes(utf8_encode($rs['wealth_source']));
        $marital_status = addslashes(utf8_encode($rs['marital_status']));
        $gender = addslashes(utf8_encode($rs['gender']));
        $nationality = addslashes(utf8_encode($rs['nationality']));

        $photo = $_FILES['photo'];



        if ($userid == '') {
            $msg = INVALIDCREDENTIAL;
        } else if (User::isUser($userid) == false) {
            $msg = NOUSER;
        } else {

            $sql = "SELECT * FROM user_identity_details WHERE userid = '" . $userid . "' ";
            $que = $db->query($sql);
            if ($que->size() == 0) {

                $sql = "INSERT INTO user_identity_details SET "
                        . " userid = '" . $userid . "',"
                        . " birth_country = '" . $birth_country . "',"
                        . " birth_place = '" . $birth_place . "',"
                        . " address_type = '" . $address_type . "',"
                        . " gross_annual_income = '" . $gross_annual_income . "',"
                        . " occupation = '" . $occupation . "',"
                        . " politically_exposed_person = '" . $politically_exposed_person . "',"
                        . " tax_residency_country = '" . $tax_residency_country . "',"
                        . " wealth_source = '" . $wealth_source . "',"
                        . " marital_status = '" . $marital_status . "',"
                        . " gender = '" . $gender . "',"
                        . " nationality = '" . $nationality . "',"
                        . " dtdate = '" . nowDateTime() . "'"
                        . " ";
                $result = $db->query($sql);
                $sql = "UPDATE users SET "
                        . " verification_step = '3' "
                        . " WHERE userid =  '" . $userid . "' "
                        . " ";
                $result = $db->query($sql);



                if ($_FILES['photo']['tmp_name'] != "") {
                    if (!Verifiation::uploadIdentityPhoto($photo, $userid)) {
                        
                    }
                }
                $message = "Success";

                $status = STATUSTRUE;
                $msg = USERADDED;
            } else {
                $msg = ALREADYEXIST;
            }
            $user_info = User::getUserinfo($userid, $userid);
            $data = $user_info['data'];
        }
        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

    public static function addNominee($rs) {

        global $db;

        $status = "false";
        $data = array();
        $user_data = array();


        $userid = intval($rs['userid']);
        $nominee_name = addslashes(utf8_encode($rs['nominee_name']));
        $nominee_dob = addslashes(utf8_encode($rs['nominee_dob']));
        $nominee_relationship = addslashes(utf8_encode($rs['nominee_relationship']));
        $nominee_address = addslashes(utf8_encode($rs['nominee_address']));

        if ($userid == '') {
            $msg = INVALIDCREDENTIAL;
        } else if (User::isUser($userid) == false) {
            $msg = NOUSER;
        } else {

            $sql = "SELECT * FROM user_nominee_details WHERE userid = '" . $userid . "' ";
            $que = $db->query($sql);
            if ($que->size() == 0) {

                $sql = "INSERT INTO user_nominee_details SET "
                        . " userid = '" . $userid . "',"
                        . " nominee_name = '" . $nominee_name . "',"
                        . " nominee_dob = '" . $nominee_dob . "',"
                        . " nominee_relationship = '" . $nominee_relationship . "',"
                        . " nominee_address = '" . $nominee_address . "',"
                        . " dtdate = '" . nowDateTime() . "'"
                        . " ";
                $result = $db->query($sql);
                $sql = "UPDATE users SET "
                        . " verification_step = '4' "
                        . " WHERE userid =  '" . $userid . "' "
                        . " ";
                $result = $db->query($sql);

                $message = "Success";

                $status = STATUSTRUE;
                $msg = USERADDED;
            } else {
                $msg = ALREADYEXIST;
            }
            $user_info = User::getUserinfo($userid, $userid);
            $data = $user_info['data'];
        }
        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

    public static function addBank($rs) {

        global $db;

        $status = "false";
        $data = array();
        $user_data = array();


        $userid = intval($rs['userid']);

        $cheque_leaf = $_FILES['cheque_leaf'];
        $digital_signature = $_FILES['digital_signature'];



        if ($userid == '') {
            $msg = INVALIDCREDENTIAL;
        } else if (User::isUser($userid) == false) {
            $msg = NOUSER;
        } else {

            $sql = "SELECT * FROM user_bank_details WHERE userid = '" . $userid . "' ";
            $que = $db->query($sql);
            if ($que->size() == 0) {

                $sql = "INSERT INTO user_bank_details SET "
                        . " userid = '" . $userid . "',"
                        . " dtdate = '" . nowDateTime() . "'"
                        . " ";
                $result = $db->query($sql);
                $sql = "UPDATE users SET "
                        . " verification_step = '5' "
                        . " WHERE userid =  '" . $userid . "' "
                        . " ";
                $result = $db->query($sql);



                if ($_FILES['cheque_leaf']['tmp_name'] != "") {
                    if (!Verifiation::uploadChequelLeafPhoto($cheque_leaf, $userid)) {
                        
                    }
                }
                if ($_FILES['digital_signature']['tmp_name'] != "") {
                    if (!Verifiation::uploadDigitalSignaturePhoto($digital_signature, $userid)) {
                        
                    }
                }
                $message = "Success";

                $status = STATUSTRUE;
                $msg = USERADDED;
            } else {
                $msg = ALREADYEXIST;
            }
            $user_info = User::getUserinfo($userid, $userid);
            $data = $user_info['data'];
        }
        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

}
