<?php

class Cart extends Models {

    protected static $table = "cart_master";

    public static function addCart($rs) {
        global $db;
        $status = false;

        $userid = $rs['userid'];
        $order_type = $rs['order_type'];
        $no_of_months = $rs['no_of_months'];
        $target_year = $rs['target_year'];
        $added_date = $rs['added_date'];
        $is_goal = $rs['is_goal'];


        $goal_amount = $rs['goal_amount'];
        $goal_name = $rs['goal_name'];
        $goal_cat_id = $rs['goal_cat_id'];


        $scheme_json = json_decode(stripslashes($rs['scheme_json']));

        if ($userid == '' && $userid <= 0) {
            $msg = INVALIDCREDENTIAL;
        } else if (UserClass::isUser($userid) == false) {
            $msg = NOUSER;
        } else {
            $sql1 = "INSERT INTO cart_master SET "
                    . " userid = '" . $userid . "',"
                    . " order_type = '" . $order_type . "',"
                    . " no_of_months = '" . $no_of_months . "',"
                    . " target_year = '" . $target_year . "',"
                    . " added_date = '" . $added_date . "',"
                    . " is_goal = '" . $is_goal . "',"
                    . " dtdate = '" . nowDateTime() . "' "
                    . " ";
            $result = $db->query($sql1);
            $cart_id = $result->insertID();



            foreach ($scheme_json as $key => $scheme_data) {

                $scheme_code = $scheme_data->scheme_code;
                $scheme_amount = $scheme_data->scheme_amount;
                $scheme_nav = $scheme_data->scheme_nav;


                $sql1 = "INSERT INTO cart_detail SET "
                        . " userid = '" . $userid . "',"
                        . " cart_id = '" . $cart_id . "',"
                        . " goal_amount = '" . $goal_amount . "',"
                        . " goal_name = '" . $goal_name . "',"
                        . " goal_cat_id = '" . $goal_cat_id . "',"
                        . " scheme_code = '" . $scheme_code . "',"
                        . " scheme_amount = '" . $scheme_amount . "',"
                        . " scheme_nav = '" . $scheme_nav . "' "
                        . " ";

                $result1 = $db->query($sql1);
            }


            $msg = "Success";
            $status = "true";
        }

        $arr = array("msg" => $msg, "status" => $status, "cart_id" => $cart_id);
        return $arr;
    }

    public static function removeCart($userid, $cart_id) {
        global $db;
        $status = STATUSFALSE;
        $data = array();

        $msg = "";
        if ($userid == '' && $userid <= 0) {
            $msg = INVALIDCREDENTIAL;
        } else if (UserClass::isUser($userid) == false) {
            $msg = NOUSER;
        } else {
            $where = "WHERE 1";

            $sql = "SELECT * FROM `cart_master` WHERE userid = '" . $userid . "' AND id = '" . $cart_id . "' ";
            $res = $db->query($sql);
            if ($res->size() > 0) {
                $db->query("DELETE FROM cart_master WHERE id = '" . $cart_id . "'  ");
                $db->query("DELETE FROM cart_detail WHERE cart_id = '" . $cart_id . "'  ");

                $msg = "Success";
                $status = "true";
            } else {
                $msg = NORECORD;
            }
        }
        $arr = array("msg" => $msg, "status" => $status);
        return $arr;
    }

    public static function cartList($userid) {
        global $db;

        $data = array();
        $status = "false";

        $where = " WHERE 1 AND userid = '" . $userid . "' ";

        $sql = "SELECT * FROM cart_master $where  ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            while ($row = $que->fetch()) {
                $cart_id = $row['id'];
                $cart = array();
                $sql1 = "SELECT * FROM cart_detail WHERE cart_id = '" . $cart_id . "'  ";
                $que1 = $db->query($sql1);
                if ($que1->size() > 0) {
                    while ($row1 = $que1->fetch()) {
                        $SCHEME_CODE = $row1['scheme_code'];
                        $scheme_data = Listing::schemeDetail($SCHEME_CODE);
                        $row1['scheme_detail'] = $scheme_data['data'];
                        $cart[] = $row1;
                    }
                    $row['cart_detail'] = $cart;
                }
                $data[] = $row;
            }

            $msg = "Success";
            $status = "true";
        } else {
            $msg = "Record not found";
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

    public static function editCartAmount($userid, $cart_id, $amount) {
        global $db;
        $status = STATUSFALSE;
        $data = array();

        $msg = "";
        if ($userid == '' && $userid <= 0) {
            $msg = INVALIDCREDENTIAL;
        } else if (UserClass::isUser($userid) == false) {
            $msg = NOUSER;
        } else {
            $where = "WHERE 1";

            $sql = "SELECT * FROM `cart_master` WHERE userid = '" . $userid . "' AND id = '" . $cart_id . "' ";
            $res = $db->query($sql);
            if ($res->size() > 0) {
                $sql1 = "UPDATE cart_detail SET "
                        . " scheme_amount = '" . $amount . "' "
                        . " WHERE cart_id = '" . $cart_id . "'  "
                        . " ";

                $result1 = $db->query($sql1);

                $msg = "Success";
                $status = "true";
            } else {
                $msg = NORECORD;
            }
        }
        $arr = array("msg" => $msg, "status" => $status);
        return $arr;
    }

    public static function checkCart($userid, $scheme_code) {
        global $db;

        $data = array();
        $status = "false";

        $where = " WHERE 1 AND cart_master.userid = '" . $userid . "' AND scheme_code = '" . $scheme_code . "' ";
        $sql = "SELECT * FROM cart_master LEFT JOIN cart_detail ON cart_master.id = cart_detail.cart_id  $where GROUP BY cart_id ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            $row = $que->fetch();
            $data[] = $row;
            $msg = "Success";
            $status = "true";
        } else {
            $msg = "Record not found";
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

}
