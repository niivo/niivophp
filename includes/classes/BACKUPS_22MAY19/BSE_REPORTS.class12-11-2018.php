<?php

class BSE_REPORTS {
//    protected $SOAP_URL     =   "http://bsestarmfdemo.bseindia.com/MFUploadService/MFUploadService.svc/Basic";
//    protected $API_USER     =   "1707101";
//    protected $API_PASS     =   "@123456";
//    protected $API_MEMBERID =   "17071";
    var     $PASS_key       =   "";
//    protected $GET_PASSWORD_ACTION  =   '<wsa:Action>http://bsestarmfdemo.bseindia.com/2016/01/IMFUploadService/getPassword</wsa:Action>';
//    protected $UPLOAD_ACTION  =   '<wsa:Action>http://bsestarmfdemo.bseindia.com/2016/01/IMFUploadService/MFAPI</wsa:Action>';

    protected $SOAP_URL     =   "https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure";
    protected $API_USER     =   API_USER;
    protected $API_PASS     =   API_PASS;
    protected $API_MEMBERID =   API_MEMBER;
    protected $GET_PASSWORD_ACTION  =   '<wsa:Action>http://www.bsestarmf.in/2016/01/IStarMFWebService/getPassword</wsa:Action>';
    protected $UPLOAD_ACTION  =   '<wsa:Action>http://www.bsestarmf.in/2016/01/IStarMFWebService/MFAPI</wsa:Action>';

    
    private function soapHeader($tp) {
         $headerStr  ='<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="http://www.bsestarmf.in/2016/01/" xmlns:star="http://schemas.datacontract.org/2004/07/StarMFWebService"><soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">';
        $headerStr .= '<wsa:Action>http://www.bsestarmf.in/2016/01/IStarMFWebService/OrderStatus</wsa:Action>';
        $headerStr .= '<wsa:To>https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure</wsa:To>';
        $headerStr .= '</soap:Header><soap:Body>';
        return $headerStr;
    }
    
    
    private function  soapFooter(){
        $footerStr   ='</soap:Body></soap:Envelope>';
        return $footerStr;
    }
    
    
    function sendRequest($xml_string){
        $headers = array(
                        "Content-type: application/soap+xml;charset=\"utf-8\"",
                    ); //SOAPAction: your op URL

            $url = $soapUrl;
           $xml_post_string    =   $xml_string;
           
            
            

            // PHP cURL  for https connection with auth
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
            curl_setopt($ch, CURLOPT_URL, $this->SOAP_URL);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            // converting
            $response = curl_exec($ch);
            curl_close($ch);
            return $response;
    }
    
    
    function __construct($passKey="ninehertz123") {
        $this->PASS_key =   $passKey;
        
    }
    
	
	
   function parseXML($xmlString) {
        $resp = array();
        $arg=array();
        $p = xml_parser_create();
        xml_parse_into_struct($p, $xmlString, $vals, $index);
        xml_parser_free($p);
        // echo "\nVals array\n";
         //print_r($vals);

        $str="";
        foreach ($vals as $val) {
           //print_r($val);
           
            $key = substr($val['tag'], strpos($val['tag'],":")+1);
            if(in_array($key,array('ENVELOPE','HEADER','ACTION','HEADER','BODY','RDERSTATUSRESPONSE','RDERSTATUSRESULT','ORDERDETAILS')))
            {
                
            }else{
                if (is_null($val['value'])){
                    $tag    = substr($val['tag'], strpos($val['tag'],":")+1);
                    $resp[$tag]="";
                }else{
                    $tag    = substr($val['tag'], strpos($val['tag'],":")+1);
                    $resp[$tag]=$val['value'];
                }
               // $str    .= "`".$tag."`='".$val['value']."',"; 
            }
            if($key=="ORDERDETAILS" and !empty($resp)){
                $ar[]=$resp;
                $resp = array();
            }
        }
		$resutl =   $this->getValidOrder($ar);
        return $resutl;
    }
	function getValidOrder($ar)
	{
		foreach($ar as $varl)
		{
			if($varl['ORDERSTATUS']=='VALID')
			{
			$array[]  = $varl; 
			}
		}
			return $array;
	}

    
    function getXSPOrderStatus($rs){
        global $db;
        $status = false;
        $fromDate   =   $rs['from_date'];
        $toDate     =   $rs['to_date'];
        
        
        $orderParam->ClientCode     = "";
        $orderParam->Filler1        = "";
        $orderParam->Filler2        = "";
        $orderParam->Filler3        = "";
        $orderParam->FromDate       = $fromDate;
        $orderParam->MemberCode     = $this->API_MEMBERID;
        $orderParam->OrderNo        = "";
        $orderParam->OrderStatus    = "All";
        $orderParam->OrderType      = "XSP";
        $orderParam->Password       = $this->API_PASS;
        $orderParam->SettType       = "All";
        $orderParam->SubOrderType   = "All";
        $orderParam->ToDate         = $toDate;
        $orderParam->TransType      = "P";
        $orderParam->UserId         = $this->API_USER; //rand(100001,999999);

        $str = self::soapHeader("");
        $str .= " <ns:OrderStatus><ns:Param>";
        foreach ($orderParam as $k => $v) {
            $str .= "<star:" . $k . ">" . $v . "</star:" . $k . ">";
        }
        $str .= "</ns:Param></ns:OrderStatus>";
        $str .= self::soapFooter();
//        echo $str;
//        exit;
        
        $resp = self::sendRequest($str);
        $body = self::parseXML($resp);
        
        
//          

        $arr = array("status" => "true", "msg" => $msg,"data"=>$body);
        return $arr;
        }
    
}
