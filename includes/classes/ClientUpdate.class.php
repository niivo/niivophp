<?php
//$path = "/v2/";
$path = "../";
require_once '../includes/classes/Mapping.class.php';
require_once($path . 'includes/app_top.php');
require_once($path . 'includes/mysql.class.php');
require_once($path . 'includes/global.inc.php');
require_once($path . 'includes/functions_general.php');
require_once($path . 'includes/classes/CAMSAPI.class.php');
require_once ($path . 'includes/config.php');
require_once($path . 'includes/classes/BSE_API.class.php');
require_once($path . 'includes/classes/User.class.php');
require_once($path . 'includes/classes/Verifiation.class.php');

class ClientUpdate extends Models 
{

function convertDate($dt) {
    
    $dtArr = explode("-", $dt);
    $date = $dtArr[2] . "/" . $dtArr[1] . "/" . $dtArr[0];

    return $date;
}
function convertDateHyphen($dt) {
    $dtArr = explode("-", $dt);
    $date = $dtArr[1] . "-" . $dtArr[2] . "-" . $dtArr[0];

    return $date;
}
function ClientCreation($userid){
    global $db;
    $status = "false";
    $user_detail = array();
    $BSE = new BSE_API;

        if ($userid == '') {
            $msg = INVALIDCREDENTIAL;
        } else if (User::isUser($userid) == false) {
            $msg = NOUSER;
        } else {
            $str = "";
            $strFatca = "";

            $user_nominee_details = Verifiation::user_nominee_details($userid);
           
            $client_nominee = $user_nominee_details['nominee_name'];
            $relation = $user_nominee_details['nominee_relationship'];

            $user_info = User::getUserinfo($userid);
            $user_detail = $user_info['data'];
            $rs         =   $user_detail;
            $father_name = $user_detail['father_name'];
//            echo "<pre>";
//            print_r($rs);
//            exit;
//           

            $pencard = $user_detail['pencard'];
            $entity_type = $user_detail['entity_type'];

            $occupation = $user_detail['occupation'];
            $occupation_data = Mapping::mapping_occupation_code($occupation);
            $UCC_CODE = $occupation_data['UCC_CODE'];
            
            
            $occupation_FATCA_CODE = $occupation_data['FATCA_CODE'];
            $occupation_FATCA_TYPE = $occupation_data['FATCA_TYPE'];
			
			
if($occupation_FATCA_TYPE == "Service"){
	 $occupation_FATCA_TYPE = "S";
}elseif($occupation_FATCA_TYPE == "Business"){
	 $occupation_FATCA_TYPE = "B";
}elseif($occupation_FATCA_TYPE == "Others"){
	 $occupation_FATCA_TYPE = "O";
}else{
 $occupation_FATCA_TYPE = "X";
	
}
            $ca_state = $user_detail['ca_state'];
            $state_data = Mapping::mapping_state_code($rs['ca_state']);
            
            $ca_pincode = $user_detail['ca_pincode'];


            $gross_annual_income = $user_detail['gross_annual_income'];

            $occupation = $user_detail['occupation'];
            
            if ($rs['pancard_type'] == 'INDIVIDUAL') {
                $E_code = "1";
                $B_code = "";
                $N_code = "N";
                $tax_status_code = "01";
            } else {
                $E_code = "4";
                $B_code = "Y";
                $N_code = "Y";
                $tax_status_code = $rs['entity_type'];
            }


            
            $place_of_birth = Mapping::mapping_country_code($rs['birth_place']); 
          $income_code_fatca = Mapping::mapping_income_code($rs['gross_annual_income']);

           
            $nationality = $rs['nationality'];
            $tpin = $rs['tpin'];
            if ($nationality == 'INDIAN') {
                $birth_nationality_code = "IN";
                $pin_number = $pencard;
            } else {
                $birth_nationality_code = "NRI";
                $pin_number = $tpin;
            }
            $tax_residency_country = $rs['tax_residency_country'];
            if ($tax_residency_country == '101') {
                $tax_residency_nationality_code = "IN";
            } else {
                $tax_residency_nationality_code = "";
            }
//            echo "<pre>";
    
//            exit;
            $occupationType = "";
            $str .= $userid . "|SI|01|";
            $str .= $UCC_CODE . "|";
            $str .= $user_detail['name'] . "|||";
            $str .= User::convertDate($rs['dob']) . "|";
            $str .= $user_detail['gender'] . "||";
            $str .= $pencard . "|" . $client_nominee . "|" . $relation . "||P||||||";
            $str .= $user_detail['account_type'] . "|";
            $str .= $user_detail['account_number'] . "||";
            $str .= $user_detail['ifsc_code'] . "|Y|||||||||||||||||||||";
            $str .= $user_detail['account_holder_name'] . "|";
            $str .= $user_detail['ca_address'] . "|";
            $str .=  "|";
            $str .=  "|";
            $str .= $user_detail['ca_city'] . "|";
            $str .= $state_data['UCC_CODE'] . "|";
            $str .= $user_detail['ca_pincode'] . "|";
            $str .= $user_detail['country'] . "|||||";
            $str .= $user_detail['email'] . "|M|02|||||||||||||||";
             $str .= $user_detail['mobile'] . "";
//             echo "<pre>";
//             print_r($str);
//             exit;

            $strFatca .= $pencard . "||";
            $strFatca .= $user_detail['name'] . "";
            $strFatca .= "|".  User::convertDateHyphen($rs['dob'])."|" . $father_name . "||";
           
            $strFatca .= $tax_status_code . "|E|" . $E_code . "|" . $place_of_birth;
             
            //$strFatca .= "|" . $birth_nationality_code . "|" . $tax_residency_nationality_code . "|";
			$strFatca .= "|" . $birth_nationality_code . "|" . $tax_residency_nationality_code . "|";
            $strFatca .= $pin_number . "|C||||||||||";
           
		   
            $strFatca .= $rs['wealthSourceCode'] . "||";
            $strFatca .= $income_code_fatca['FATCA_CODE'] . "|||N|";
            $strFatca .= $occupation_FATCA_CODE . "|";
            $strFatca .= $occupation_FATCA_TYPE . "|||||||||||B|N||||||||||||||||||||||||||" . $B_code . "|" . $N_code . "|";
            $strFatca .= $rs['aadhar_no'] . "|N|";
            $strFatca .= $_SERVER['REMOTE_ADDR'] . "#" . date("d-m-y") . ":" . date("h:i") . "||";
            $strA=explode("|",$strFatca);
           // $strA=explode("|","CITPS6359D||ANSHUMAN SAXENA|02-02-1990|||01|E|1|DELHI|IN|IN|CITPS6359D|C||||||||||01||33|||N|03|S|||||||||||B|N||||||||||||||||||||||||||Y|N|331753457209|N|172.164.112.69#19-12-17;15:26||");
//$strA=explode("|","DHXPP7307A||sdg|01-06-1995|dfgyu||01|E|1|India|IN||DHXPP7307A|C||||||||||01||31|||N|41|S|||||||||||B|N|||||||||||||||||||||||||||N||N|112.79.73.62#01-06-19:03:51||"   );



                        
            try {
		
                $resp = $BSE->uploadClient($str);
			
			
                if ($resp['status'] == 'false') {
								
                    return array("status" => $status, "msg" => $resp['msg'], "retry" => "1", "data" => $user_detail);
                }else{
				
                    $db->query("update users set  client_created=1 where userid='".$userid."'");
                }
				
				  $resp_fatca = $BSE->FATCAuploadClient($strFatca);

				if ($resp_fatca['status'] == 'true') {
			 
					$db->query("update users set fatca_uploaded=1 where userid='".$userid."'");

                }
				else{
	
					return array("status" => $status, "msg" => $resp_fatca['msg'], "retry" => "1", "data" => $user_detail);
				
				}
				return array("status" => "Data Updated Successfully");
				


          
				
            } catch (Exception $e) {
                return array("status" => $status, "msg" => SOMETHINGWRONG, "retry" => "1", "data" => $user_detail);
            }
        }
}


   public function post_log($data)
    {
        $log_filename =  "../uploads/BSE_APIs";
        if (!file_exists($log_filename))
        {
            // create directory/folder uploads.
            mkdir($log_filename, 0777, true);
            chmod($log_filename, 0777);
        }

        if (!file_exists($log_filename . "/" . date('Y')))
        {
            // create directory/folder uploads.
            mkdir($log_filename . "/" . date('Y'), 0777, true);
            chmod($log_filename . "/" . date('Y'), 0777);
        }

        if (!file_exists($log_filename . "/" . date('Y') . "/" . date('M')))
        {
            // create directory/folder uploads.
            mkdir($log_filename . "/" . date('Y') . "/" . date('M'), 0777, true);
            chmod($log_filename . "/" . date('Y') . "/" . date('M'), 0777);
        }

        //$data = date("Y-m-d H:i:s")." - ".$data;
        $log_file_data = $log_filename . "/" . date('Y') . "/" . date('M') . '/' . date('d-M-Y') . '.log';
        file_put_contents($log_file_data, $data . "\n", FILE_APPEND);
        chmod($log_file_data, 0777);
    }


}