<?php

class Mapping extends Models {

    public static function countryCode() {
        global $db;

        $data = array();
        $status = "false";

        $sql = "SELECT * FROM mapping_country_code ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            while ($row = $que->fetch()) {
                $state[] = $row;
            }
            $data = $state;
            $msg = SUCCESS;
            $status = "true";
        } else {
            $msg = NORECORD;
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

    public static function entityTaxCode() {
        global $db;

        $data = array();
        $status = "false";

        $sql = "SELECT * FROM mapping_entity_tax_code  ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            while ($row = $que->fetch()) {

                $state[] = $row;
            }
            $data = $state;
            $msg = SUCCESS;
            $status = "true";
        } else {
            $msg = NORECORD;
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

 

    public static function incomeCode() {
        global $db;

        $data = array();
        $status = "false";

        $sql = "SELECT * FROM mapping_income_code ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            while ($row = $que->fetch()) {
                $data[] = $row;
            }
            $msg = SUCCESS;
            $status = "true";
        } else {
            $msg = NORECORD;
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }
    
    
    public static function nationalityCode() {
        global $db;

        $data = array();
        $status = "false";

        $sql = "SELECT * FROM mapping_nationality_code ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            while ($row = $que->fetch()) {
                $data[] = $row;
            }
            $msg = SUCCESS;
            $status = "true";
        } else {
            $msg = NORECORD;
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }
    public static function occupationCode() {
        global $db;

        $data = array();
        $status = "false";

        $sql = "SELECT * FROM mapping_occupation_code ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            while ($row = $que->fetch()) {
                $data[] = $row;
            }
            $msg = SUCCESS;
            $status = "true";
        } else {
            $msg = NORECORD;
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }
    
    public static function otherProfileCode() {
        global $db;

        $data = array();
        $status = "false";

        $sql = "SELECT * FROM mapping_other_profile_code ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            while ($row = $que->fetch()) {
                $data[] = $row;
            }
            $msg = SUCCESS;
            $status = "true";
        } else {
            $msg = NORECORD;
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }
    
    public static function pepCode() {
        global $db;

        $data = array();
        $status = "false";

        $sql = "SELECT * FROM mapping_pep_code ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            while ($row = $que->fetch()) {
                $data[] = $row;
            }
            $msg = SUCCESS;
            $status = "true";
        } else {
            $msg = NORECORD;
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }
    
    public static function poaCode() {
        global $db;

        $data = array();
        $status = "false";

        $sql = "SELECT * FROM mapping_poa_code ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            while ($row = $que->fetch()) {
                $data[] = $row;
            }
            $msg = SUCCESS;
            $status = "true";
        } else {
            $msg = NORECORD;
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }
    
    public static function stateCode() {
        global $db;

        $data = array();
        $status = "false";

        $sql = "SELECT * FROM mapping_state_code ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            while ($row = $que->fetch()) {
                $data[] = $row;
            }
            $msg = SUCCESS;
            $status = "true";
        } else {
            $msg = NORECORD;
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }
    
    public static function wealthSourceCode() {
        global $db;

        $data = array();
        $status = "false";

        $sql = "SELECT * FROM mapping_wealth_source_code ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            while ($row = $que->fetch()) {
                $data[] = $row;
            }
            $msg = SUCCESS;
            $status = "true";
        } else {
            $msg = NORECORD;
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }
    
  
     public static function mapping_occupation_code($occupation_code) {
        global $db;

        $data = (object)[];
        $sql = "SELECT * FROM mapping_occupation_code WHERE NIIVO_CODE = '" . $occupation_code . "' ";
        
     
        $que = $db->query($sql);
        if ($que->size() > 0) {
            $row = $que->fetch();
            $data = $row;
        }
        return $data;
    }
    
  
     public static function mapping_income_code($gross_annual_income) {
        global $db;

        $data = (object)[];
        $sql = "SELECT * FROM mapping_income_code WHERE NIIVO_CODE = '" . $gross_annual_income . "' ";
        
     
        $que = $db->query($sql);
        if ($que->size() > 0) {
            $row = $que->fetch();
            $data = $row;
        }
        return $data;
    }

    public static function mapping_state_code($NIIVO_CODE) {
        global $db;

        $data = (object)[];
        $sql = "SELECT * FROM mapping_state_code WHERE NIIVO_CODE = '" . $NIIVO_CODE . "' ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            $row = $que->fetch();
            $data = $row;
        }
        return $data;
    }
    
        
    public static function mapping_country_code($NIIVO_CODE) {
        global $db;

        $data = (object)[];
        $sql = "SELECT FRONTEND_OPTION_NAME FROM mapping_country_code WHERE NIIVO_CODE = '" . $NIIVO_CODE . "' ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            $row = $que->fetch();
            $data = $row['FRONTEND_OPTION_NAME'];
        }
        return $data;
    }
    
    public static function mapping_poa_code($NIIVO_CODE) {
        global $db;

        $data = (object)[];
        $sql = "SELECT * FROM mapping_poa_code WHERE NIIVO_CODE = '" . $NIIVO_CODE . "' ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            $row = $que->fetch();
            $data = $row;
        }
        return $data;
    }
}
