<?php
//require_once '../includes/classes/Listing.class.php';
require_once '../includes/classes/Models.class.php';
require_once('../includes/mysql.class.php');

class Cart extends Models {

    protected static $table = "cart_master";

    public static function addCart($rs) {
        global $db;
        $status = false;
// print_r($rs); exit();
        $userid = $rs['userid'];
        $order_type = $rs['order_type'];
       
        if($rs['no_of_months'] ==""){
          $no_of_months = 0;  
        }else{
            $no_of_months = $rs['no_of_months'];
        }
        
         // $no_of_months = 0;
        if($rs['target_year']==""){
            $target_year =  '0';
        }else
        {
         $target_year = $rs['target_year'];
     }
       
        $added_date = $rs['added_date'];
        $is_goal = $rs['is_goal'];
        if( $rs['goal_amount']==""){
               $goal_amount=0; 

        }else{
              $goal_amount = $rs['goal_amount'];
        }
        
        if($rs['goal_name']==""){
                $goal_name=0;

        }else{
               $goal_name = $rs['goal_name'];
        }
        
        if( $rs['goal_cat_id']==""){
                $goal_cat_id =0;

        }else{
             $goal_cat_id = $rs['goal_cat_id'];
        }
       


        $scheme_json = json_decode(stripslashes($rs['scheme_json']));

        if ($userid == '' && $userid <= 0) {
            $msg = INVALIDCREDENTIAL;
        } else if (UserClass::isUser($userid) == false) {
            $msg = NOUSER;
        } else {
            $sql1 = "INSERT INTO cart_master SET "
                    . " userid = '" . $userid . "',"
                    . " order_type = '" . $order_type . "',"
                    . " no_of_months = '" . $no_of_months . "',"
                    . " target_year = '" . $target_year . "',"
                    . " added_date = '" . $added_date . "',"
                    . " is_goal = '" . $is_goal . "',"
                    . " dtdate = '" . nowDateTime() . "' "
                    . " ";
            $result = $db->query($sql1);
            $cart_id = $result->insertID();

            // print_r($result); exit();


            foreach ($scheme_json as $key => $scheme_data) {
// echo "hieeee";
                $scheme_code = $scheme_data->scheme_code;
                $scheme_amount = $scheme_data->scheme_amount;
                $scheme_nav = $scheme_data->scheme_nav;


                $sql2 = "INSERT INTO cart_detail SET "
                        . " userid = '" . $userid . "',"
                        . " cart_id = '" . $cart_id . "',"
                        . " goal_amount = '" . $goal_amount . "',"
                        . " goal_name = '" . $goal_name . "',"
                        . " goal_cat_id = '" . $goal_cat_id . "',"
                        . " scheme_code = '" . $scheme_code . "',"
                        . " scheme_amount = '" . $scheme_amount . "',"
                        . " scheme_nav = '" . $scheme_nav . "' "
                        . " ";

                $result1 = $db->query($sql2);
                // print_r($result1); exit();
            }
// exit();

            $msg = SUCCESS;
            $status = "true";
        }

        $arr = array("msg" => $msg, "status" => $status, "cart_id" => $cart_id);
        return $arr;
    }

    public static function removeCart($userid, $cart_id) {
        global $db;
        $status = STATUSFALSE;
        $data = array();

        $msg = "";
        if ($userid == '' && $userid <= 0) {
            $msg = INVALIDCREDENTIAL;
        } else if (UserClass::isUser($userid) == false) {
            $msg = NOUSER;
        } else {
            $where = "WHERE 1";

            $sql = "SELECT * FROM `cart_master` WHERE userid = '" . $userid . "' AND id = '" . $cart_id . "' ";
            $res = $db->query($sql);
            if ($res->size() > 0) {
                $db->query("DELETE FROM cart_master WHERE id = '" . $cart_id . "'  ");
                $db->query("DELETE FROM cart_detail WHERE cart_id = '" . $cart_id . "'  ");

                $msg = SUCCESS;
                $status = "true";
            } else {
                $msg = NORECORD;
            }
        }
        $arr = array("msg" => $msg, "status" => $status);
        return $arr;
    }

    public static function cartList($userid) {
        global $db;

        $data = array();
        $status = "false";

        $where = " WHERE 1 AND userid = '" . $userid . "' ";

        $sql = "SELECT * FROM cart_master $where  ";
       
        $que = $db->query($sql);
        if ($que->size() > 0) {
            while ($row = $que->fetch()) {
                $cart_id = $row['id'];
                $cart = array();
                $sql1 = "SELECT * FROM cart_detail WHERE cart_id = '" . $cart_id . "'  "; 
                 // print_r($sql1); exit();
                $que1 = $db->query($sql1);
                if ($que1->size() > 0) {
                    while ($row1 = $que1->fetch()) {
                        $SCHEME_CODE = $row1['scheme_code'];
                        $scheme_data = self::schemeDetailCart($SCHEME_CODE);
                        $row1['scheme_detail'] = $scheme_data['data'];
						//print_r($row1['scheme_detail']); exit();
						//$array
                        $cart[] = $row1;
						
                    }
                    $row['cart_detail'] = $cart;
                }else{
      $cart2 =  array( "0" => array(
                                "id" => "",
                                "userid" => "",
                                "cart_id" => "",
                                "goal_amount" => "",
                                "goal_name" => "",
                                "goal_cat_id" => "",
                                "scheme_code" => "",
                                "scheme_amount" => "",
                                "scheme_nav" => "",
                                "scheme_detail" => array(
                                                    "AMFI_SCHEME_CODE" => "",
                                                    "ISIN" => "",
                                                    "ISIN_DIV" => "",
                                                    "SCHEME_NAME" => "",
                                                    "NAV" => "",
                                                    "ONE_M_PERF" => "",
                                                    "THREE_M_PERF" => "",
                                                    "SIX_M_PERF" => "",
                                                    "ONE_Y_PERF" => "",
                                                    "TWO_Y_PERF" => "",
                                                    "THREE_Y_PERF" => "",
                                                    "FIVE_Y_PERF" => "",
                                                    "SEVEN_Y_PERF" => "", 
                                                    "TEN_Y_PERF" => "",
                                                    "PERSISTENCE_SCORE" => "",
                                                    "RISK_AVERSION_SCORE" => "",
                                                    "RISK_TAKING_SCORE" => "",
                                                    "LONG_TERM_SCORE" => "",
                                                    "TICKER" => "",
                                                    "DS192" => "",
                                                    "SHARE_CLASS" => "",
                                                    "FUND_NAME" => "",
                                                    "FUND_BENCHMARK" => "",
                                                    "FUND_CLASS" => "",
                                                    "FUND_CATEGORY" => "",
                                                    "MIN_INIT" => "",
                                                    "MIN_SUBS" => "",
                                                    "FUND_CATEGORIZATION" => "",
                                                    "EXPENSE_RATIO" => "", 
                                                    "DESCRIPTION" =>"",
                                                    "STARMF_SCHEME_CODE"=>"",
                                                    "REDEMPTION_ALLOWED"=>"",
                                                    "PUR_ALLOWED"=>"",
                                                    "STARMF_SCHEME_NAME"=>"",
                                                   "STARMF_RTA_SCHEME_CODE"=>"",
                                                   "STARMF_DIV_REINVESTFLAG"=>"",
                                                   "STARMF_RTA_CODE"=>"",
                                                   "STARMF_SIP_FLAG"=>"", 
                                                   "L1_FLAG"=>"",
                                                   "I_FLAG"=>"",
                                                   "STARMF_MIN_PUR_AMT"=>"",
                                                   "SIP_DATES" => "",
                                                   "SIP_MIN_INSTALLMENT_AMT"=>"",
                                                   "SIP_MULTPL_AMT"=>"",
                                                   "SIP_MIN_INSTALLMENT_NUM"=>"",
                                                   "SIP_MAX_INSTALLMENT_NUM"=>"",
                                                   "FUNDS_GUJARATI"=>"",
                                                   "FUNDS_HINDI"=>"",
                                                   "FUNDS_MARATHI"=>"",
                                                   "FUNDS_ENGLISH"=>""
                                                 
                                                ) ) );
                               $row['cart_detail'] = $cart2;        
                }
                
              
                $data[] = $row;
            }

            $msg = SUCCESS;
            $status = "true";
        } else {
            $msg = NORECORD;
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

    public static function editCartAmount($userid, $cart_id, $amount) {
        global $db;
        $status = STATUSFALSE;
        $data = array();

        $msg = "";
        if ($userid == '' && $userid <= 0) {
            $msg = INVALIDCREDENTIAL;
        } else if (UserClass::isUser($userid) == false) {
            $msg = NOUSER;
        } else {
            $where = "WHERE 1";

            $sql = "SELECT * FROM `cart_master` WHERE userid = '" . $userid . "' AND id = '" . $cart_id . "' ";
            $res = $db->query($sql);
            if ($res->size() > 0) {
                $sql1 = "UPDATE cart_detail SET "
                        . " scheme_amount = '" . $amount . "' "
                        . " WHERE cart_id = '" . $cart_id . "'  "
                        . " ";

                $result1 = $db->query($sql1);

                $msg = SUCCESS;
                $status = "true";
            } else {
                $msg = NORECORD;
            }
        }
        $arr = array("msg" => $msg, "status" => $status);
        return $arr;
    }

    public static function checkCart($userid, $scheme_code) {
        global $db;

        $data = array();
        $status = "false";

        $where = " WHERE 1 AND cart_master.userid = '" . $userid . "' AND scheme_code = '" . $scheme_code . "' ";
        $sql = "SELECT * FROM cart_master LEFT JOIN cart_detail ON cart_master.id = cart_detail.cart_id  $where GROUP BY cart_id ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            $row = $que->fetch();
            $data[] = $row;
            $msg = SUCCESS;
            $status = "true";
        } else {
            $msg = NORECORD;
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }
	
	
	    public static function schemeDetailCart($SCHEME_CODE) {
        global $db;

        $data = array();
        $status = "false";

        $sql = "SELECT ID,	AMC_CODE AS AMFI_SCHEME_CODE,	SCHEME_NAME,	NAV ,	MAIN_CATEGORY,	SUB_CATEGORY,	TRADE_DATE,	RET1MONTH AS ONE_M_PERF,	RET3MONTH AS THREE_M_PERF,	RET6MONTH AS SIX_M_PERF,	RET1YEAR AS ONE_Y_PERF,	RET3YEAR AS THREE_Y_PERF,	RET5YEAR AS FIVE_Y_PERF,	RET7YEAR AS SEVEN_Y_PERF,	RET10YEAR AS TEN_Y_PERF,	RET_SINCE_INCP AS TWO_Y_PERF,	BSE_SCHEME_CODE AS STARMF_SCHEME_CODE,	BSE_RTA_SCHEME_CODE AS STARMF_RTA_SCHEME_CODE,	BSE_ISIN AS ISIN ,	BSE_SCHEME_NAME AS STARMF_SCHEME_NAME ,		BSE_PUR_ALLOWED AS PUR_ALLOWED,		BSE_MIN_PUR_AMT AS STARMF_MIN_PUR_AMT,	BSE_REDEMPTION_ALLOWED AS REDEMPTION_ALLOWED,	BSE_MIN_REDEMPTION_QTY ,	BSE_REDEMPTION_AMT_MIN,	BSE_REDEMTION_AMT_MAX,	BSE_RTA_CODE AS STARMF_RTA_CODE, BSE_DIV_REINVESTFLAG AS STARMF_DIV_REINVESTFLAG,	BSE_SIP_FLAG AS STARMF_SIP_FLAG,	BSE_L1_FLAG AS L1_FLAG,	BSE_I_FLAG AS I_FLAG,		BSE_SIP_DATES AS SIP_DATES,		BSE_SIP_MIN_INSTALLMENT_AMT AS SIP_MIN_INSTALLMENT_AMT,	BSE_SIP_MAX_INSTALLMENT_AMT ,	BSE_SIP_MULTPL_AMT AS IP_MULTPL_AMT,	BSE_SIP_MIN_INSTALLMENT_NUM AS SIP_MIN_INSTALLMENT_NUM,		BSE_SIP_MAX_INSTALLMENT_NUM AS SIP_MAX_INSTALLMENT_NUM	FROM `products`  WHERE BSE_SCHEME_CODE = '" . $SCHEME_CODE . "' ";
		
        $que = $db->query($sql);
        if ($que->size() > 0) {
            $row = $que->fetch();
				$row['STARMF_SCHEME_NAME'] = utf8_encode($row['STARMF_SCHEME_NAME']);
					$row['FUND_NAME'] = "";
					$row['FUND_CLASS'] = "";
					$row['FUND_CATEGORY'] = "";
					$row['FUND_CATEGORIZATION'] = "";
					$row['DESCRIPTION'] = "";
					if(is_null($row['ONE_M_PERF']) || empty($row['ONE_M_PERF'])){$row['ONE_M_PERF'] ="N.A.";}
					if(is_null($row['THREE_M_PERF']) || empty($row['THREE_M_PERF'])){$row['THREE_M_PERF'] ="N.A.";}
					if(is_null($row['SIX_M_PERF']) || empty($row['SIX_M_PERF'])){$row['SIX_M_PERF'] ="N.A.";}
					if(is_null($row['ONE_Y_PERF']) || empty($row['ONE_Y_PERF'])){$row['ONE_Y_PERF'] ="N.A.";}
					if(is_null($row['THREE_Y_PERF']) || empty($row['THREE_Y_PERF'])){$row['THREE_Y_PERF'] ="N.A.";}
					if(is_null($row['FIVE_Y_PERF']) || empty($row['FIVE_Y_PERF'])){$row['FIVE_Y_PERF'] ="N.A.";}
					if(is_null($row['SEVEN_Y_PERF']) || empty($row['SEVEN_Y_PERF'])){$row['SEVEN_Y_PERF'] ="N.A.";}
					if(is_null($row['TEN_Y_PERF']) || empty($row['TEN_Y_PERF'])){$row['TEN_Y_PERF'] ="N.A.";}
					if(is_null($row['TWO_Y_PERF']) || empty($row['TWO_Y_PERF'])){$row['TWO_Y_PERF'] ="N.A.";}
      
            $data = $row;
            $msg = SUCCESS;
            $status = "true";
        } else {
            $msg = NORECORD;
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

}

