<?php

class BSE_ORDER_API {


//    protected $SOAP_URL = "http://bsestarmfdemo.bseindia.com/MFOrderEntry/MFOrder.svc";
//    protected $API_USER = "1707101";
//    protected $API_MEMBER = "17071";
//    protected $API_PASS = "@123456";
//    var $PASS_key = "";
    protected $GET_PASSWORD_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://bsestarmf.in/MFOrderEntry/getPassword</wsa:Action>';
    protected $ORDER_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://bsestarmf.in/MFOrderEntry/orderEntryParam</wsa:Action>';
    protected $SOAP_URL = "http://www.bsestarmf.in/MFOrderEntry/MFOrder.svc";
//        protected $API_USER     =   "1438201";
//        protected $API_PASS     =   "@123456";
//        protected $API_MEMBER =   "14382";

    protected $API_USER = API_USER;
    protected $API_PASS = API_PASS;
    protected $API_MEMBER = API_MEMBER;
    var $PASS_KEY = "";

    private function soapHeader($tp) {
        $headerStr = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:bses="http://bsestarmf.in/">
                            <soap:Header>';
        $headerStr .= ($tp == "PASS") ? $this->GET_PASSWORD_ACTION : $this->ORDER_ACTION;
        $headerStr .= '<wsa:To xmlns:wsa="http://www.w3.org/2005/08/addressing">' . $this->SOAP_URL . '</wsa:To>';
        $headerStr .= '</soap:Header><soap:Body>';

        return $headerStr;
    }

    private function soapFooter() {
        $footerStr = '</soap:Body></soap:Envelope>';
        return $footerStr;
    }

    function sendRequest($xml_string) {
		$this->post_log($xml_string . "\n"); 
        $headers = array(
            "Content-type: application/soap+xml;charset=\"utf-8\"",
        ); //SOAPAction: your op URL

        $url = $soapUrl;
        $xml_post_string = $xml_string;


        // PHP cURL  for https connection with auth
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $this->SOAP_URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // converting
        $response = curl_exec($ch);


        curl_close($ch);
		$this->post_log($response . "\n"); 
        $xml = new SimpleXMLElement($response);
     
        $xml->registerXPathNamespace("soap", "http://www.w3.org/2003/05/soap-envelope");
        $body = $xml->xpath("//soap:Body");
      
        return $body;
    }

    function getPassword() {
        $str = self::soapHeader("PASS");
        $str .= "<bses:getPassword>" .
                "<bses:UserId>" . $this->API_USER . "</bses:UserId>" .
                "<bses:Password>" . $this->API_PASS . "</bses:Password>" .
                "<bses:PassKey>" . $this->PASS_KEY . "</bses:PassKey>" .
                "</bses:getPassword>";

        $str .= self::soapFooter();   
  
        $resp = self::sendRequest($str);   
           // print_r($resp); exit();
        $resp = ($resp[0]->getPasswordResponse->getPasswordResult);
     
        $a = explode("|", $resp);

        if ($a[0] == 100)
            return $a[1];
    }

    function __construct($passKey = "ninehertz123") {
        $this->PASS_KEY = $passKey;
        if (MODE) {
            $this->SOAP_URL = "http://www.bsestarmf.in/MFOrderEntry/MFOrder.svc";
            $this->GET_PASSWORD_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://bsestarmf.in/MFOrderEntry/getPassword</wsa:Action>';
            $this->ORDER_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://bsestarmf.in/MFOrderEntry/orderEntryParam</wsa:Action>';
        } else {
            $this->SOAP_URL = "http://bsestarmfdemo.bseindia.com/MFOrderEntry/MFOrder.svc";
            $this->GET_PASSWORD_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://bsestarmf.in/MFOrderEntry/getPassword</wsa:Action>';
            $this->ORDER_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://bsestarmf.in/MFOrderEntry/orderEntryParam</wsa:Action>';
        }
    }

    function orderEntry($rs, $order_id, $refNo) {
        global $db;
        $status = false;
        $orderType = "NEW";
//        $localOrder = self::saveLocalOrder($rs);
//        if ($localOrder['stauts'] === false)
//            return array("status" => $status, "order_no" => $localOrder['id'], "msg" => $localOrder['msg'], "retry" => "1",);

        $orderParam = new stdClass();
        try {
            $tempPass = self::getPassword();

            if (intval($rs['scheme_amount']) >= 200000) {
				
                $schemeCD = $rs['scheme_code'] . "-L1";
            } else {
                $schemeCD = $rs['scheme_code'];
            }
            $transNo = rand(1000, 9999) . time();
            $clintCode = $rs['userid'];
            $amount = $rs['scheme_amount'];
            $remarks = $rs['remarks'];

           if ($tempPass != '') {
           
                $orderParam->TransCode = $orderType;
                $orderParam->TransNo = $transNo; //rand(100001,999999);
                $orderParam->OrderId = "";
                $orderParam->UserID = $this->API_USER;
                $orderParam->MemberId = $this->API_MEMBER;
                $orderParam->ClientCode = $clintCode;
                $orderParam->SchemeCd = $schemeCD;
                $orderParam->BuySell = "P";
                $orderParam->BuySellType = "FRESH";
                $orderParam->DPTxn = "P";
                $orderParam->OrderVal = $amount;
                $orderParam->Qty = "";
                $orderParam->AllRedeem = "N";
                $orderParam->FolioNo = "";
                $orderParam->Remarks = $remarks;
                $orderParam->KYCStatus = "Y";
                $orderParam->RefNo = $refNo;
                $orderParam->SubBrCode = "";
                $orderParam->EUIN = API_EUIN;
//                $orderParam->EUIN = "E193918";
                $orderParam->EUINVal = "N";
                $orderParam->MinRedeem = "N";
                $orderParam->DPC = "Y";
                $orderParam->IPAdd = "180.188.253.61";
                $orderParam->Password = $tempPass;
                $orderParam->PassKey = $this->PASS_KEY;
                $orderParam->Parma1 = "";
                $orderParam->Param2 = "";
                $orderParam->Param3 = "";
                $str = self::soapHeader("");
                $str .= "<bses:orderEntryParam>";
			
                foreach ($orderParam as $k => $v) {
                    $str .= "<bses:" . $k . ">" . $v . "</bses:" . $k . ">";
                }
                $str .= " </bses:orderEntryParam>";
                $str .= self::soapFooter();

                $type = "ONETIME-REQUEST";

                $xml_file = User::xmlUpload($clintCode, $str, $type);

                $resp = self::sendRequest($str);
                // print_r($str); exit();

                $type = "ONETIME-RESPONSE";

                $xml_file = User::xmlUpload($clintCode, $resp, $type);

                $resp = ($resp[0]->orderEntryParamResponse->orderEntryParamResult);
                // print_r($resp); exit();
                $a = explode("|", $resp);
                $b = explode(":", $a[6]);
//print_r($resp); exit();
    
                if ($b[0] != "FAILED") {
                    $orderNo = trim($b[9]);
                    $orderNoArr = explode(" ", $orderNo);
                    $orderNo = $orderNoArr[0];
                    $msg = ORDERCONFIRM;
                    $status = true;
                    self::updateLocalOrder($order_id, "CONFIRM", trim($orderNo), "", $xml_file);
                    $rs = array("order_id" => $order_id, "userid" => $clintCode, "scheme_code" => $schemeCD, "amount" => $amount, "type" => "purchase");
                } else {
                    $orderNo = -1;
                    $msg = FAILED . $b['1'];
                    self::updateLocalOrder($order_id, "FAILED", $orderNo, $b['1'], $xml_file);
                }

                $arr = array("status" => $status, "order_id" => $transNo, "bse_order_no" => $orderNo, "retry" => "0", "msg" => $msg);

                return $arr;
            }
        } catch (Exception $e) {
            return array("status" => $status, "msg" => SOMETHINGWRONG, "retry" => "1", "order_id" => "", "bse_order_no" => "", "error" => $e->getMessage());
        }
    }

    function getFolioNo($order) {
        global $db;
        $folioNo = "";
        $sql = "select FOLIO_NO from holdings where inward_txn_no=".$order;
     
        $result = $db->query($sql);
        if ($result->size() > 0) {
            $rs = $result->fetch();
            $folioNo = $rs['FOLIO_NO'];
        }
         return $folioNo;
    }

    function orderWithdrawEntry($rs) {
      
        global $db;
        $status = false;
        $orderType = "MOD";
        $units="";
        $orderParam = new stdClass();
        $amount ="";
        try {
        

            $tempPass = self::getPassword();

            $transNo = time();
            $niivoOrderId = $rs['id'];
            $clintCode = $rs['userid'];
            $schemeCD = $rs['scheme_code'];
            $order_id = $rs['order_id'];
            $val = $rs['amount'];
            $fullamount = $rs['fullamount'];
            $fullWithdraw = $rs['full_withdraw'];
			$fullunits = $rs['fullunits'];
             
            $inward_txn_no = strtoupper($rs['INWARD_TXN_NO']);
            if($fullWithdraw == "Y"){
                $amount = "0";
                $units= "0";
            }else{
				
				$a = explode("=", $val);
                
					if($a[1]=="units"){
						$units = $a[0];
						$amount = '0';
					}else{
						$amount = $a[0];
						$units='0';
					} 
            }
          
//print_r($rs); exit();
          $schemename = Holding::schemes($schemeCD);
          $fundname= $schemename[0];
      
    
            $folioNo = self::getFolioNo($inward_txn_no);
           
            $remarks = $rs['remarks'];
            if ($tempPass != '') {
				
                $orderParam->TransCode = $orderType;
                $orderParam->TransNo = $transNo; //rand(100001,999999);
                $orderParam->OrderId = "";
                $orderParam->UserID = $this->API_USER;
                $orderParam->MemberId = $this->API_MEMBER;
                $orderParam->ClientCode = $clintCode;
                $orderParam->SchemeCd = $schemeCD;
                $orderParam->BuySell = "R";
                $orderParam->BuySellType = "FRESH";
                $orderParam->DPTxn = "P";
                $orderParam->OrderVal = $amount;
                $orderParam->Qty = $units;
                $orderParam->AllRedeem = $fullWithdraw;
                $orderParam->FolioNo = $folioNo;
                $orderParam->Remarks = $remarks;
                $orderParam->KYCStatus = "Y";
                $orderParam->RefNo = "";
                $orderParam->SubBrCode = "";
                $orderParam->EUIN = API_EUIN;
//                $orderParam->EUIN = "E193918";
                $orderParam->EUINVal = "N";
                $orderParam->MinRedeem = "N";
                $orderParam->DPC = "Y";
                $orderParam->IPAdd = "180.188.253.61";
                $orderParam->Password = $tempPass;
                $orderParam->PassKey = $this->PASS_KEY;
                $orderParam->Parma1 = "";
                $orderParam->Param2 = "";
                $orderParam->Param3 = "";
                $str = self::soapHeader("");
                $str .= "<bses:orderEntryParam>";
                foreach ($orderParam as $k => $v) {
                    $str .= "<bses:" . $k . ">" . $v . "</bses:" . $k . ">";
                }
                $str .= " </bses:orderEntryParam>";
                $str .= self::soapFooter();

                $type = "ONETIME-WITHDRAW-REQUEST";
                $xml_file = User::xmlUpload($clintCode . "-" . $folioNo, $str, $type);

                $resp = self::sendRequest($str);

               $resp = ($resp[0]->orderEntryParamResponse->orderEntryParamResult);

                $type = "ONETIME-WITHDRAW-RESPONSE";
                $xml_file = User::xmlUpload($clintCode . "-" . $folioNo, $resp, $type);

               $a = explode("|", $resp);
              
                $b = explode(":", $a[6]);
               
         

                if ($b[0] != "FAILED") {
                    if (strlen($b[9]) > 10)
                        $orderNo = $a[2];
                    else
                        $orderNo = $b[9];

                    $remarks = $b[9];

                    $msg = ORDERCONFIRM;
                    $status = true;
                    $sql = "insert into withdraw(dtdate,niivo_order_id,order_id,userid,folio_no,bse_order_no,scheme_code,remarks,full_withdraw,amount,status,qty)VALUES(" .
                            "'" . nowDateTime() . "'," .
                            "'" . $niivoOrderId . "'," .
                            "'" . $transNo . "'," .
                            "'" . $clintCode . "'," .
                            "'" . $folioNo . "'," .
                            "'" . $orderNo . "'," .
                            "'" . $schemeCD . "'," . 
                            "'" . $remarks . "'," .
                            "'" . $fullWithdraw . "'," .
                            "'" . ($amount). "',0,".
                            "'" . ($units)."')";

                    $result = $db->query($sql);
                    $insertID = $result->insertID();

                   if ($result) {
                       $rs = array("order_id" => $insertID, "userid" => $clintCode, "scheme_code" => $schemeCD, "amount" => ($amount * -1), "type" => "withdraw","bse_order_no"=>"123456");
                       
                       self::saveTransaction($rs);
                   }
                } else {
                    $orderNo = -1;
                    $msg = FAILED . $b['1'];
                }
   
$fullamount = round($fullamount, 2);
$amount = round($amount, 2);
$units= $units;
 
if($fullWithdraw == 'Y'){
	  $arr = array("status" => "true", "order_id" => $transNo, "bse_order_no" => $orderNo, "retry" => "0", "msg" => $msg,"folioNo"=>$folioNo, "fundname"=>$fundname, "amount"=>$amount, "units"=>$units, "fullWithdraw"=>$fullWithdraw, "fullamount"=>$fullamount,"fullunits"=>$fullunits ,"order_status"=>"Full Withdraw Confirmed");
 }else{
	  $arr = array("status" =>"true", "order_id" => $transNo, "bse_order_no" => $orderNo, "retry" => "0", "msg" => $msg,"folioNo"=>$folioNo, "fundname"=>$fundname, "amount"=>$amount, "units"=>$units, "fullWithdraw"=>$fullWithdraw, "fullamount"=>$fullamount,"fullunits"=>$fullunits ,"order_status"=>"Partial Withdraw Confirmed");
 }
               

                return $arr;
            }
        } catch (Exception $e) {
            return array("status" => $status, "msg" => SOMETHINGWRONG, "retry" => "1", "order_id" => "", "bse_order_no" => "", "error" => $e->getMessage());
        }
    }

    function cancelOrder($rs) {
        global $db;
        $status = false;
        $orderType = "CXL";

        $orderParam = new stdClass();
        $tempPass = self::getPassword();
        $transNo = "";

        $bse_order_no = $rs['order_id'];
        $clintCode = $rs['userid'];
        $schemeCD = ""; //$rs['scheme_code'];
        $amount = ""; //$rs['amount'];
        $remarks = $rs['remarks'];

        $sql = "SELECT * FROM orders WHERE 1 AND bse_order_no = '" . $bse_order_no . "' ";

        $que = $db->query($sql);
        if ($que->size() == 0) {
            $arr = array("status" => "false", "order_id" => $transNo, "bse_order_no" => $bse_order_no, "msg" => NORECORD);
        } else {

            if ($tempPass != '') {
                $orderParam->TransCode = $orderType;
                $orderParam->TransNo = time() . rand(101, 999);
                $orderParam->OrderId = $bse_order_no;
                $orderParam->UserID = $this->API_USER;
                $orderParam->MemberId = $this->API_MEMBER;
                $orderParam->ClientCode = $clintCode;
                $orderParam->SchemeCd = $schemeCD;
                $orderParam->BuySell = "P";
                $orderParam->BuySellType = "FRESH";
                $orderParam->DPTxn = "P";
                $orderParam->OrderVal = $amount;
                $orderParam->Qty = "";
                $orderParam->AllRedeem = "N";
                $orderParam->FolioNo = "";
                $orderParam->Remarks = $remarks;
                $orderParam->KYCStatus = "Y";
                $orderParam->RefNo = "";
                $orderParam->SubBrCode = "";
                $orderParam->EUIN = API_EUIN;
//                $orderParam->EUIN = "E193918";
                $orderParam->EUINVal = "N";
                $orderParam->MinRedeem = "N";
                $orderParam->DPC = "Y";
                $orderParam->IPAdd = "180.188.253.61";
                $orderParam->Password = $tempPass;
                $orderParam->PassKey = $this->PASS_KEY;
                $orderParam->Parma1 = "";
                $orderParam->Param2 = "";
                $orderParam->Param3 = "";
                $str = self::soapHeader("");
                $str .= "<bses:orderEntryParam>";
                foreach ($orderParam as $k => $v) {
                    $str .= "<bses:" . $k . ">" . $v . "</bses:" . $k . ">";
                }
                $str .= " </bses:orderEntryParam>";
                $str .= self::soapFooter();

                $resp = self::sendRequest($str);
                
                $resp = ($resp[0]->orderEntryParamResponse->orderEntryParamResult);
               

                $a = explode("|", $resp);
                $b = explode(":", $a[6]);
               
                if ($b[0] == "CXL CONF") {
                    $orderNo = $b[9];
                    $msg = ORDERCANCEL;
                    $status = true;
                    self::updateLocalOrder($rs['order_id'], "CANCEL", $bse_order_no, "CANCEL BY NIIVO :: PAYMENT FAILED");
                } else {
                    $orderNo = -1;
                    $msg = FAILED . $b['1'];
                    self::deleteLocalOrder($bse_order_no);
                }

                $arr = array("status" => $status, "order_id" => $transNo, "bse_order_no" => $orderNo, "msg" => $msg);
            }
        }
        return $arr;
    }

    function saveLocalOrder($rs) {
        global $db;


        $status = false;
        $clintCode = $rs['userid'];
        $orderType = $rs['order_type'];
        $schemoCD = $rs['scheme_code'];
        $amount = $rs['amount'];
        $remarks = trim($rs['remarks']);

        $quantity = $rs['quantity'];
        $is_goal = $rs['is_goal'];

        $fund_category = $rs['fund_category'];
        $goal_name = $rs['goal_name'];
        $investment_type = $rs['investment_type'];
        $goal_year = $rs['goal_year'];
        $goal_ammount = $rs['goal_ammount'];
        $scheme = json_decode(stripslashes($rs['scheme_json']));
        $bseOrderNo = array();
        $user_data = User::getUserinfo($clintCode);
        $user_info = $user_data['data'];

        $advisor_id = $user_info['advisor_id'];
        $refNo = rand(100, 999) . time();
        $L1_flag = 0;
        if (intval($amount) >= 200000) {
            $L1_flag = 1;
        }
		
		
		$user_data = User::getUserinfo($clintCode);
		$BankID = strtoupper($user_info['bank_id']);
		
        if (count($scheme) == 0)
            return array("msg" => SCHEMENOTEMPTY, "status" => $status);
		
		else if($BankID == "")
			return array("msg" => "INVALID BANK", "status" => $status);
			
        else {
			
			
            foreach ($scheme as $key => $scheme_data) {
                $scheme_name = $scheme_data->scheme_name;
                $scheme_code = $scheme_data->scheme_code;
                $scheme_nav = $scheme_data->nav;
                $scheme_amount = $scheme_data->scheme_amount;
                $scheme_qty = $scheme_data->scheme_qty;


				
              
                $sql = "insert into orders(`order_id`,`order_type`,`dtdate`,`advisor_id`,`userid`,`nav`,`scheme_code`,`l1_flag`,`amount`,`quantity`,`is_goal`,
`remarks`)VALUES(" .
                        "'" . $refNo . "'," .
                        "'" . $orderType . "'," .
                        "'" . nowDateTime() . "'," .
                        "'" . $advisor_id . "'," .
                        "'" . $clintCode . "'," .
                        "'" . $scheme_nav . "'," .
                        "'" . $scheme_code . "'," .
                        "'" . $L1_flag . "'," .
                        "'" . $scheme_amount . "'," .
                        "'" . $scheme_qty . "'," .
                        "'" . $is_goal . "'," .
                        "'" . $remarks . "')";
						
						
                $result = $db->query($sql);
                $id = $result->insertID();
                $requst = array('userid' => $clintCode, 'scheme_code' => $scheme_code, "scheme_amount" => $scheme_amount, "remarks" => $remarks);
				

                $bseResp = $this->orderEntry($requst, $id, $refNo);
				
                $bseOrderNo[] = $bseResp;
				
            }

            if ($is_goal) {
                $sql1 = "INSERT INTO goal_detail SET "
                        . " order_id = '" . $id . "',"
                        . " main_orderid = '" . $refNo . "',"
                        . " fund_category = '" . $fund_category . "',"
                        . " goal_name = '" . $goal_name . "',"
                        . " investment_type = '" . $investment_type . "',"
                        . " goal_year = '" . $goal_year . "',"
                        . " goal_ammount = '" . $goal_ammount . "',"
                        . " dtdate = '" . nowDateTime() . "' "
                        . " ";
                $resg = $db->query($sql1);
            }
            return array("msg" => SUCCESS, "status" => true, "id" => $id, "order_id" => $refNo, "schemes" => $bseOrderNo);
        } 
    }

    function updateLocalOrder($order_id, $order_status, $bse_order_no, $reason = "", $xml_file) {
        global $db;
        $status = false;

        $sql = "update orders set " .
                "order_status='" . $order_status . "'," .
                "bse_order_no='" . $bse_order_no . "'," .
                "xml_file='" . $xml_file . "'," .
                "failed_reson='" . $reason . "' where id=" . $order_id;

        $result = $db->query($sql);
        $id = $result->insertID();
        return array("msg" => SUCCESS, "status" => true, "id" => $id, "order_id" => $orderID);
    }

    function deleteLocalOrder($order_id) {
        global $db;
        $status = false;

        $sql = "delete from orders where bse_order_no='" . $order_id . "'";
        $result = $db->query($sql);
        $id = $result->insertID();
        return array("msg" => SUCCESS, "status" => true, "id" => $id, "order_id" => $orderID);
    }

    
    public static function goalDetail($order_id) {
        global $db;

        $data = (object) [];
        $status = STATUSFALSE;

        $sql = "SELECT * FROM goal_detail WHERE main_orderid = '" . $order_id . "' ";
        $que = $db->query($sql);
        $row = $que->fetch();
        if ($que->size() > 0) {
            $data = $row;
        }
        return $data;
    }

    public static function orderDetail($order_id, $nav = 0, $userid) {
        global $db;

        $data = array();
        $total_amount = 0;

        $sql = "SELECT * FROM orders WHERE order_id = '" . $order_id . "' AND userid = '" . $userid . "' ";
		
        $que = $db->query($sql);
        if ($que->size() > 0) {
            while ($row = $que->fetch()) {
            
				$scheme_data = Listing::schemeDetailnew($row['scheme_code']);
				$scheme_detail = $scheme_data['data'];
				$row['scheme_name'] = $scheme_detail['BSE_SCHEME_NAME'];
				$row['currentnav'] = $scheme_detail['NAV'];
				$data[] = $row;
            }
			

        }
        return $data;
    }

    public static function schemeNav($scheme_code) {
        global $db;

        $data = array();
        $total_amount = 0;
        $NAV = -1;

        $sql = "SELECT NAV FROM products WHERE BSE_SCHEME_CODE = '" . $scheme_code . "' ";

        $que = $db->query($sql);
        if ($que->size() > 0) {
            $row = $que->fetch();
            $NAV = $row['NAV'];
			
        }

        return $NAV;
    }


    public static function ratingOrder($userid, $order_id, $rating) {
        global $db;
        $status = STATUSFALSE;


        if ($userid == '' || $order_id == '') {
            $msg = INVALIDCREDENTIAL;
        } else {

            $where2 = " WHERE 1 AND order_id = '" . $order_id . "' AND userid = '" . $userid . "' ";
            $sql2 = "SELECT * FROM orders $where2 ";
            $que2 = $db->query($sql2);
            if ($que2->size() == 0) {
                $msg = NORECORD;
            } else {
                $where = " WHERE 1 AND order_id = '" . $order_id . "' AND userid = '" . $userid . "' ";
                $sql = "SELECT * FROM order_rating $where ";
                $que = $db->query($sql);
                if ($que->size() == 0) {
                    $sql = "INSERT INTO order_rating SET "
                            . " userid = '" . $userid . "',"
                            . " order_id = '" . $order_id . "' ,"
                            . " rating = '" . $rating . "',"
                            . " dtdate = '" . nowDateTime() . "'"
                            . " ";
                    $res = $db->query($sql);
                    $status = "true";
                    $msg = SUCCESS;
                } else {
                    $msg = "Record already exist";
                }
            }
        }
        $arr = array("msg" => $msg, "status" => $status);
        return $arr;
    }



    public static function getWithdrawQty($filio_no) {
        global $db;
        $wQty = 0;
        $wAmt = 0;
        $sql = "select round(sum(qty),3) as wQty,round(sum(amount),3) as wAmt,full_withdraw from withdraw where folio_no='" . trim($filio_no) . "' and status=1";
        $result = $db->query($sql);
        if ($result->size() > 0) {
            $rs = $result->fetch();
            $wQty = (!is_null($rs['wQty'])) ? $rs['wQty'] : 0;
            $wAmt = (!is_null($rs['wAmt'])) ? $rs['wAmt'] : 0;
            $fwid = ($rs['full_withdraw'] == "Y") ? 1 : 0;
        }
        return array("qty" => $wQty, "amt" => $wAmt, "full_withdraw" => $fwid);
    }



    public static function myMoney($userid) {
        global $db;
        // echo $userid; exit();
        $status = STATUSFALSE;
        $data = array();

        $data = BSE_ORDER_API::orderAmount($userid);
        // print_r($data); exit();
        $status = "true";
        $msg = SUCCESS;
        $arr = array("msg" => $msg, "status" => $status, "data" => $data);
        return $arr;
    }

    public static function latestSipDate($userid) {
        global $db;

        $due_date = "";
        $sql = "select * from sip_installment_dues where client_code=" . $userid . " "
                . "and ref_no in(select order_id from orders) and is_paid=0 "
                . "AND  due_date >= DATE_SUB(NOW(), INTERVAL 2 DAY) "
                . "order by due_date ASC";

        $result = $db->query($sql);
        if ($result->size() > 0) {
            $rs = $result->fetch();
            $due_date = $rs['due_date'];
        }

        return $due_date;
    }


    function saveTransaction($rs) {
        global $db;

        $order_id = $rs['order_id'];
        $userid = $rs['userid'];
        $bse_order_no = $rs['bse_order_no'];
        $schemeCode = $rs['scheme_code'];
        $amount = $rs['amount'];
        $bse_order_no = $rs['bse_order_no'];
        $type = strtoupper($rs['type']);
        $inv_type = $rs['inv_type'];

        $NAV = $rs['nav'];
        $qty = $rs['qty'];


        if (!empty($bse_order_no)) {
            $sql = "select * from transaction where bse_order_no='" . $bse_order_no . "' and userid='" . $userid . "'";
            $result = $db->query($sql);
            if ($result->size() <= 0) {

                $sql = "insert into transaction(order_id,bse_order_no,dtdate,scheme_code,inv_type,userid,nav,qty,amount,type)VALUES(" .
                        "'" . $order_id . "'," .
                        "'" . $bse_order_no . "'," .
                        "'" . nowDateTime() . "'," .
                        "'" . $schemeCode . "'," .
                        "'" . $inv_type . "'," .
                        "'" . $userid . "'," .
                        "'" . $NAV . "'," .
                        "'" . $qty . "'," .
                        "'" . $amount . "'," .
                        "'" . $type . "')";
                $result = $db->query($sql);
            }
        }
    }

    function sipDueReport($userid) {
        global $db;
        $status = "false";
        $data = array();
        $sql = "select * from sip_installment_dues where client_code=" . $userid . " "
                . "and ref_no in(select order_id from orders) and is_paid=0 "
                . "AND due_date BETWEEN  CURDATE() AND DATE_SUB(CURDATE(), INTERVAL -40 DAY) "
                . "order by due_date ASC";

        $result = $db->query($sql);

        if ($result->size() > 0) {
			
            while ($rs = $result->fetch()) {

                $order_id = $rs['ref_no'];
				$order_detail = BSE_ORDER_API::orderDetail($order_id, $nav, $userid);
                $rs['order_detail'] = $order_detail;

                if (count($order_detail) > 0) {
                   $rs['is_goal'] = $rs['order_detail'][0]['is_goal'];
                } else {
					
                   $rs['is_goal'] = "0";
				}

               $goal_detail = BSE_ORDER_API::goalDetail($order_id);
               $rs['goal_detail'] = $goal_detail;

if(!empty($order_detail)){
	 $data[] = $rs;
	 $msg = SUCCESS;
     $status = "true";
}else{
	 $msg = NOSIPFOUND;
}
               
}
            
        } else {
            $msg = NOSIPFOUND;
        }
        $arr = array("msg" => $msg, "status" => $status, "data" => $data);
        return $arr;
    }

    

    public static function getOrderDetail($order_id) {
        global $db;

//        $order_id = $rs['order_id'];
        $data = array();
        $total_amount = 0;
        $status = "false";


        $sql = "SELECT * FROM orders WHERE order_id = '" . $order_id . "'  ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            $row = $que->fetch();
            $row['nav'] = BSE_ORDER_API::schemeNav($row['scheme_code']);
			$scheme_data = Listing::schemeDetailnew($row['scheme_code']);
			$scheme_detail = $scheme_data['data'];
            $row['scheme_name'] = $scheme_detail['BSE_SCHEME_NAME'];
        
            $total_amount += $row['amount'];
            $data = $row;

            $msg = "Succes";
            $status = "true";
        } else {
            $msg = "Record not found";
        }
        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

    public static function sipRegistrationDate($order_id) {
    global $db;
        $date = "";
        $sql = "SELECT dtdate FROM sip_registration WHERE order_id = '" . $order_id . "'  ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            $row = $que->fetch();
            $date = $row['dtdate'];
        }
        return $date;
    }


	  public static function siplist($userid) {
			global $db;
        $data = array();
		$status = "false";
		
     
	    $sql = " SELECT * FROM `orders` where order_type IN('SIP' , 'XSIP') AND order_status NOT IN ('CONFIRM','CANCEL') AND userid ='" . $userid . "'  ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
			while($row = $que->fetch()){
				
			   $scheme_data = Listing::schemeDetailnew($row['scheme_code']);
			   $scheme_detail = $scheme_data['data'];
               $row['scheme_name'] = $scheme_detail['BSE_SCHEME_NAME'];
         	   $row['currentnav'] = $scheme_detail['NAV'];
			$data[] = $row;
			$msg = "Succes";
            $status = "true";
        }
		}else{
			$msg = "Record Not Found";
		}
        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }


//-------- log
   public function post_log($data)
    {
        $log_filename =  "../uploads/BSE_APIs";
        if (!file_exists($log_filename))
        {
            // create directory/folder uploads.
            mkdir($log_filename, 0777, true);
            chmod($log_filename, 0777);
        }

        if (!file_exists($log_filename . "/" . date('Y')))
        {
            // create directory/folder uploads.
            mkdir($log_filename . "/" . date('Y'), 0777, true);
            chmod($log_filename . "/" . date('Y'), 0777);
        }

        if (!file_exists($log_filename . "/" . date('Y') . "/" . date('M')))
        {
            // create directory/folder uploads.
            mkdir($log_filename . "/" . date('Y') . "/" . date('M'), 0777, true);
            chmod($log_filename . "/" . date('Y') . "/" . date('M'), 0777);
        }

        //$data = date("Y-m-d H:i:s")." - ".$data;
        $log_file_data = $log_filename . "/" . date('Y') . "/" . date('M') . '/' . date('d-M-Y') . '.log';
        file_put_contents($log_file_data, $data . "\n", FILE_APPEND);
        chmod($log_file_data, 0777);
    }

}
?>
