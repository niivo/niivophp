<?php

class AdvisorCode extends Models {

    protected static $table = "advisor_code";

    public static function setAdvisor($userid, $advisor_code) {
        global $db;
        $data = array();
        $status = STATUSFALSE;

        if ($advisor_code == '' && $userid <= 0) {
            $msg = INVALIDCREDENTIAL;
        } else {
            $sql = "select * from advisor_code where code = '" . $advisor_code . "' ";
            $res = $db->query($sql);
            if ($res->size() > 0) {
                $row = $res->fetch();
                $data = $row;
                $advisor_id = $row['id'];
                $sql1 = " UPDATE users SET "
                        . " advisor_id    =   '" . $advisor_id . "' "
                        . " WHERE "
                        . " userid = '" . $userid . "' "
                        . " ";
                $db->query($sql1);
                $msg = SUCCESS;
                $status = STATUSTRUE;
            } else {
                $msg = NOADVISOREXIST;
            }
        }
        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

    public static function getAdvisor($userid) {
        global $db;

        $data = array();
        $status = STATUSFALSE;

        if ($userid == '' && $userid <= 0) {
            $msg = INVALIDCREDENTIAL;
        } else {
            $userinfo = User::getUserinfo($userid);
            $user_detail = $userinfo['data'];

            $advisor_id = $user_detail['advisor_id'];
            $sql = "select * from advisor_code where id = '" . $advisor_id . "' ";
            $res = $db->query($sql);
            if ($res->size() > 0) {
                $row = $res->fetch();
                $data = $row;
                $msg = SUCCESS;
                $status = STATUSTRUE;
            } else {
                $msg = NORECORD;
            }
        }
        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

    public static function removeAdvisor($userid) {
        global $db;

        $data = array();
        $status = STATUSFALSE;

        if ($userid == '' && $userid <= 0) {
            $msg = INVALIDCREDENTIAL;
        } else {

            $sql1 = " UPDATE users SET "
                    . " advisor_id    =   '0' "
                    . " WHERE "
                    . " userid = '" . $userid . "' "
                    . " ";
            $db->query($sql1);

            $msg = SUCCESS;
            $status = STATUSTRUE;
        }
        $arr = array("status" => $status, "msg" => $msg);
        return $arr;
    }

}
