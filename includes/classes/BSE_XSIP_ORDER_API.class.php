<?php

class BSE_XSIP_ORDER_API {

//    protected $SOAP_URL = "http://bsestarmfdemo.bseindia.com/MFOrderEntry/MFOrder.svc";
//    protected $GET_PASSWORD_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://bsestarmf.in/MFOrderEntry/getPassword</wsa:Action>';
//    protected $ORDER_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://bsestarmf.in/MFOrderEntry/sipOrderEntryParam</wsa:Action>';
//    protected $SOAP_URL = "http://www.bsestarmf.in/MFOrderEntry/MFOrder.svc";
//    protected $GET_PASSWORD_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://bsestarmf.in/MFOrderEntry/getPassword</wsa:Action>';
//    protected $ORDER_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://bsestarmf.in/MFOrderEntry/sipOrderEntryParam</wsa:Action>';
//        protected $API_USER     =   "1438202";
//        protected $API_PASS     =   "123456";
//        protected $API_MEMBER =   "14382";

    protected $API_USER = API_USER;
    protected $API_PASS = API_PASS;
    protected $API_MEMBER = API_MEMBER;
    protected $EUIN_VAL = API_EUIN;
    var $PASS_key = "";

    private function soapHeader($tp) {
        $headerStr = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:bses="http://bsestarmf.in/">
                            <soap:Header>';
        $headerStr .= ($tp == "PASS") ? $this->GET_PASSWORD_ACTION : $this->ORDER_ACTION;
        $headerStr .= '<wsa:To xmlns:wsa="http://www.w3.org/2005/08/addressing">' . $this->SOAP_URL . '</wsa:To>';
        $headerStr .= '</soap:Header><soap:Body>';
        return $headerStr;
    }

    private function soapFooter() {
        $footerStr = '</soap:Body></soap:Envelope>';
        return $footerStr;
    }

    function sendRequest($xml_string) {
$this->post_log($xml_string . "\n"); 
        $headers = array(
            "Content-type: application/soap+xml;charset=\"utf-8\"",
        ); //SOAPAction: your op URL

        $url = $soapUrl;
        $xml_post_string = $xml_string;



        // PHP cURL  for https connection with auth
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $this->SOAP_URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // converting
        $response = curl_exec($ch);
		//print_r($response); exit();
        curl_close($ch);

$this->post_log($response . "\n"); 
        $xml = new SimpleXMLElement($response);
        $xml->registerXPathNamespace("soap", "http://www.w3.org/2003/05/soap-envelope");
        $body = $xml->xpath("//soap:Body");
//        print_r($response);
//        echo "Ashish";
      
        return $body;
    }

    function getPassword() {
        $str = self::soapHeader("PASS");
        $str .= "<bses:getPassword>" .
                "<bses:UserId>" . $this->API_USER . "</bses:UserId>" .
                "<bses:Password>" . $this->API_PASS . "</bses:Password>" .
                "<bses:PassKey>" . $this->PASS_key . "</bses:PassKey>" .
                "</bses:getPassword>";
        $str .= self::soapFooter();

        $resp = self::sendRequest($str);
        $resp = ($resp[0]->getPasswordResponse->getPasswordResult);
        $a = explode("|", $resp);


        if ($a[0] == 100)
            return $a[1];
    }

    function __construct($passKey = "ninehertz123") {
        $this->PASS_key = $passKey;
        if (MODE) {
            $this->SOAP_URL = "http://www.bsestarmf.in/MFOrderEntry/MFOrder.svc";
            $this->GET_PASSWORD_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://bsestarmf.in/MFOrderEntry/getPassword</wsa:Action>';
            $this->ORDER_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://bsestarmf.in/MFOrderEntry/xsipOrderEntryParam</wsa:Action>';
        } else {
            $this->SOAP_URL = "http://bsestarmfdemo.bseindia.com/MFOrderEntry/MFOrder.svc";
            $this->GET_PASSWORD_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://bsestarmf.in/MFOrderEntry/getPassword</wsa:Action>';
            $this->ORDER_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://bsestarmf.in/MFOrderEntry/xsipOrderEntryParam</wsa:Action>';
        }
    }
    
//    function checkUserMandate($clintCode){
//        global $db;
//        $mandateInfo    =   array();
//        $sql    ="select mandate_id,is_mendate,mandate_limit from users where userid=".$clintCode;
//        $result =   $db->query($sql);
//        if ($result->size()>0){
//            $rs =   $result->fetch();
//            $mandateInfo    =   $rs;
//        }
//    }

    function saveXSipOrder($rs) {
        global $db;
        $status = false;
        
        $clintCode = $rs['userid'];
        $orderType = $rs['order_type'];
        $schemoCD = $rs['scheme_code'];
        $amount = $rs['amount'];
        $remarks = $rs['remarks'];

        $quantity = $rs['quantity'];
        $is_goal = $rs['is_goal'];

        $installment = $rs['installment'];
        $startDate = $rs['startDate'];

        $fund_category = $rs['fund_category'];
        $goal_name = $rs['goal_name'];
        $investment_type = $rs['investment_type'];
        $goal_year = $rs['goal_year'];
        $goal_ammount = $rs['goal_ammount'];
        $mandate_id     =   $rs['mandate_id'];
           
        $scheme         = json_decode(stripslashes($rs['scheme_json']));
        $bseOrderNo     =   array();
//        $mandateInfo    =   self::checkUserMandate($clintCode);
//        $madateStatus   =   0;
//        $mandate_id     =   "";
//        $mandate_limit  =   "";
//        if (!empty($mandateInfo)){
//            if($mandateInfo['is_mendate']=="VERIFIED"){
//                $madateStatus   =   1;
//                $mandate_id     =   $mandateInfo['mandate_id'];
//                $mandate_limit  =   $mandateInfo['mandate_limit'];
//            }
//        }
        
        $refNo = time();

        if (count($scheme) == 0)
            return array("msg" => SCHEMENOTEMPTY, $status = false);
        else {
            foreach ($scheme as $key => $scheme_data) {


                $scheme_name    = $scheme_data->scheme_name;
                $scheme_code    = $scheme_data->scheme_code;
                $scheme_nav     = $scheme_data->nav;
                $scheme_amount  = $scheme_data->scheme_amount;
                $scheme_qty     = $scheme_data->scheme_qty;

                $sql = "insert into orders(order_id,order_type,dtdate,userid,nav,scheme_code,amount,quantity,is_goal,remarks)VALUES(" .
                        "'" . $refNo . "'," .
                        "'" . $orderType . "'," .
                        "'" . nowDateTime() . "'," .
                        "'" . $clintCode . "'," .
                        "'" . $scheme_nav . "'," .
                        "'" . $scheme_code . "'," .
                        "'" . $scheme_amount . "'," .
                        "'" . $scheme_qty . "'," .
                        "'" . $is_goal . "'," .
                        "'" . $remarks . "')";
                $result = $db->query($sql);
                $id = $result->insertID();
                $requst = array('userid' => $clintCode, 'scheme_code' => $scheme_code, "scheme_amount" => $scheme_amount, "remarks" => $remarks, "startDate" => $startDate, "installment" => $installment);

                $bseResp = $this->xsipOrderEntry($requst, $id, $refNo,$mandate_id);

                $bseOrderNo[] = $bseResp;
            }
            
            if ($is_goal) {
                $sql1 = "INSERT INTO goal_detail SET "
                        . " order_id = '" . $id . "',"
                        . " main_orderid = '" . $refNo . "',"
                        . " fund_category = '" . $fund_category . "',"
                        . " goal_name = '" . $goal_name . "',"
                        . " investment_type = '" . $investment_type . "',"
                        . " goal_year = '" . $goal_year . "',"
                        . " goal_ammount = '" . $goal_ammount . "',"
                        . " dtdate = '" . nowDateTime() . "' "
                        . " ";
                $resg = $db->query($sql1);
            }


            return array("msg" => SUCCESS, "status" => true, "id" => $id, "order_id" => $orderID, "schemes" => $bseOrderNo);
        }
    }

    function updateLocalOrder($order_id, $order_status, $bse_order_no, $reason = "") {
        global $db;
        $status = false;

        $sql = "update orders set " .
                "order_status='" . $order_status . "'," .
                "sip_registration_no='" . $bse_order_no . "'," .
                "failed_reson='" . $reason . "' where order_id=" . $order_id;
        $result = $db->query($sql);
        $id = $result->insertID();
        return array("msg" => SUCCESS, "status" => true, "id" => $id, "order_id" => $orderID);
    }

    function deleteLocalOrder($order_id) {
        global $db;
        $status = false;

        $sql = "delete from orders where bse_order_no='" . $order_id . "'";
        $result = $db->query($sql);
        $id = $result->insertID();
        return array("msg" => SUCCESS, "status" => true, "id" => $id, "order_id" => $orderID);
    }

    public static function orderList($userid) {
        global $db;

        $data = array();
        $status = "false";

        $sql = "SELECT * FROM orders WHERE 1 ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            while ($row = $que->fetch()) {
                $SCHEME_CODE = $row['scheme_code'];

                $scheme_data = Listing::schemeDetail($SCHEME_CODE);
                $scheme_detail = $scheme_data['data'];
                if ($scheme_detail['FUND_NAME'] != null) {
                    $row['FUND_NAME'] = $scheme_detail['FUND_NAME'];
                } else {
                    $row['FUND_NAME'] = "";
                }
                $state[] = $row;
            }
            $data = $state;
            $msg = SUCCESS;
            $status = "true";
        } else {
            $msg = NORECORD;
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

    function xsipOrderEntry($rs, $order_id, $refNo,$mandate_id) {

        global $db;
        $status = false;
        $orderType = "NEW";

        $orderParam = new stdClass();
        try {

            $tempPass = self::getPassword();

            $transNo = time();

//            $transNo = $localOrder['order_id'];

            $clintCode = $rs['userid'];
            $schemeCD = $rs['scheme_code'];
            $amount = $rs['scheme_amount'];
            $remarks = $rs['remarks'];
            $startDate = $rs['startDate'];
            $frequency = "MONTHLY";
            $noOfInstallment = $rs['installment'];

            if ($tempPass != '') {
                $orderParam->TransactionCode = $orderType;
                $orderParam->UniqueRefNo = time() . rand(101, 999);
//                $orderParam->UniqueRefNo = $transNo; //rand(100001,999999);
                $orderParam->SchemeCode = $schemeCD;
                $orderParam->MemberCode = $this->API_MEMBER;
                $orderParam->ClientCode = $clintCode;
                $orderParam->UserId = $this->API_USER;

                $orderParam->InternalRefNo = $refNo;

                $orderParam->TransMode = "P";
                $orderParam->DpTxnMode = "P";
                $orderParam->StartDate = $startDate;
                $orderParam->FrequencyType = $frequency;
                $orderParam->FrequencyAllowed = 1;
                $orderParam->InstallmentAmount = $amount;
                $orderParam->NoOfInstallment = $noOfInstallment;
                $orderParam->Remarks = $remarks;
                $orderParam->FolioNo = "";
                $orderParam->FirstOrderFlag = "N";
                $orderParam->Brokerage = "";
                $orderParam->MandateID = $mandate_id;
                $orderParam->SubberCode = "";
                $orderParam->Euin = $this->EUIN_VAL;
                //$orderParam->Euin = "E193918";
                $orderParam->EuinVal = "N";

                $orderParam->DPC = "Y";

                $orderParam->XsipRegID = "";
                $orderParam->IPAdd = "180.188.253.61";
                $orderParam->Password = $tempPass;
                $orderParam->PassKey = $this->PASS_key;
                $orderParam->Parma1 = "";
                $orderParam->Param2 = "";
                $orderParam->Param3 = "";


                $str = self::soapHeader("");

                $str .= "<bses:xsipOrderEntryParam>";
                foreach ($orderParam as $k => $v) {
                    $str .= "<bses:" . $k . ">" . $v . "</bses:" . $k . ">";
                }
                $str .= " </bses:xsipOrderEntryParam>";
                $str .= self::soapFooter();
//                echo $str;
//                //exit;
//              

                $type = "XSIP-REQUEST";
                $xml_file = User::xmlUpload($clintCode, $str, $type);

                $resp = $this->sendRequest($str);
                
                
                $type = "XSIP-RESPONSE";
                $xml_file = User::xmlUpload($clintCode, $resp, $type);

               
                $resp = ($resp[0]->xsipOrderEntryParamResponse->xsipOrderEntryParamResult);
                

                $a = explode("|", $resp);
                $b = explode(":", $a[6]);
                // print_r($b);
                if ($b[0] != "FAILED") {
                    $orderNo = $b[1];
                    $msg = "Order Confirm";
                    $status = true;
                    self::updateLocalOrder($refNo, "CONFIRM", trim($orderNo));
                    $rs = array("order_id" => $refNo, "userid" => $clintCode, "scheme_code" => $schemeCD, "amount" => $amount, "type" => "purchase");
                } else {
                    $orderNo = -1;
                    $msg = FAILED . $b['1'];
                    self::updateLocalOrder($refNo, "FAILED", $orderNo, $b['1']);
                }

                $arr = array("status" => $status, "order_id" => $transNo, "bse_order_no" => $orderNo, "retry" => "0", "msg" => $msg);
                return $arr;
            }
        } catch (Exception $e) {
            return array("status" => $status, "msg" => SOMETHINGWRONG, "retry" => "1", "order_id" => "", "bse_order_no" => "", "error" => $e->getMessage());
        }
    }

    function xsipOrderCancel($rs) {
        global $db;
        $status = false;


        $userid = $rs['userid'];
        $orderid = $rs['orderid'];
        $is_goal = $rs['is_goal'];
        $goal_id = $rs['goal_id'];

		

        $sql = "SELECT * FROM orders WHERE 1 AND userid = '" . $userid . "' AND order_id = '" . $orderid . "' ";
        $que = $db->query($sql);
        if ($que->size() == 0) {
            $arr = array("status" => "false", "order_id" => $orderid, "msg" => NORECORD);
        } else {
            if ($is_goal == 1) {
                $sql1 = "select * from goal_detail where main_orderid = '" . $orderid . "' AND id = '" . $goal_id . "' ";
                $res1 = $db->query($sql1);
                if ($res1->size() > 0) {

                    $sql1 = "DELETE FROM goal_detail where main_orderid = '" . $orderid . "' AND id = '" . $goal_id . "' ";
                    $result1 = $db->query($sql1);
                }
            }
            while ($row = $que->fetch()) {
				
			    $sip_regisration_no = $row['sip_registration_no'];
                $scheme_code = $row['scheme_code'];
                $scheme_amount = $row['amount'];
                $remarks = $row['remarks'];

                $requst = array('userid' => $userid, 'orderid' => $orderid, "regn_id" => $sip_regisration_no, "scheme_code" => $scheme_code, "scheme_amount" => $scheme_amount, "remarks" => $remarks, "installment" => $installment);
                $sipResp = $this->sipCancel($requst);
            }
            $status = "true";
            $msg = ORDERCANCEL;
            $arr = array("status" => $status, "order_id" => $orderid, "retry" => "0", "msg" => $msg);
        }

        return $arr;
    }

    function sipCancel($rs) {
        global $db;
        $status = false;
        $orderType = "CXL";
		$orderParam = new stdClass();
		$data = array();
        try {
			
			//	$sql = "SELECT * FROM `orders` WHERE bse_order_no ='".$rs['INWARD_TXN_NO']."'";
			//	 $que = $db->query($sql);
			//		if ($que->size() > 0) {
				//		$row = $que->fetch(); 
					//	print_r($row);
						$sip_registration_no = "871531"; //$row['sip_registration_no'];
						$schemeCD = "SCGP"; //$row['scheme_code'];
						$niivoOrderId = "4481516182"; //$row['order_id'];
				//	}
					
							$sql1 = "SELECT * FROM `sip_installment_dues` WHERE `sip_registration_no`='".$sip_registration_no."'";
							$que1 = $db->query($sql1);
							
							if ($que1->size() > 0) {
								$row1 = $que1->fetch(); 
							//	print_r($row1); 
								$data['startdate'] = $row1['start_date'];
								$data['reg_no'] = $row1['sip_registration_no'];
								$data['amount'] = $row1['amount'];
								
															
								$scheme_data = Listing::schemeDetailnew($row['scheme_code']);
								$scheme_detail = $scheme_data['data'];
								$data['scheme_name'] = $scheme_detail['BSE_SCHEME_NAME'];
							
							
							}
							
						
          $tempPass = self::getPassword();

            $transNo = time();

            
            $clintCode = $rs['userid'];
           

            if ($tempPass != '') {
                $orderParam->TransactionCode = $orderType;
                $orderParam->UniqueRefNo = date('Ymd') . rand(101, 999);
//                $orderParam->UniqueRefNo = $transNo; //rand(100001,999999);
                $orderParam->SchemeCode = $schemeCD;
                $orderParam->MemberCode = $this->API_MEMBER;
                $orderParam->ClientCode = $clintCode;
                $orderParam->UserId = $this->API_USER;

                $orderParam->InternalRefNo = "";

                $orderParam->TransMode = "P";
                $orderParam->DpTxnMode = "P";
                $orderParam->StartDate = "";
                $orderParam->FrequencyType = "";
                $orderParam->FrequencyAllowed = "";

			    $orderParam->InstallmentAmount ="";
                $orderParam->NoOfInstallment = "";
                $orderParam->Remarks = "";
                $orderParam->FolioNo = "";
                $orderParam->FirstOrderFlag = "";
                $orderParam->SubberCode = "";
                $orderParam->Euin = $this->EUIN_VAL;

                $orderParam->EuinVal = "N";
                $orderParam->DPC = "";
                $orderParam->XsipRegID = $sip_registration_no;

				$orderParam->IPAdd ="";
                $orderParam->Password = $tempPass;
                $orderParam->PassKey = $this->PASS_key;
                $orderParam->Param1 = "";
                $orderParam->Param2 = "";
                $orderParam->Param3 = "";


                $str = self::soapHeader("");

                $str .= "<bses:xsipOrderEntryParam>";
                foreach ($orderParam as $k => $v) {
                    $str .= "<bses:" . $k . ">" . $v . "</bses:" . $k . ">";
                }
                $str .= " </bses:xsipOrderEntryParam>";
                $str .= self::soapFooter();

                $resp = $this->sendRequest($str);


                $resp = ($resp[0]->xsipOrderEntryParamResponse->xsipOrderEntryParamResult);

				
                $a = explode("|", $resp); 
                $b = explode(":", $a[6]);

                if ($b[0] != "FAILED") {
					
                    $orderNo = $b[1];
                    $msg = "Order Cancelled";
                    $status = true;
                    self::cancelLocalOrder($niivoOrderId, "CANCEL","",$a[6]);
                } else {
                    $orderNo = -1;
                    $msg = FAILED . $b['1'];
                }

                $arr = array("status" => $status, "order_id" => $a[1], "bse_order_no" => $orderNo, "retry" => "0", "msg" => $msg,     "data" =>$data);
                return $arr;
            }
		 $arr = array("status" => $status, "order_id" => $a[1], "bse_order_no" => $orderNo, "retry" => "0", "msg" => $msg,     "data" =>$data);
                return $arr;
        } catch (Exception $e) {
            return array("status" => $status, "msg" => SOMETHINGWRONG, "retry" => "1", "order_id" => "", "bse_order_no" => "", "error" => $e->getMessage());
        }
    } 
	
/* 
function sipCancel($rs) {
			$data['startdate'] = "2019-08-23";
							$data['reg_no'] ="6587841";
								$data['amount'] = "100";
								$data['scheme_name'] = "Aditya Birla fund XXX";
						//		$data['bse_response'] = "bse cancelled response";
		    $arr = array("status" => true, "order_id" =>"123456789", "bse_order_no" => "1234567345", "retry" => "0", "msg" => "success", "data" => $data);
		// $arr = array("status" => "false", "order_id" =>"123456789", "bse_order_no" => "1234567345", "retry" => "0", "msg" => "success", "data" => $data);
                return $arr;
}*/

    function cancelLocalOrder($order_id, $order_status, $bse_order_no, $reason = "") {
        global $db;
        $status = false;

        $sql = "update orders set " .
                "order_status='" . $order_status . "'," .
                "failed_reson='" . $reason . "' where order_id= " .$order_id.  " AND order_status='CONFIRM'";
		
        $result = $db->query($sql);
        $id = $result->insertID();
		
        return array("msg" => SUCCESS, "status" => true, "id" => $id, "order_id" => $order_id);
    }


	//-------- log
   public function post_log($data)
    {
        $log_filename =  "../uploads/BSE_APIs";
        if (!file_exists($log_filename))
        {
            // create directory/folder uploads.
            mkdir($log_filename, 0777, true);
            chmod($log_filename, 0777);
        }

        if (!file_exists($log_filename . "/" . date('Y')))
        {
            // create directory/folder uploads.
            mkdir($log_filename . "/" . date('Y'), 0777, true);
            chmod($log_filename . "/" . date('Y'), 0777);
        }

        if (!file_exists($log_filename . "/" . date('Y') . "/" . date('M')))
        {
            // create directory/folder uploads.
            mkdir($log_filename . "/" . date('Y') . "/" . date('M'), 0777, true);
            chmod($log_filename . "/" . date('Y') . "/" . date('M'), 0777);
        }

        //$data = date("Y-m-d H:i:s")." - ".$data;
        $log_file_data = $log_filename . "/" . date('Y') . "/" . date('M') . '/' . date('d-M-Y') . '.log';
        file_put_contents($log_file_data, $data . "\n", FILE_APPEND);
        chmod($log_file_data, 0777);
    }
}
