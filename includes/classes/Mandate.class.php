<?php

class Mandate {
//    protected $SOAP_URL     =   "http://bsestarmfdemo.bseindia.com/MFUploadService/MFUploadService.svc/Basic";
//    protected $API_USER     =   "1707101";
//    protected $API_PASS     =   "@123456";
//    protected $API_MEMBERID =   "17071";
        var     $PASS_key       =   "";
//    protected $GET_PASSWORD_ACTION  =   '<wsa:Action>http://bsestarmfdemo.bseindia.com/2016/01/IMFUploadService/getPassword</wsa:Action>';
//    protected $UPLOAD_ACTION  =   '<wsa:Action>http://bsestarmfdemo.bseindia.com/2016/01/IMFUploadService/MFAPI</wsa:Action>';

    protected $SOAP_URL     =   "http://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Basic";
    
    protected $SOAP_URL_MENDATE     =   "https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure";
    
    protected $API_USER     =   API_USER;
    protected $API_PASS     =   API_PASS;
    protected $API_MEMBERID =   API_MEMBER;
    protected $GET_PASSWORD_ACTION  =   '<wsa:Action>http://www.bsestarmf.in/2016/01/IStarMFWebService/getPassword</wsa:Action>';
    protected $UPLOAD_ACTION  =   '<wsa:Action>http://www.bsestarmf.in/2016/01/IStarMFWebService/MFAPI</wsa:Action>';

    
    private function soapHeader($tp) {
        $headerStr  ='<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="http://www.bsestarmf.in/2016/01/">
                            <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">';
        $headerStr .= ($tp == "PASS") ? $this->GET_PASSWORD_ACTION : $this->UPLOAD_ACTION;
        $headerStr .= '<wsa:To>http://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Basic</wsa:To>';
        $headerStr .= '</soap:Header><soap:Body>';
        return $headerStr;
    }
    private function soapMandateHeader($tp) {
        
        $headerStr  ='<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="http://www.bsestarmf.in/2016/01/" xmlns:star="http://schemas.datacontract.org/2004/07/StarMFWebService"><soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">';
        $headerStr .= '<wsa:Action>http://www.bsestarmf.in/2016/01/IStarMFWebService/EMandateAuthURL</wsa:Action>';
        $headerStr .= '<wsa:To>https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Secure</wsa:To>';
        $headerStr .= '</soap:Header><soap:Body>';
        return $headerStr;
    }
    
    private function  soapFooter(){
        $footerStr   ='</soap:Body></soap:Envelope>';
        return $footerStr;
    }
    
    
    function sendRequest($xml_string){
		$this->post_log($xml_string . "\n"); 
        $headers = array(
                        "Content-type: application/soap+xml;charset=\"utf-8\"",
                    ); //SOAPAction: your op URL
        
           $url = $this->SOAP_URL;
           
           $xml_post_string    =   $xml_string;
           

            // PHP cURL  for https connection with auth
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            // converting
            $response = curl_exec($ch); 
            curl_close($ch);
           
            
			$this->post_log($response . "\n"); 
            $xml = new SimpleXMLElement($response);
            $xml->registerXPathNamespace("soap", "http://www.w3.org/2003/05/soap-envelope");
            $body = $xml->xpath("//soap:Body");
            return $body;
    }
    
    
    function sendRequestMandate($xml_string){
		$this->post_log($xml_string . "\n"); 
        $headers = array(
                        "Content-type: application/soap+xml;charset=\"utf-8\"",
                    ); //SOAPAction: your op URL
        
           $url = $this->SOAP_URL_MENDATE;
           
           $xml_post_string    =   $xml_string;
           

            // PHP cURL  for https connection with auth
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            // converting
            $response = curl_exec($ch);

            curl_close($ch);

			$this->post_log($response . "\n"); 
            return $response;
    }
    
    
    function __construct($passKey="ninehertz123") {
        $this->PASS_key =   $passKey;
        
    }
    
    function getPassword(){
        $str = self::soapHeader("PASS");
        $str    .=   "<ns:getPassword>".         
                    "<ns:UserId>".$this->API_USER."</ns:UserId>".     
                    "<ns:MemberId>".$this->API_MEMBERID."</ns:MemberId>".
                    "<ns:Password>".$this->API_PASS."</ns:Password>".         
                    "<ns:PassKey>".$this->PASS_key."</ns:PassKey>".
                    "</ns:getPassword>";
        $str .= self::soapFooter();
		
       
        //$resp   =    self::sendRequest($str);
        $resp = self::sendRequest($str);
		
        $resp = ($resp[0]->getPasswordResponse->getPasswordResult);
       
       
        $a = explode("|", $resp);
       

        if ($a[0] == 100)
            return $a[1];
    }
    
    function makeMandateString($rs,$amount){
        global $db;
        $clientCode =   $rs['userid'];
        $amount     =   $amount;
        $mandateType=   "N";
        $accountNo  =   $rs['account_number'];
        $acType     =   $rs['account_type'];
        $ifscCode   =   $rs['ifsc_code'];
        $startDate  =   date("d/m/Y",  strtotime("+1 Day"));
        $y    =   date("Y")+100;
        $endDate  =   date("d/m/".$y);
        $str    =   $clientCode."|".$amount."|".$mandateType."|".$accountNo."|".$acType."|".$ifscCode."||".$startDate."|".$endDate;
       
        return $str;
    }
    
    function updateUserMandateStatus($userid,$mandateID,$mandateLimit, $mandateStatus){
        global $db;
       $sql    =   "update users set mandate_id=".$mandateID.",mandate_limit=".$mandateLimit.
               ",is_mendate='".$mandateStatus."' where userid='".$userid."'";
      
        $result =   $db->query($sql);
        
    }
    
    function mandateRegister($rs){
        $status = false;
        $userID =   $rs['userid'];
        $mandateAmount  =  $rs['amount'];
		
        if ($userID=="" or intval($userID)<=0 )
            return array("status" => $status, "msg" => "Invalid Client Code");
        else{
			
             $tempPass = self::getPassword();
			
            $userInfo   = User::getUserinfo($userID);
            $userDetail =   $userInfo['data'];
         
            if ($userDetail['kyc_status'] == " ")
                return array("status" => $status, "msg" => "Your KYC is not complete, Register for KYC");
            else{
				
                if ($tempPass != '') {
                    
                    $str=   $this->makeMandateString($userDetail,$mandateAmount);
                    $orderParam->Flag = "06";
                    $orderParam->UserId = $this->API_USER; //rand(100001,999999);
                    $orderParam->EncryptedPassword = $tempPass;
                    $orderParam->param = $str;
                    $str = self::soapHeader("");
                    $str .= " <ns:MFAPI>";
                    foreach ($orderParam as $k => $v) {
                        $str .= "<ns:" . $k . ">" . $v . "</ns:" . $k . ">";
                    }
                    $str .= " </ns:MFAPI>";
                    $str .= self::soapFooter();
				

                    $resp = self::sendRequest($str);

                    $resp = ($resp[0]->MFAPIResponse->MFAPIResult);
                   
                    $a = explode("|", $resp);

                    if ($a[0] == 100){
                        $msg   =    $a[1];
                        $status=    "true";
                        $mandate_id =   $a[2];
                        $this->updateUserMandateStatus($userID,$mandate_id,$mandateAmount,"NOT REGISTERED");
                    }else{
                        $msg   =    $a[1];
                        $status=    "false";
                    }
    //          

                    $arr = array("status" => $status, "msg" => $msg,"mandate_id"=>$mandate_id,"url"=>"http://google.com");
                    return $arr;
                }
            }
        }
    }
	
	function mandateStatus($data){
			global $db;
			$userid =   $data['userid'];		
			$sql    =   "update users set is_mendate='REGISTERED' where userid='".$userid."'";	

			$result =   $db->query($sql);
			
			if($result){
			echo "updated successfully"; 
			}
			
		

		
		
	}
    
    function parseXML($xmlString) {
        $resp = array();
        $p = xml_parser_create();
        xml_parse_into_struct($p, $xmlString, $vals, $index);
        xml_parser_free($p);
        // echo "\nVals array\n";
        // print_r($vals);


        foreach ($vals as $val) {
            if ($val['tag'] == "B:RESPONSESTRING") {
                $resp['url'] = $val['value'];
            }
            if ($val['tag'] == "B:STATUS") {
                $resp['status'] = $val['value'];
            }
        }

        return $resp;
    }
    
    function mandateAuthURL($rs){
        $status = false;
        $userID =   $rs['userid'];
        $mandate_id  =  trim($rs['mandate_id']);
                        
        

        $orderParam->ClientCode = $userID;
        $orderParam->MandateID = $mandate_id;
        $orderParam->MemberCode =$this->API_MEMBERID;
        $orderParam->Password =$this->API_PASS;
        $orderParam->UserId = $this->API_USER; //rand(100001,999999);

        $str = self::soapMandateHeader("");
        $str .= " <ns:EMandateAuthURL><ns:Param>";
        foreach ($orderParam as $k => $v) {
            $str .= "<star:" . $k . ">" . $v . "</star:" . $k . ">";
        }
        $str .= "</ns:Param></ns:EMandateAuthURL>";
        $str .= self::soapFooter();

       // print_r($str);


        $resp = self::sendRequestMandate($str);


        $body = self::parseXML($resp);


        $a = explode("|", $resp);

        if ($body['status'] == 100) {
            $url   =    $body['url'];
            $status=    "true";
            $msg        =   "success";
        }else{
            $msg   =    $body['url'];
            $status=    "false";
        }

        $arr = array("status" => $status, "msg" => $msg,"url"=>$url);
        return $arr;
    }
	
	
    
	   public function post_log($data)
    {
        $log_filename =  "../uploads/BSE_APIs";
        if (!file_exists($log_filename))
        {
            // create directory/folder uploads.
            mkdir($log_filename, 0777, true);
            chmod($log_filename, 0777);
        }

        if (!file_exists($log_filename . "/" . date('Y')))
        {
            // create directory/folder uploads.
            mkdir($log_filename . "/" . date('Y'), 0777, true);
            chmod($log_filename . "/" . date('Y'), 0777);
        }

        if (!file_exists($log_filename . "/" . date('Y') . "/" . date('M')))
        {
            // create directory/folder uploads.
            mkdir($log_filename . "/" . date('Y') . "/" . date('M'), 0777, true);
            chmod($log_filename . "/" . date('Y') . "/" . date('M'), 0777);
        }

        //$data = date("Y-m-d H:i:s")." - ".$data;
        $log_file_data = $log_filename . "/" . date('Y') . "/" . date('M') . '/' . date('d-M-Y') . '.log';
        file_put_contents($log_file_data, $data . "\n", FILE_APPEND);
        chmod($log_file_data, 0777);
    }

    
}
