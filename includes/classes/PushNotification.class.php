<?php

class PushNotification extends Models {

    public $live = false;

    function sendGcmNotify($reg_id, $message, $type = 'PUSH') {
        $ttl = 86400;
        $randomNum = rand(10, 100);

        if (!is_array($reg_id)) {
            $fields = array(
                'registration_ids' => array($reg_id),
                'data' => array("message" => $message, 'dictionary' => "", 'type' => $type),
            );
        } else {
            $fields = array(
                'registration_ids' => $reg_id,
                'data' => array("message" => $message, 'dictionary' => "", 'type' => $type),
                'delay_while_idle' => false,
                'time_to_live' => $ttl,
                'collapse_key' => "" . $randomNum . ""
            );
        }


        $headers = array(
            'Authorization: key=' . GOOGLE_API_KEY,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, GOOGLE_GCM_URL);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Problem occurred: ' . curl_error($ch));
        }
       
//        print_r($fields);
//        echo $result;
        if (($response = curl_exec($ch)) === FALSE) {
            echo curl_error($ch);
            exit();
        }

        curl_close($ch);
    }

    function updateBedge($deviceId) {

        $bedgecount = UserDevices::get("device_token='$deviceId'", "bedge")->bedge;

        UserDevices::save(array("bedge" => $bedgecount + 1), "device_token='$deviceId'");

        return $bedgecount + 1;
    }

    function sendMessageDevice($data, $msg, $type = "") {
        if (count($data)) {
            foreach ($data as $val) {
                if ($val['device_type'] == "ANDROID") {
                    $andrdArr[] = $val['device_token'];
                    $arr = array();
                } else
                    $arr['device_token'] = $val['device_token'];
            }
            $dictionary = '';
         
            if (!empty($andrdArr))
                $this->sendGcmNotify($andrdArr, $msg, $type);
        }
    }

   function dueNotification($userid,$msg){
       global $db;
       $dict    =   array();
       $result =   $db->query("select device_token,device_type,user_id from user_devices where user_id=".$userid);
       if ($result->size()>0){
           while ($rs = $result->fetch()) {
                $data[] = $rs;
           }
         
            if (!empty($data))
                self::sendMessageDevice($data, $msg, $dict);
       }
   }

}

?>