<?php 

require_once('../includes/classes/Models.class.php');
// require_once('../includes/mysql.class.php');
class Holding extends Models {


   public static function HoldingValue($userid) {     
        global $db;  
        $data = array();  
        $status = "false";
     
	$holding_amount = "";
	$invested_amount ="";
	$count =0;
	$sql = "SELECT h.* FROM holdings h , orders o WHERE h.pan1 IN ( SELECT pencard FROM users WHERE userid ='".$userid."' ) AND h.HOLDING_AMOUNT > 0  AND h.INWARD_TXN_NO = o.bse_order_no AND o.order_type = 'ONETIME' AND o.payment_status = 1 UNION SELECT h.* FROM holdings h , orders o WHERE h.pan1 IN (SELECT pencard FROM users WHERE userid ='".$userid."')  AND h.HOLDING_AMOUNT > 0  AND h.INWARD_TXN_NO = o.bse_order_no AND o.order_type IN ('SIP', 'XSIP') UNION SELECT h.* FROM holdings h  WHERE h.pan1 IN (SELECT pencard FROM users WHERE userid ='".$userid."')  AND h.HOLDING_AMOUNT > 0  AND h.INWARD_TXN_NO NOT IN  (SELECT bse_order_no FROM orders)";
                $que = $db->query($sql);
                if ($que->size() > 0) {
					while($row = $que->fetch()){
							
							$invested_amount +=  $row['INVESTED_AMOUNT'];
							$holding_amount +=  $row['HOLDING_AMOUNT'];
							$count++;
							$msg = SUCCESS;
							$status = "true";
					}
						$data= array('invest' => $invested_amount,'total_holdings'=> $holding_amount,'count'=>$count);
                }else {
            $msg = NORECORD;
				}

       $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }


    public static function fetch_holding_details($inward_txn_no) {
           
            global $db;

               $sql ="SELECT h.* FROM holdings h , orders o WHERE h.pan1 IN ( SELECT pencard FROM users WHERE userid ='".$userid."' ) AND h.HOLDING_AMOUNT > 0  AND h.INWARD_TXN_NO = o.bse_order_no AND o.order_type = 'ONETIME' AND o.payment_status = 1 UNION SELECT h.* FROM holdings h , orders o WHERE h.pan1 IN (SELECT pencard FROM users WHERE userid ='".$userid."')  AND h.HOLDING_AMOUNT > 0  AND h.INWARD_TXN_NO = o.bse_order_no AND o.order_type IN ('SIP', 'XSIP') UNION SELECT h.* FROM holdings h  WHERE h.pan1 IN (SELECT pencard FROM users WHERE userid ='".$userid."')  AND h.HOLDING_AMOUNT > 0  AND h.INWARD_TXN_NO NOT IN  (SELECT bse_order_no FROM orders)";
				//print_r($sql); 
   					$que = $db->query($sql);
  
		            if ($que->size() > 0) {
			            $row = $que->fetch();
			            $total_holdings = $row['holding_amount'];   
						$total_invest = $row['INVESTED_AMOUNT'];
						$data= array('invest' => $total_invest,'total_holdings'=> $total_holdings);
					           
		        	} 
      
            return $data;
    }
         

      public static function schemeDetails($scheme_code,$inward_txn_no) {
           
            global $db;

            $data1 = array();
            $sql = " SELECT * FROM products WHERE BSE_SCHEME_CODE = '" . $scheme_code . "' ";
            
            $que = $db->query($sql);
            if ($que->size() > 0) {
            $row = $que->fetch();
            $data1[] = $row;

            }
                 $sql1 = " SELECT order_status  FROM orders WHERE  bse_order_no = '" . $inward_txn_no . "' ";
            $que = $db->query($sql1);
            if ($que->size() > 0) {
            $row = $que->fetch();
            $data1[] = $row;

            }
          
            return $data1;
    }

        public static function RecentOrders($userid) {

            global $db;

            $data = array();
            $status = "false";
            $sql = "SELECT * FROM orders WHERE userid='".$userid."' ORDER BY dtdate desc";
           
            $que = $db->query($sql);
            $data1=array();
            if ($que->size() > 0) {
            while ($row = $que->fetch()){
           
				$payment_status = $row['payment_status'];
				$inward_txn_no = $row['bse_order_no'];
				$folio = $row['folio_no'];
				$order_id = $row['order_id'];
				$id = $row['id'];                       
				$orderdate = $row['dtdate'];
				$scheme_code = $row['scheme_code'];
				
					
                    if($payment_status== 1){		
						$res = Holding::check_holdings($inward_txn_no);
                       
                        if(!empty($res)){
                        
						$order_date = strtotime($orderdate);
                        $start_date = strtotime(date('Y-m-d H:i:s' , strtotime('-5 day 00:00:00')));
                        $end_date = strtotime(date('Y-m-d H:i:s' , strtotime(' 18:30:00')));
							
							if($order_date>=$start_date && $order_date<=$end_date ){
								$fundname = self::schemes($scheme_code);			
								$data1['fundname']= $fundname[0];  
								$data1['investdate']= date('d M Y H:i:s' , strtotime($row['dtdate']));
								$data1['investamount']= $row['amount'];
								$data1['tag']= "Payment received";
								$data1['orderno']=$row['bse_order_no']; 
								$data1['qty']= $row['quantity'];
								$data[]=$data1;
							}
						}else{
							
                        $order_date = strtotime($orderdate); 
                        $start_date = strtotime(date('Y-m-d H:i:s' , strtotime('-6 day 00:00:00')));
                        $end_date = strtotime(date('Y-m-d H:i:s' ));
                        
                        if($order_date>=$start_date && $order_date<=$end_date){
							$fundname = self::schemes($scheme_code);
							$data1['fundname']= $fundname[0];     
							$data1['investdate'] =	date('d M Y H:i:s' , strtotime($row['dtdate']));
							$data1['investamount']= $row['amount'];
							$data1['tag']= "Order Confirmed";
							$data1['orderno']=$row['bse_order_no']; 
							$data1['qty']= $row['quantity'];
							$data[]=$data1;
                        }
					}
				}
                    
				if($payment_status== 0){
                
					$order_date= strtotime($orderdate);
					$date1= strtotime(date('Y-m-d 00:00:00'));
					$date2=  strtotime(date('Y-m-d 18:30:00'));

					if($order_date >= $date1 && $order_date <= $date2){
						$fundname = self::schemes($scheme_code);
						$data1['fundname']= $fundname[0];                
						$data1['investdate'] =	date('d M Y H:i:s' , strtotime($row['dtdate']));
						$data1['investamount']= $row['amount'];
						$data1['tag'] = "Payment pending"; 
						$data1['orderno']=$row['bse_order_no']; 
						$data1['qty']= $row['quantity'];
						$data[]= $data1;
					   
					}                                                                        
                     
					$newdate =strtotime( date('Y-m-d H:i:s' , strtotime('-5 day 18:30:00')));

					if(strtotime($orderdate) >= $newdate ){
					// remove from recent orders
						if($date2 > strtotime($orderdate)){
							
							$fundname =self::schemes($scheme_code);
							$data1['fundname']= $fundname[0];
							$data1['investdate'] =	date('d M Y H:i:s' , strtotime($row['dtdate']));
							$data1['investamount']= $row['amount'];
							$data1['tag']="order cancelled";
							$data1['orderno'] = $row['bse_order_no'];
							$data1['qty'] = $row['quantity'];
							$data[]= $data1;
				   
						}
				    }	
				}   
						  
				if($payment_status == -1){
							  
					$order_date= strtotime($orderdate);
					$date1= strtotime(date('Y-m-d 00:00:00'));
					$date2=  strtotime(date('Y-m-d 18:30:00'));
					$curr_time =  strtotime(date('Y-m-d H:i:s'));
					
					if($curr_time <= $date2 ){
						
					
					if($order_date >= $date1 && $order_date <= $date2){
					 
						
						$fundname = self::schemes($scheme_code);
						$data1['fundname']= $fundname[0];
						$data1['investdate'] =	date('d M Y H:i:s' , strtotime($row['dtdate']));
						$data1['investamount']= $row['amount'];
						$data1['tag']="Payment Failure";
						$data1['orderno'] = $row['bse_order_no'];
						$data1['qty'] = $row['quantity'];
						$data[]= $data1;
					}
					}

			}
                    
            }
			
            $msg = SUCCESS;
            $status = "true";
			}else {
            $msg = NORECORD;
			}
		
		$withdraw_res = array();
		$sql2 = "select * from withdraw where userid='".$userid."'ORDER BY dtdate desc";
		
		 $que2 = $db->query($sql2);
           
            if ($que2->size() > 0) {
            while ($row2 = $que2->fetch()){
								$orderdate= $row2['dtdate'];								
								$bse_order_no = trim($row2['bse_order_no'], "  ");								
							    $new_date = date('Y-m-d H:i:s' , strtotime('-5 day 18:30:00'));
                                $newdate =strtotime($new_date);
								$withdraw_flag = $row2['full_withdraw'];
                                if(strtotime($orderdate) >= $newdate ){
								
										$sql3 = "select * from rta_feed where inward_txn_no='".$bse_order_no."'";
										$que3 = $db->query($sql3);
										    if ($que3->size() > 0) {
												
														$scheme_code = $row2['scheme_code'];
														$fundname =self::schemes($scheme_code);
														$withdraw_res['fundname']= $fundname[0];
														$withdraw_res['investdate'] = date('d M Y H:i:s' , strtotime($row2['dtdate']));
														$withdraw_res['investamount'] = $row2['amount'];
														$withdraw_res['orderno'] = $row2['bse_order_no'];
														$withdraw_res['qty'] = $row2['qty'];
														
														if($withdraw_flag == "Y"){
															$withdraw_res['tag'] = "Full Withdraw Confirmed";
															}
														else{
															$withdraw_res['tag'] = "Partial Withdraw Confirmed";
															}
														
														$data[] = $withdraw_res;
											}
											else{
														$scheme_code = $row2['scheme_code'];
														$fundname =self::schemes($scheme_code);
														
														$withdraw_res['fundname']= $fundname[0];
														$withdraw_res['investdate']= $row2['dtdate'];
														$withdraw_res['investamount']= $row2['amount'];
														$withdraw_res['orderno']=$row2['bse_order_no'];
														$withdraw_res['qty']=$row2['qty'];
														
														if($withdraw_flag == "Y"){
															$withdraw_res['tag'] = "Full Withdraw Confirmed";
															}
														else{
															$withdraw_res['tag'] = "Partial Withdraw Confirmed";
															}
														$data[] =$withdraw_res;
											}
							}
					
			}
			
			
            $msg = SUCCESS;
            $status = "true";
        } else {
            $msg = NORECORD;
        }
	
		
        if(empty($data)){
            $arr = array("status" => 'false', "msg" => $msg, "data" => $data);
        }else{
             $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        }
	
				

            
            return $arr;
            }

             public static function check_holdings($inward_txn_no) {
          
            global $db;
            $data = array();
            $sql = "SELECT * FROM holdings WHERE INWARD_TXN_NO ='".$inward_txn_no."'";

             $que = $db->query($sql);
            if ($que->size() > 0){
            $row = $que->fetch();
            $data[]=$row;       
            }

            return $data;
            }


      
                   public static function withdraw($folio) {
          
            global $db;
            $data = array();
            $sql = "SELECT * FROM withdraw WHERE folio_no ='".$folio."'";
             $que = $db->query($sql);
            if ($que->size() > 0){
                
            $row = $que->fetch();
             $data[] = $row;
            }
            // print_r($data);
            return $data;
            }

           public static function investment($userid,  $category){        
					global $db;
					$data = array();
					
					$invest ;
					$total_holdings;
					$data1=array();
					
					$data1=Holding::HoldingValue($userid);

					$invest ;
					$total_holdings;
					
					foreach ($data1 as $key) {
						$invest = $key['invest'];
						$total_holdings = $key['total_holdings'];
					}
					$sql = "SELECT h.* , o.order_type ,o.order_status FROM holdings h , orders o WHERE h.pan1 IN ( SELECT pencard FROM users WHERE userid ='".$userid."' ) AND h.HOLDING_AMOUNT > 0  AND h.INWARD_TXN_NO = o.bse_order_no AND o.order_type = 'ONETIME' AND o.payment_status = 1 UNION SELECT h.* , o.order_type ,o.order_status  FROM holdings h , orders o WHERE h.pan1 IN (SELECT pencard FROM users WHERE userid ='".$userid."')  AND h.HOLDING_AMOUNT > 0  AND h.INWARD_TXN_NO = o.bse_order_no AND o.order_type IN ('SIP', 'XSIP') UNION SELECT h.* ,'' , ''  FROM holdings h , orders o WHERE h.pan1 IN (SELECT pencard FROM users WHERE userid ='".$userid."')  AND h.HOLDING_AMOUNT > 0  AND h.INWARD_TXN_NO NOT IN  (SELECT bse_order_no FROM orders)";
					
					$que = $db->query($sql);					
					if ($que->size() > 0) {
						while ($row = $que->fetch()){ 	
					 
							$scheme_code = $row['BSE_SCHEME_CODE'];
							$fundname =self::schemes($scheme_code);
							$sub_category_code = $fundname[2];
							$order_type= $row['order_type'];
							$order_status = $row['order_status'];
							 
							$orders1['fundname']= $fundname[0];  
							$orders1['folio_no']= $row['FOLIO_NO'];		
							$orders1['amount']= round($row['HOLDING_AMOUNT'],2);	
							$orders1['nav']= $row['HOLDING_NAV'];	
							$orders1['quantity']= $row['HOLDING_UNITS'];	
							$orders1['dtdate']= $row['TRADE_DATE'];	
							$orders1['returns']= round($row['RETURNS'],2);	
							$orders1['INWARD_TXN_NO']= $row['INWARD_TXN_NO'];	
							$orders1['BSE_SCHEME_CODE']= $row['BSE_SCHEME_CODE'];	
							$orders1['investment_amount']= $row['INVESTED_AMOUNT'];	
							$orders1['order_type']= $row['order_type'];	
							$orders1['order_status'] = $row['order_status'];
							$per = ($row['HOLDING_AMOUNT']/$total_holdings)*100;
							$orders1['portfolio_percent']= round($per,2);	
																	
										if(	$order_type == "XSIP" || $order_type == "SIP" ){
											if($order_status == 'CANCEL'){
												$orders1['order_status']="Cancelled";	
											}else{
												$orders1['order_status']="Active";
											}
										}else{
											$orders1['order_status']="Confirmed";
											}
								 
										if(trim($category,' ') == "e"){
											
										if($sub_category_code == '2' || $sub_category_code == '4' || $sub_category_code == '5' || $sub_category_code == '10' || $sub_category_code == '1' || $sub_category_code == '3' || $sub_category_code == '8' || $sub_category_code == '7' || $sub_category_code == '9' ){
											
													$orders1['scheme_category'] = "e";
												
														$orders[]= $orders1;
														$status = "true";
														$msg = SUCCESS;
												}
													
										}
									
																				
											
									
										if(trim($category,' ')  == "d"){
											if($sub_category_code == '12' || $sub_category_code == '13' || $sub_category_code == '14' || $sub_category_code == '15' || $sub_category_code == '16' || $sub_category_code == '17' || $sub_category_code == '18' || $sub_category_code == '20' || $sub_category_code == '19' || $sub_category_code == '21' || $sub_category_code == '22' || $sub_category_code == '23' || $sub_category_code == '24' || $sub_category_code == '25' || $sub_category_code == '26' || $sub_category_code == '27'){
											
											$orders1['scheme_category'] = "d";
											$orders[]= $orders1;
											$status = "true";
											$msg = SUCCESS;
											}
												
											
										}
										
										if(trim($category,' ')  == "h"){
											 if($sub_category_code == '28' || $sub_category_code == '29' || $sub_category_code == '30' || $sub_category_code == '31' || $sub_category_code == '32' || $sub_category_code == '33' || $sub_category_code == '34' ){
										
													$orders1['scheme_category'] = "h";
													$orders[]= $orders1;
													$status = "true";
													$msg = SUCCESS;
											}
										
										}
										
												if(trim($category,' ')  == "all" || trim($category,' ')  == " " ){
												$orders1['scheme_category'] = "all";
											$status = "true";
											$msg = SUCCESS;
											$orders[]= $orders1;
										}
							
									
								
								}
								
									if(empty($orders)){
										$arr = array("status" => "false", "msg" => NORECORD, "data" => "no data", "invest"=>$invest, "total_holdings"=>$total_holdings);
									}else{
										$arr = array("status" => $status, "msg" => $msg, "data" => $orders, "invest"=>$invest, "total_holdings"=>$total_holdings);
									}
								
							}else {
							$msg = NORECORD;
							$status = "false";
							 $arr = array("status" => $status, "msg" => $msg, "data" =>"no data");
						}

				return $arr;
						}
						
								
								
						
				
			
			
        public static function schemes($scheme_code) {
      
            global $db;
            $data = array();
            $sql = "SELECT * FROM products WHERE BSE_SCHEME_CODE ='".$scheme_code."'";
        
             $que = $db->query($sql);
            if ($que->size() > 0){
        
                $row = $que->fetch();
               
                $fundname = $row['BSE_SCHEME_NAME'];
                $isin = $row['BSE_ISIN'];
				$Sub_category_code = $row['SUB_CATEGORY_CODE'];
                $data[] = $fundname;
                $data[]=$isin;
				$data[] = $Sub_category_code;
       
            }
            
            return $data;
        }

                       public static function schemesnav($scheme_code) {
    
            global $db; 
            $sql = "SELECT nav FROM products WHERE BSE_SCHEME_CODE ='".$scheme_code."'";
            $que = $db->query($sql);
            if ($que->size() > 0){
            $row = $que->fetch();
            $nav = $row;
            $data = $nav;
           
            }
            return $data;
        }

        public static function holdingreturns($folio_no) {
        
            global $db;
            $data = array();
            $sql = "SELECT * FROM holdings WHERE folio_no ='".$folio_no."'";
            // print_r($sql); 
             $que = $db->query($sql);
            if ($que->size() > 0){
        
            $row = $que->fetch();
            $returns = $row['returns'];
            $data[] = $returns;
           
            }
            return $data;
        }


        public static function sip_date($order_id) {
          
            global $db;
            
            $sql = "SELECT dtdate FROM sip_registration WHERE order_id ='".$order_id."'";
          
             $que = $db->query($sql);
            if ($que->size() > 0){        
				$row = $que->fetch();           
				$data = $row;
           
            }
            return $data;
        }


    public static function goalDetail($order_id) {
		        global $db;

		        $data = (object) [];
		        $status = STATUSFALSE;

		        $sql = "SELECT * FROM goal_detail WHERE main_orderid = '" . $order_id . "' ";
		        $que = $db->query($sql);
		        $row = $que->fetch();
		        if ($que->size() > 0) {
		            $data = $row;
					
		        }
		        return $data;
    }


    public static function fundnames($isin){
        global $db;

        $data = array();
        $status = STATUSFALSE;

        $sql = "SELECT * FROM funds_name WHERE ISIN = '" . $isin . "' ";
        
          $que = $db->query("SET NAMES utf8");
        $que = $db->query($sql);
        $row = $que->fetch();
        if ($que->size() > 0) {
            $data = $row;

        }
       
        return $data;
    }

        public static function allInvest($userid) {
            
        global $db;

        $data = array();
        $status ="false";
        $sql =  "select pencard from users where userid='".$userid."'";
		 
		 $que = $db->query($sql);
		
			if ($que->size() > 0) {
			$row = $que->fetch();
			$pan=$row['pencard'];
			}
		 
      $sql1 =	"SELECT * FROM rta_feed WHERE pan1 ='".$pan."' ORDER BY STR_TO_DATE(`PROCESSING_DATE`,'%Y-%m-%d') DESC";

        $que = $db->query($sql1);

         if ($que->size() > 0) {
            while ($row = $que->fetch()) {
					$data1['nav']=round($row['TXN_NAV'],2);	
					$data1['amount']= round( $row['TXN_AMOUNT'],2);	
				    $data1['units']=  $row['TXN_UNITS'];
					$data1['order_status']=  $row['TXN_DESCRIPTION'];
				    $data1['order_no']= $row['INWARD_TXN_NO'];
					$data1['folio_no']=  $row['FOLIO_NO'];			
					$data1['trade_date']=  $row['TRADE_DATE'];					
				    $bse_scheme_code =  $row['BSE_SCHEME_CODE'];				  				   
				    $fundname =self::schemes($bse_scheme_code);
            		$data1['fundname']=$fundname[0];
					$data1['txn_mode']=  $row['TXN_MODE'];
					$data[] = $data1;				
                            
            }
            $msg = SUCCESS;
            $status = "true";
        } else {
            $msg = NORECORD;
        }
             $arr = array("status" => $status, "msg" => $msg, "data" => $data);
            return $arr;
       
    }


	   
       public static function ViewTransaction($folio, $scheme_code) {
            
        global $db;

        $data = array();
		$data1 = array();
        $status="false";
      
        $sql = "SELECT * FROM rta_feed WHERE folio_no ='".$folio."' AND BSE_SCHEME_CODE= '".$scheme_code."' ORDER BY STR_TO_DATE(`PROCESSING_DATE`,'%Y-%m-%d') DESC";
      
       $que = $db->query($sql);

         if ($que->size() > 0) {
           while( $row = $que->fetch()){
						

					$data['nav']= $row['TXN_NAV'];	
					//$data['amount']= $row['TXN_AMOUNT'];
					$data['amount']=   round($row['TXN_AMOUNT'],2);	
				    $data['units']=  $row['TXN_UNITS'];
					$data['order_status']=  $row['TXN_DESCRIPTION'];
				    $data['order_no']= $row['INWARD_TXN_NO'];
					$data['folio_no']=  $row['FOLIO_NO'];
				    $bse_scheme_code =  $row['BSE_SCHEME_CODE'];
					$data['trade_date']=  $row['TRADE_DATE'];
					$data['txn_mode']=  $row['TXN_MODE'];
				  
				   
				    $fundname =self::schemes($bse_scheme_code);
            		$data['fundname']=$fundname[0];
					$data1[] = $data;
					
        }
	
            $msg = SUCCESS;
            $status = "true";
        } else {
            $msg = NORECORD;
        }
             $arr = array("status" => $status, "msg" => $msg, "data" => $data1);
            return $arr;
       
    }

     public static function vieworders($folio_no) {
            
        global $db;

        $data = array();

        $sql = "SELECT * FROM orders WHERE folio_no = '".$folio_no."' ";
      
        $que = $db->query($sql);

         if ($que->size() > 0) {
            while ($row = $que->fetch()) {
     
              $data[] = $row;
                            
            }
           
         
        }
            return $data;
       
    }

                   

      public static function RedeemValues($isin) {
   
			global $db;
            // $data1 = array();
            $sql = " SELECT * FROM products WHERE BSE_ISIN='".$isin."'";
            // print_r($sql);
            $que = $db->query($sql);
			if($que->query->num_rows != 0){
				if ($que->size() > 0) {           
				$row = $que->fetch();
				// print_r($row);				
				}
			}

           return $row;
            }

             public static function withdrawal($folio_no, $scheme_code) {

        global $db;
       
        $data = array(); 
        $status = "false";


		$sql1 = "SELECT * FROM withdraw WHERE folio_no='".$folio_no."' AND DATE(dtdate) BETWEEN  DATE_ADD(CURDATE(), INTERVAL -6 DAY) AND  	CURDATE() ORDER BY dtdate ASC";
		$que1 = $db->query($sql1);

		 if ($que1->size() > 0) {
					$msg = "You have already Withdraw that scheme";
					$status = "false";
			$arr = array("status" => $status, "msg" => $msg);
		}else{
					 $sql =" SELECT * FROM holdings WHERE folio_no='".$folio_no."' AND BSE_SCHEME_CODE ='".$scheme_code."' AND holding_amount > 0 "; 

        $que = $db->query($sql);
        if ($que->size() > 0) {
            while ($row = $que->fetch()) {
         			
								$scheme_code = $row['BSE_SCHEME_CODE'];
            	     			$fundname =self::schemes($scheme_code);
           						$isin = $row['ISIN'];
           						
           						$fund_details = self::fundnames($isin);
                                $orders = self::vieworders($folio_no);
					if(empty($orders)){
									$order_id = "0";
                               //  $investedamount =  $row['INVESTED_AMOUNT'];
                                 $id= "0";
						}else{
							
                             foreach ($orders as $key) {
                                 $is_goal = $key['is_goal'];
                                 $order_id = $key['bse_order_no'];
                              //   $investedamount +=$key['amount'];
                                 $id= $key['id'];
                             }
						}
                                // $is_goal = $orders['is_goal'];
           						$res = self::RedeemValues($isin);

           					$data['HOLDING_UNITS']= $row['HOLDING_UNITS'];
           					$data['HOLDING_AMOUNT']=  $row['HOLDING_AMOUNT'];	
           					$data['FUNDNAME']=$fundname[0];
           					$data['NAV']= $res['NAV'];           					
           					$data['BSE_REDEMPTION_ALLOWED']= $res['BSE_REDEMPTION_ALLOWED'];
           					$data['BSE_REDEMPTION_TXN_MODE']=$res['BSE_REDEMPTION_TXN_MODE'];
           					$data['BSE_MIN_REDEMPTION_QTY']=$res['BSE_MIN_REDEMPTION_QTY'];
           					$data['BSE_REDEMPTION_QTY_MULTPL']=$res['BSE_REDEMPTION_QTY_MULTPL'];
           					$data['BSE_MAX_REDEMPTION_QTY']=$res['BSE_MAX_REDEMPTION_QTY'];
           					$data['BSE_REDEMPTION_AMT_MIN']=$res['BSE_REDEMPTION_AMT_MIN'];
           					$data['BSE_REDEMTION_AMT_MAX']=$res['BSE_REDEMTION_AMT_MAX'];
           					$data['BSE_REDEMPTION_AMT_MULTPL']=$res['BSE_REDEMPTION_AMT_MULTPL'];
           					$data['BSE_REDEMPTION_CUTOFF_TIME']=$res['BSE_REDEMPTION_CUTOFF_TIME'];
                            $data['scheme_code']=$scheme_code;
                            $data['id']= $id;
						     $data['investedamount']= $row['INVESTED_AMOUNT'];
                             $data['is_goal']= $is_goal;
                             $data['order_id']= $order_id;
							 $data['INWARD_TXN_NO']= $row['INWARD_TXN_NO'];
           				
                        $goal_detail = self::goalDetail($order_id);

                        if(empty($goal_detail)){
                        $data['goal_detail'] =" 0";
                        }else{
                        $data['goal_detail'] = $goal_detail;
                        }

           				  if(empty($fund_details)){
           				    $data['FUND_NAME'] = "";
           				    $data['FUNDS_GUJARATI'] = "";
           				    $data['FUNDS_HINDI'] = "";
           				    $data['FUNDS_MARATHI'] = "";
           				}else{
           				     $data['FUND_NAME'] =  $fundname[0];
           				    $data['FUNDS_GUJARATI'] = $fund_details['FUNDS_GUJARATI'];
           				    $data['FUNDS_HINDI'] = $fund_details['FUNDS_HINDI'];
           				    $data['FUNDS_MARATHI'] = $fund_details['FUNDS_MARATHI'];
           				}
           					
            }

            $msg = SUCCESS;
            $status = "true";
        } else {
            $msg = NORECORD;
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
}
       
        return $arr;
    }


    	

 public static function user_details($userid) {

        global $db;
        // print_r($pan); exit();
        $data = array(); 
        $status = "false";
   
        $sql = "select * from user_details where userid='".$userid."'";
   
        $que = $db->query($sql);
        if ($que->size() > 0){
            while ($row = $que->fetch()) {

          	$row['wealthSourceCode'] = $row['wealth_source'];
            $row['occupationType'] = $row['occupation_type'];
			
            $occupation = $row['occupation'];
			
            $occupation_string = Mapping::mapping_occupation_code($occupation);
             $row['occupation_string']= $occupation_string['FRONTEND_OPTION_NAME'];
           
            $ca_state_code = $row['ca_state'];
            $ca_state = Mapping::mapping_state_code($ca_state_code);
            $row['state']= $ca_state['FRONTEND_OPTION_NAME'];
            $account_type = $row['account_type'];

            if ($account_type == 'SB') {
                $row['account_type_string'] = "Saving Bank";
            } else if ($account_type == 'CB') {
                $row['account_type_string'] = "Current Bank";
            } else if ($account_type == 'NE') {
                $row['account_type_string'] = "NRE Account";
            } else if ($account_type == 'NO') {
                $row['account_type_string'] = "NRO Account";
            }

            $users = self::users($userid);
            // print_r($users); 
            foreach ($users as $key) {
              $row['name']= $key['name'];
              $row['dob']= $key['dob'];
               $row['pencard']= $key['pencard'];
                $row['mobile']= $key['mobile'];
            }


            // $row['users']=$users[0];
          
          
                $data[]= $row;
                
              
            }


            $msg = SUCCESS;
            $status = "true";
        } else {
            $msg = NORECORD;
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

    public static function users($userid) {

        global $db;

        $data = array(); 
        $status = "false";
   
        $sql = "SELECT name, dob, pencard ,mobile FROM users WHERE userid = ".$userid." ";
   
        $que = $db->query($sql);
        if ($que->size() > 0){
             while ($row = $que->fetch()) {
                $data[]= $row;
             }
        }

        return $data;
    }

        function updateLocalOrder($order_id, $order_status) {
        global $db;
        $status = false;

        $sql = "update orders set " .
                "order_status='" . $order_status . "'" . 
                " where id=" . $order_id;
                // print_r($sql); exit();      
                  $result = $db->query($sql);
        $id = $result->insertID();
        return array("msg" => SUCCESS, "status" => true, "id" => $id, "order_id" => $orderID);
    }


public static function mymoneyInvestment($order_no) {
            
        global $db;

        $data = array();
        $status="false";
                   
                  
                 $data = array();

        $sql = "SELECT * FROM orders WHERE bse_order_no = '".$order_no."' ";
      
        $que = $db->query($sql);

         if ($que->size() > 0) {
			while($res = $que->fetch()){
     
           
					$order_id = $res['order_id'];
                    $scheme_code = $res['scheme_code'];
                    $folio =$res['folio_no'];
                    $sip_registration_no = $res['sip_registration_no'];
                    $is_goal = $res['is_goal'];

                    $fundname =self::schemes($scheme_code);
                     
                    $returns =self::holdingreturns($folio_no);
                    $isin = $fundname[1];   
                   
                    
                    $fund_details = self::fundnames($isin);
                    $sip_registration_date= self::sip_date($order_id);
              

                    $res['fundname']= $fundname[0];                  
          
                    if(empty($sip_registration_date)){
                    $res['sip_registration_date']='0';
                    }else{
                    $res['sip_registration_date']=$sip_registration_date['dtdate'];
                    }

                        if(!empty($returns)){
                        $res['returns']= $returns;
                        }else{
                        $res['returns']= 0;
                        }
                   
                              $goal_detail = self::goalDetail($order_id);

                                 if(empty($goal_detail)){
                                  $res['goal_detail'] =" 0";
                              }else{
                                  $res['goal_detail'] = $goal_detail;
                              }
                                                
           
                              if(empty($fund_details)){
                                $res['FUND_NAME'] = "";
                                $res['FUNDS_GUJARATI'] = "";
                                $res['FUNDS_HINDI'] = "";
                                $res['FUNDS_MARATHI'] = "";
                            }else{
                                 $res['FUND_NAME'] =  $fundname[0];
                                $res['FUNDS_GUJARATI'] = $fund_details['FUNDS_GUJARATI'];
                                $res['FUNDS_HINDI'] = $fund_details['FUNDS_HINDI'];
                                $res['FUNDS_MARATHI'] = $fund_details['FUNDS_MARATHI'];
                            }
                                               $withdraw = self::withdraw($folio_no);
                  
                       foreach ($withdraw as $key ) {
                        $withdrawqty =$key['qty'];
                        $withdrawAmt =  $key['amount'];
                        $full_withdraw =  $key['full_withdraw'];    
                       }
					   
                       // $res['fundname']= $fundname[0]; 
                        
                        if(empty($withdraw) ){
                        $res['withdrawQty'] = 0;
                        $res['withdrawAmt'] = 0;
                        $res['full_withdraw'] = 0;
                        }else{ 
                            
                                $res['withdrawQty'] =   $withdrawqty;
                            
                                $res['withdrawAmt'] = $withdrawAmt;
                            
                                $res['full_withdraw'] = $full_withdraw;
                            
                            
                        }
                        
                        $nav = self::schemesnav($scheme_code);
                        $nav=  $nav['nav'];
                        $quantity = $res['quantity'];
                        $value = $quantity * $nav;

                        $res['current_value'] = round($value, 2);

                    
                     
                $data= $res;
                       
			}
 
 
           
         
        }
        
           
            return $data;
       
    }

  /*  function withdrawScheme($folio_no) {
        global $db;
        $status = false;

			$sql1 = "SELECT * FROM withdraw WHERE folio_no='".$folio_no."' AND DATE(dtdate) BETWEEN  DATE_ADD(CURDATE(), INTERVAL -6 DAY) AND  	CURDATE() ORDER BY dtdate ASC";
			$que1 = $db->query($sql1);
			//print_r($que1); exit();
			 if ($que1->size() > 0) {

				$data['withdrawn']= "Y";
				}else{
				$data['withdrawn']= "N";
				}
        return array("msg" => SUCCESS, "status" => true, "id" => $id, "order_id" => $orderID);
    }*/
}
?>
