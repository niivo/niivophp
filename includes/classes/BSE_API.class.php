<?php

class BSE_API {
//    protected $SOAP_URL     =   "http://bsestarmfdemo.bseindia.com/MFUploadService/MFUploadService.svc/Basic";
//    protected $API_USER     =   "1707101";
//    protected $API_PASS     =   "@123456";
//    protected $API_MEMBERID =   "17071";
    var     $PASS_key       =   "";
//    protected $GET_PASSWORD_ACTION  =   '<wsa:Action>http://bsestarmfdemo.bseindia.com/2016/01/IMFUploadService/getPassword</wsa:Action>';
//    protected $UPLOAD_ACTION  =   '<wsa:Action>http://bsestarmfdemo.bseindia.com/2016/01/IMFUploadService/MFAPI</wsa:Action>';

    protected $SOAP_URL     =   "http://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Basic";
    protected $API_USER     =   API_USER;
    protected $API_PASS     =   API_PASS;
    protected $API_MEMBERID =   API_MEMBER;
    protected $GET_PASSWORD_ACTION  =   '<wsa:Action>http://www.bsestarmf.in/2016/01/IStarMFWebService/getPassword</wsa:Action>';
    protected $UPLOAD_ACTION  =   '<wsa:Action>http://www.bsestarmf.in/2016/01/IStarMFWebService/MFAPI</wsa:Action>';

    
    private function soapHeader($tp) {
        $headerStr  ='<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="http://www.bsestarmf.in/2016/01/">
                            <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">';
        $headerStr .= ($tp == "PASS") ? $this->GET_PASSWORD_ACTION : $this->UPLOAD_ACTION;
        $headerStr .= '<wsa:To>http://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Basic</wsa:To>';
        $headerStr .= '</soap:Header><soap:Body>';
        return $headerStr;
    }
    
    
    private function  soapFooter(){
        $footerStr   ='</soap:Body></soap:Envelope>';
        return $footerStr;
    }
    
    
    function sendRequest($xml_string){
        $headers = array(
                        "Content-type: application/soap+xml;charset=\"utf-8\"",
                    ); //SOAPAction: your op URL

            $url = $soapUrl;
           $xml_post_string    =   $xml_string;
           
           	$this->post_log($xml_string . "\n");  
            

            // PHP cURL  for https connection with auth
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
            curl_setopt($ch, CURLOPT_URL, $this->SOAP_URL);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            // converting
            $response = curl_exec($ch); 
            curl_close($ch);
          
            	$this->post_log($response . "\n"); 
            $xml = new SimpleXMLElement($response);
            $xml->registerXPathNamespace("soap", "http://www.w3.org/2003/05/soap-envelope");
            $body = $xml->xpath("//soap:Body");
            return $body;
    }
    
    
    function __construct($passKey="ninehertz123") {
        $this->PASS_key =   $passKey;
        
    }
    
    function getPassword(){
        $str = self::soapHeader("PASS");
        $str    .=   "<ns:getPassword>".         
                    "<ns:UserId>".$this->API_USER."</ns:UserId>".     
                    "<ns:MemberId>".$this->API_MEMBERID."</ns:MemberId>".
                    "<ns:Password>".$this->API_PASS."</ns:Password>".         
                    "<ns:PassKey>".$this->PASS_key."</ns:PassKey>".
                    "</ns:getPassword>";
        $str .= self::soapFooter();
	
        $resp   =    self::sendRequest($str);
        $resp = self::sendRequest($str);
		
        $resp = ($resp[0]->getPasswordResponse->getPasswordResult);
       
        $a = explode("|", $resp);
       

        if ($a[0] == 100)
            return $a[1];
    }
    
    function uploadClient($str){
        $status = false;
         $tempPass = self::getPassword();
		
       
      
        if ($tempPass != '') {
            $orderParam->Flag = "02";
            $orderParam->UserId = $this->API_USER; //rand(100001,999999);
            $orderParam->EncryptedPassword = $tempPass;
            $orderParam->param = $str;
            $str = self::soapHeader("");
            $str .= " <ns:MFAPI>";
            foreach ($orderParam as $k => $v) {
                $str .= "<ns:" . $k . ">" . $v . "</ns:" . $k . ">";
            }
            $str .= " </ns:MFAPI>";
            $str .= self::soapFooter();
			
            $resp = self::sendRequest($str);
            $resp = ($resp[0]->MFAPIResponse->MFAPIResult);
			//print_r($resp);
            $a = explode("|", $resp);
			
            if ($a[0] == 100){
                $msg   =    $a[1];
                $status=    "true";
            }else{
                $msg   =    $a[1];
                $status=    "false";
            }
//          

            $arr = array("status" => $status, "msg" => $msg);
        
        }
		    return $arr;
    }
    
    function FATCAuploadClient($str){

        $status = false;
         $tempPass = self::getPassword();
      
        if ($tempPass != '') {
			
					//print_r($str); exit(); 
            $orderParam->Flag = "01";
            $orderParam->UserId = $this->API_USER; //rand(100001,999999);
            $orderParam->EncryptedPassword = $tempPass;
            $orderParam->param = $str;
            $str = self::soapHeader("");
            $str .= " <ns:MFAPI>";
            foreach ($orderParam as $k => $v) {
                $str .= "<ns:" . $k . ">" . $v . "</ns:" . $k . ">";
            }
            $str .= " </ns:MFAPI>";
            $str .= self::soapFooter();
			 
			//$this->post_log($str . "\n"); 
            $resp = self::sendRequest($str);
            $resp = ($resp[0]->MFAPIResponse->MFAPIResult);
			//$this->post_log($resp . "\n"); 
            $a = explode("|", $resp);
         // print_r($resp); exit();
            if ($a[0] == 100){
				
                $msg   =    $a[1];
                $status=    "true";
            }else{
                $msg   =    $a[1];
                $status=    "false";
            }
//          

            $arr = array("status" => $status, "msg" => $msg);
           
        }
		 return $arr;
    }
    
    function paymentStatus($str){
        
		$status = false;
        $paymentStatus  =   0;
        $tempPass = self::getPassword();
		
      
        if ($tempPass != '') {
            $orderParam->Flag = "11";
            $orderParam->UserId = $this->API_USER; //rand(100001,999999);
            $orderParam->EncryptedPassword = $tempPass;
            $orderParam->param = $str;
           
            $str = self::soapHeader("");
            $str .= " <ns:MFAPI>";
            foreach ($orderParam as $k => $v) {
                $str .= "<ns:" . $k . ">" . $v . "</ns:" . $k . ">";
            }
            $str .= " </ns:MFAPI>";
            $str .= self::soapFooter();
          
            $resp = self::sendRequest($str);
			

            $resp = ($resp[0]->MFAPIResponse->MFAPIResult);
            $a = explode("|", $resp);
			
            if ($a[0] == 100){
                $msg   =    $a[1];
                $pos = strpos(strtoupper($msg), "APPROVED");
                if ($pos !== false) {
                    $paymentStatus=1;
                }
                $status=    "true";
            }else{
                $msg   =    $a[1];
                $status=    "false";
            }
//          
            $arr = array("status" => $status, "msg" => $msg,"payment_status"=>$paymentStatus);
        
        }
		    return $arr;
    }
	
	function changePassword(){
        global $db;
        $status = false;
		$result   = $db->query("select API_PASS from api_keys where id=1");
        if ($result->size()>0){
            $tempPass = self::getPassword();
            
            $rs         =   $result->fetch();
            $oldPass    =   $rs['API_PASS'];
            $newPass    =   "@".date("ymd");
            echo $str        =   $oldPass."|".$newPass."|".$newPass;
        
            if ($tempPass != '') {
                $orderParam->Flag = "04";
                $orderParam->UserId = $this->API_USER; //rand(100001,999999);
                $orderParam->EncryptedPassword = $tempPass;
                $orderParam->param = $str;
                $str = self::soapHeader("");
                $str .= " <ns:MFAPI>";
                foreach ($orderParam as $k => $v) {
                    $str .= "<ns:" . $k . ">" . $v . "</ns:" . $k . ">";
                }
                $str .= " </ns:MFAPI>";
                $str .= self::soapFooter();
                
                $type="ChangePassword";
                $clintCode  = rand(11111,99999);
              
                $xml_file = User::xmlUpload($clintCode, $str, $type);
                
                $resp = self::sendRequest($str);
                
                $resp = ($resp[0]->MFAPIResponse->MFAPIResult);
                $a = explode("|", $resp);

                if ($a[0] == 100){
                    $msg   =    $a[1];
                    $status=    "true";
                    self::updatePassword($newPass);
                }else{
                    $msg   =    $a[1];
                    $status=    "false";
                }
    //          

                $arr = array("status" => $status, "msg" => $msg);
                
            }
        }  else {
            $arr = array("status" => $status, "msg" => $msg);
        }
        return $arr;
    }
    
    function updatePassword($newPass){
        global $db;
        $sql    =   "update api_keys set API_PASS='".$newPass."',UPDATED_AT='".nowDateTime()."'";
        $result =   $db->query($sql);
    }
    //-------- log
   public function post_log($data)
    {
        $log_filename =  "../uploads/BSE_APIs";
        if (!file_exists($log_filename))
        {
            // create directory/folder uploads.
            mkdir($log_filename, 0777, true);
            chmod($log_filename, 0777);
        }

        if (!file_exists($log_filename . "/" . date('Y')))
        {
            // create directory/folder uploads.
            mkdir($log_filename . "/" . date('Y'), 0777, true);
            chmod($log_filename . "/" . date('Y'), 0777);
        }

        if (!file_exists($log_filename . "/" . date('Y') . "/" . date('M')))
        {
            // create directory/folder uploads.
            mkdir($log_filename . "/" . date('Y') . "/" . date('M'), 0777, true);
            chmod($log_filename . "/" . date('Y') . "/" . date('M'), 0777);
        }

        //$data = date("Y-m-d H:i:s")." - ".$data;
        $log_file_data = $log_filename . "/" . date('Y') . "/" . date('M') . '/' . date('d-M-Y') . '.log';
        file_put_contents($log_file_data, $data . "\n", FILE_APPEND);
        chmod($log_file_data, 0777);
    }

    
}
