<?php

class FileUpload {



protected $GET_PASSWORD_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://tempuri.org/IStarMFFileUploadService/GetPassword</wsa:Action>';
protected $UPLOAD_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://tempuri.org/IStarMFFileUploadService/UploadFile</wsa:Action>';
protected $SOAP_URL ="";



protected $API_USER = API_USER;
protected $API_PASS = API_PASS;
protected $API_MEMBERID = API_MEMBER;

//    protected $API_USER     =   "1438201";
//    protected $API_PASS     =   "@123456";
//    protected $API_MEMBERID =   "14382";

private function soapHeader($tp) {
    $headerStr = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:tem="http://tempuri.org/" xmlns:star="http://schemas.datacontract.org/2004/07/StarMFFileUploadService"><soap:Header>';
    $headerStr .= ($tp == "PASS") ? $this->GET_PASSWORD_ACTION : $this->UPLOAD_ACTION;
    $headerStr .= '<wsa:To xmlns:wsa="http://www.w3.org/2005/08/addressing">http://bsestarmfdemo.bseindia.com/StarMFFileUploadService/StarMFFileUploadService.svc/Basic</wsa:To>';
    $headerStr .= '</soap:Header><soap:Body>';
    return $headerStr;
    }

    private

    function soapFooter() {
        $footerStr = '</soap:Body></soap:Envelope>';
        return $footerStr;
    }
    
    function parseXML($xmlString) {
        $resp = array();
        $p = xml_parser_create();
        xml_parse_into_struct($p, $xmlString, $vals, $index);
        xml_parser_free($p);
        // echo "\nVals array\n";
//       print_r($vals);
//       exit;

        foreach ($vals as $val) {
            if ($val['tag'] == "B:RESPONSESTRING") {
                $resp['pass'] = $val['value'];
            }
            if ($val['tag'] == "B:STATUS") {
                $resp['status'] = $val['value'];
            }
			if ($val['tag']=="B:MESSAGE"){
				$resp['pass']	=	$val['value'];
			}
        }
        return $resp;
    }
    
    

    function sendRequest($xml_string) {
        $headers = array(
            "Content-type: application/soap+xml;charset=\"utf-8\"",
        ); //SOAPAction: your op URL
  $this->post_log($xml_string . "\n"); 
        $url = $soapUrl;
        $xml_post_string = $xml_string;
//       echo $xml_post_string;
//               exit;
        // PHP cURL  for https connection with auth
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $this->SOAP_URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // converting
        $response = curl_exec($ch);
        
  $this->post_log($response . "\n"); 
        curl_close($ch);


        return $response;
    }

    function __construct($passKey = "ninehertz123") {
        $this->PASS_key = $passKey;
        if (MODE)
            $this->SOAP_URL = "http://www.bsestarmf.in/StarMFFileUploadService/StarMFFileUploadService.svc/Basic";
        else
            $this->SOAP_URL = "http://bsestarmfdemo.bseindia.com/StarMFFileUploadService/StarMFFileUploadService.svc/Basic";
            
    }

    function getPassword() {
        $str = self::soapHeader("PASS");
        $str .= "<tem:GetPassword>" .
                 "<tem:Param>".
                "<star:MemberId>" . $this->API_MEMBERID . "</star:MemberId>" .
                "<star:Password>" . $this->API_PASS . "</star:Password>" .
                "<star:UserId>" . $this->API_USER . "</star:UserId>" .
                 "</tem:Param>".
                "</tem:GetPassword>";
        $str .= self::soapFooter();
        
        $resp = self::sendRequest($str);
        
        $body = self::parseXML($resp);
        if ($body['status'] == 100)
            return $body['pass'];
    }



   function imageToByte($path){
        $byte_array = file_get_contents($path);
        $image = base64_encode($byte_array);
        return $image;
    }
    
    function uploadFile($userID,$imagePath){
        $arr    =   array();
         $tempPass   = self::getPassword();
        
        if ($tempPass != '') {
            $flName = basename($imagePath);// $this->API_MEMBERID.$userID."05122017.tiff";
            $image= self::imageToByte($imagePath);
            
            $orderParam->ClientCode = $userID;
            $orderParam->DocumentType = "NRM";
            $orderParam->EncryptedPassword = $tempPass;
            $orderParam->FileName = $flName;
            $orderParam->Filler1 = "";
            $orderParam->Filler2 = "";
            $orderParam->Flag = "UCC";
            $orderParam->MemberCode = $this->API_MEMBERID;
            $orderParam->UserId = $this->API_USER;
            $orderParam->pFileBytes = $image;

            $str = self::soapHeader("");
            
         
         
            $str .= " <tem:UploadFile>";
            $str .= " <tem:data>";
            foreach ($orderParam as $k => $v) {
                $str .= "<star:" . $k . ">" . $v . "</star:" . $k . ">";
            }
            $str .= " </tem:data>";
            $str .= " </tem:UploadFile>";
            $str .= self::soapFooter();
        
            $resp = self::sendRequest($str);
            $body = self::parseXML($resp);
        
            $arr = array("status" => $body['status'], "msg" => $body['pass']);
            
        }
        return $arr;
    }
	
	  //-------- log
   public function post_log($data)
    {
        $log_filename =  "../uploads/BSE_APIs";
        if (!file_exists($log_filename))
        {
            // create directory/folder uploads.
            mkdir($log_filename, 0777, true);
            chmod($log_filename, 0777);
        }

        if (!file_exists($log_filename . "/" . date('Y')))
        {
            // create directory/folder uploads.
            mkdir($log_filename . "/" . date('Y'), 0777, true);
            chmod($log_filename . "/" . date('Y'), 0777);
        }

        if (!file_exists($log_filename . "/" . date('Y') . "/" . date('M')))
        {
            // create directory/folder uploads.
            mkdir($log_filename . "/" . date('Y') . "/" . date('M'), 0777, true);
            chmod($log_filename . "/" . date('Y') . "/" . date('M'), 0777);
        }

        //$data = date("Y-m-d H:i:s")." - ".$data;
        $log_file_data = $log_filename . "/" . date('Y') . "/" . date('M') . '/' . date('d-M-Y') . '.log';
        file_put_contents($log_file_data, $data . "\n", FILE_APPEND);
        chmod($log_file_data, 0777);
    }
}
