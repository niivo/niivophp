<?php

class BSE_SIP_ORDER_API {

//    protected $SOAP_URL = "http://bsestarmfdemo.bseindia.com/MFOrderEntry/MFOrder.svc";
//    protected $GET_PASSWORD_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://bsestarmf.in/MFOrderEntry/getPassword</wsa:Action>';
//    protected $ORDER_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://bsestarmf.in/MFOrderEntry/sipOrderEntryParam</wsa:Action>';
//    protected $SOAP_URL = "http://www.bsestarmf.in/MFOrderEntry/MFOrder.svc";
//    protected $GET_PASSWORD_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://bsestarmf.in/MFOrderEntry/getPassword</wsa:Action>';
//    protected $ORDER_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://bsestarmf.in/MFOrderEntry/sipOrderEntryParam</wsa:Action>';
//        protected $API_USER     =   "1438202";
//        protected $API_PASS     =   "123456";
//        protected $API_MEMBER =   "14382";

    protected $API_USER = API_USER;
    protected $API_PASS = API_PASS;
    protected $API_MEMBER = API_MEMBER;
    protected $EUIN_VAL = API_EUIN;
    var $PASS_key = "";

    private function soapHeader($tp) {
        $headerStr = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:bses="http://bsestarmf.in/">
                            <soap:Header>';
        $headerStr .= ($tp == "PASS") ? $this->GET_PASSWORD_ACTION : $this->ORDER_ACTION;
        $headerStr .= '<wsa:To xmlns:wsa="http://www.w3.org/2005/08/addressing">' . $this->SOAP_URL . '</wsa:To>';
        $headerStr .= '</soap:Header><soap:Body>';
        return $headerStr;
    }

    private function soapFooter() {
        $footerStr = '</soap:Body></soap:Envelope>';
        return $footerStr;
    }

    function sendRequest($xml_string) {

        $headers = array(
            "Content-type: application/soap+xml;charset=\"utf-8\"",
        ); //SOAPAction: your op URL

        $url = $soapUrl;
        $xml_post_string = $xml_string;



        // PHP cURL  for https connection with auth
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $this->SOAP_URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // converting
        $response = curl_exec($ch);
        curl_close($ch);


        $xml = new SimpleXMLElement($response);
        $xml->registerXPathNamespace("soap", "http://www.w3.org/2003/05/soap-envelope");
        $body = $xml->xpath("//soap:Body");
        return $body;
    }

    function getPassword() {
        $str = self::soapHeader("PASS");
        $str .= "<bses:getPassword>" .
                "<bses:UserId>" . $this->API_USER . "</bses:UserId>" .
                "<bses:Password>" . $this->API_PASS . "</bses:Password>" .
                "<bses:PassKey>" . $this->PASS_key . "</bses:PassKey>" .
                "</bses:getPassword>";
        $str .= self::soapFooter();

        $resp = self::sendRequest($str);
        $resp = ($resp[0]->getPasswordResponse->getPasswordResult);
        $a = explode("|", $resp);


        if ($a[0] == 100)
            return $a[1];
    }

    function __construct($passKey = "ninehertz123") {
        $this->PASS_key = $passKey;
        if (MODE) {
            $this->SOAP_URL = "http://www.bsestarmf.in/MFOrderEntry/MFOrder.svc";
            $this->GET_PASSWORD_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://bsestarmf.in/MFOrderEntry/getPassword</wsa:Action>';
            $this->ORDER_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://bsestarmf.in/MFOrderEntry/sipOrderEntryParam</wsa:Action>';
        } else {
            $this->SOAP_URL = "http://bsestarmfdemo.bseindia.com/MFOrderEntry/MFOrder.svc";
            $this->GET_PASSWORD_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://bsestarmf.in/MFOrderEntry/getPassword</wsa:Action>';
            $this->ORDER_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://bsestarmf.in/MFOrderEntry/sipOrderEntryParam</wsa:Action>';
        }
    }

    function saveSipOrder($rs) {
        global $db;
        $status = false;
        $clintCode = $rs['userid'];
        $orderType = $rs['order_type'];
        $schemoCD = $rs['scheme_code'];
        $amount = $rs['amount'];
        $remarks = $rs['remarks'];

        $quantity = $rs['quantity'];
        $is_goal = $rs['is_goal'];

        $installment = $rs['installment'];
        $startDate = $rs['startDate'];

        $fund_category = $rs['fund_category'];
        $goal_name = $rs['goal_name'];
        $investment_type = $rs['investment_type'];
        $goal_year = $rs['goal_year'];
        $goal_ammount = $rs['goal_ammount'];
        $scheme = json_decode(stripslashes($rs['scheme_json']));
        $bseOrderNo = array();

        $refNo = rand(100, 999) . time();

        if (count($scheme) == 0)
            return array("msg" => "Scheme Code can't be empty", $status = false);
        else {
            foreach ($scheme as $key => $scheme_data) {


                $scheme_name = $scheme_data->scheme_name;
                $scheme_code = $scheme_data->scheme_code;
                $scheme_nav = $scheme_data->nav;
                $scheme_amount = $scheme_data->scheme_amount;
                $scheme_qty = $scheme_data->scheme_qty;

                $sql = "insert into orders(order_id,order_type,dtdate,userid,nav,scheme_code,amount,quantity,is_goal,remarks)VALUES(" .
                        "'" . $refNo . "'," .
                        "'" . $orderType . "'," .
                        "'" . nowDateTime() . "'," .
                        "'" . $clintCode . "'," .
                        "'" . $scheme_nav . "'," .
                        "'" . $scheme_code . "'," .
                        "'" . $scheme_amount . "'," .
                        "'" . $scheme_qty . "'," .
                        "'" . $is_goal . "'," .
                        "'" . $remarks . "')";
                $result = $db->query($sql);
                $id = $result->insertID();
                $requst = array('userid' => $clintCode, 'scheme_code' => $scheme_code, "scheme_amount" => $scheme_amount, "remarks" => $remarks, "startDate" => $startDate, "installment" => $installment);

                $bseResp = $this->sipOrderEntry($requst, $id, $refNo);

                $bseOrderNo[] = $bseResp;
            }
            
            if ($is_goal) {
                $sql1 = "INSERT INTO goal_detail SET "
                        . " order_id = '" . $id . "',"
                        . " main_orderid = '" . $refNo . "',"
                        . " fund_category = '" . $fund_category . "',"
                        . " goal_name = '" . $goal_name . "',"
                        . " investment_type = '" . $investment_type . "',"
                        . " goal_year = '" . $goal_year . "',"
                        . " goal_ammount = '" . $goal_ammount . "',"
                        . " dtdate = '" . nowDateTime() . "' "
                        . " ";
                $resg = $db->query($sql1);
            }


            return array("msg" => "Success", "status" => true, "id" => $id, "order_id" => $orderID, "schemes" => $bseOrderNo);
        }
    }

    function updateLocalOrder($order_id, $order_status, $bse_order_no, $reason = "") {
        global $db;
        $status = false;

        echo $sql = "update orders set " .
                "order_status='" . $order_status . "'," .
                "sip_registration_no='" . $bse_order_no . "'," .
                "failed_reson='" . $reason . "' where order_id=" . $order_id;
        $result = $db->query($sql);
		exit;
        $id = $result->insertID();
        return array("msg" => "Success", "status" => true, "id" => $id, "order_id" => $orderID);
    }

    function deleteLocalOrder($order_id) {
        global $db;
        $status = false;

        $sql = "delete from orders where bse_order_no='" . $order_id . "'";
        $result = $db->query($sql);
        $id = $result->insertID();
        return array("msg" => "Success", "status" => true, "id" => $id, "order_id" => $orderID);
    }

    public static function orderList($userid) {
        global $db;

        $data = array();
        $status = "false";

        $sql = "SELECT * FROM orders WHERE 1 ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            while ($row = $que->fetch()) {
                $SCHEME_CODE = $row['scheme_code'];

                $scheme_data = Listing::schemeDetail($SCHEME_CODE);
                $scheme_detail = $scheme_data['data'];
                if ($scheme_detail['FUND_NAME'] != null) {
                    $row['FUND_NAME'] = $scheme_detail['FUND_NAME'];
                } else {
                    $row['FUND_NAME'] = "";
                }
                $state[] = $row;
            }
            $data = $state;
            $msg = "Success";
            $status = "true";
        } else {
            $msg = "Record not found";
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
    }

    function sipOrderEntry($rs, $order_id, $refNo) {

        global $db;
        $status = false;
        $orderType = "NEW";


//        $localOrder = self::saveLocalOrder($rs);
//        if ($localOrder['stauts'] === false)
//            return array("status" => $status, "order_no" => $localOrder['order_id'], "msg" => $localOrder['msg']);
        $orderParam = new stdClass();
        try {

            $tempPass = self::getPassword();

            $transNo = time();

//            $transNo = $localOrder['order_id'];

            $clintCode = $rs['userid'];
            $schemeCD = $rs['scheme_code'];
            $amount = $rs['scheme_amount'];
            $remarks = $rs['remarks'];
            $startDate = $rs['startDate'];
            $frequency = "MONTHLY";
            $noOfInstallment = $rs['installment'];

            if ($tempPass != '') {
                $orderParam->TransactionCode = $orderType;
                $orderParam->UniqueRefNo = time() . rand(101, 999);
//                $orderParam->UniqueRefNo = $transNo; //rand(100001,999999);
                $orderParam->SchemeCode = $schemeCD;
                $orderParam->MemberCode = $this->API_MEMBER;
                $orderParam->ClientCode = $clintCode;
                $orderParam->UserID = $this->API_USER;

                $orderParam->InternalRefNo = $refNo;

                $orderParam->TransMode = "P";
                $orderParam->DpTxnMode = "P";
                $orderParam->StartDate = $startDate;
                $orderParam->FrequencyType = $frequency;
                $orderParam->FrequencyAllowed = 1;
                $orderParam->InstallmentAmount = $amount;
                $orderParam->NoOfInstallment = $noOfInstallment;
                $orderParam->Remarks = $remarks;
                $orderParam->FolioNo = "";
                $orderParam->FirstOrderFlag = "Y";
                $orderParam->SubberCode = "";
                $orderParam->Euin = $this->EUIN_VAL;
                //$orderParam->Euin = "E193918";
                $orderParam->EuinVal = "N";

                $orderParam->DPC = "Y";

                $orderParam->RegId = "";
                $orderParam->IPAdd = "180.188.253.61";
                $orderParam->Password = $tempPass;
                $orderParam->PassKey = $this->PASS_key;
                $orderParam->Parma1 = "";
                $orderParam->Param2 = "";
                $orderParam->Param3 = "";


                $str = self::soapHeader("");

                $str .= "<bses:sipOrderEntryParam>";
                foreach ($orderParam as $k => $v) {
                    $str .= "<bses:" . $k . ">" . $v . "</bses:" . $k . ">";
                }
                $str .= " </bses:sipOrderEntryParam>";
                $str .= self::soapFooter();

                $type = "SIP";
                $xml_file = User::xmlUpload($clintCode, $str, $type);

                $resp = $this->sendRequest($str);


                $resp = ($resp[0]->sipOrderEntryParamResponse->sipOrderEntryParamResult);
				
                $a = explode("|", $resp);
                $b = explode(":", $a[6]);
              
                if ($b[0] != "FAILED") {
                    $orderNo = $b[1];
                    $msg = "Order Confirm";
                    $status = true;
                    self::updateLocalOrder($transNo, "CONFIRM", trim($orderNo));
                    $rs = array("order_id" => $transNo, "userid" => $clintCode, "scheme_code" => $schemeCD, "amount" => $amount, "type" => "purchase");
                } else {
                    $orderNo = -1;
                    $msg = "Failed" . $b['1'];
                    self::updateLocalOrder($transNo, "FAILED", $orderNo, $b['1']);
                }

                $arr = array("status" => $status, "order_id" => $transNo, "bse_order_no" => $orderNo, "retry" => "0", "msg" => $msg);
                return $arr;
            }
        } catch (Exception $e) {
            return array("status" => $status, "msg" => "Oops, something went wrong.\n Please try again.", "retry" => "1", "order_id" => "", "bse_order_no" => "", "error" => $e->getMessage());
        }
    }

    function sipOrderCancel($rs) {
        global $db;
        $status = false;


        $userid = $rs['userid'];
        $orderid = $rs['orderid'];
        $is_goal = $rs['is_goal'];
        $goal_id = $rs['goal_id'];


        $sql = "SELECT * FROM orders WHERE 1 AND userid = '" . $userid . "' AND order_id = '" . $orderid . "' ";
        $que = $db->query($sql);
        if ($que->size() == 0) {
            $arr = array("status" => "false", "order_id" => $orderid, "msg" => "Record not found");
        } else {
            if ($is_goal == 1) {
                $sql1 = "select * from goal_detail where main_orderid = '" . $orderid . "' AND id = '" . $goal_id . "' ";
                $res1 = $db->query($sql1);
                if ($res1->size() > 0) {

                    $sql1 = "DELETE FROM goal_detail where main_orderid = '" . $orderid . "' AND id = '" . $goal_id . "' ";
                    $result1 = $db->query($sql1);
                }
            }
            while ($row = $que->fetch()) {
                $sip_regisration_no = $row['sip_regisration_no'];
                $scheme_code = $row['scheme_code'];
                $scheme_amount = $row['scheme_amount'];
                $remarks = $row['remarks'];

                $requst = array('userid' => $userid, 'orderid' => $orderid, "regn_id" => $sip_regisration_no, "scheme_code" => $scheme_code, "scheme_amount" => $scheme_amount, "remarks" => $remarks, "installment" => $installment);
                $sipResp = $this->sipCancel($requst);
            }
            $status = "true";
            $msg = "Order Cancel Successfully";
            $arr = array("status" => $status, "order_id" => $orderid, "retry" => "0", "msg" => $msg);
        }

        return $arr;
    }

    function sipCancel($rs) {
        global $db;
        $status = false;
        $orderType = "CXL";

        $orderParam = new stdClass();
        try {

            $tempPass = self::getPassword();

//            $transNo = $localOrder['order_id'];
            $transNo = time();
            $niivoOrderId = $rs['orderid'];
            $sip_regisration_no = $rs['regn_id'];
            $clintCode = $rs['userid'];
            $schemeCD = $rs['scheme_code'];
            $amount = $rs['scheme_amount'];
            $remarks = $rs['remarks'];

            $noOfInstallment = $rs['installment'];

            if ($tempPass != '') {
                $orderParam->TransactionCode = $orderType;
                $orderParam->UniqueRefNo = time() . rand(101, 999);
//                $orderParam->UniqueRefNo = $transNo; //rand(100001,999999);
                $orderParam->SchemeCode = $schemeCD;
                $orderParam->MemberCode = $this->API_MEMBER;
                $orderParam->ClientCode = $clintCode;
                $orderParam->UserID = $this->API_USER;

                $orderParam->InternalRefNo = "";

                $orderParam->TransMode = "P";
                $orderParam->DpTxnMode = "P";
                $orderParam->StartDate = "";
                $orderParam->FrequencyType = "";
                $orderParam->FrequencyAllowed = "";
                $orderParam->InstallmentAmount = $amount;
                $orderParam->NoOfInstallment = $noOfInstallment;
                $orderParam->Remarks = $remarks;
                $orderParam->FolioNo = "";
                $orderParam->FirstOrderFlag = "";
                $orderParam->SubberCode = "";
                $orderParam->Euin = $this->EUIN_VAL;
                //$orderParam->Euin = "E193918";
                $orderParam->EuinVal = "N";

                $orderParam->DPC = "";

                $orderParam->RegId = $sip_regisration_no;
                $orderParam->IPAdd = "180.188.253.61";
                $orderParam->Password = $tempPass;
                $orderParam->PassKey = $this->PASS_key;
                $orderParam->Parma1 = "";
                $orderParam->Param2 = "";
                $orderParam->Param3 = "";


                $str = self::soapHeader("");

                $str .= "<bses:sipOrderEntryParam>";
                foreach ($orderParam as $k => $v) {
                    $str .= "<bses:" . $k . ">" . $v . "</bses:" . $k . ">";
                }
                $str .= " </bses:sipOrderEntryParam>";
                $str .= self::soapFooter();



                $resp = $this->sendRequest($str);


                $resp = ($resp[0]->sipOrderEntryParamResponse->sipOrderEntryParamResult);


                $a = explode("|", $resp);
                $b = explode(":", $a[6]);
                // print_r($b);
                if ($b[0] != "FAILED") {
                    $orderNo = $b[1];
                    $msg = "Order Confirm";
                    $status = true;
                    self::cancelLocalOrder($niivoOrderId, "CANCEL", "CANCELLED BY USER");
                } else {
                    $orderNo = -1;
                    $msg = "Failed" . $b['1'];
                }

                $arr = array("status" => $status, "order_id" => $transNo, "bse_order_no" => $orderNo, "retry" => "0", "msg" => $msg);
                return $arr;
            }
        } catch (Exception $e) {
            return array("status" => $status, "msg" => "Oops, something went wrong.\n Please try again.", "retry" => "1", "order_id" => "", "bse_order_no" => "", "error" => $e->getMessage());
        }
    }

    function cancelLocalOrder($order_id, $order_status, $bse_order_no, $reason = "") {
        global $db;
        $status = false;

        $sql = "update orders set " .
                "order_status='" . $order_status . "'," .
                "failed_reson='" . $reason . "' where id=" . $order_id;
        $result = $db->query($sql);
        $id = $result->insertID();
        return array("msg" => "Success", "status" => true, "id" => $id, "order_id" => $orderID);
    }

}
