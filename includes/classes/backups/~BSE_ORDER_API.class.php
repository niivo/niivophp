<?php

class BSE_ORDER_API {

//    protected $SOAP_URL = "http://bsestarmfdemo.bseindia.com/MFOrderEntry/MFOrder.svc";
//    protected $API_USER = "1707101";
//    protected $API_MEMBER = "17071";
//    protected $API_PASS = "@123456";
//    var $PASS_key = "";
    protected $GET_PASSWORD_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://bsestarmf.in/MFOrderEntry/getPassword</wsa:Action>';
    protected $ORDER_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://bsestarmf.in/MFOrderEntry/orderEntryParam</wsa:Action>';

//    protected $GET_PASSWORD_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://bsestarmf.in/MFOrderEntry/getPassword</wsa:Action>';
//    protected $ORDER_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://bsestarmf.in/MFOrderEntry/orderEntryParam</wsa:Action>';
     protected $SOAP_URL = "http://www.bsestarmf.in/MFOrderEntry/MFOrder.svc";
        protected $API_USER     =   "1438202";
        protected $API_PASS     =   "123456";
        protected $API_MEMBER =   "14382";
        var $PASS_key = "";


    private function soapHeader($tp) {
        $headerStr = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:bses="http://bsestarmf.in/">
                            <soap:Header>';
        $headerStr .= ($tp == "PASS") ? $this->GET_PASSWORD_ACTION : $this->ORDER_ACTION;
        $headerStr .= '<wsa:To xmlns:wsa="http://www.w3.org/2005/08/addressing">' . $this->SOAP_URL . '</wsa:To>';
        $headerStr .= '</soap:Header><soap:Body>';
        return $headerStr;
    }

    private function soapFooter() {
        $footerStr = '</soap:Body></soap:Envelope>';
        return $footerStr;
    }

    function sendRequest($xml_string) {
        $headers = array(
            "Content-type: application/soap+xml;charset=\"utf-8\"",
        ); //SOAPAction: your op URL

        $url = $soapUrl;
        $xml_post_string = $xml_string;


        // PHP cURL  for https connection with auth
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $this->SOAP_URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // converting
        $response = curl_exec($ch);


        curl_close($ch);

        $xml = new SimpleXMLElement($response);
        $xml->registerXPathNamespace("soap", "http://www.w3.org/2003/05/soap-envelope");
        $body = $xml->xpath("//soap:Body");
        return $body;
    }

    function getPassword() {
        $str = self::soapHeader("PASS");
        $str .= "<bses:getPassword>" .
                "<bses:UserId>" . $this->API_USER . "</bses:UserId>" .
                "<bses:Password>" . $this->API_PASS . "</bses:Password>" .
                "<bses:PassKey>" . $this->PASS_key . "</bses:PassKey>" .
                "</bses:getPassword>";
        $str .= self::soapFooter();
        $resp = self::sendRequest($str);

        $resp = ($resp[0]->getPasswordResponse->getPasswordResult);
        $a = explode("|", $resp);

        if ($a[0] == 100)
            return $a[1];
    }

    function __construct($passKey = "ninehertz123") {
        $this->PASS_key = $passKey;
    }

    function orderEntry($rs, $order_id) {
        global $db;
        $status = false;
        $orderType = "NEW";
//        $localOrder = self::saveLocalOrder($rs);
//        if ($localOrder['stauts'] === false)
//            return array("status" => $status, "order_no" => $localOrder['id'], "msg" => $localOrder['msg'], "retry" => "1",);

        $orderParam = new stdClass();
        try {
            $tempPass = self::getPassword();

            $transNo = time();
            $clintCode = $rs['userid'];
            $schemeCD = $rs['scheme_code'];
            $amount = $rs['scheme_amount'];
            $remarks = $rs['remarks'];

            if ($tempPass != '') {
                $orderParam->TransCode = $orderType;
                $orderParam->TransNo = $transNo.rand(101,999);
                $orderParam->OrderId = "";
                $orderParam->UserID = $this->API_USER;
                $orderParam->MemberId = $this->API_MEMBER;
                $orderParam->ClientCode = $clintCode;
                $orderParam->SchemeCd = $schemeCD;
                $orderParam->BuySell = "P";
                $orderParam->BuySellType = "FRESH";
                $orderParam->DPTxn = "P";
                $orderParam->OrderVal = $amount;
                $orderParam->Qty = "";
                $orderParam->AllRedeem = "N";
                $orderParam->FolioNo = "";
                $orderParam->Remarks = $remarks;
                $orderParam->KYCStatus = "Y";
                $orderParam->RefNo = "";
                $orderParam->SubBrCode = "";
                $orderParam->EUIN = "E193918";
                $orderParam->EUINVal = "N";
                $orderParam->MinRedeem = "N";
                $orderParam->DPC = "Y";
                $orderParam->IPAdd = "180.188.253.61";
                $orderParam->Password = $tempPass;
                $orderParam->PassKey = $this->PASS_key;
                $orderParam->Parma1 = "";
                $orderParam->Param2 = "";
                $orderParam->Param3 = "";
                $str = self::soapHeader("");
                $str .= "<bses:orderEntryParam>";
                foreach ($orderParam as $k => $v) {
                    $str .= "<bses:" . $k . ">" . $v . "</bses:" . $k . ">";
                }
                $str .= " </bses:orderEntryParam>";
                $str .= self::soapFooter();

                $resp = self::sendRequest($str);
                //echo "<pre>";
                //echo $resp;
                $resp = ($resp[0]->orderEntryParamResponse->orderEntryParamResult);
                // echo $resp;
                $a = explode("|", $resp);
                $b = explode(":", $a[6]);
                if ($b[0] != "FAILED") {
                    $orderNo = trim($b[9]);
                    $orderNoArr = explode(" ", $orderNo);
                    $orderNo = $orderNoArr[0];
                    $msg = "Order Confirm";
                    $status = true;
                    self::updateLocalOrder($order_id, "CONFIRM", trim($orderNo));
                    $rs = array("order_id" => $order_id, "userid" => $clintCode, "scheme_code" => $schemeCD, "amount" => $amount, "type" => "purchase");
                } else {
                    $orderNo = -1;
                    $msg = "Failed" . $b['1'];
                    self::updateLocalOrder($order_id, "FAILED", $orderNo, $b['1']);
                }

                $arr = array("status" => $status, "order_id" => $transNo, "bse_order_no" => $orderNo, "retry" => "0", "msg" => $msg);

                return $arr;
            }
        } catch (Exception $e) {
            return array("status" => $status, "msg" => "Oops, something went wrong.\n Please try again.", "retry" => "1", "order_id" => "", "bse_order_no" => "", "error" => $e->getMessage());
        }
    }

    function orderWithdrawEntry($rs) {
        global $db;
        $status = false;
        $orderType = "NEW";

        $orderParam = new stdClass();
        try {

            $tempPass = self::getPassword();
            $transNo = time();
            $clintCode = $rs['userid'];
            $schemeCD = $rs['scheme_code'];
            $amount = $rs['amount'];
            $fullWithdraw = strtoupper($rs['full_withdraw']);
            $amount = ($fullWithdraw == "Y") ? "" : $rs['amount'];

            $remarks = $rs['remarks'];
            if ($tempPass != '') {
                $orderParam->TransCode = $orderType;
                $orderParam->TransNo = $transNo; //rand(100001,999999);
                $orderParam->OrderId = "";
                $orderParam->UserID = $this->API_USER;
                $orderParam->MemberId = $this->API_MEMBER;
                $orderParam->ClientCode = $clintCode;
                $orderParam->SchemeCd = $schemeCD;
                $orderParam->BuySell = "R";
                $orderParam->BuySellType = "FRESH";
                $orderParam->DPTxn = "P";
                $orderParam->OrderVal = $amount;
                $orderParam->Qty = "";
                $orderParam->AllRedeem = "N";
                $orderParam->FolioNo = "";
                $orderParam->Remarks = $remarks;
                $orderParam->KYCStatus = "Y";
                $orderParam->RefNo = "";
                $orderParam->SubBrCode = "";
                $orderParam->EUIN = "E193918";
                $orderParam->EUINVal = "N";
                $orderParam->MinRedeem = "N";
                $orderParam->DPC = "Y";
                $orderParam->IPAdd = "180.188.253.61";
                $orderParam->Password = $tempPass;
                $orderParam->PassKey = $this->PASS_key;
                $orderParam->Parma1 = "";
                $orderParam->Param2 = "";
                $orderParam->Param3 = "";
                $str = self::soapHeader("");
                $str .= "<bses:orderEntryParam>";
                foreach ($orderParam as $k => $v) {
                    $str .= "<bses:" . $k . ">" . $v . "</bses:" . $k . ">";
                }
                $str .= " </bses:orderEntryParam>";
                $str .= self::soapFooter();

                $resp = self::sendRequest($str);

                $resp = ($resp[0]->orderEntryParamResponse->orderEntryParamResult);
                // echo $resp;
                $a = explode("|", $resp);
                $b = explode(":", $a[6]);
                if ($b[0] != "FAILED") {
                    $orderNo = $b[9];
                    $msg = "Order Confirm";
                    $status = true;
                    $sql = "insert into withdraw(dtdate,userid,bse_order_no,scheme_code,amount,status)VALUES(" .
                            "'" . nowDateTime() . "'," .
                            "'" . $clintCode . "'," .
                            "'" . $orderNo . "'," .
                            "'" . $schemeCD . "'," .
                            "'" . ($amount) . "',1)";

                    $result = $db->query($sql);
                    $insertID = $result->insertID();
                    if ($result) {
                        $rs = array("order_id" => $insertID, "userid" => $clintCode, "scheme_code" => $schemeCD, "amount" => ($amount * -1), "type" => "withdraw");
                        self::saveTransaction($rs);
                    }
                } else {
                    $orderNo = -1;
                    $msg = "Failed" . $b['1'];
                }

                $arr = array("status" => $status, "order_id" => $transNo, "bse_order_no" => $orderNo, "retry" => "0", "msg" => $msg);

                return $arr;
            }
        } catch (Exception $e) {
            return array("status" => $status, "msg" => "Oops, something went wrong.\n Please try again.", "retry" => "1", "order_id" => "", "bse_order_no" => "", "error" => $e->getMessage());
        }
    }

    function cancelOrder($rs) {
        global $db;
        $status = false;
        $orderType = "CXL";

        $orderParam = new stdClass();
        $tempPass = self::getPassword();
        $transNo = "";

        $bse_order_no = $rs['order_id'];
        $clintCode = $rs['userid'];
        $schemeCD = ""; //$rs['scheme_code'];
        $amount = ""; //$rs['amount'];
        $remarks = $rs['remarks'];



        $sql = "SELECT * FROM orders WHERE 1 AND bse_order_no = '" . $bse_order_no . "' ";

        $que = $db->query($sql);
        if ($que->size() == 0) {
			$rs=	$que->fetch();
            $arr = array("status" => "false", "order_id" => $transNo, "bse_order_no" => $bse_order_no, "msg" => "Record not found");
        } else {

            if ($tempPass != '') {
                $orderParam->TransCode = $orderType;
                $orderParam->TransNo = rand(100001, 999999);
                $orderParam->OrderId = $bse_order_no;
                $orderParam->UserID = $this->API_USER;
                $orderParam->MemberId = $this->API_MEMBER;
                $orderParam->ClientCode = $clintCode;
                $orderParam->SchemeCd = $schemeCD;
                $orderParam->BuySell = "P";
                $orderParam->BuySellType = "FRESH";
                $orderParam->DPTxn = "P";
                $orderParam->OrderVal = $amount;
                $orderParam->Qty = "";
                $orderParam->AllRedeem = "N";
                $orderParam->FolioNo = "";
                $orderParam->Remarks = $remarks;
                $orderParam->KYCStatus = "Y";
                $orderParam->RefNo = "";
                $orderParam->SubBrCode = "";
                $orderParam->EUIN = "E193918";
                $orderParam->EUINVal = "N";
                $orderParam->MinRedeem = "N";
                $orderParam->DPC = "Y";
                $orderParam->IPAdd = "180.188.253.61";
                $orderParam->Password = $tempPass;
                $orderParam->PassKey = $this->PASS_key;
                $orderParam->Parma1 = "";
                $orderParam->Param2 = "";
                $orderParam->Param3 = "";
                $str = self::soapHeader("");
                $str .= "<bses:orderEntryParam>";
                foreach ($orderParam as $k => $v) {
                    $str .= "<bses:" . $k . ">" . $v . "</bses:" . $k . ">";
                }
                $str .= " </bses:orderEntryParam>";
                $str .= self::soapFooter();

                $resp = self::sendRequest($str);
                //echo "<pre>";
                //echo $resp;
                $resp = ($resp[0]->orderEntryParamResponse->orderEntryParamResult);
               // echo $resp;
				

                $a = explode("|", $resp);
                $b = explode(":", $a[6]);
               // print_r($a);
               // print_r($b);
                if ($b[0] == "CXL CONF") {
                    $orderNo = $b[9];
                    $msg = "Order Cancel Successfully";
                    $status = true;
                    self::updateLocalOrder($rs['order_id'], "CANCEL", $bse_order_no, "CANCEL BY NIIVO :: PAYMENT FAILED");
                }else{
					$orderNo = -1;
                    $msg = "Failed" . $b['1'];
					self::deleteLocalOrder($bse_order_no);
				}

                $arr = array("status" => $status, "order_id" => $rs['order_id'], "bse_order_no" => $orderNo, "msg" => $msg);
            }
        }
        return $arr;
    }

    function saveLocalOrder($rs) {
        global $db;
        $status = false;
        $clintCode = $rs['userid'];
        $orderType = $rs['order_type'];
        $schemoCD = $rs['scheme_code'];
        $amount = $rs['amount'];
        $remarks = $rs['remarks'];

        $quantity = $rs['quantity'];
        $is_goal = $rs['is_goal'];

        $fund_category = $rs['fund_category'];
        $goal_name = $rs['goal_name'];
        $investment_type = $rs['investment_type'];
        $goal_year = $rs['goal_year'];
        $goal_ammount = $rs['goal_ammount'];
        $scheme = json_decode(stripslashes($rs['scheme_json']));
        $bseOrderNo = array();

        $orderID = time();

        if (count($scheme) == 0)
            return array("msg" => "Scheme Code can't be empty", $status = false);
        else {
            foreach ($scheme as $key => $scheme_data) {


                $scheme_name = $scheme_data->scheme_name;
                $scheme_code = $scheme_data->scheme_code;
                $scheme_nav = $scheme_data->nav;
                $scheme_amount = $scheme_data->scheme_amount;
                $scheme_qty = $scheme_data->scheme_qty;

                $sql = "insert into orders(order_id,order_type,dtdate,userid,nav,scheme_code,amount,quantity,is_goal,remarks)VALUES(" .
                        "'" . $orderID . "'," .
                        "'" . $orderType . "'," .
                        "'" . nowDateTime() . "'," .
                        "'" . $clintCode . "'," .
                        "'" . $scheme_nav . "'," .
                        "'" . $scheme_code . "'," .
                        "'" . $scheme_amount . "'," .
                        "'" . $scheme_qty . "'," .
                        "'" . $is_goal . "'," .
                        "'" . $remarks . "')";
                $result = $db->query($sql);
                $id = $result->insertID();
                $requst = array('userid' => $clintCode, 'scheme_code' => $scheme_code, "scheme_amount" => $scheme_amount, "remarks" => $remarks);
                $bseResp = $this->orderEntry($requst, $id);
                $bseOrderNo[] = $bseResp;
            }

            if ($is_goal) {
                $sql1 = "INSERT INTO goal_detail SET "
                        . " order_id = '" . $id . "',"
                        . " main_orderid = '" . $orderID . "',"
                        . " fund_category = '" . $fund_category . "',"
                        . " goal_name = '" . $goal_name . "',"
                        . " investment_type = '" . $investment_type . "',"
                        . " goal_year = '" . $goal_year . "',"
                        . " goal_ammount = '" . $goal_ammount . "',"
                        . " dtdate = '" . nowDateTime() . "' "
                        . " ";
                $resg = $db->query($sql1);
            }


            return array("msg" => "Success", "status" => true, "id" => $id, "order_id" => $orderID, "schemes" => $bseOrderNo);
        }
    }

    function updateLocalOrder($order_id, $order_status, $bse_order_no, $reason = "") {
        global $db;
        $status = false;

        $sql = "update orders set " .
                "order_status='" . $order_status . "'," .
                "bse_order_no='" . $bse_order_no . "'," .
                "failed_reson='" . $reason . "' where id=" . $order_id;

        $result = $db->query($sql);
        $id = $result->insertID();
        return array("msg" => "Success", "status" => true, "id" => $id, "order_id" => $orderID);
    }

    function deleteLocalOrder($order_id) {
        global $db;
        $status = false;

        $sql = "delete from orders where bse_order_no='" . $order_id . "'";
        $result = $db->query($sql);
        $id = $result->insertID();
        return array("msg" => "Success", "status" => true, "id" => $id, "order_id" => $orderID);
    }

    public static function orderList($userid, $order_status, $payment_status) {
        global $db;

        $data = array();
        $goal_detail = (object) [];
        $total_amount = 0;
        $status = "false";

        $where = " WHERE 1 AND userid = '" . $userid . "' ";
        if ($order_status != '') {
            $where .= " AND order_status = '" . $order_status . "' ";
        }
        if ($payment_status != '') {
            $where .= " AND payment_status = '" . $payment_status . "' ";
        }

        $total_order_amount = BSE_ORDER_API::totalOrderAmount($userid, $order_status, $payment_status);
        $total_amount = $total_order_amount['total_amount'];
        $total_current_value = $total_order_amount['total_current_value'];

        $sql = "SELECT DISTINCT(order_id) AS order_id,orders.* FROM orders $where  ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            while ($row = $que->fetch()) {

                $SCHEME_CODE = $row['scheme_code'];
                $order_id = $row['order_id'];
                $is_goal = $row['is_goal'];
                $quantity = $row['quantity'];

                $nav = BSE_ORDER_API::schemeNav($SCHEME_CODE);
                $value = $quantity * $nav;
                $row['current_value'] = round($value, 2);

                $amount = $row['amount'];
                $row['fund_invested'] = round($amount / $total_amount * 100, 2);

                $goal_detail = BSE_ORDER_API::goalDetail($order_id);
                $row['goal_detail'] = $goal_detail;

                if ($is_goal == 1) {
                    $order_detail = BSE_ORDER_API::orderDetail($order_id);
                    $row['order_detail'] = $order_detail;
                }
                $scheme_data = Listing::schemeDetail($SCHEME_CODE);
                $scheme_detail = $scheme_data['data'];
                if ($scheme_detail['FUND_NAME'] != null) {
                    $row['FUND_NAME'] = $scheme_detail['FUND_NAME'];
                } else {
                    $row['FUND_NAME'] = "";
                }
                $state[] = $row;
            }
            $data = $state;
            $msg = "Success";
            $status = "true";
        } else {
            $msg = "Record not found";
        }

        $arr = array("status" => $status, "msg" => $msg, "data" => $data, "total_current_value" => $total_current_value, "total_amount" => $total_amount);
        return $arr;
    }

    public static function goalDetail($order_id) {
        global $db;

        $data = (object) [];
        $status = STATUSFALSE;

        $sql = "SELECT * FROM goal_detail WHERE main_orderid = '" . $order_id . "' ";
        $que = $db->query($sql);
        $row = $que->fetch();
        if ($que->size() > 0) {
            $data = $row;
        }
        return $data;
    }

    public static function orderDetail($order_id) {
        global $db;

        $data = array();
        $total_amount = 0;

        $sql = "SELECT * FROM orders WHERE order_id = '" . $order_id . "' ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            while ($row = $que->fetch()) {
                $total_amount += $row['amount'];
                $data[] = $row;
            }
//            $data['total_amount'] = $total_amount;
        }
        return $data;
    }

    public static function schemeNav($scheme_code) {
        global $db;

        $data = array();
        $total_amount = 0;

        $sql = "SELECT NAV FROM view_niivo_master WHERE STARMF_SCHEME_CODE = '" . $scheme_code . "' ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            $row = $que->fetch();
            $NAV = $row['NAV'];
        }
        return $NAV;
    }

    public static function totalOrderAmount($userid, $order_status, $payment_status) {
        global $db;

        $data = array();
        $tot_amt = 0;

        $where = " WHERE 1 AND userid = '" . $userid . "' ";
        if ($order_status != '') {
            $where .= " AND order_status = '" . $order_status . "' ";
        }
        if ($payment_status != '') {
            $where .= " AND payment_status = '" . $payment_status . "' ";
        }

        $sql = "SELECT * FROM orders $where ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            while ($row = $que->fetch()) {
                $quantity = $row['quantity'];
                $scheme_code = $row['scheme_code'];
                $nav = BSE_ORDER_API::schemeNav($scheme_code);
                $value = $quantity * $nav;
                $total_value += $value;
                $total_amount += $row['amount'];
            }
            $data['total_amount'] = $total_amount;
            $data['total_current_value'] = round($total_value, 2);
        }
        return $data;
    }

    public static function ratingOrder($userid, $order_id, $rating) {
        global $db;
        $status = STATUSFALSE;


        if ($userid == '' || $order_id == '') {
            $msg = INVALIDCREDENTIAL;
        } else {

            $where2 = " WHERE 1 AND order_id = '" . $order_id . "' AND userid = '" . $userid . "' ";
            $sql2 = "SELECT * FROM orders $where2 ";
            $que2 = $db->query($sql2);
            if ($que2->size() == 0) {
                $msg = NORECORD;
            } else {
                $where = " WHERE 1 AND order_id = '" . $order_id . "' AND userid = '" . $userid . "' ";
                $sql = "SELECT * FROM order_rating $where ";
                $que = $db->query($sql);
                if ($que->size() == 0) {
                    $sql = "INSERT INTO order_rating SET "
                            . " userid = '" . $userid . "',"
                            . " order_id = '" . $order_id . "' ,"
                            . " rating = '" . $rating . "',"
                            . " dtdate = '" . nowDateTime() . "'"
                            . " ";
                    $res = $db->query($sql);
                    $status = "true";
                    $msg = "Success.";
                } else {
                    $msg = "Record already exist";
                }
            }
        }
        $arr = array("msg" => $msg, "status" => $status);
        return $arr;
    }

    public static function myMoney($userid) {
        global $db;
        $status = STATUSFALSE;
        $data = array();
        $tot_amt = 0;

        $where = " WHERE 1 AND userid = '" . $userid . "' AND payment_status = '1' AND order_status = 'CONFIRM' ";

        $sql = "SELECT * FROM orders $where ";
        $que = $db->query($sql);
        if ($que->size() > 0) {
            while ($row = $que->fetch()) {
                $quantity = $row['quantity'];
                $scheme_code = $row['scheme_code'];
                $nav = BSE_ORDER_API::schemeNav($scheme_code);
                $value = $quantity * $nav;
                $total_value += $value;
                $total_amount += $row['amount'];
            }
            $status = "true";
            $msg = "Success.";
            $total_order_amont = BSE_ORDER_API::totalOrderAmount($userid,"CONFIRM","1");
            $data['total_amount'] = $total_order_amont['total_amount'];
           $data['total_current_value'] = $total_order_amont['total_current_value'];
        } else {
            $msg = "No Amount Invested";
        }
        $arr = array("msg" => $msg, "status" => $status, "data" => $data);
        return $arr;
    }
    
    
    
    function saveTransaction($rs) {
        global $db;

        $order_id = $rs['order_id'];
        $userid = $rs['userid'];
        $schemeCode = $rs['scheme_code'];
        $amount = $rs['amount'];
        $bse_order_no = $rs['bse_order_no'];
        $type = strtoupper($rs['type']);
        if ($type=="WITHDRAW"){
            $NAV    =   UserClass::navValue($schemeCode);
            $qty    =   ($NAV>0)?round((abs($amount)/$NAV),3):0;
            $qty    =   ($qty*-1);
        }else{
            $NAV = $rs['nav'];
            $qty = $rs['qty'];
        }

//        if (!empty($bse_order_no)) {
        $sql = "insert into transaction(order_id,dtdate,scheme_code,userid,nav,qty,amount,type)VALUES(" .
                "'" . $order_id . "'," .
                "'" . nowDateTime() . "'," .
                "'" . $schemeCode . "'," .
                "'" . $userid . "'," .
                "'" . $NAV . "'," .
                "'" . $qty . "'," .
                "'" . $amount . "'," .
                "'" . $type . "')";
        $result = $db->query($sql);
//        }
    }

}
