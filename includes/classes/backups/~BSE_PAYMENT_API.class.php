<?php

class BSE_PAYMENT_API {

    protected $SOAP_URL = "http://www.bsestarmf.in/StarMFPaymentGatewayService/StarMFPaymentGatewayService.svc/Basic";
    protected $API_USER = "1438202";
    protected $API_MEMBER = "14382";
    protected $API_PASS = "123456";
//    
//    protected $API_USER = API_USER;
//    protected $API_PASS = API_PASS;
//    protected $API_MEMBER = API_MEMBER;
    
    var $PASS_key = "";
    protected $GET_PASSWORD_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://tempuri.org/IStarMFPaymentGatewayService/GetPassword</wsa:Action>';
    protected $PAYMNET_ACTION = '<wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">http://tempuri.org/IStarMFPaymentGatewayService/PaymentGatewayAPI</wsa:Action>';

    private function soapHeaderPassword($tp) {
        $headerStr = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:tem="http://tempuri.org/" xmlns:star="http://schemas.datacontract.org/2004/07/StarMFPaymentGatewayService">';
        $headerStr .= '<soap:Header>';

        $headerStr .= ($tp == "PASS") ? $this->GET_PASSWORD_ACTION : $this->PAYMNET_ACTION;
        $headerStr .= '<wsa:To xmlns:wsa="http://www.w3.org/2005/08/addressing">http://www.bsestarmf.in/StarMFPaymentGatewayService/StarMFPaymentGatewayService.svc/Basic</wsa:To>';
        $headerStr .= '</soap:Header><soap:Body>';
        return $headerStr;
    }

    private function soapHeaderPayment($tp) {
        $headerStr = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:tem="http://tempuri.org/" xmlns:star="http://schemas.datacontract.org/2004/07/StarMFPaymentGatewayService" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays">';
        $headerStr .= '<soap:Header>';

        $headerStr .= ($tp == "PASS") ? $this->GET_PASSWORD_ACTION : $this->PAYMNET_ACTION;
        $headerStr .= '<wsa:To xmlns:wsa="http://www.w3.org/2005/08/addressing">http://www.bsestarmf.in/StarMFPaymentGatewayService/StarMFPaymentGatewayService.svc/Basic</wsa:To>';
        $headerStr .= '</soap:Header><soap:Body>';
        return $headerStr;
    }

    function parseXML($xmlString) {
        $resp = array();
        $p = xml_parser_create();
        xml_parse_into_struct($p, $xmlString, $vals, $index);
        xml_parser_free($p);
        // echo "\nVals array\n";
        // print_r($vals);


        foreach ($vals as $val) {
            if ($val['tag'] == "B:RESPONSESTRING") {
                $resp['pass'] = $val['value'];
            }
            if ($val['tag'] == "B:STATUS") {
                $resp['status'] = $val['value'];
            }
        }

        return $resp;
    }

    private function soapFooter() {
        $footerStr = '</soap:Body></soap:Envelope>';
        return $footerStr;
    }

    function sendRequest($xml_string) {
        $headers = array(
            "Content-type: application/soap+xml;charset=\"utf-8\"",
        ); //SOAPAction: your op URL

        $url = $soapUrl;
        $xml_post_string = $xml_string;
// print_r($xml_string);
//        exit;
        // PHP cURL  for https connection with auth
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $this->SOAP_URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // converting
        $response = curl_exec($ch);

        curl_close($ch);


        return $response;
    }

    function getPassword() {
        $str = self::soapHeaderPassword("PASS");
        $str .= "<tem:GetPassword>" .
                "<tem:Param>" .
                "<star:MemberId>14382</star:MemberId>" .
                "<star:PassKey>" . $this->PASS_key . "</star:PassKey>" .
                "<star:Password>" . $this->API_PASS . "</star:Password>" .
                "<star:UserId>1438202</star:UserId>" .
                "</tem:Param>" .
                "</tem:GetPassword>";
        $str .= self::soapFooter();
        $resp = self::sendRequest($str);
        $body = self::parseXML($resp);
        if ($body['status'] == 100)
            ;
        return $body['pass'];
    }

    function __construct($passKey = "ninehertz123") {
        $this->PASS_key = $passKey;
    }

    function makeOrderStringXml($orders) {
        $str = "";
        foreach ($orders as $v) {
            $str .= "<arr:string>" . $v . "</arr:string>";
        }
        return $str;
    }

    function payOrder($rs, $order_id) {
        global $db;

        $status = false;
        $orderType = "NEW";
//        $localOrder = self::saveLocalOrder($rs);
//        if ($localOrder['stauts'] === false)
//            return array("status" => $status, "order_no" => $localOrder['id'], "msg" => $localOrder['msg'], "retry" => "1",);

        $orderParam = new stdClass();
        try {
            $tempPass = self::getPassword();


            $transNo = time();
            $clintCode = $rs['userid'];

            $user_data = User::getUserinfo($clintCode);

            $user_info = $user_data['data'];
            $account_no = $user_info['account_no'];
            $ifsc = strtoupper($user_info['ifsc']);
            $BankID = strtoupper(substr($ifsc, 0, 3));

            $orders = explode(",", $rs['orders']);
            $amount = $rs['amount'];


            if ($tempPass != '') {
                $orderParam->AccNo = $account_no;
                $orderParam->BankID = $BankID; //rand(100001,999999);
                $orderParam->ClientCode = $clintCode;
                $orderParam->EncryptedPassword = $tempPass;
                $orderParam->IFSC = $ifsc;
                $orderParam->LogOutURL = "http://172.104.187.141/services/thank-you.php";
                $orderParam->MemberCode = $this->API_MEMBER;
                $orderParam->Mode = "DIRECT";
                $orderParam->Orders = self::makeOrderStringXml($orders);
                $orderParam->TotalAmount = $amount;

                $str = self::soapHeaderPayment("");
                $str .= "<tem:PaymentGatewayAPI> <tem:Param>";
                foreach ($orderParam as $k => $v) {
                    $str .= "<star:" . $k . ">" . $v . "</star:" . $k . ">";
                }
                $str .= " </tem:Param></tem:PaymentGatewayAPI>";
                $str .= self::soapFooter();
				
                $resp = self::sendRequest($str);
                $body = self::parseXML($resp);

                if ($body['status'] == 100) {
                    $msg = $body['pass'];
                    $status = true;
                    self::updateLocalOrder($orders, -1, "");
                } else {
                    $msg = $body['pass'];
                }
                $arr = array("status" => $status, "retry" => "0", "msg" => $msg);

                return $arr;
            }
        } catch (Exception $e) {
            return array("status" => $status, "msg" => "Oops, something went wrong.\n Please try again.", "retry" => "1", "order_id" => "", "bse_order_no" => "", "error" => $e->getMessage());
        }
    }

    function updateLocalOrder($orders, $order_status, $reason = "") {
        global $db;
        $status = false;
        foreach ($orders as $order) {
            $sql = "update orders set " .
                    "payment_status=" . $order_status . "," .
                    "failed_reson='" . $reason . "' where bse_order_no=" . $order;

            $result = $db->query($sql);
        }
        $id = $result->insertID();
        return array("msg" => "Success", "status" => true, "id" => $id, "order_id" => $orderID);
    }

}
