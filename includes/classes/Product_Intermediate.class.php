<?php 

class Product_Intermediate extends Models{
	
	public static function EquityFund($sub_section,$type='RG'){
	
		global $db;
		$data = array();
        $status = "false";
	
		if ($sub_section == '') {
			$msg = INVALIDCREDENTIAL;
		} 
		else {
			 $SHARE_CLASS  =  (strtoupper($type)=="RD")?"SCHEME_PLAN = 'DIVIDEND' AND FUND_TYPE = 'INDIRECT PLAN'" : "SCHEME_PLAN = 'GROWTH' AND FUND_TYPE = 'INDIRECT PLAN'";
			
			
			if ($sub_section == 'Large') {
                $sub_Category_code = "SUB_CATEGORY_CODE  IN (2)";
			} else if ($sub_section == 'Mid') {
                $sub_Category_code = " SUB_CATEGORY_CODE  IN (4)";
            }else if ($sub_section == 'Small') {
                $sub_Category_code = "SUB_CATEGORY_CODE  IN (5)";
            } else if ($sub_section == 'Thematic') {
                $sub_Category_code = "SUB_CATEGORY_CODE  IN (10)";
            } else if ($sub_section == 'Broad') {
                $sub_Category_code = "SUB_CATEGORY_CODE  IN (1,3)";
            }else if ($sub_section == 'Contra') {
                $sub_Category_code = "SUB_CATEGORY_CODE  IN (8,7)";
            }
			
			$table_data = self::Product_query($sub_Category_code, $SHARE_CLASS);
						
            if (!empty($table_data[0])) {
			
			
                $data = $table_data;
                $msg = SUCCESS;
                $status = "true";
            } else {
                $msg = NORECORD;
            }

		}
		
		$arr = array("status" => $status, "msg" => $msg, "data" => $data);
        return $arr;
		
	} 
	
	
	 public static function DebtFund($sub_section,$type='RG') {
        global $db;

        $data = array();
        $status = "false";
		
		if ($sub_section == '') {
			$msg = INVALIDCREDENTIAL;
		} else{
			
			 $SHARE_CLASS  =  (strtoupper($type)=="RD")?"SCHEME_PLAN = 'DIVIDEND' AND FUND_TYPE = 'INDIRECT PLAN'" : "SCHEME_PLAN = 'GROWTH' AND FUND_TYPE = 'INDIRECT PLAN'";
			
			if ($sub_section == 'Liquid') {
                $sub_Category_code = "SUB_CATEGORY_CODE  IN(12,13)";
			} else if ($sub_section == 'Low') {
                $sub_Category_code = "SUB_CATEGORY_CODE  IN(14,16,15) ";
            }else if ($sub_section == 'Short') {
                $sub_Category_code = "SUB_CATEGORY_CODE  IN(17)";
            } else if ($sub_section == 'Medium') {
                $sub_Category_code = "SUB_CATEGORY_CODE  IN(18)";
            } else if ($sub_section == 'Long') {
                $sub_Category_code = "SUB_CATEGORY_CODE  IN(19,20)";
            }else if ($sub_section == 'Banking') {
                $sub_Category_code = "SUB_CATEGORY_CODE  IN(24)";
            }else if ($sub_section == 'Credit') {
                $sub_Category_code = "SUB_CATEGORY_CODE  IN(23) ";
            }else if ($sub_section == 'Corporate') {
                $sub_Category_code = "SUB_CATEGORY_CODE  IN(22) ";
            }else if ($sub_section == 'Other') {
                $sub_Category_code = "SUB_CATEGORY_CODE  IN(21,27,25,26)";
            }
			$table_data = self::Product_query($sub_Category_code, $SHARE_CLASS);
			
			   if (!empty($table_data[0])) {		
				
					$data = $table_data;
					$msg = SUCCESS;
					$status = "true";
				} else {
					$msg = NORECORD;
				}
		}
		
		$arr = array("status" => $status, "msg" => $msg, "data" => $data);

        return $arr;
		
	 }
	 
	 	 public static function HybridFund($sub_section,$type='RG') {
        global $db;

        $data = array();
        $status = "false";
		
		if ($sub_section == '') {
			$msg = INVALIDCREDENTIAL;
		} else{
			
			 $SHARE_CLASS  =  (strtoupper($type)=="RD")?"SCHEME_PLAN = 'DIVIDEND' AND FUND_TYPE = 'INDIRECT PLAN'" : "SCHEME_PLAN = 'GROWTH' AND FUND_TYPE = 'INDIRECT PLAN'";
			
			if ($sub_section == 'Arbitrage') {
                $sub_Category_code = " SUB_CATEGORY_CODE  IN(33) ";
			} else if ($sub_section == 'Balanced') {
                $sub_Category_code = "SUB_CATEGORY_CODE  IN(29)";
            }else if ($sub_section == 'EquityOriented') {
                $sub_Category_code = "SUB_CATEGORY_CODE  IN(34,30)";
            } else if ($sub_section == 'DebtOriented') {
                $sub_Category_code = "SUB_CATEGORY_CODE  IN(28)";
            } else if ($sub_section == 'MultiAsset') {
                $sub_Category_code = "SUB_CATEGORY_CODE  IN(31,32)";
            }
			
			$table_data = self::Product_query($sub_Category_code, $SHARE_CLASS);
			
			   if (!empty($table_data[0])) {		
				
					$data = $table_data;
					$msg = SUCCESS;
					$status = "true";
				} else {
					$msg = NORECORD;
				}
			
		}
		
		$arr = array("status" => $status, "msg" => $msg, "data" => $data);

        return $arr;
		
	 }
	 
	 
	 public static function OtherFund($sub_section,$type='RG') {
        global $db;

        $data = array();
        $status = "false";
		
		if ($sub_section == '') {
			$msg = INVALIDCREDENTIAL;
		} else{
			
			 $SHARE_CLASS  =  (strtoupper($type)=="RD")?"SCHEME_PLAN = 'DIVIDEND' AND FUND_TYPE = 'INDIRECT PLAN'" : "SCHEME_PLAN = 'GROWTH' AND FUND_TYPE = 'INDIRECT PLAN'";
			
			if ($sub_section == 'Childrens') {
                $sub_Category_code = "SUB_CATEGORY_CODE  IN(36)";
			} else if ($sub_section == 'Retirement') {
                $sub_Category_code = "SUB_CATEGORY_CODE  IN(35) ";
            }else if ($sub_section == 'Index') {
                $sub_Category_code = "SUB_CATEGORY_CODE  IN(37)";
            }
			
			$table_data = self::Product_query($sub_Category_code, $SHARE_CLASS);
			
			   if (!empty($table_data[0])) {		
				
					$data = $table_data;
					$msg = SUCCESS;
					$status = "true";
				} else {
					$msg = NORECORD;
				}
			
		}
		
		$arr = array("status" => $status, "msg" => $msg, "data" => $data);

        return $arr;
		
	 }
	 
		public static function NewFund($type='RG') {
			global $db;

			$data = array();
			$status = "false";
				
			
	$sql ="SELECT ID,	AMC_CODE AS AMFI_SCHEME_CODE,	SCHEME_NAME,	NAV ,	MAIN_CATEGORY,	SUB_CATEGORY,	TRADE_DATE,	RET1MONTH AS ONE_M_PERF,	RET3MONTH AS THREE_M_PERF,	RET6MONTH AS SIX_M_PERF,	RET1YEAR AS ONE_Y_PERF,	RET3YEAR AS THREE_Y_PERF,	RET5YEAR AS FIVE_Y_PERF,	RET7YEAR AS SEVEN_Y_PERF,	RET10YEAR AS TEN_Y_PERF,	RET_SINCE_INCP AS TWO_Y_PERF,	BSE_SCHEME_CODE AS STARMF_SCHEME_CODE,	BSE_RTA_SCHEME_CODE AS STARMF_RTA_SCHEME_CODE,	BSE_ISIN AS ISIN ,	BSE_SCHEME_NAME AS STARMF_SCHEME_NAME ,		BSE_PUR_ALLOWED AS PUR_ALLOWED,		BSE_MIN_PUR_AMT AS STARMF_MIN_PUR_AMT,	BSE_REDEMPTION_ALLOWED AS REDEMPTION_ALLOWED,	BSE_MIN_REDEMPTION_QTY ,	BSE_REDEMPTION_AMT_MIN,	BSE_REDEMTION_AMT_MAX,	BSE_RTA_CODE AS STARMF_RTA_CODE, BSE_DIV_REINVESTFLAG AS STARMF_DIV_REINVESTFLAG,	BSE_SIP_FLAG AS STARMF_SIP_FLAG,	BSE_L1_FLAG AS L1_FLAG,	BSE_I_FLAG AS I_FLAG,		BSE_SIP_DATES AS SIP_DATES,		BSE_SIP_MIN_INSTALLMENT_AMT AS SIP_MIN_INSTALLMENT_AMT,	BSE_SIP_MAX_INSTALLMENT_AMT ,	BSE_SIP_MULTPL_AMT AS IP_MULTPL_AMT,	BSE_SIP_MIN_INSTALLMENT_NUM AS SIP_MIN_INSTALLMENT_NUM,		BSE_SIP_MAX_INSTALLMENT_NUM AS SIP_MAX_INSTALLMENT_NUM	FROM `products`  WHERE STR_TO_DATE(`BSE_START_DATE`, '%M %d %Y')  <= CURRENT_DATE AND  STR_TO_DATE(`BSE_END_DATE`, '%M %d %Y')  <= DATE_ADD(NOW(), INTERVAL 30 DAY)  "; 
			$que = $db->query($sql);

			if ($que->size() > 0) {
				while ($row = $que->fetch()) {
					$row['STARMF_SCHEME_NAME'] = utf8_encode($row['STARMF_SCHEME_NAME']);
					$row['FUND_NAME'] = "";
					$row['FUND_CLASS'] = "";
					$row['FUND_CATEGORY'] = "";
					$row['FUND_CATEGORIZATION'] = "";
					$row['DESCRIPTION'] = "";
					if(is_null($row['ONE_M_PERF']) || empty($row['ONE_M_PERF'])){$row['ONE_M_PERF'] ="N.A.";}
					if(is_null($row['THREE_M_PERF']) || empty($row['THREE_M_PERF'])){$row['THREE_M_PERF'] ="N.A.";}
					if(is_null($row['SIX_M_PERF']) || empty($row['SIX_M_PERF'])){$row['SIX_M_PERF'] ="N.A.";}
					if(is_null($row['ONE_Y_PERF']) || empty($row['ONE_Y_PERF'])){$row['ONE_Y_PERF'] ="N.A.";}
					if(is_null($row['THREE_Y_PERF']) || empty($row['THREE_Y_PERF'])){$row['THREE_Y_PERF'] ="N.A.";}
					if(is_null($row['FIVE_Y_PERF']) || empty($row['FIVE_Y_PERF'])){$row['FIVE_Y_PERF'] ="N.A.";}
					if(is_null($row['SEVEN_Y_PERF']) || empty($row['SEVEN_Y_PERF'])){$row['SEVEN_Y_PERF'] ="N.A.";}
					if(is_null($row['TEN_Y_PERF']) || empty($row['TEN_Y_PERF'])){$row['TEN_Y_PERF'] ="N.A.";}
					if(is_null($row['TWO_Y_PERF']) || empty($row['TWO_Y_PERF'])){$row['TWO_Y_PERF'] ="N.A.";}
                    $state[] = $row;
				}
				$data = $state;
				$msg = SUCCESS;
				$status = "true";
			} else {
				$msg = NORECORD;
			}

			$arr = array("status" => $status, "msg" => $msg, "data" => $data);

			return $arr;
    }
	
			public static function TaxsavingFund($type='RG') {
			global $db;

			$data = array();
			$status = "false";
			
			$SHARE_CLASS  =  (strtoupper($type)=="RD")?"SCHEME_PLAN = 'DIVIDEND' AND FUND_TYPE = 'INDIRECT PLAN'" : "SCHEME_PLAN = 'GROWTH' AND FUND_TYPE = 'INDIRECT PLAN'";			
			
			$sub_Category_code = "SUB_CATEGORY_CODE  IN(11) ";
		   
		$table_data = self::Product_query($sub_Category_code, $SHARE_CLASS);
			
			   if (!empty($table_data[0])) {		
				
					$data = $table_data;
					$msg = SUCCESS;
					$status = "true";
				} else {
					$msg = NORECORD;
				}

			$arr = array("status" => $status, "msg" => $msg, "data" => $data);

			return $arr;
    }
	
	
	function Product_query($sub_Category_code , $SHARE_CLASS){
		
		global $db;
		$data = array();
		
 		$sql ="SELECT ID,	AMC_CODE AS AMFI_SCHEME_CODE,	SCHEME_NAME,	NAV ,	MAIN_CATEGORY,	SUB_CATEGORY,	TRADE_DATE,	RET1MONTH AS ONE_M_PERF,	RET3MONTH AS THREE_M_PERF,	RET6MONTH AS SIX_M_PERF,	RET1YEAR AS ONE_Y_PERF,	RET3YEAR AS THREE_Y_PERF,	RET5YEAR AS FIVE_Y_PERF,	RET7YEAR AS SEVEN_Y_PERF,	RET10YEAR AS TEN_Y_PERF,	RET_SINCE_INCP AS TWO_Y_PERF,	BSE_SCHEME_CODE AS STARMF_SCHEME_CODE,	BSE_RTA_SCHEME_CODE AS STARMF_RTA_SCHEME_CODE,	BSE_ISIN AS ISIN ,	BSE_SCHEME_NAME AS STARMF_SCHEME_NAME ,		BSE_PUR_ALLOWED AS PUR_ALLOWED,		BSE_MIN_PUR_AMT AS STARMF_MIN_PUR_AMT,	BSE_REDEMPTION_ALLOWED AS REDEMPTION_ALLOWED,	BSE_MIN_REDEMPTION_QTY ,	BSE_REDEMPTION_AMT_MIN,	BSE_REDEMTION_AMT_MAX,	BSE_RTA_CODE AS STARMF_RTA_CODE, BSE_DIV_REINVESTFLAG AS STARMF_DIV_REINVESTFLAG,	BSE_SIP_FLAG AS STARMF_SIP_FLAG,	BSE_L1_FLAG AS L1_FLAG,	BSE_I_FLAG AS I_FLAG,		BSE_SIP_DATES AS SIP_DATES,		BSE_SIP_MIN_INSTALLMENT_AMT AS SIP_MIN_INSTALLMENT_AMT,	BSE_SIP_MAX_INSTALLMENT_AMT ,	BSE_SIP_MULTPL_AMT AS IP_MULTPL_AMT,	BSE_SIP_MIN_INSTALLMENT_NUM AS SIP_MIN_INSTALLMENT_NUM,		BSE_SIP_MAX_INSTALLMENT_NUM AS SIP_MAX_INSTALLMENT_NUM	FROM `products` WHERE ".$sub_Category_code." AND ".$SHARE_CLASS." AND BSE_PUR_ALLOWED = 'Y' order by BSE_SCHEME_NAME  ";
	$que = $db->query($sql);
            if ($que->size() > 0) {
                while ($row = $que->fetch()) {
                    $row['STARMF_SCHEME_NAME'] = utf8_encode($row['STARMF_SCHEME_NAME']);
					$row['FUND_NAME'] = "";
					$row['FUND_CLASS'] = "";
					$row['FUND_CATEGORY'] = "";
					$row['FUND_CATEGORIZATION'] = "";
					$row['DESCRIPTION'] = "";
					
					if(is_null($row['ONE_M_PERF']) || empty($row['ONE_M_PERF'])){$row['ONE_M_PERF'] ="N.A.";}
					if(is_null($row['THREE_M_PERF']) || empty($row['THREE_M_PERF'])){$row['THREE_M_PERF'] ="N.A.";}
					if(is_null($row['SIX_M_PERF']) || empty($row['SIX_M_PERF'])){$row['SIX_M_PERF'] ="N.A.";}
					if(is_null($row['ONE_Y_PERF']) || empty($row['ONE_Y_PERF'])){$row['ONE_Y_PERF'] ="N.A.";}
					if(is_null($row['THREE_Y_PERF']) || empty($row['THREE_Y_PERF'])){$row['THREE_Y_PERF'] ="N.A.";}
					if(is_null($row['FIVE_Y_PERF']) || empty($row['FIVE_Y_PERF'])){$row['FIVE_Y_PERF'] ="N.A.";}
					if(is_null($row['SEVEN_Y_PERF']) || empty($row['SEVEN_Y_PERF'])){$row['SEVEN_Y_PERF'] ="N.A.";}
					if(is_null($row['TEN_Y_PERF']) || empty($row['TEN_Y_PERF'])){$row['TEN_Y_PERF'] ="N.A.";}
					if(is_null($row['TWO_Y_PERF']) || empty($row['TWO_Y_PERF'])){$row['TWO_Y_PERF'] ="N.A.";}
					
					$state[] = $row;
				
                }
			
                $data = $state;
                
            }

		return $data;
	}
	
	function Goals_list( $fund_category , $sip_flag, $userid, $duration ){
		
		global $db;
		$data = array();
        $status = "false";
	//	print_r($fund_category);
		 if ($sip_flag == 'Y'){
            $flag = " BSE_SIP_FLAG = 'Y' ";
         }else{
			 $flag= " BSE_PUR_ALLOWED = 'Y'";
		 }
		
		$select = "SELECT ID,	AMC_CODE AS AMFI_SCHEME_CODE,	SCHEME_NAME,	NAV ,	MAIN_CATEGORY,	SUB_CATEGORY,	TRADE_DATE,	RET1MONTH AS ONE_M_PERF,	RET3MONTH AS THREE_M_PERF,	RET6MONTH AS SIX_M_PERF,	RET1YEAR AS ONE_Y_PERF,	RET3YEAR AS THREE_Y_PERF,	RET5YEAR AS FIVE_Y_PERF,	RET7YEAR AS SEVEN_Y_PERF,	RET10YEAR AS TEN_Y_PERF,	RET_SINCE_INCP AS TWO_Y_PERF,	BSE_SCHEME_CODE AS STARMF_SCHEME_CODE,	BSE_RTA_SCHEME_CODE AS STARMF_RTA_SCHEME_CODE,	BSE_ISIN AS ISIN ,	BSE_SCHEME_NAME AS STARMF_SCHEME_NAME ,		BSE_PUR_ALLOWED AS PUR_ALLOWED,		BSE_MIN_PUR_AMT AS STARMF_MIN_PUR_AMT,	BSE_REDEMPTION_ALLOWED AS REDEMPTION_ALLOWED,	BSE_MIN_REDEMPTION_QTY ,	BSE_REDEMPTION_AMT_MIN,	BSE_REDEMTION_AMT_MAX,	BSE_RTA_CODE AS STARMF_RTA_CODE, BSE_DIV_REINVESTFLAG AS STARMF_DIV_REINVESTFLAG,	BSE_SIP_FLAG AS STARMF_SIP_FLAG,	BSE_L1_FLAG AS L1_FLAG,	BSE_I_FLAG AS I_FLAG,		BSE_SIP_DATES AS SIP_DATES,		BSE_SIP_MIN_INSTALLMENT_AMT AS SIP_MIN_INSTALLMENT_AMT,	BSE_SIP_MAX_INSTALLMENT_AMT ,	BSE_SIP_MULTPL_AMT AS IP_MULTPL_AMT,	BSE_SIP_MIN_INSTALLMENT_NUM AS SIP_MIN_INSTALLMENT_NUM,		BSE_SIP_MAX_INSTALLMENT_NUM AS SIP_MAX_INSTALLMENT_NUM	FROM `products` ";
		 
		 
		
		 switch($fund_category){
			 case "1":
					
						$sql1 = "SELECT  TIMESTAMPDIFF( YEAR, dob, CURDATE() ) AS AGE, kyc_status FROM users WHERE userid ='".$userid."'";
						$que1 = $db->query($sql1);
					
						if ($que1->size() > 0) {						
							$row1 = $que1->fetch();
							$age = $row1['AGE'];	
							$kyc_status = $row1['kyc_status'];
						}
						 
					if(!empty($age)){
						if($age < 40){
							$sql = $select." WHERE `SUB_CATEGORY_CODE`  IN (30, 34) AND SCHEME_PLAN = 'GROWTH' AND FUND_TYPE = 'INDIRECT PLAN'  AND ".$flag." ORDER BY CAST(`SUBCATEGORY_RANKING_SCORE` AS DECIMAL(6,3)) DESC LIMIT 3";
							$growth = "10";
							
 						}else if($age >= 40){
							$sql = $select." WHERE `SUB_CATEGORY_CODE`='28'  AND SCHEME_PLAN = 'GROWTH' AND FUND_TYPE = 'INDIRECT PLAN'  AND ".$flag." ORDER BY cast(`SUBCATEGORY_RANKING_SCORE` as decimal(6,3)) DESC LIMIT 3";
							$growth = "7";
						}
					}else{
						if($kyc_status == "COMPLETE"){
							$sql = $select." WHERE `SUB_CATEGORY_CODE`  IN (30, 34) AND SCHEME_PLAN = 'GROWTH' AND FUND_TYPE = 'INDIRECT PLAN'  AND ".$flag." ORDER BY CAST(`SUBCATEGORY_RANKING_SCORE` AS DECIMAL(6,3)) DESC LIMIT 3";
							$growth = "10";
						}else{
							 	$msg = INVALIDCREDENTIAL;
						}								
						 }
						
						
			 break;
				
			 
			 case "2":
						$sql="(".$select." WHERE `SUB_CATEGORY_CODE`='30' AND SCHEME_PLAN = 'GROWTH' AND FUND_TYPE = 'INDIRECT PLAN' AND ".$flag." ORDER BY CAST(`SUBCATEGORY_RANKING_SCORE` AS DECIMAL(6,3)) DESC LIMIT 1)  UNION (".$select." WHERE `SUB_CATEGORY_CODE`='28' AND SCHEME_PLAN = 'GROWTH' AND FUND_TYPE = 'INDIRECT PLAN' AND ".$flag."  ORDER BY CAST(`SUBCATEGORY_RANKING_SCORE` AS DECIMAL(6,3)) DESC LIMIT 1)";
						$growth = "7.50";
						
			 break;
			 
			 case "3":
						$sql = $select." WHERE `SUB_CATEGORY_CODE`='11' AND SCHEME_PLAN = 'GROWTH' AND FUND_TYPE = 'INDIRECT PLAN' AND ".$flag." ORDER BY CAST(`SUBCATEGORY_RANKING_SCORE` AS DECIMAL(6,3)) DESC LIMIT 3";
						$growth = "10";
			 break;
			 
			 case "4":
						 if($duration < 2){
							$sql = $select." WHERE `SUB_CATEGORY_CODE`='33' AND SCHEME_PLAN = 'GROWTH' AND FUND_TYPE = 'INDIRECT PLAN' AND ".$flag." ORDER BY cast(`SUBCATEGORY_RANKING_SCORE` as decimal(6,3))  DESC LIMIT 3";
							$growth = "6";
							 
						 }
						 else if($duration >= 2){
							$sql = $select." WHERE `SUB_CATEGORY_CODE`='1' AND SCHEME_PLAN = 'GROWTH' AND FUND_TYPE = 'INDIRECT PLAN' AND ".$flag." ORDER BY cast(`SUBCATEGORY_RANKING_SCORE` as decimal(6,3)) DESC LIMIT 3";
							$growth = "10";
							 
						 }else{
							 	$msg = INVALIDCREDENTIAL;						 
						 }
			 break;
			 
			 case "5":
					if($duration  <= 1){
							$sql= $select." WHERE `SUB_CATEGORY_CODE` IN (12,13) AND SCHEME_PLAN = 'GROWTH' AND FUND_TYPE = 'INDIRECT PLAN' AND ".$flag."  ORDER BY cast(`SUBCATEGORY_RANKING_SCORE` as decimal(6,3)) DESC LIMIT 3";
							$growth = "5";
					}
					else if($duration >= 2 && $duration <= 4){
							$sql= $select." WHERE `SUB_CATEGORY_CODE` IN(2) AND SCHEME_PLAN = 'GROWTH' AND FUND_TYPE = 'INDIRECT PLAN'  AND ".$flag." ORDER BY cast(`SUBCATEGORY_RANKING_SCORE` as decimal(6,3)) DESC LIMIT 3";
							$growth = "6";
					} 
					else if($duration > 4 ){
							$sql= $select." WHERE `SUB_CATEGORY_CODE` IN(1)  AND SCHEME_PLAN = 'GROWTH' AND FUND_TYPE = 'INDIRECT PLAN' AND ".$flag." ORDER BY cast(`SUBCATEGORY_RANKING_SCORE` as decimal(6,3)) DESC LIMIT 3";
							$growth = "10";
					}else{
							 	$msg = INVALIDCREDENTIAL;						 
						 }
			 break;
					
			 case "6":
						if($duration <= 1 ){
							$sql = $select." WHERE `SUB_CATEGORY_CODE` IN (12,13) AND SCHEME_PLAN = 'GROWTH' AND FUND_TYPE = 'INDIRECT PLAN'  AND ".$flag." ORDER BY cast(`SUBCATEGORY_RANKING_SCORE` as decimal(6,3)) DESC LIMIT 3";
							$growth = "5";
						}
						else if($duration == 2  ){
							$sql = $select." WHERE `SUB_CATEGORY_CODE` IN (33) AND SCHEME_PLAN = 'GROWTH' AND FUND_TYPE = 'INDIRECT PLAN' AND ".$flag."  ORDER BY cast(`SUBCATEGORY_RANKING_SCORE` as decimal(6,3)) DESC LIMIT 3";
							$growth = "6";
						}
						else if($duration >= 2 && $duration <= 4){
							$sql = $select." WHERE `SUB_CATEGORY_CODE` IN (2) AND SCHEME_PLAN = 'GROWTH' AND FUND_TYPE = 'INDIRECT PLAN'  AND ".$flag." ORDER BY cast(`SUBCATEGORY_RANKING_SCORE` as decimal(6,3)) DESC LIMIT 3";
							$growth = "10";
						}
						else if($duration > 4){
							$sql = $select." WHERE `SUB_CATEGORY_CODE` IN (1) AND SCHEME_PLAN = 'GROWTH' AND FUND_TYPE = 'INDIRECT PLAN'  AND ".$flag."  ORDER BY cast(`SUBCATEGORY_RANKING_SCORE` as decimal(6,3)) DESC LIMIT 3";
							$growth = "10";
						}else{
							 	$msg = INVALIDCREDENTIAL;						 
						 }
			 break;
			 
			 case "7":
						if($duration <= 1 ){ 
							$sql = $select." WHERE `SUB_CATEGORY_CODE` IN(12,13) AND SCHEME_PLAN = 'GROWTH' AND FUND_TYPE = 'INDIRECT PLAN'  AND ".$flag." ORDER BY cast(`SUBCATEGORY_RANKING_SCORE` as decimal(6,3)) DESC LIMIT 3";
							$growth = "5";
						}
						else if($duration == 2  ){
							$sql = $select." WHERE `SUB_CATEGORY_CODE` IN (33)  AND SCHEME_PLAN = 'GROWTH' AND FUND_TYPE = 'INDIRECT PLAN' AND ".$flag." ORDER BY cast(`SUBCATEGORY_RANKING_SCORE` as decimal(6,3)) DESC LIMIT 3";
							$growth = "6";
						}
						else if($duration >= 2 && $duration <= 4){
							$sql = $select." WHERE `SUB_CATEGORY_CODE` IN (28) AND SCHEME_PLAN = 'GROWTH' AND FUND_TYPE = 'INDIRECT PLAN' AND ".$flag." ORDER BY cast(`SUBCATEGORY_RANKING_SCORE` as decimal(6,3)) DESC LIMIT 3";
							$growth = "8";
						}
						else if($duration > 4){
							$sql = $select." WHERE `SUB_CATEGORY_CODE` IN (30) AND SCHEME_PLAN = 'GROWTH' AND FUND_TYPE = 'INDIRECT PLAN'  AND ".$flag." ORDER BY cast(`SUBCATEGORY_RANKING_SCORE` as decimal(6,3)) DESC LIMIT 3";
							$growth = "10";
						}else{
							 	$msg = INVALIDCREDENTIAL;						 
						 }
			 break;
			 
			 case "8":
						$sql="	(".$select." WHERE  SUB_CATEGORY_CODE IN (13,16)	OR SUB_CATEGORY_CODE IN (12) AND LOWER(BSE_SCHEME_NAME) LIKE '%regular%%daily dividend payout%' AND FUND_TYPE = 'INDIRECT PLAN'   AND ".$flag." ORDER BY CAST(`SUBCATEGORY_RANKING_SCORE` AS DECIMAL(6,3)) DESC LIMIT 1) UNION (".$select." WHERE LOWER(BSE_SCHEME_NAME) LIKE '%regular%%dividend payout%%monthly%'
						OR  SUB_CATEGORY_CODE IN (33)  AND ".$flag." ORDER BY CAST(`SUBCATEGORY_RANKING_SCORE` AS DECIMAL(6,3)) DESC LIMIT 1)";
						$growth = "5";
												
			break;
			 
			 case "9":
						$sql =	"(".$select." WHERE `SUB_CATEGORY_CODE` IN (2) AND SCHEME_PLAN = 'GROWTH' AND FUND_TYPE = 'INDIRECT PLAN'  AND ".$flag." ORDER BY CAST(`SUBCATEGORY_RANKING_SCORE` AS DECIMAL(6,3)) DESC LIMIT 1) UNION  (".$select." WHERE `SUB_CATEGORY_CODE` IN (1) AND SCHEME_PLAN = 'GROWTH' AND FUND_TYPE = 'INDIRECT PLAN' AND ".$flag." ORDER BY CAST(`SUBCATEGORY_RANKING_SCORE` AS DECIMAL(6,3)) DESC LIMIT 1)";
						$growth = "10";
			 break;
			 
			 default:
						$msg = INVALIDCREDENTIAL;
						
		 }
		 if(!empty($sql)){
		    $que = $db->query("SET NAMES utf8"); //the main trick
                $que = $db->query($sql);
                if ($que->size() > 0) {
                    while ($row = $que->fetch()) {
                        $row['STARMF_SCHEME_NAME'] = utf8_encode($row['STARMF_SCHEME_NAME']);
						$row['FUND_NAME'] = "";
						$row['FUND_CLASS'] = "";
						$row['FUND_CATEGORY'] = "";
						$row['FUND_CATEGORIZATION'] = "";
						$row['DESCRIPTION'] = "";
						
					if(is_null($row['ONE_M_PERF']) || empty($row['ONE_M_PERF'])){$row['ONE_M_PERF'] ="N.A.";}
					if(is_null($row['THREE_M_PERF']) || empty($row['THREE_M_PERF'])){$row['THREE_M_PERF'] ="N.A.";}
					if(is_null($row['SIX_M_PERF']) || empty($row['SIX_M_PERF'])){$row['SIX_M_PERF'] ="N.A.";}
					if(is_null($row['ONE_Y_PERF']) || empty($row['ONE_Y_PERF'])){$row['ONE_Y_PERF'] ="N.A.";}
					if(is_null($row['THREE_Y_PERF']) || empty($row['THREE_Y_PERF'])){$row['THREE_Y_PERF'] ="N.A.";}
					if(is_null($row['FIVE_Y_PERF']) || empty($row['FIVE_Y_PERF'])){$row['FIVE_Y_PERF'] ="N.A.";}
					if(is_null($row['SEVEN_Y_PERF']) || empty($row['SEVEN_Y_PERF'])){$row['SEVEN_Y_PERF'] ="N.A.";}
					if(is_null($row['TEN_Y_PERF']) || empty($row['TEN_Y_PERF'])){$row['TEN_Y_PERF'] ="N.A.";}
					if(is_null($row['TWO_Y_PERF']) || empty($row['TWO_Y_PERF'])){$row['TWO_Y_PERF'] ="N.A.";}

                        $state[] = $row;
                    }
                    $data = $state;
                    $msg = SUCCESS;
                    $status = "true";
                } else {
                    $msg = NORECORD;
                }
				
		 }else{
			 $msg = INVALIDCREDENTIAL;
		 }
		 
		 
		$arr = array("status" => $status, "msg" => $msg, "data" => $data,"growth"=>$growth);
        
		return $arr;
			
	}


}
?>