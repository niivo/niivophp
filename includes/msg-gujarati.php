<?php
define("INVALIDCREDENTIAL", "અમાન્ય ઓળખપત્રો");
define("NIIVOCODESENT", "આપનો નીવો વેરિફિકેશન કૉડ મોકલી આપવામાં આવ્યો છે.");
define("MSGNOTSENT", "મેસેજ મોકલાયો નથી, કૃપા કરી ફરીથી પ્રયત્ન કરો.");
define("MOBILEEXIST", "આ મોબાઇલ નંબર પહેલેથી જ રજિસ્ટર થયેલો છે. કૃપા કરી અન્ય કોઈ નંબર વડે પ્રયત્ન કરો.");
define("OTPVERIFIED", "ઓટીપીનું વેરિફિકેશન સફળતાપૂર્વક થઈ ગયું છે.");
define("ENTEROTP", "કૃપા કરી સાચો ઓટીપી દાખલ કરો.");
define("MOBILENOTEXIST", "આ મોબાઇલ નંબર અસ્તિત્વમાં નથી. કૃપા કરી માન્ય નંબર વડે ફરી પ્રયત્ન કરો.");
define("LOGINSUCCESS", "લોગઇન સફળતાપૂર્વક થઈ ગયું છે.");
define("MOBILEPASSNOMATCH", "મોબાઇલ અને પાસવર્ડનો મેળ ખાતો નથી.");
define("SUCCESS", "સફળ");
define("PASSCHANGED", "પાસવર્ડ સફળતાપૂર્વક બદલાઈ ગયો છે");
define("OLDPASSINCORRECT", "જૂનો પાસવર્ડ સાચો નથી");
define("NOUSER", "યુઝર અસ્તિત્વમાં નથી");
define("SOMETHINGWRONG", "ઓહ, કંઇક ખોટું થયું છે.\ કૃપા કરી ફરી પ્રયત્ન કરો.");
define("INVALIDREQUEST", "અમાન્ય વિનંતી");
define("SOMEERROR", "કોઈ ક્ષતિ છે. કૃપા કરી ફરી પ્રયત્ન કરો.");
define("INVALIDIFSC", "અમાન્ય આઇએફએસસી કૉડ");
define("INVALIDIFSC", "બેંક મળી શકી નથી. કૃપા કરી અન્ય કોઈ બેંકનો આઇએફએસસી કૉડ પસંદ કરો.");
define("LANGUAGECHANGED", "ભાષા સફળતાપૂર્વક બદલાઈ ગઈ છે");
define("SCHEMENOTEMPTY", "સ્કીમનો કૉડ ખાલી રાખી શકાય નહીં");
define("NORECORD", "રેકોર્ડ પ્રાપ્ત થયો નથી");
define("ORDERCANCEL", "ઓર્ડર સફળતાપૂર્વક રદ થઈ ગયો છે");
define("FAILED", "નિષ્ફળ");
define("ORDERCONFIRM", "ઓર્ડરની પુષ્ટી કરો");
define("RECORDEXIST", "રેકોર્ડ અગાઉથી જ અસ્તિત્વમાં છે");
define("NOSIPFOUND", "એસઆઇપીનો કોઈ હપ્તો બાકી હોવાનું જાણવા મળ્યું નથી");
define("NOADVISOREXIST", "એડવાઇઝર કૉડ અસ્તિત્વમાં નથી.");
define("SIPDUE", "[બાકી_તારીખ] સુધીમાં [ફંડનું_નામ] માટે રૂ. [રકમ]ના એસઆઇપી હપ્તા બાકી છે");




define("STATUSTRUE", "true");
define("STATUSFALSE", "false");

define("ACCOUNTDEACTIVATED", "Your account is deactived by Charity team for review!");

define("LOGIN", "Successful Login");
define("USERUPDATED", "user updated successfully");
define("USERADDED", "Added successfully");

define("NOPAGE", "Page doesn't exist");
define("ALREADYEXIST", "Already exist");
define("USEREMAILEXIST", "This email address already registered");

define("FBEXIST", "This fb user already registered. Please try another one");
define("SUCCESSFULLYREGISTER", "Congratulations! You have successfully registered.");
define("RESETLINK", "Reset password link has been expired. Please try again.");
define("PASSWORD", "Please enter your password.");
define("CPASSWORD", "Please enter your confirm password.");




define("RESETLINK", "Reset password link has been expired. Please try again.");
//login error msg//
define("LOGINERROR", "Email and password do not match.");

//account validation msg
define("SUSPEND", "Your account is suspended.Please contact to admin.");
define("BLOCK", "Your account is blocked.Please contact to admin.");

define("PASSNOTMATCH", "Password and confirm password does not match.");

define("PASSSEND", "A password reset link was sent to your email address.");
define("PASSCHANGE", "Your password has been changed successfully.");
define("EMAILNOTEXIST", "Your email does not exist.");


