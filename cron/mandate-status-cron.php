<?php

 $path = "/var/www/html/production/public/api";

require_once($path . 'includes/app_top.php');
require_once($path . 'includes/mysql.class.php');
require_once($path . 'includes/global.inc.php');
require_once($path . 'includes/functions_general.php');

require_once($path . 'includes/classes/MandateStatus.class.php');


require_once ($path.'includes/config.php');

$status_table	=	array('REGISTERED BY MEMBER',
'APPROVED',
'REJECTED'.
'INITIAL REJECTION',
'UNDER PROCESSING',
'RETURNED BY EXCHANGE',
'RECEIVED BY SPONSOR BANK',
'REJECTION AT NPCI PRIOR TO DESTINATION BANK',
'CANCELLED BY INVESTOR',
'APPROVED BY SPONSOR BANK',
'REJECTED BY SPONSOR BANK');

function updateUsersMendateDetail($rs,$status){
    global $db;
    $sqlDel =   "delete from user_mandate_details where userid=".$rs['client_code'];
    $resDel =   $db->query($sqlDel);
    
    $sql    =   "INSERT INTO `user_mandate_details` (`userid`, `dtdate`, `approve_date`, `amount`, `account_no`, `bank_branch`, `client_name`, `mandateId`, `mandate_type`, `register_date`, `remarks`, `mandate_status`,`umrn_no`) VALUES(".
            "'".$rs['client_code']."',".
            "NOW(),".
            "'".$rs['approve_date']."',".
            "'".$rs['amount']."',".
            "'".$rs['account_no']."',".
            "'".$rs['bank_branch']."',".
            "'".$rs['client_name']."',".
            "'".$rs['mandateId']."',".
            "'".$rs['mandate_type']."',".
            "'".$rs['register_date']."',".
            "'".$rs['remarks']."',".
			 "'".$rs['mandate_status']."',".
            "'".$rs['umrn_no']."')";
    
    $result =   $db->query($sql);
    
	if ($status==0 or $status==4 or $status==6 or $status==8)
			$mendateStatus	=	"REGISTERED";
	elseif ($status==2)
			$mendateStatus	=	"VERIFIED";
	else
			$mendateStatus	=	"REJECTED";
		
    echo $sqlUpdt    =   "update users set  mandate_limit=".$rs['amount'].",is_mendate='".$mendateStatus."' where userid=".$rs['client_code'];
    $resultUpdt =   $db->query($sqlUpdt);
    
}
function checkMandateStatus(){
    global $db;
	global $status_table;
    $MAN_STATUS =   new MandateStatus;
	//$resp   =   $MAN_STATUS->mandateDetail('10000015', '1920962');
	//$mendateStatus	=	(array_search($resp['mandate_status'],$status_table));
	// if ($resp['status']==100){
     //           updateUsersMendateDetail($resp,$mendateStatus);
     //       }
	//		echo "<pre>";
	//		print_r($resp);
	//		exit;
    
   
    $sql    =   "select * from users where is_mendate='REGISTERED' and mandate_id!=''";
	//$sql    =   "select * from users where mandate_id='1838059'";
	
    $result =   $db->query($sql);
	
    if ($result->size()>0){
        while($rs   =   $result->fetch()){
            $resp   =   $MAN_STATUS->mandateDetail($rs['userid'], $rs['mandate_id']);
			$mendateStatus	=	(array_search($resp['mandate_status'],$status_table));
			
            if ($resp['status']==100){
                updateUsersMendateDetail($resp,$mendateStatus);
            }
            
        }
    }
}
checkMandateStatus();
?>
