<?php

$path = "/var/www/html/uatapi/";
//$path = "../";
require_once($path . 'includes/app_top.php');
require_once($path . 'includes/mysql.class.php');
require_once($path . 'includes/global.inc.php');
require_once($path . 'includes/functions_general.php');
require_once ($path . 'includes/classes/Models.class.php');
require_once ($path . 'includes/classes/Listing.class.php');
require_once ($path . 'includes/classes/PushNotification.class.php');
$PUSH   =   new PushNotification();
function getLocalMsg($clientCode){
    global $db;
    $default_lang   =   "EN";
    $sql = "SELECT * FROM users WHERE userid = " . $clientCode;
    $que = $db->query($sql);
    if ($que->size()>0){
        $row = $que->fetch();
        $default_lang = $row['default_lang'];
    }
    return $default_lang;
}

function weekNotification(){
    global $db;
    global $PUSH;
    $today  =   date("Y-m-d");
    $date   =   date("Y-m-d");
	$endDate   =   date("Y-m-d",  strtotime('-1 day', strtotime($today)));
    
    //$sql    ="select * from sip_installment_dues where due_date <='".$date."' and is_paid=0 and client_code=10000023";
     $sql    ="select * from sip_installment_dues where due_date between '".$endDate."' and '".$date ."' and is_paid=0 ";

	 //echo $sql    ="select * from sip_installment_dues where due_date between '".$endDate."' and '".$date ."' and is_paid=0 ".
              " AND sip_registration_no in(select sip_registration_no from orders where order_status!='CANCEL')";
	//exit;

    $result =   $db->query($sql);
    if ($result->size()>0){
        while($rs   =   $result->fetch()){
            $userLang   = getLocalMsg($rs['client_code']);
            $fund_name   =   Listing::getFundNameFromRTA($rs['scheme_code'],$rs['fund_name'],$userLang);
            
            switch ($userLang){
                case "HI":
                    $msg    =   $fund_name." के लिए रू.".$rs['amount']." की एसआईपी किश्त ".FormatDate($rs['due_date'])." तक देय है";
                    break;
                case "MH":
                    $msg    =   $fund_name." साठी रु.".$rs['amount']." चा एसआयपी हप्ता ".FormatDate($rs['due_date'])." ला देय आहे";
                    break;
                case "GJ":
                    $msg    =    FormatDate($rs['due_date'])." સુધીમાં ".$fund_name." માટે રૂ. ".$rs['amount']." ના એસઆઇપી હપ્તા બાકી છે";
                    break;
                default :
                    $msg        =   "SIP Installment due for '".$fund_name."' of Rs.".$rs['amount']." by ".FormatDate($rs['due_date']);
              
            }

            //$msg        =   "'".$fund_name."' के लिए रू.".$rs['amount']." की एसआईपी किश्त ".FormatDate($rs['due_date'])." तक देय है";
//            $msg        =   "SIP Installment due for '".$fund_name."' of Rs.".$rs['amount']." by ".FormatDate($rs['due_date']);
			//echo $msg;
			
           $PUSH->dueNotification($rs['client_code'],$msg);
        }
    }
}

weekNotification();

?>
