﻿<?php

//$path = "/var/www/employeetracker.us/public_html/niivo/";
$path = "/var/www/html/production/public/api/";

require_once($path . 'includes/app_top.php');


require_once($path . 'includes/mysql.class.php');
require_once($path . 'includes/global.inc.php');
require_once($path . 'includes/functions_general.php');

require_once($path . 'includes/classes/BSE_API.class.php');
require_once($path . 'includes/classes/BSE_ORDER_API.class.php');
require_once ($path.'includes/config.php');


$BSE_API= new BSE_API;
$BSE_ORDER_API=new BSE_ORDER_API;

// Before 23 Aug 2018 is it only run for onetime transaction. on 23rd it is change to check SIP also
$where = "WHERE 1 AND order_status = 'CONFIRM' AND payment_status != 1 AND order_type in('ONETIME','SIP') AND DATE_FORMAT(`dtdate`,'%Y-%m-%d') >= (NOW()- INTERVAL 2 DAY) ";
//$where = "WHERE 1 AND order_status = 'CONFIRM' AND payment_status != 1 AND order_type = 'ONETIME' AND DATE_FORMAT(`dtdate`,'%Y-%m-%d') >= (NOW()- INTERVAL 2 DAY) ";
//$where = "WHERE 1 AND order_status = 'CONFIRM' AND payment_status = 1 AND order_type = 'ONETIME' AND `dtdate` >= '2018-04-05' ";
//$where = "WHERE 1 AND order_status = 'CONFIRM' AND payment_status = 1 AND order_type = 'ONETIME'  ";
//$where = "WHERE 1 AND  payment_status != 1 AND order_type = 'ONETIME' and bse_order_no='31648070'";


$sql = "SELECT DISTINCT(order_id) AS order_id,orders.* FROM orders $where ";


$que = $db->query($sql);
if ($que->size() > 0) {
    while ($row = $que->fetch()) {
         $userid = $row['userid'];
         $bse_order_no = $row['bse_order_no'];
//echo "<pre>"; 
        if ($bse_order_no != '') {
              $str = "$userid|$bse_order_no|BSEMF";
			 
            $payment = $BSE_API->paymentStatus($str);
			print_r($payment);

//echo "<pre>"; 
		
            $payment_status = $payment['payment_status'];
            if ($payment_status == 1) {
                echo $sql = " update orders set " .
                        " payment_status= '1' " .
                        " where bse_order_no = '" . $bse_order_no . "' ";
			
                $result = $db->query($sql);

                $requst = array('userid' => $userid, 'order_id' => $row['order_id'],'bse_order_no'=>$bse_order_no, "scheme_code" => $row['scheme_code'], "amount" => $row['amount'], "bse_order_no" => $bse_order_no, "type" => "purchase", "qty" => $row['quantity'], "nav" => $row['nav']);
				
								
                $save_transactin = BSE_ORDER_API::saveTransaction($requst);
			
            }
        }
    }
}

function childOrderPaymentStatus(){
    global $db;
    global $BSE_API;
    $sql    =   "select * from sip_child_order_detail where payment_status=0";
    $result =   $db->query($sql);
    if ($result->size()>0){
        while ($row = $result->fetch()) {
            $userid = $row['client_code'];
            $bse_order_no = $row['order_no'];
            if ($bse_order_no != '') {
                $str = "$userid|$bse_order_no|BSEMF";
                
                $payment = $BSE_API->paymentStatus($str);
		
                $payment_status = $payment['payment_status'];
                if ($payment_status == 1) {
                    $sql = " update sip_child_order_detail set " .
                            " payment_status= '1' " .
                            " where order_no = '" . $bse_order_no . "' ";
                    $result = $db->query($sql);

                    $requst = array('userid' => $userid, 'order_id' => $row['id'],'bse_order_no'=>$bse_order_no, "scheme_code" => $row['scheme_code'], "amount" => $row['amount'],"inv_type"=>"SIP", "type" => "purchase", "qty" => $row['qty'], "nav" => $row['nav']);
				//print_r($requst);
								
                    $save_transactin = BSE_ORDER_API::saveTransaction($requst);
                }
            }
        }
    }
}
childOrderPaymentStatus();

?>
