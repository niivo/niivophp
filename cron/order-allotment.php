<?php
$path = "/var/www/html/production/public/api";
require_once($path . 'includes/app_top.php');
require_once($path . 'includes/mysql.class.php');
require_once($path . 'includes/global.inc.php');
require_once($path . 'includes/functions_general.php');
require_once($path . 'includes/classes/BSE_ORDER_API.class.php');
require_once($path . 'includes/user.class.php');
$BSE_ORDER=    new BSE_ORDER_API;

function getFile($reportPath = '') {
    $array = array();
	
    if (file_exists($reportPath)) {
        $f = fopen($reportPath, "r");
        while (!feof($f)) {
            $line = fgets($f);
            
            if(strlen($line)>0){
                $arrays = explode('|', trim($line));
                if (count($arrays) > 0)
                    $array[] = $arrays;
            }
        }
    }else {
        echo "File not found";
    }
 
    return $array;
}

function getRepot($exeDt,$reportDt) {

    $main_path = "/niivo/data/downloads/";
   // $main_path = "/var/www/html/niivo/uploads/";
    $dt	=	$exeDt;

//$dt=	"20171219";
//$reportDt="20180122";
    $statusFileName = "orderstatus/OrderStatusReport_".$reportDt.".txt";
    $statusPath = $main_path . $statusFileName;
    $status_file_array = getFile($statusPath);


    $allotmentfileName = "allotmentstatus/AllotmentStatement_".$exeDt."_".$reportDt.".txt";
    $allotmentPath = $main_path . $allotmentfileName;
    $allotment_file_array = getFile($allotmentPath);
    
   // print_r($status_file_array);
	//exit;
   //print_r($allotment_file_array);
	//exit;
    if (count($status_file_array) > 0) {
        foreach ($status_file_array as $row1) {
            $order_no = $row1[3];
            $valid_status = $row1[17];
			$type			=	$row1[10];
			
            if ($order_no != '' && $valid_status == 'VALID') {
                if ($type=="P"){
                    checkArray($allotment_file_array, $order_no);
                }
//                else if ($type=="R"){
//                    addWithdraw($row1);
//                }
            }
        }
    }
}

function addWithdraw($rs){
    global $db;
    global $BSE_ORDER;
    
    $clintCode  =   $rs[5];
    $schemeCD   =   $rs[7];
    $amount     =   $rs[11];
    $type       =   'withdraw';
    $bse_order_no   =   trim($rs[3]);
    $sql    =   "select * from withdraw where bse_order_no=".$bse_order_no;
    $result =   $db->query($sql);
    if ($result->size()>0){
        $rs =   $result->fetch();
        $order_id   =   $rs['id'];
        $rs = array("order_id" => $order_id, "userid" => $clintCode, "scheme_code" => $schemeCD,
                    "amount" => ($amount * -1), "type" => "withdraw","inv_type"=>"ONETIME","bse_order_no"=>$bse_order_no);
      
        $result=    $db->query("update withdraw set status=1, amount=".$amount." where trim(bse_order_no)='".trim($bse_order_no)."'");
        $BSE_ORDER::saveTransaction($rs);
        
    }
            
   
}


function getWithdraw($exeDt,$reportDt){
    global $db;
    global $BSE_ORDER;
    
    $main_path = "/niivo/data/downloads/";
   // $main_path = "/var/www/html/niivo/uploads/";
    $dt	=	$exeDt;

    //$dt=	"20171219";
    //$reportDt="20180122";
    $statusFileName = "redemptionstatus/RedemptionStatement_".$exeDt."_".$reportDt.".txt";
    $statusPath = $main_path . $statusFileName;
    $reedem_file_array = getFile($statusPath);
	
    if (count($reedem_file_array) > 0) {
		echo "<pre>";
		print_r($reedem_file_array);

        foreach ($reedem_file_array as $rs) {
            $clintCode  =   $rs[15];
            $schemeCD   =   $rs[5];
            $amount     =   $rs[20];
            $type       =   'withdraw';
            $bse_order_no   =   trim($rs[1]);
            $qty        =   $rs[19];
            $nav        =   $rs[18];
            $isValid    =   $rs[21];
            $folioNo    =   $rs[12];
            if ($isValid=="Y"){
                $sql    =   "select * from withdraw where trim(bse_order_no)='".$bse_order_no."'";
                $result =   $db->query($sql);
                if ($result->size()>0){
                    $rs =   $result->fetch();
                    $order_id   =   $rs['id'];
				
                    $rsT = array("order_id" => $order_id, "userid" => $clintCode, "scheme_code" => $schemeCD,"nav"=>$nav,"qty"=>$qty,
                                "amount" => ($amount * -1), "type" => "withdraw","inv_type"=>"ONETIME","bse_order_no"=>$bse_order_no);
					
                    $result=    $db->query("update withdraw set status=1, amount=".$amount.",".
                                "qty=".$qty.",nav=".$nav.",folio_no=".$folioNo."".
                                " where trim(bse_order_no)='".trim($bse_order_no)."'");

                    $BSE_ORDER::saveTransaction($rsT);
				  
					
                }
            }
        }
		
    }
   
}

function checkArray($allotment_file_array, $order_no) {
	//echo "<pre>";
    foreach ($allotment_file_array as $row2) {
		
        if (isset($row2[1]) && $row2[1] == $order_no ) {
			//echo $row2[10];
            $folio_no = $row2[12];
			$qty		=	$row2[19];
			
            updateOrder($order_no, $folio_no,$qty);
        }
    }
}

function updateOrder($order_no, $folio_no,$qty) {
    global $db;

      $sql = "select * from orders where bse_order_no = '" . $order_no . "' ";
    
    $res = $db->query($sql);
    if ($res->size() > 0) {
         $sql1 = " UPDATE orders SET "
                . " folio_no    =   '" . trim($folio_no) . "',"
                . " is_allotment    =   '1' ,"
                . " order_status    =   'ALLOTTED' ,"
				. " quantity    =  ".$qty." "
                . " WHERE "
                . " bse_order_no = '" . trim($order_no). "' "
                . " ";
		//		echo "<br>";
        $db->query($sql1);
        
    }
}

$today  =   date("Y-m-d");//"2018-01-22";
//$today  =   "2018-10-30";

$dt     =	date("Ymd",strtotime($today));
$dt1    =   date("Ymd",  strtotime("-1 days",  strtotime($today)));
$dt2    =   date("Ymd",  strtotime("-2 days",  strtotime($today)));
$dt3    =   date("Ymd",  strtotime("-3 days",  strtotime($today)));
$dt4    =   date("Ymd",  strtotime("-4 days",  strtotime($today)));
$dt5    =   date("Ymd",  strtotime("-5 days",  strtotime($today)));
$dt6    =   date("Ymd",  strtotime("-6 days",  strtotime($today)));


//echo "<pre>";

getRepot($dt,$dt6);
getRepot($dt,$dt5);
getRepot($dt,$dt4);
getRepot($dt,$dt3);
getRepot($dt,$dt2);
getRepot($dt,$dt1);
getRepot($dt,$dt);


getWithdraw($dt,$dt6);
getWithdraw($dt,$dt5);
getWithdraw($dt,$dt4);
getWithdraw($dt,$dt3);
getWithdraw($dt,$dt2);
getWithdraw($dt,$dt1);
getWithdraw($dt,$dt);
echo "Done";
