<?php

$path = "/var/www/html/v2/";
//$path = "../";
require_once($path . 'includes/app_top.php');
require_once($path . 'includes/mysql.class.php');
require_once($path . 'includes/global.inc.php');
require_once($path . 'includes/functions_general.php');
require_once($path . 'includes/classes/CAMSAPI.class.php');
require_once ($path . 'includes/config.php');

function convertDate($dt) {
    
    $dtArr = explode("-", $dt);
    $date = $dtArr[2] . "/" . $dtArr[1] . "/" . $dtArr[0];

    return $date;
}
function convertDateHyphen($dt) {
    $dtArr = explode("-", $dt);
    $date = $dtArr[2] . "-" . $dtArr[1] . "-" . $dtArr[0];

    return $date;
}

function ClientCreation($userid){
    global $db;
    $status = "false";
    $user_detail = array();
    $BSE = new BSE_API;

        if ($userid == '') {
            $msg = INVALIDCREDENTIAL;
        } else if (User::isUser($userid) == false) {
            $msg = NOUSER;
        } else {
            $str = "";
            $strFatca = "";

            $user_nominee_details = Verifiation::user_nominee_details($userid);
           
            $client_nominee = $user_nominee_details['nominee_name'];
            $relation = $user_nominee_details['nominee_relationship'];

            $user_info = User::getUserinfo($userid);
            $user_detail = $user_info['data'];
            $rs         =   $user_detail;
            $father_name = $user_detail['father_name'];
//            echo "<pre>";
//            print_r($rs);
//            exit;
//           

            $pencard = $user_detail['pencard'];
            $entity_type = $user_detail['entity_type'];

            $occupation = $user_detail['occupation'];
            $occupation_data = Mapping::mapping_occupation_code($occupation);
            $UCC_CODE = $occupation_data['UCC_CODE'];
            
            
            $occupation_FATCA_CODE = $occupation_data['FATCA_CODE'];
            $occupation_FATCA_TYPE = $occupation_data['FATCA_TYPE'];

            $ca_state = $user_detail['ca_state'];
            $state_data = Mapping::mapping_state_code($rs['ca_state']);
            
            $ca_pincode = $user_detail['ca_pincode'];


            $gross_annual_income = $user_detail['gross_annual_income'];

            $occupation = $user_detail['occupation'];
            
            if ($rs['pancard_type'] == 'INDIVIDUAL') {
                $E_code = "1";
                $B_code = "";
                $N_code = "N";
                $tax_status_code = "01";
            } else {
                $E_code = "4";
                $B_code = "Y";
                $N_code = "Y";
                $tax_status_code = $rs['entity_type'];
            }


            
            $place_of_birth = Mapping::mapping_country_code($rs['birth_place']); 
        
           
            $nationality = $rs['nationality'];
            $tpin = $rs['tpin'];
            if ($nationality == 'INDIAN') {
                $birth_nationality_code = "IN";
                $pin_number = $pencard;
            } else {
                $birth_nationality_code = "NRI";
                $pin_number = $tpin;
            }
            $tax_residency_country = $rs['tax_residency_country'];
            if ($tax_residency_country == 'India') {
                $tax_residency_nationality_code = "IN";
            } else {
                $tax_residency_nationality_code = "";
            }
//            echo "<pre>";
//            print_r($rs);
//            exit;
            $occupationType = "";
            $str .= $userid . "|SI|01|";
            $str .= $UCC_CODE . "|";
            $str .= $user_detail['name'] . "|||";
            $str .= convertDate($rs['dob']) . "|";
            $str .= $user_detail['gender'] . "||";
            $str .= $pencard . "|" . $client_nominee . "|" . $relation . "||P||||||";
            $str .= $user_detail['account_type'] . "|";
            $str .= $user_detail['account_number'] . "||";
            $str .= $user_detail['ifsc_code'] . "|Y|||||||||||||||||||||";
            $str .= $user_detail['account_holder_name'] . "|";
            $str .= $user_detail['ca_address'] . "|";
            $str .=  "|";
            $str .=  "|";
            $str .= $user_detail['ca_city'] . "|";
            $str .= $state_data['UCC_CODE'] . "|";
            $str .= $user_detail['ca_pincode'] . "|";
            $str .= $user_detail['country'] . "|||||";
            $str .= $user_detail['email'] . "|M|02|||||||||||||||";
             $str .= $user_detail['mobile'] . "";
//             echo "<pre>";
//             print_r($str);
//             exit;

            $strFatca .= $pencard . "||";
            $strFatca .= $user_detail['name'] . "";
            $strFatca .= "|".  convertDateHyphen($rs['dob'])."|" . $father_name . "||";
           
            $strFatca .= $tax_status_code . "|E|" . $E_code . "|" . $place_of_birth;
             
            $strFatca .= "|" . $birth_nationality_code . "|" . $tax_residency_nationality_code . "|";
            $strFatca .= $pin_number . "|C||||||||||";
           
            $strFatca .= $rs['wealthSourceCode'] . "||";
            $strFatca .= $rs['anualIncomeCode'] . "|||N|";
            $strFatca .= $occupation_FATCA_CODE . "|";
            $strFatca .= $occupation_FATCA_TYPE . "|||||||||||B|N||||||||||||||||||||||||||" . $B_code . "|" . $N_code . "|";
            $strFatca .= $rs['aadhar_no'] . "|N|";
            $strFatca .= $_SERVER['REMOTE_ADDR'] . "#" . date("d-m-y") . ":" . date("h:i") . "||";
            $strA=explode("|",$strFatca);
            //$strA=explode("|","CITPS6359D||ANSHUMAN SAXENA|02-02-1990|||01|E|1|DELHI|IN|IN|CITPS6359D|C||||||||||01||33|||N|03|S|||||||||||B|N||||||||||||||||||||||||||Y|N|331753457209|N|172.164.112.69#19-12-17;15:26||");
            
//            echo $strFatca;
//            echo "<pre>";
//            print_r($strA);
//            exit;
           
            try {
                $resp = $BSE->uploadClient($str);

                if ($resp['status'] == 'false') {
                    return array("status" => $status, "msg" => $resp['msg'], "retry" => "1", "data" => $user_detail);
                }else{
                    $db->query("update users set client_created=1 where userid='".$userid."'");
                }
               // $resp = $BSE->FATCAuploadClient($strFatca);
//                print_r($resp);
//                exit;
//                
                if ($resp['status'] == 'false') {
                    return array("status" => $status, "msg" => $resp['msg'], "retry" => "1", "data" => $user_detail);
                }
            } catch (Exception $e) {
                return array("status" => $status, "msg" => SOMETHINGWRONG, "retry" => "1", "data" => $user_detail);
            }
        }
}


function getPanStatus(){
    global $db;
    $CAMS   =   new CAMSAPI;
    $sql    =   "select * from users where kyc_status in('PENDING','REQUESTED')";
  
    $result    =   $db->query($sql);
    if ($result->size()>0){
        $rs     =   $result->fetch();
//        ClientCreation($rs['userid']);
//        exit;
        $arr    =   array("pancard_no"=>$rs['pencard']);
       
        $panData   =   $CAMS->panVerify($arr);
        $data   =   $panData['data'];
       
        if ($panData['status']=='true'){
           
            if (!$data['kyc_status']){
                ClientCreation($rs['userid']);
                $db->query("update users set kyc_status='COMPLETE' where userid=".$rs['userid']);
            }
        }
        
    }
}

getPanStatus();


?>
