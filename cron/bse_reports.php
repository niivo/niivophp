<?php

$path = "/var/www/html/production/public/api";
//$path = "../";
require_once($path . 'includes/app_top.php');
require_once($path . 'includes/mysql.class.php');
require_once($path . 'includes/global.inc.php');
require_once($path . 'includes/functions_general.php');
require_once($path . 'includes/classes/BSE_REPORTS.class.php');
require_once ($path . 'includes/config.php');

function convertDate($dt) {
    
    $dtArr = explode("-", $dt);
    $date = $dtArr[2] . "/" . $dtArr[1] . "/" . $dtArr[0];

    return $date;
}
function convertDateHyphen($dt) {
    $dtArr = explode("/", $dt);
    $date = $dtArr[2] . "-" . $dtArr[1] . "-" . $dtArr[0];

    return $date;
}
$bseReports = new BSE_REPORTS;

function saveXSPorderResponse($arr){
    global $db;
    $str="";
    foreach($arr as $key=>$val){
        $str    .= "`".$key."`='".$val."',"; 
    }
        $str    =   rtrim($str,',');
        $sql    =   "insert into xsp_orders set "    .$str;
        $result =   $db->query($sql);
        return $result->insertID();
}

function checkOrderExists($xspOrderID){
    global $db;
    $sql    =   "select id from orders where bse_order_no='".$xspOrderID."'";
    $result =   $db->query($sql);
    return $result->size();
}

function createOrder($from,$to){
    global $db;
    global $bseReports;
    $arr=array("from_date"=>$from,"to_date"=>$to);
    $xspOrderArrs =   $bseReports->getXSPOrderStatus($arr);
	
    if ($xspOrderArrs['status']=="true"){
        foreach($xspOrderArrs['data'] as $xspOrderArr){
            if (!checkOrderExists($xspOrderArr['ORDERNUMBER'])){
				$xspOrderID =   saveXSPorderResponse($xspOrderArr);
                
				$sql    =   "select * from xsp_orders where id=".$xspOrderID;
                $result =   $db->query($sql);
                if ($result->size()>0){
                    while($rs   =   $result->fetch()){

                    if (strtoupper($rs['ORDERSTATUS'])=="VALID"){
                        $remarks    =   "XSIP order for ".$scheme_code." by ".$rs['CLIENTCODE'];
                        $scheme_nav = BSE_ORDER_API::schemeNav($rs['SCHEMECODE']);
                        $qty        =   round(($rs['AMOUNT']/$scheme_nav),2);
                         $sql = "insert into orders(order_id,order_type,bse_order_no,dtdate,userid,nav,scheme_code,l1_flag,amount,quantity,folio_no,sip_registration_no,child_order_id,is_childorder,payment_status,is_goal,order_status,remarks)VALUES(" .
                                "'" . $rs['INTERNALREFNO'] . "'," .
                                "'XSIP'," .
                                 "'" . $rs['ORDERNUMBER'] . "'," .
                                "'" . convertDateHyphen($rs['DATE']) . "'," .
                                "'" . $rs['CLIENTCODE'] . "'," .
                                "'" . $scheme_nav . "'," .
                                "'" . $rs['SCHEMECODE'] . "'," .
                                "'0'," .
                                "'" . $rs['AMOUNT'] . "'," .
                                "'" . abs($qty) . "'," .
                                "'" . $rs['FOLIONO'] . "'," .
                                "'" . $rs['SIPREGNNO'] . "'," .
                                "" . $rs['id'] . "," .
                                "0," .
                                "1," .
                                "0," .
                                "'CONFIRM'," .
                                "'" . $remarks . "')";

                        $result1 =   $db->query($sql);
                        $requst = array('userid' => $rs['CLIENTCODE'], 'order_id' => $rs['INTERNALREFNO'],'bse_order_no'=>$rs['ORDERNUMBER'], "scheme_code" => $rs['SCHEMECODE'], "amount" => $rs['AMOUNT'], "bse_order_no" => $rs['ORDERNUMBER'],"inv_type"=> "XSIP","type" => "purchase","qty" => $qty, "nav" => $scheme_nav);
                								
						$save_transactin = BSE_ORDER_API::saveTransaction($requst);

                    }
                }
                }
            }
        }
    }else{
		echo "order not found";
	}
   
}
$today  =   date("d/m/Y");//"2018-01-22";
//$today  =   "29/10/2018";
//echo $today;
//exit;

createOrder($today,$today);
?>
